#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

static float radius=.2f; //radijus kugle

//pozicija kocke
float kX=-1.5f;
float kY=1.1f;
float kZ=0.0f;
float lX=-1.5f;

//pozicija kugle
float lY;
float lZ;
float acY; //akceleracija kugle
int start; //animacija pokrenuta
int drop; //kugla u stanju padanja

void reset() //resetiramo animaciju na pocetne vrijednosti
{
    drop=0;
    start=0;
    lX=-1.5f;
    lY=1.1f;
    lZ=0.0f;
    acY=.0f;
}

void animacija()
{
    if (start){
        if (drop){
            lX+=0.006f;
            lY-=acY;
            acY+=0.004f;
            if (lY<-2.0f) start=0;
        }else{
            lX+=0.006f;
            if (lX>-1.23f) drop=1;
        }

    }
}

static void init(void)
{
    //postavljamo bijelo svjetlo
    GLfloat ambientLight[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat diffuseLight[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat specularLight[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat position[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 100.0 };

    glMaterialfv(GL_FRONT, GL_SPECULAR, specularLight);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    //Boja kao materijal
    glEnable(GL_COLOR_MATERIAL);

    //eliminacija skrivenih povrsina
    glEnable(GL_DEPTH_TEST);

    reset();
}

void drawCube() {
    glNormal3f(0.0,0.0,1.0);
    glBegin(GL_POLYGON); //prednja strana
    glVertex3f(  0.2, -0.2, 0.2 );
    glVertex3f(  0.2,  0.2, 0.2 );
    glVertex3f( -0.2,  0.2, 0.2 );
    glVertex3f( -0.2, -0.2, 0.2 );
    glEnd();
    glNormal3f(0.0,0.0,-1.0);
    glBegin(GL_POLYGON); //straznja strana
    glVertex3f(  0.2, -0.2, -0.2 );
    glVertex3f(  0.2,  0.2, -0.2 );
    glVertex3f( -0.2,  0.2, -0.2 );
    glVertex3f( -0.2, -0.2, -0.2 );
    glEnd();
    glNormal3f(-1.0,0.0,0.0);
    glBegin(GL_POLYGON); // lijeva strana
    glVertex3f( -0.2,  0.2,  0.2 );
    glVertex3f( -0.2, -0.2,  0.2 );
    glVertex3f( -0.2, -0.2, -0.2 );
    glVertex3f( -0.2,  0.2, -0.2 );
    glEnd();
    glNormal3f(0.0,1.0,0.0);
    glBegin(GL_POLYGON); //gornja strana
    glVertex3f( -0.2,  0.2,  0.2 );
    glVertex3f(  0.2,  0.2, -0.2 );
    glVertex3f(  0.2,  0.2, -0.2 );
    glVertex3f( -0.2,  0.2,  0.2 );
    glEnd();
    glNormal3f(0.0,-1.0,0.0);
    glBegin(GL_POLYGON); //donja strana
    glVertex3f( -0.2,  -0.2,  0.2 );
    glVertex3f(  0.2,  -0.2, -0.2 );
    glVertex3f(  0.2,  -0.2, -0.2 );
    glVertex3f( -0.2,  -0.2,  0.2 );
    glEnd();

}

void display(void)
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    animacija(); //animiramo kretanje kugle prije iscrtavanja

    //iscrtavanje kugle
    glPushMatrix ();
    glTranslatef (lX, lY,lZ);
    glColor3f(1, 1, 0);
    glutSolidSphere (radius, 50, 50);
    glPopMatrix ();

    //iscrtavanje suplje kocke u dvije boje
    glPushMatrix ();
    glTranslatef (kX, kY,kZ);
    glColor3f(1,0,0);
    drawCube();
    glPushMatrix();
    glColor3f(0, 0, 1);
    glScalef(0.99, 0.99, 0.99);
    drawCube();
    glPopMatrix();
    glPopMatrix ();
    //kraj iscrtavanja

    glFlush();
    glutSwapBuffers();
    glutPostRedisplay();
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLint) w, (GLint) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
        glOrtho (-1.5, 1.5, -1.5*(GLfloat)h/(GLfloat)w,
                     1.5*(GLfloat)h/(GLfloat)w, -10.0, 10.0);
    else
        glOrtho (-1.5*(GLfloat)w/(GLfloat)h,
                     1.5*(GLfloat)w/(GLfloat)h, -1.5, 1.5, -10.0, 10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(-15, 0, 1, 0);



}

void keyPressed(unsigned char key, int x, int y) {
    (void)(x);
    (void)(y);
    if (key == 'g') { //pokrecemo animaciju
        start = 1;
    } else if (key == 's') { //zaustavljamo animaciju
        start = 0;
    } else if (key == 'r') { //resetiramo animaciju
        reset();
    } else if (key == 27) { //escape je ascii 27, izlaz iz programa
        exit(0);
    }
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Kugla pada iz kocke - Erika Fafandjel");
    init();                                 //inicijalizacija programa
    glutKeyboardFunc(keyPressed);
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}

