Opis

Po pokretanju programa zuta kugla se nalazi u crvenoj (plavoj iznutra) kocki u gornjem lijevom dijelu prozora. Pokretanjem animacije kugla ispada iz kocke, kotrljajuci se do ruba i padanjem po parabolicnoj putanji (simulacija horizontalnog hitca)
- tipkom 'g' pokrecemo animaciju kugle
- tipkom 's' zaustavljamo (pauziramo) animaciju kugle
- tipkom 'r' resetiramo (na pocetne vrijednosti) animaciju kugle
- tipkom ESC izlazimo iz programa

Kocku sam nacrtala na način da mi je vanjska kocka obojana crveno i nacrtana u punoj veličini dok sam unutarnju kocku obojala u plavo i skalirala za 0.99 kako bi postigla efekt da je kocka obojana iznutra u plavu a iz vana u crveno.

Što se tiče osvjetljenja dodala sam bijelo ambijentalno, difuzno i tockasto (specular) svijetlo na određenoj poziciji u beskonacnosti udaljeno (directional light). Također dodana je i shininess (100) kako bi posjajili kuglu. Koristim boju kao materijal a ne svijetlo. Radi ispravnog osvjetljenja izracunate su normale za sve plohe.

U main funkciji se nalazi inicijalizacija GLUT komponenti te poziv na funkcije za inicijalizaciju scene i call-back funkcija za procesuiranje tipki na tipkovnici (keyPressed) te funkcije za reshape prozora ukoliko se njegova veličina promjeni da sukladno tome podesimo i pogled. Također tu je i poziv za display funkciju koja se poziva svaki puta kada scenu treba ponovno iscrtati u kojoj prikazujemo animaciju i iscrtavamo odgovarajuću kocku i kuglu.  

Literatura koja mi je pomogla u rjesavanju ove zadace je slijedeca:
http://en.wikibooks.org/wiki/OpenGL_Programming/GLStart/Tut3
http://www.opengl.org
http://www.falloutsoftware.com/tutorials/gl/gl3.htm
