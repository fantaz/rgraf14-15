Marin Krmpotić - 3. zadatak
--------------------------------------------

== Pokretanje programa =====================

Program se pokreće bez argumenata.

============================================


== Opis rješenja ===========================

Prilikom pokretanja programa vidljiva je tamno siva kocka i portal uz nju. U istom
trenutku na sceni postoji i druga (žuta) kocka koja u inicijalnom stanju nema širine
pa samim time nije vidljiva. Kada se portal pokrene i počne prelaziti preko kocke, širina
sive kocke se počne smanjivati za istu vrijednost za koju se širina žute kocke tada počne
povećavati. Time dobivamo dojam promjene boje dijela jedne kocke, a zapravo je riječ o
dvjema kockama. Kada portal u potpunosti prijeđe preko kocke siva kocka više nema
širine i time postaje nevidljiva.

Portal je poligon koji se sastavljen od 90 točaka kojima se aproksimira kružnica. Prikazujemo
ga u dva koraka: iscrtavanje unutrašnjosti poligona te iscrtavanje linije poligona.

Postoji jedan izvor svjetla koji je dovoljan da se kocka primjetno osjenči.

============================================


== Kontrole ================================

 s - početak animacije
 x - zaustavljanje animacije
 ESC - izlaz iz programa

============================================


== Reference ===============================

http://www.opengl-tutorial.org/ - osnove OpenGL-a
http://www.videotutorialsrock.com/opengl_tutorial/basics/home.php - podizanje razvojnog okruženja
https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Examples.html - objašnjenje prikaza objekata
http://slabode.exofire.net/circle_draw.shtml - efikasni algoritam za iscrtavanje kružnice

============================================
