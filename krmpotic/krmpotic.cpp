/*
 * Marin Krmpotic - 3. zadatak
 */

#include <iostream>
#include <GL/glut.h>
#include <math.h>

/* Global variables */
char title[] = "Marin Krmpotic - 3. zadatak";
float ringPosition = 1.5f;                            // X-coordinate of the ring
bool animationActive = false;
float materialsBorder = 1.0f;                         // X-coordinate of where the gray and gold meet
float rightFaceState = -1.0f;                         // When -1 then it is gray, when +1 it is gold
float leftFaceState = -1.0f;                          // When -1 then it is gray, when +1 it is gold

void drawGrayCube();
void drawYellowCube();
void drawCircle(float radius, int segments);
void convertToGold();
void moveRing(int);

/* Initialize OpenGL Graphics */
void initGL() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);              // Set background color to black and opaque
    glClearDepth(1.0f);                                // Set background depth to farthest
    glEnable(GL_DEPTH_TEST);                           // Enable depth testing for z-culling
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glDepthFunc(GL_LEQUAL);                            // Set the type of depth-test
    glShadeModel(GL_SMOOTH);                           // Enable smooth shading
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Nice perspective corrections
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Now we can have colors with alpha value

    /* Set the light color and source */
    GLfloat lightColor[] = {1.0f, 1.0f, 1.0f, 1.0f};   // White light
    GLfloat lightSource[] = {3.0f, 5.0f, 3.0f, 1.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
    glLightfv(GL_LIGHT0, GL_POSITION, lightSource);
}

/* Display handler */
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
    glMatrixMode(GL_MODELVIEW);                         // To operate on model-view matrix

    glLoadIdentity();                                   // Reset the model-view matrix
    glTranslatef(0.0f, 0.0f, -5.0f);                    // Move into the screen


    glRotatef(-31.0f, -1.0f, 1.2f, 0.0f);               // Rotate the scene


    drawGrayCube();
    drawYellowCube();
    drawCircle(3, 90);                                  // Draw a circle with radius 3 consisting of 90 segments

    glutSwapBuffers();
}

void drawGrayCube() {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBegin(GL_QUADS);
    /* Top face */
    glColor3f(0.3f, 0.3f, 0.3f);                        // Gray
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f( materialsBorder, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f,  1.0f);
    glVertex3f( materialsBorder, 1.0f,  1.0f);

    /* Bottom face */
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f( materialsBorder, -1.0f,  1.0f);
    glVertex3f(-1.0f, -1.0f,  1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f( materialsBorder, -1.0f, -1.0f);

    /* Front face */
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( materialsBorder,  1.0f, 1.0f);
    glVertex3f(-1.0f,  1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f( materialsBorder, -1.0f, 1.0f);

    /* Back face */
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f,  1.0f, -1.0f);
    glVertex3f( 1.0f,  1.0f, -1.0f);

    /* Left face */
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f,  1.0f,  1.0f);
    glVertex3f(-1.0f,  1.0f, leftFaceState);
    glVertex3f(-1.0f, -1.0f, leftFaceState);
    glVertex3f(-1.0f, -1.0f,  1.0f);

    /* Right face */
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f,  1.0f, rightFaceState);
    glVertex3f(1.0f,  1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f, rightFaceState);
    glEnd();
}

void drawYellowCube() {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBegin(GL_QUADS);
    /* Top face */
    glColor3f(1.0f, 1.0f, 0.0f);                        // Yellow
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f( 1.0f, 1.0f, -1.0f);
    glVertex3f(materialsBorder, 1.0f, -1.0f);
    glVertex3f(materialsBorder, 1.0f,  1.0f);
    glVertex3f( 1.0f, 1.0f,  1.0f);

    /* Bottom face */
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f( 1.0f, -1.0f,  1.0f);
    glVertex3f(materialsBorder, -1.0f,  1.0f);
    glVertex3f(materialsBorder, -1.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);

    /* Front face */
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( 1.0f,  1.0f, 1.0f);
    glVertex3f(materialsBorder,  1.0f, 1.0f);
    glVertex3f(materialsBorder, -1.0f, 1.0f);
    glVertex3f( 1.0f, -1.0f, 1.0f);

    /* Back face */
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f,  1.0f, -1.0f);
    glVertex3f( 1.0f,  1.0f, -1.0f);

    /* Left face */
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f,  1.0f,  1.0f);
    glVertex3f(-1.0f,  1.0f, -leftFaceState);
    glVertex3f(-1.0f, -1.0f, -leftFaceState);
    glVertex3f(-1.0f, -1.0f,  1.0f);

    /* Right face */
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f,  1.0f, -rightFaceState);
    glVertex3f(1.0f,  1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f, -rightFaceState);
    glEnd();
}

void drawCircle(float radius, int segments)
{
    glColor4f(1.0f, 1.0f, 1.0f, 0.3f);                  // White transparent
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    float theta = 2 * 3.1415926 / float(segments);
    float c = cosf(theta);                              // Precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = radius;                                   // We start at angle = 0
    float y = 0;

    glBegin(GL_POLYGON);
    for(int i = 0; i < segments; i++)
    {
        glVertex3f(ringPosition, y, x);                 // Output vertex

        /* Apply the rotation matrix */
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);                  // White solid
    glLineWidth(4.0f);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glBegin(GL_POLYGON);
    for(int i = 0; i < segments; i++)
    {
        glVertex3f(ringPosition, y, x);

        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
    glEnd();

}

void convertToGold() {
    if (ringPosition < 1.0 && ringPosition >= -1.0) {
        materialsBorder = ringPosition;                 // Move the border between gray and yellow cube
        rightFaceState = 1.0f;                          // Paint the right face in yellow
    } else if (ringPosition < -1.0) {
        leftFaceState = 1.0f;                           // Paint the left face in yellow
    }
}

void moveRing(int) {
    if (animationActive && ringPosition > -1.5) {
        ringPosition -= 0.05;                           // Move the ring in steps of 0.05
        convertToGold();
        glutPostRedisplay();                            // Something has changed - redraw the scene
        glutTimerFunc(25, moveRing, 0);                 // Call again after 25ms
    }
}

void keyPressHandler(unsigned char key, int, int) {
    switch(key) {
        case 's':
            animationActive = true;                     // Start the animation when s pressed
            moveRing(0);
            break;
        case 'x':
            animationActive = false;                    // Stop the animation when s pressed
            moveRing(0);
            break;
        case 27: // esc                                 // Exit the program on ESC
            exit(0);
    }
}

/* Reshape handler */
void reshape(GLsizei width, GLsizei height) {

    if (height == 0) height = 1;
    GLfloat aspectRatio = (GLfloat)width / (GLfloat)height;

    glViewport(0, 0, width, height);                    // Set the viewport to cover the new window

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(100.0f, aspectRatio, 0.1f, 100.0f);

    /* Not needed since we have rotated the whole scene:
    gluLookAt(3.0f, 2.0f, 2.0f,
              0.0f, 0.0f,  -1.0f,
              0.0f, 1.0f,  0-.0f);
    */

}

/* Main function */
int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(50, 50);

    /* Define the handlers */
    glutCreateWindow(title);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressHandler);

    initGL();

    glutMainLoop();
    return 0;
}

