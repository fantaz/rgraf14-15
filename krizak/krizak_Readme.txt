
Rjesenje je osmisljeno tako da se nacrtaju 2 kocke na istom prostoru te da im se dimenzije mjenjaju ovisno o poziciji portala. Sa lijeve strane portala se crta siva, a sa desne zlatna kocka.

Vrijednosti xleft, xright, yup i ydown su stavljene u 1/-1 jer je bilo najlakse raditi oko ishodista. Zfront i zback
su pomaknuti zbog problema sa renderiranjem oko ishodista.
Radius r portala je 2 zbog traženog promjera portala od 4 piksela.

Pomoć pri rješavanju problema našao sam na stranicama poput:
www.opengl.org
stackoverflow.com

Korištenje programa:
Program se pokreće bez argumenata.
Simulacija se pokreće tipkom 's' te zaustavlja tipkom 'x'.
Brzina kretanja portala se može povećati tipkom 'd' te smanjiti tipkom 'c'.
Pritiskom na tipku 'Esc' program se zatvara.

