
#include <iostream>
#include <stdio.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

using namespace std;

//grey cube
float x[]={-1, 1};
float y[]={-1, 1};
float z[]={-1, 1};

//gold cube
float x1[]={1, 1};

//portal
float xportal = 2;
float r = 2;

//key
int pomakni = 0;
float speed = 0.03;

//ispis
void output(){
    glColor3f(0.85,0.85,0.1);
    //ispis brzine
    glRasterPos2f(3,3);
    char poruka[100];
    snprintf(poruka, 50, "Speed: %.2f", speed);
    int len=strlen(poruka);

    for (int i=0; i<len; i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, poruka[i]);
    }

    //ispis pozicije portala
    glRasterPos2f(3,2.5);
    snprintf(poruka, 50, "Kretanje: %d, xportal: %.2f", pomakni, xportal);
    len=strlen(poruka);

    for (int i=0; i<len; i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, poruka[i]);
    }

    //ispis komandi
    glRasterPos2f(-8,6);
    snprintf(poruka, 100, "Start: s, Stop: x, Povecavanje brzine: f, Smanjivanje brzine: v, Izlaz: Esc");
    len=strlen(poruka);

    for (int i=0; i<len; i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, poruka[i]);
    }

    //ispis uputa
    glRasterPos2f(-11,5.5);
    snprintf(poruka, 100, "Pri negativnoj brzini portal mijenja smjer kretanja. Pri dolasku do ruba interakcija ne prestaje.");
    len=strlen(poruka);

    for (int i=0; i<len; i++){
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, poruka[i]);
    }
}

void init (){

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    float position[] = {x1[1]+5, y[1]+2, z[1]-1, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, position);

    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


void resize(int width, int height) {

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)width/(float)height, 5, 200);
}

//crtanje sive kocke
void drawCube1 (){

    glColor3f(0.1, 0.1, 0.1);
    glBegin(GL_QUADS);

    int i=1, j=1, k=1;

    for (int l=0; l<24; l++){
        //normale stranica za osvjetljavanje
        switch (l){
            case 0:
                glNormal3f(1, 0, 0);
                break;
            case 4:
                glNormal3f(-1, 0, 0);
                break;
            case 8:
                glNormal3f(0, 1, 0);
                break;
            case 12:
                glNormal3f(0, -1, 0);
                break;
            case 16:
                glNormal3f(0, 0, -1);
                break;
            case 20:
                glNormal3f(0, 0, 1);
                break;
        }

        //crtanje stranica
        if (l>0 && l%4==0) i=0;
        if (l==10 || l==14 || l== 18 || l== 22) i=1;
        if (l%2!=0 && l<16){
            if (k==0) k=1; else k=0;
        }
        if (l==16) k=0;
        if (l==20) k=1;
        if (l%2==0 && l<8){
            if (j==0) j=1; else j=0;
        }
        if (l==12) j=0;
        if (l%2!=0 && l>16){
            if (j==0) j=1; else j=0;
        }
        glVertex3f(x[i], y[j], z[k]);
    }

    glEnd();

}

//crtanje zlatne kocke
void drawCube2 (){

    glColor3f(0.85, 0.85, 0.1);
    glBegin(GL_QUADS);

    int i=1, j=1, k=1;

    for (int l=0; l<24; l++){

        switch (l){
            case 0:
                glNormal3f(1, 0, 0);
                break;
            case 4:
                glNormal3f(-1, 0, 0);
                break;
            case 8:
                glNormal3f(0, 1, 0);
                break;
            case 12:
                glNormal3f(0, -1, 0);
                break;
            case 16:
                glNormal3f(0, 0, -1);
                break;
            case 20:
                glNormal3f(0, 0, 1);
                break;
        }

        if (l>0 && l%4==0) i=0;
        if (l==10 || l==14 || l== 18 || l== 22) i=1;
        if (l%2!=0 && l<16){
            if (k==0) k=1; else k=0;
        }
        if (l==16) k=0;
        if (l==20) k=1;
        if (l%2==0 && l<8){
            if (j==0) j=1; else j=0;
        }
        if (l==12) j=0;
        if (l%2!=0 && l>16){
            if (j==0) j=1; else j=0;
        }
        glVertex3f(x1[i], y[j], z[k]);
    }
    glEnd();

}

//portal
void drawPortal (){

    glColor4f(4, 4, 4, 0.4);

    glBegin(GL_TRIANGLE_FAN);

    float yport, zport;

    for (float angle=0; angle<=2*M_PI; angle+=0.1){

        yport = r*sin(angle);
        zport = r*cos(angle);
        glVertex3f(xportal,yport, zport);
    }
    glEnd();
}

void scene (){

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(x1[1]+3, y[1]+2, z[1]+9, (x[0]+x1[1])/2, (y[0]+y[1])/2, (z[1]+z[0])/2, 0, 1, 0);

    if (xportal>x[0]) drawCube1();
    if (xportal<x1[1]) drawCube2();
    drawPortal();
    output();

    glutSwapBuffers();
}

void update (int ){

    //micanje portala
    if(pomakni && xportal>=x[0]-1 && xportal<=x1[1]+1){
        xportal -= speed;
        //lijeva granica portala
        if (xportal<x[0]-1){
            xportal = -2;
            pomakni = 0;
        }
        //desna granica portala
        if (xportal>x[1]+1){
            xportal = 2;
            pomakni = 0;
        }
    }

    //siva kocka se smanjuje sa lijeve strane portala, a zlatna raste sa desne dok je portal na kocki
    if(xportal<=x[1] && xportal>=x[0]){
        x[1]=xportal;
        x1[0]=xportal;
    }
    //drugi smjer
    if(xportal>=x1[0] && xportal<=x1[1]){
        x[1]=xportal;
        x1[0]=xportal;
    }
    //update
    glutPostRedisplay();
    glutTimerFunc(10, update,0);

}

void keyPress (unsigned char key, int , int ){
    switch (key){
        case 's':
            pomakni = 1;
            break;
        case 'x':
            pomakni = 0;
            break;
        case 'd':
            speed += 0.01;
            break;
        case 'c':
            speed -= 0.01;
            break;
        case 27:
            exit(0);
    }
}

int main (int argc, char** argv){

    //inicijalizacija
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(800, 600);

    //otvaranje prozora
    glutCreateWindow("Portal");
    init();

    //funkcije za izvrsavanje
    glutDisplayFunc(scene);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyPress);

    glutTimerFunc(10, update,0);
    glutMainLoop();
}
