Program iscrtava zelenu sferu na isprekidanoj putanji koju sam nacrtao spajaju�i to�ke u 3D pomo�u glEnable(GL_LINE_STIPPLE);
Sfera inicijalno kre�e od lijevog gornjeg kuta sa sredi�tem u to�ki (-3.2,2.4,-6.0).
Pomi�u�i se, sfera se okre�e za 90 stupnjeva �to ovisi o varijabli ROT te nadalje varijabli Step kojoj 
pridru�ujem vrijednost kuta 90. Varijabla Step slu�i mi kao brojac pomo�u kojeg mogu napraviti odre�eni pomak mno�e�i
je sa koeficijentima smjera x i y(0.4,0.2025) osi,(z fiksan) koje zatim zbrajam sa trenutnom pozicijom sfere.
Trag je napravljen pomo�u funkcije GL_QUAD(4 vertexa). Dakle Kada sfera miruje x vrijednosti svih to�aka pravokutnika 
su na -3.2(sredi�te) time dobivamo du�inu okomitu na sredi�te. Stisnemo li tipku 'S' sfera se po�inje gibati i 
pravokutnik tako�er pomi�e svoje dvije to�ke u smjeru u kojem se sfera kre�e dok su preostale dvije lijeve to�ke fiksne.
 Ostatak pi�e u komentarima.
Koristio sam sljede�e izvore znanja: http://www.zemris.fer.hr/predmeti/irg/knjiga.pdf
				     http://www.opengl-tutorial.org/
				     http://www.felixgers.de/teaching/jogl/stippledLines.html
				     https://www.opengl.org/sdk/docs/man2/xhtml/glBlendFunc.xml