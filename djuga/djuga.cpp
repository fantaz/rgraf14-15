/*
 * Written by Theo Djuga January 2015
 *
 * Spinning and moving Sphere with the trajectory and path is
 * displayed until the ESC key is pressed.  For pausing
 * the program press 'X' or 'x' and for starting press 'S'
 * or 's'.
 */

#include <GL/glut.h> //potreban include za gl-funkcije
#include <stdio.h> //Potrebno za ispis i upis podataka
//#include <windows.h> Potrebno ukoliko se radi na windowsima

const double r=0.5; //radius sfere
const int slices = 20;//Broj subdivizija oko z osi
const int stacks = 20;//Broj subdivizija oko z osi
static int RunMode = 0; //0-pauzirana animacija, 1-Animacija u tijeku
static double Step = 0.0;//brojac ua prvu putanju u desno
static double Step1 = 0.0;//brojac za drugu putanju u lijevo
static double Step2 = 0.0;//brojac za trecu putanje u desno
const double AnimateSpeed = 0.01;//brzina animacije
static double pomx = 0.0;//pomak kugle u smjeru x
static double pomy = 0.0;//pomak kugle u smjeru y
static double x = -3.2;//x pocetna pocicija sfere
static double y = 2.4;//y pocetna pozicija sfere
static double x1 = 0.0;//krajnja pozicija prvog traga
static double y1 = 0.0;//krajnja pozicija prvog traga
static double x2 = 0.0;//krajnja pozicija drugog traga
static double y2 = 0.0;//krajnja pozicija drugog traga
//promjena velicine prozora, argumenti sirina i visina u pixelima
static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;//racunamo prva dva parametra glFrustum funkcije left i right tocke
    glViewport(0, 0, width, height);//koji dio prozora prikazuje samu scenu
    glMatrixMode(GL_PROJECTION);//rad smatricama koji slĳedi odnosit ce se na podesavanje matrice projekcĳe
    glLoadIdentity();//djelovat ce nad matricom modela i resetirat ce je na jedinicnu matricu
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);//Ugrađena naredba kojom se stvara matrica perspektivne projekcĳe

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}
//obavlja crtanje same scene
static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(RunMode==1){//povecavamo brojac ako je animacija aktivna

        Step+=AnimateSpeed;
    }
     const double rot = Step*90.0;//Rotacija za 90 stupnjeva svake sekunde


    if(Step<=10){//pocetni put kugle te traga iz sredista kugle
        x=-3.2;
        y=2.4;
        pomy = Step*0.2025;//mnozimo brojac sa koeficijentom smjera
        pomx = Step*0.41;//da su koef. isti kugla bi se linearno pomicala u kraj ekrana a to ne zelimo

    glColor4f(1.0f,0.0f,0.0f, 0.5f);//boja traga-trula visnja

    glBegin(GL_QUADS);//funkcija za scrtanje cetverokuta
    glVertex3f(x,y-r , -6.0);//donji lijevi kut
    glVertex3f(x+pomx, y-pomy-r, -6.0);//donji desni kut
    glVertex3f(x+pomx, y-pomy+r, -6.0);//gornji desni kut
    glVertex3f(x, y+r, -6.0);//gornji lijevi kut
    glEnd();//kraj crtanja
    x1=x+pomx;//potrebno kako nam se nebi izbrisao trag jer x+pomx
    y1=y-pomy;//i y+pomy se mijenjaju tokom izvodenja programa
    }
    else if(Step>10){//kada nacrtamo prvi pravokutnik zadrzimo ga na krajnjim vrijednostima
        glColor4f(1.0f,0.0f,0.0f, 0.5f);

        glBegin(GL_QUADS);
    glVertex3f(-3.2,2.4-r , -6.0);//ostaje fiksno iz sredista start-a kugle
    glVertex3f(x1-0.65, y1-r+0.3, -6.0);//-0.65 i +0.3 dodajemo kako bi dovrsili crtanje prvog traga u zeljenoj tocki(donja desna)
    glVertex3f(x1, y1+r, -6.0);//krajnja pozicija gornje desne tocke 1. traga
    glVertex3f(-3.2, 2.4+r, -6.0);//fiksno
    glEnd();


    }
    if(Step>=10 && Step<13){
        x=0.9;//mijenjamo vrijednosti x i y,
        y=0.38;//sada su startne tocke druge staze
        if(RunMode==1){

            Step1+=AnimateSpeed;//novi brojac ispocetka za 2. trag i sferu da nebi poremetili konstantnu brzinu animacije
        }
        pomx = -Step1*0.4;//negativni koef. smjera za x dakle kugla skreće u lijevo
        pomy = Step1*0.425;//y i dalje pada ali s vecim nagibom

    glColor4f(1.0f,0.0f,0.0f, 0.5f);//boja traga s prozirnoscu alfa=0.5, poluprozirno

    glEnable (GL_BLEND);//omogucujemo mijesanje boja 1. i 2. traga
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBegin(GL_QUADS);//ponovno crtanje traga ali za 2. putanju
    glVertex3f(x,y-r , -6.0f);
    glVertex3f(x,y+r , -6.0f);
    glVertex3f(x+pomx, y-pomy+r, -6.0f);//pomicemo granice 3. vertexa i 4. vertexa jer trag mijenja smjer
    glVertex3f(x+pomx, y-pomy-r, -6.0f);
    glEnd();
    x2=x+pomx;//krajnje vrijednosti putanje broj 2.
    y2=y-pomy;
    glDisable(GL_BLEND);//onemogucavanje blend-a

    }
    else if(Step>=13){

        glColor4f(1.0f,0.0f,0.0f, 0.5f);//boja traga

        glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBegin(GL_QUADS);//crtanje 2. fiksnog traga
    glVertex3f(0.9,0.38-r , -6.0f);
    glVertex3f(0.9,0.38+r , -6.0f);
    glVertex3f(x2, y2+r, -6.0f);
    glVertex3f(x2+0.68, y2-r+0.67, -6.0f);//
    glEnd();

    glDisable(GL_BLEND);

    }
    if(Step>=13){
        x=-0.3;//pocetne tocke 3. putanje sfere i 3. traga
        y=-0.89;
        if(RunMode==1){

            Step2+=AnimateSpeed;//treci brojac povecavamo od 0.0
        }
        pomy = Step2*0.2025;//ponovno idemo prema desnom donjem kutu
        pomx = Step2*0.41;

        glColor4f(1.0f,0.0f,0.0f, 0.5f);//boja



    glBegin(GL_QUADS);//crtanje 3. traga
    glVertex3f(x,y-r , -6.0f);
    glVertex3f(x+pomx, y-pomy-r, -6.0f);
    glVertex3f(x+pomx, y-pomy+r, -6.0f);
    glVertex3f(x, y+r, -6.0f);
glEnd();

    }
    if(Step>20.5){//kada sfera dode do donjeg desnog ruba zaustavi animacija
        RunMode=0;

    }
glEnd();


glColor3d(0,1,0);//zelena boja sfere
glPushMatrix();//kopira matricu na vrhu i smjesta je u spremnik.
        glTranslated(x+pomx,y-pomy,-6.0);//pomici sferu mijenjajuci pomx,1.putanja:x=-3.2 i y=2.4..itd.
        glRotated(rot,0,0,1);//rotiraj prije transliranja kuglu
        glutSolidSphere(r,slices,stacks);//crtanje sfere, naredbe izmedu push i pop odvijaju se od dolje prema gore
glPopMatrix();//uklanja matricu sa vrha spremnika(stoga)

glLineStipple(1, 0x00FF);//http://www.felixgers.de/teaching/jogl/stippledLines.html
glEnable(GL_LINE_STIPPLE);//omoguci isprekidane linije
glLineWidth(3.5);//sirina linije
glBegin(GL_LINES);//crtaj linije
glVertex3f(-4.0, 2.8, -6.0);//pocetna tocka
glVertex3f(0.9, 0.38, -6.0);//krajnja tocka
glEnd();//zavrsi crtanje
glBegin(GL_LINES);
glVertex3f(0.9, 0.38, -6.0);
glVertex3f(-0.3,-0.89, -6.0);
glEnd();
glBegin(GL_LINES);
glVertex3f(-0.3, -0.89, -6.0);
glVertex3f(3.14, -2.59, -6.0);

glEnd();
glDisable(GL_LINE_STIPPLE);//onemoguci isprekidane linije

 glutSwapBuffers();//slika se na ekranu počinje prikazivati iz
//spremnika u kojem smo završili s crtanjem nove scene, a stari spremnik sada postaje spremnik u kojem
//započinjemo s crtanjem sljedeće scene(GLUT_DOUBLE)

if(RunMode==1){//ako je animacija aktivna ponovno crtaj

glutPostRedisplay();

}

}
//provjerava korisnikov unos i mijenja stanje iz pauze u start animacije i obrnuto ili izlazi iz samog programa u ovom slucaju
static void key(unsigned char key, int , int)
{
    switch (key)
    {
        case 27 ://tipka ESCAPE
            exit(0);
            break;

        case 's':
            if(RunMode==0) RunMode=1;//ako je animacija pauzirana pokreni je
            break;

        case 'x':
            if (RunMode==1) RunMode=0;//ako je animacija aktivna pauziraj je
            break;
        case 'S':
            if(RunMode==0) RunMode=1;//posto je programski jezik case sensitive
            break;

        case 'X':
            if (RunMode==1) RunMode=0;
            break;
    }

    glutPostRedisplay();//prikaz frameova
}

static void idle(void)
{
    glutPostRedisplay();//ponovni prikaz sa razlicitim vrijednostima
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

// Zapocinje izvodenje samog programa
int main(int argc, char *argv[])
{
    glutInit(&argc, argv);//inicijalizacija biblioteke GLUT
    glutInitWindowSize(640,480);//velicina prozora za crtanje u pixelima (sirina,visina)
    glutInitWindowPosition(0,0);//pozicija gornjeg lijevog kuta (x,y) koordinate
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);//nacin iscrtavanja scene:boja rgb|dva graficka spremnika|

    glutCreateWindow("Moja putujuca sfera!");//stvaranje prozora,argument:naslov,vraća:int windowID

    glutReshapeFunc(resize);//kada se promjeni velicina prozora generiramo dogadaj resize
    glutDisplayFunc(&display);//poziva se metoda "display" za prikaz scene u prozoru,argument:pokazivac na metodu
    glutKeyboardFunc(key);//svaki put kada korisnik stisne tipku generira se dogadaj
    glutIdleFunc(idle);

    glClearColor(1,1,1,1);//pozadinska boja bijela, prozirnost=1, dakle neprozirno.
    glEnable(GL_CULL_FACE);//ukljucuje/iskljucuje odbacivanje poligona koji se vide sa prednje ili zadnje strane
    glCullFace(GL_BACK);//odbacivanje sa zadnje strane

    glEnable(GL_DEPTH_TEST);//ukjlucivanje z-spremnika jer je sfera 3D objekt
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);//ukljuci svjetlinu
    glEnable(GL_NORMALIZE);//ukljucuje automatsku normalizaciju vektora normala
    glEnable(GL_COLOR_MATERIAL);//boja materijala
    glEnable(GL_LIGHTING);//ukljucuje osvjetljenje

//izvor svjetlosti s parametrima:
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);//ambijentalna komponenta svjetlosti
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);//difuzna
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);//odsjaj
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);//pozicija

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);//materijal objekta samo GL_FRONT(prednja strana)|ambijentalna boja|
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);//difuzna boja|vrijednost
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);//odsjaj
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);//eksponent odsjaja

    printf(" Press [s] to run,  [x] to pause and [ESC] to exit.\n");//ispis pomocnog teksta za upravljanje animacijom
    glutMainLoop(); //beskonacna petlja osluskivanja i generiranja dogadaja te registriranih metoda

    return EXIT_SUCCESS; //nikad!, zbog prethodne beskonacne petlje
}
