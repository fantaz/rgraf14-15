Opis projekta: Zadatak br.2
	Projekt je napravljen u Microsoft Visual Studio Express 2013 for Windows Desktop. Tutorial instalacije sam pronasao na internetu koji je naveden pod literaturu.
	Kako Windows ne dolazi sa instaliranim GLUT-om bio sam primoran skinuti  Nvidia's Cg toolkit.


	Prvo sam includao knjizice koje su mi bile potrebne za rad. <GL/gl.h> i <GL/glu.h> knjizice nisu bile potrebne jer se nalaze u <GL/glut.h>.
	Poslije slijedi inicijalizacija globalnih varijabla. Npr. za polumjer sfere, duljine stranica kocke, velicina prozora...

	Vecina funkcija, varijabli i opcenito koda je komentirana u samom kodu.
	Za pokretanje animacije ne treba imati nista dodatno instalirano. Animacija inicijalno stoji. Te se pritiskom na malo slovo 'g' (go)
	animacija pokrece brzinom koja je definirana u samom programu. Pritiskom na tipku 's' (stop) animacija trenutno staje te se moze opet pokrenuti
	pritiskom na tipku 'g'. Pritiskom na tipku 'Esc' animacija se prekida te program staje.

	Namjesteno je da kada animacija zavrsi da se gornji dio sfere vidi. Sto potvrduje zavrsetak animacije. U tom trenutku pritiskom na
	tipke 's' ili 'g' ne dogada se nista.

	Sto se tice brzine animacije ona se moze mijenjati sa funkcijom "void timer(); gdje varijabla delay nam govori koliko cesto ce se funkcija
	"animacija();" okidati.

	Velicina prozora je definira sa 800x800 sto se moze mijenjati u globalnoj varijabli "velicina_prozora".
	Funkcija "granica_pada" je izracunata (duljina_stranice - polumjer) + 0.20;-->Ovih 0.20 je udaljenost po x.osi. Stavljeno je da sfera ne pocne padati tocno u trenutku
	kada prode rub jel mi se onda dogada glitch, sfera prolazi kroz kuglu. Nazalost ta udaljenost od 0.20 je namjestena jer nisam uspio pronaci rjesenje!

	Funkcija "pozicija_kraja" odreduje "kraj" prozora(window). Globalnu varijablu sam podijelo sa 100 da bi mogao manevrirati sa pikselima.
	Otprilike sam odredio gdje se nalazi pocetno stanje moje sfere kroz "poziciju scene", te sam u trenutku kada bi sfera dosla do "kraja" animacija bi stala.

	Za kocku sam nacrtao 5 stranice(face) jel mi prednja stranica nije trebala. Prvotno sam "drawBox" crtao pomocu for petlje, ali nisam uspio pronaci nacin kako da onda 
	pojedinoj stranici(face) promijenio boju pa sam se odlucio za primitivniji nacin crtanja kocke.

	Pocetno stanje sfere sam stavio na {0, 0, 0} te sam ju oko toga nacrtao funkcijom "sphere" sa definiranim polumjerom.

	Animacija je odradena tako da se prvotno pomice po x.osi i u slucaju kada dode do "granice" pocne se pomicati i po y.osi koja se mnozi sa gravitacijom.
	
	U "display" funkciji sam odradio translaciju, rotaciju te ubacio sve potrebne funkcije
	
	U "init" funkciji sam odredio vertekse kocke, te sam omogucio (GL_COLOR_MATERIAL) za boje, (GL_DEPTH_TEST) da makne poligone koji se ne vide te svjetlost.




Literatura:
	Instalirao sam OpenGL za Windows pomocu ovog tutoriala:
	http://web.eecs.umich.edu/~sugih/courses/eecs487/glut-howto/

Vecinom sam stavljao linkove na cijeli tutorial a ne na posebne lekcije

	https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html
	http://www.swiftless.com/opengltuts.html
	http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
	http://www.lighthouse3d.com/tutorials/glut-tutorial/animation/
	http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard/
	https://www.opengl.org/discussion_boards/showthread.php/133879-Using-Arrow-keys-in-OpenGL
	https://www.opengl.org/discussion_boards/showthread.php/179378-How-to-stop-glutTimerFunc-animation
	https://www.opengl.org/documentation/specs/glut/spec3/node11.html
	http://stackoverflow.com/questions/7631457/how-to-pause-an-animation-with-opengl-glut
	http://stackoverflow.com/questions/23918/opengl-rotation
	http://stackoverflow.com/questions/3515059/how-to-rotate-a-specific-object-in-opengl
	http://lazyfoo.net/tutorials/OpenGL/index.php#Matrices and Coloring Polygons
	http://www.opengl-tutorial.org/
	http://www.sumantaguha.com/files/materials/ch2.pdf
	http://nehe.gamedev.net/tutorial/collision_detection/17005/
	http://gamedev.stackexchange.com/questions/75681/opengl-calculating-camera-view-matrix
	https://www.opengl.org/discussion_boards/showthread.php/132502-Color-tables
	http://stackoverflow.com/questions/20141807/animating-an-object-in-parabolic-trajectory-in-isometric-view-in-cocos2d-x
	https://www.opengl.org/documentation/specs/glut/spec3/node11.html
	https://www.opengl.org/discussion_boards/showthread.php/125513-How-can-I-get-the-current-object-position
	https://www.opengl.org/sdk/docs/man2/xhtml/glBegin.xml
