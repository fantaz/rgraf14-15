
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

//Inicijalizacija globalnih varijabli
int delay = 30; //ms
float grav = 0.003;	//Gravitacija otprilike
float pomak_y = 0;		//Pomak po y osi
bool keystate = false; //Pocetno stanje--->Animacija stoji
float granica;
float polumjer = 0.5;
float duljina_stranice = 1.0;
float prikaz;					//U nedostatku boljih pojmova, koristio u funkciji pozicija_kraja
float velicina_prozora = 800;

float sphere_position[3] = { 0, 0, 0 }; //Pozicija sfere


//Ovisno o tipki koja je pritisnuta nasa animacija stoji ili se "krece"
void pressedkey(unsigned char key, int x, int y) {

    switch (key) {
    case 'g':
        keystate = true; //Animacija se krece
        break;
    case 's':
        keystate = false;	//Animacija stoji
        break;
    case 27:
        exit(0);	//Sa ESC key izlazimo van
        break;
    }
    key = x;
    key = y;
}
GLfloat n[6][3] = {  // Normale za 6 stranica kocke
    { -1.0, 0.0, 0.0 },
    { 0.0, 1.0, 0.0 },
    { 1.0, 0.0, 0.0 },
    { 0.0, -1.0, 0.0 },
    { 0.0, 0.0, 1.0 },
    { 0.0, 0.0, -1.0 }
};

GLint faces[6][4] = {  // Indeksi verteksa za stranice kocke
    { 0, 1, 2, 3 },
    { 3, 2, 6, 7 },
    //{ 7, 6, 5, 4 },//Ovo bi trebala biti prednja stranica koja nam ne treba
    { 4, 5, 1, 0 },
    { 5, 6, 2, 1 },
    { 7, 4, 0, 3 }
};

GLfloat v[8][3];  //Ovdje stavljamo koordinate vertexa dolje u programu

GLfloat light_position[] = { 0.5, 0.0, 2.0, 1.0 };  // Varijabla za osvjetljenje

//Crtanje nase kocke-->5 stranica, prednja stranica nam ne treba jer je kocka otvorena
void drawBox()
{

    glBegin(GL_QUADS);
    glColor3f(1.0, 0.0, 0.5);
    glNormal3fv(&n[0][0]);
    glVertex3fv(&v[faces[0][0]][0]);
    glVertex3fv(&v[faces[0][1]][0]);
    glVertex3fv(&v[faces[0][2]][0]);
    glVertex3fv(&v[faces[0][3]][0]);
    glEnd();

    glBegin(GL_QUADS);
    glColor3f(0.0, 0.0, 1.0);
    glNormal3fv(&n[1][0]);
    glVertex3fv(&v[faces[1][0]][0]);
    glVertex3fv(&v[faces[1][1]][0]);
    glVertex3fv(&v[faces[1][2]][0]);
    glVertex3fv(&v[faces[1][3]][0]);
    glEnd();

    glBegin(GL_QUADS);
    glColor3f(1.0, 0.0, 0.5);
    glNormal3fv(&n[2][0]);
    glVertex3fv(&v[faces[2][0]][0]);
    glVertex3fv(&v[faces[2][1]][0]);
    glVertex3fv(&v[faces[2][2]][0]);
    glVertex3fv(&v[faces[2][3]][0]);
    glEnd();

    glBegin(GL_QUADS);
    glColor3f(1.0, 0.0, 0.5);
    glNormal3fv(&n[3][0]);
    glVertex3fv(&v[faces[3][0]][0]);
    glVertex3fv(&v[faces[3][1]][0]);
    glVertex3fv(&v[faces[3][2]][0]);
    glVertex3fv(&v[faces[3][3]][0]);
    glEnd();

    glBegin(GL_QUADS);
    glColor3f(0.0, 0.0, 1.0);
    glNormal3fv(&n[4][0]);
    glVertex3fv(&v[faces[4][0]][0]);
    glVertex3fv(&v[faces[4][1]][0]);
    glVertex3fv(&v[faces[4][2]][0]);
    glVertex3fv(&v[faces[4][3]][0]);
    glEnd();
}
//Crtanje sfere
void sphere() {
    glTranslatef(polumjer + sphere_position[0],
                 polumjer + sphere_position[1],
                 polumjer + sphere_position[2]
                );
    glutSolidSphere(polumjer, 40, 30);
}
//Mjesto na kojem kugla pocne padati na x.osi
void granica_pada() {

    granica = (duljina_stranice - polumjer) + 0.20;//Ovih 0.20 sam stavio da kugla ne pocne padati bas tocno kada prode rub (0.5) jel bi u suprotnom prolazila kroz kuglu
}
//Izracunavamo rub prozora gdje se animacija treba zaustaviti
void pozicija_kraja() {
    float skupljac_prasine;
    skupljac_prasine = velicina_prozora / 100;	//Da mozemo baratati sa pikselima 800X800 prozor
    prikaz = skupljac_prasine - 2.65;		//Izracun kraja


}
//Funkcija animacija u ovisnosti o vremenu koje je proteklo (varijabla delay) se pomice po x i y osi
void animacija() {

    if (sphere_position[0] >= granica) {	//Krece se po x-osi do odredene granice
        sphere_position[1] = -pomak_y;
        pomak_y += grav;
    }

    if (sphere_position[1] <= -prikaz) {	//Provjeravamo da li je doslo do kraja prozora
        keystate = false;
        return;
    }
    sphere_position[0] += 0.001;	//Pomice se po x.osi cijelo vrijeme, ovako kada prode granicu_pada simuliramo parabolu jel se onda pocnemo gibati i po y.osi dok se nastavljamo kretati po x.osi

    glutPostRedisplay();			//Ovdje cijelo vrijeme vrtimo funkciju display
}

//Timerom okidamo nasu animaciju svakih x sekundi, u nasem slucaju 30ms koji su spremljeni u globalnu varijablu delay
void timer(int /*time*/)
{
    if (keystate) {		//Provjeravamo da li je stisnut key za pocetak animacije
        animacija();
    }
    glutTimerFunc(delay, timer, 0);
}

//Crta linije na rubovima kocke da se isticu
void drawWireframe(int face)
{
    int i;
    glBegin(GL_LINE_LOOP);	//Crta segmente linije od prvog verteksa do zadnjeg
    for (i = 0; i < 4; i++)
        glVertex3fv((GLfloat *)v[faces[face][i]]);
    glEnd();
}
//Display funkcija
void display()
{
    int i;
    int angle = 25;		//Velicina kuta kojim rotiramo
    glClearColor(0, 0, 0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the window and the depth buffer
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    // Pozicija scene
    glTranslatef(-2, 1, -10); // -2 na desno x.os, 1 prema gore y.os, -10 od nas z.os

    glRotatef(angle, 0, -1, 0);       //Rotacija oko y osi za odredeni kut

    glPushMatrix();
    drawBox();
    //Crtamo obrube kocke, da vidimo linije
    for (i = 0; i < 6; i++) {
        drawWireframe(i);
    }
    glPopMatrix();

    glPushMatrix();
    glColor3f(0.0, 1.0, 0.0);
    sphere();

    glPopMatrix();
    pozicija_kraja();
    granica_pada();

    // Animacija, ocisno o stanju
    if (keystate) {
        animacija();
    }

    glutSwapBuffers();

}

void resize(GLsizei width, GLsizei height)
{
    if (height == 0) height = 1;                // Da se ne dijeli sa nulom
    GLfloat aspect = (GLfloat)width / (GLfloat)height;


    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, aspect, 1, 1000); // view angle u y, aspect, near, far
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void init() {

    // Vertex kocke
    v[0][0] = v[1][0] = v[2][0] = v[3][0] = 0;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = duljina_stranice;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = 0;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = duljina_stranice;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = 0;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = duljina_stranice;

    //Pozicioniranje svjetla
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL); //Omogucavamo boje

    //Omogucavamo depth bufferina da eliminiramo elemente koji se ne vide
    glEnable(GL_DEPTH_TEST);
}

//Main funkcija
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(velicina_prozora, velicina_prozora);
    glutInitWindowPosition(200, 200);

    //Create the window
    glutCreateWindow("Zadatak 2.");

    init();
    //Set handler functions
    glutDisplayFunc(display);
    glutKeyboardFunc(pressedkey);
    glutReshapeFunc(resize);
    glutTimerFunc(0, timer, 0);

    glutMainLoop();
    return 0;
}
