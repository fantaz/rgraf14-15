#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>

GLUquadricObj* Qobj; // Quadrics objekt za crtanje cilindrica	
double Time; // Vrijeme u sekundama

// Torso, Neck, Head
void DrawBody()
{
	// Torso (Kvadar(Kocka))
	glColor3ub(160, 140, 120); // Odabir boje		
	glPushMatrix();
	glScaled(10, 20, 5); // Skaliranje
	glutSolidCube(1); // Kreiranje kocke
	glPopMatrix();

	// Neck (Cilindar)
	glColor3ub(160, 120, 120); // Opet boja
	glTranslated(0, 10, 0); // Translacija mog cilindra
	glPushMatrix();
	glRotated(90, -1, 0, 0); // Biram kut rotacije	
	gluCylinder(Qobj, 2, 2, 2, 10, 10); // Tu sam napravio cilindar		
	glPopMatrix();

	// Head (Sfera(Kugla))
	glColor3ub(140, 170, 130); // Opet boja	
	glTranslated(0, 6, 0); // Translacija
	glutSolidSphere(5, 20, 20); // Kreacija sfere		
}

// Kreiranje ruke				
void DrawArm(int Side)
{
	glPushMatrix();

	// Shoulder (Sfera)
	glColor3ub(40, 110, 150); // Nema  smisla vise komentirat boju	
	glTranslated(6.5*Side, -7, 0); 
	glutSolidSphere(2, 10, 10); 

	// Upper arm(Biceps) (Cilindar)
	glColor3ub(170, 130, 180); 
	glRotated(cos(8*Time+Side)*20 + 110, 1, 0, 0); // Kosinus sam odabrao kao limiter jer ima sam svoje granice pa nisam morao stavljati nikakve uvjete kretanja
	gluCylinder(Qobj, 1, 1, 10, 10, 10); 

	// Elbow (Sfera)
	glColor3ub(240, 160, 120); 
	glTranslated(0, 0, 10); 
	glutSolidSphere(1.6, 10, 10); 

	// Forearm (Cilindar)
	glColor3ub(210, 170, 90); 
	glRotated(cos(8*Time+3)*Side*20 - 110, 1, 0, 0); 
	gluCylinder(Qobj, 1, 1, 8, 10, 10); 

	// Wrist (Sfera)
	glColor3ub(130, 140, 70); 
	glTranslated(0, 0, 8); 
	glutSolidSphere(1.2, 10, 10); 

	// Fist (Kocka)
	glColor3ub(120, 120, 160); 
	glTranslated(0, 0, 1); 
	glScaled(1, 3, 2); 
	glutSolidCube(1); 

	glPopMatrix();
}

// Kreacija noge
void DrawLeg(int Side)
{
	glPushMatrix();

	// Zdjelica(Ja mislim) (Sfera)
	glColor3ub(70, 60, 120); 
	glTranslated(5*Side, -27, 0); // Ovjde treba napomenuti varijablu side koja mi samo odlucuje jel mi to desna il ljeva noga
	glutSolidSphere(2, 10, 10); 

	// Upper leg (Cilindar)
	glColor3ub(100, 110, 210); 
	glRotated(cos(8*Time)*10 + 40, 1, 0, 0); 
	gluCylinder(Qobj, 1, 1, 8, 10, 10); 

	// Knee (Sfera)
	glColor3ub(160, 100, 70); 
	glTranslated(0, 0, 8); 
	glutSolidSphere(1.4, 10, 10); 

	// Lower leg (Cilindar)
	glColor3ub(70, 190, 230); 
	glRotated(-cos(8*Time)*20 + 90, 1, 0, 0); 
	gluCylinder(Qobj, 1, 1, 8, 10, 10); 

	// Ankle (Sfera)
	glColor3ub(170, 130, 110); 
	glTranslated(0, 0, 8); 
	glutSolidSphere(1.2, 10, 10); 

	// Foot (Kocka)
	glColor3ub(80, 100, 140); 
	glTranslated(0, 1.5, 0); 
	glRotated(cos(8*Time)*10 - 40, 1, 0, 0); 
	glScaled(2, 4, 1); 
	glutSolidCube(1); 

	glPopMatrix();
}

// Tu si refresham scenu
void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(50, 1, 0.1, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Ovjde si određujem poziciju i rotaciju robota i mjenjam si ovu varijablu time sta si koristim u onom cosinusu gore
	Time = glutGet(GLUT_ELAPSED_TIME) * 0.001;  
	glTranslated(0, cos(8*Time)*2, -60); 
	glRotated(40*Time, 0, 1, 0); 


	// Crtam samo dijelove robota(tj. ne crtam ja nego pozivam funkcije koje crtaju)
	DrawBody(); // Torso, Neck, Head
	DrawArm(+1); // Right arm
	DrawArm(-1); // Left arm
	DrawLeg(+1); // Right leg
	DrawLeg(-1); // Left leg

	
	glutSwapBuffers();
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	// GLUT setup
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(512, 512);
	glutCreateWindow("Dancer");

	// OpenGL setup(za lighting,boje depth buffer i takve stvari)
	glEnable(GL_DEPTH_TEST); 
	glEnable(GL_LIGHTING); 
	glEnable(GL_LIGHT0); 
	glEnable(GL_COLOR_MATERIAL); 
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE); 
	glEnable(GL_NORMALIZE); 
	glClearColor(0.75, 0.75, 0.75, 1); // background boja

	// Ovo mi je za cilindar quadrics objekt
	Qobj = gluNewQuadric();

	
	glutDisplayFunc(Display);

	glutMainLoop();
	return 0;
}
