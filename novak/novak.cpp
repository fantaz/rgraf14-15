#include <GL/glut.h>
using  namespace std;

/*
	rotiramo tijelo od s -90 stupnjeva ka nuli
	stoga nam je currentAngle = -90, a tezimo nuli
*/

float time_cnt=0;
float xwatch=1.0-0.02;
float ywatch=1.0-0.02;
float currentAngle=-90.0;
int refresh=1;
bool work=false; // radimo?

void myscene() {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, -1.0, 0.0, 3.0); // pogled
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// konfiguracija naseg pogleda na scenu
	gluLookAt(1.0, 0.0, 3.0, 1.0, 0.0, 1.0, 0.0, 1.0,0.0); 

	glRotatef(5.0, 1.0, 0.0, 0.0);
	glRotatef(40.0, 0.0, 1.0, 0.0);
	glRotatef(0.0, 0.0, 0.0, 1.0);

	glEnable(GL_LIGHTING); // osvjetljenje na max!
	glEnable(GL_COLOR_MATERIAL); // potrebno radi boja kreacija

	float col[4]={0,0,0,0};
    glMaterialfv(GL_FRONT,GL_AMBIENT,col);
    glMaterialfv(GL_FRONT,GL_SPECULAR,col);
	
	glTranslatef(0, 0, 1.0);
	glRotatef(currentAngle,1.0,0.0,0.0);

	/*
		provuci liniju oko koje rotiramo tijelo
			- cijelim ekranom
			- crna boja
	*/
	glLineWidth(4); 
	glBegin(GL_LINES);
	glColor3f(0,0,0);
	glVertex3f(5.0,0.0,0.0);
	glVertex3f(-5.0,0.0,0.0);
	glEnd();

	/*
		ocrtaj zeleni okvir
			- treba nam 12 linija
			- grupirao sam ih radi preglednosti u touple
	*/
	glLineWidth(4);
	glBegin(GL_LINES);
	  glColor3f(0,1,0);

	  // donje horizontale
	  glVertex3f(0.0, 0.0, 0.0);
	  glVertex3f(0.0, 0.0, 1.0);

	  glVertex3f(1.0, 0.0, 0.0);
	  glVertex3f(1.0, 0.0, 1.0);

	  // cetiri vertikale
	  glVertex3f(0.0, 0.0, 0.0);
	  glVertex3f(0.0, 1.0, 0.0);

	  glVertex3f(1.0, 0.0, 0.0);
	  glVertex3f(1.0, 1.0, 0.0);

	  glVertex3f(1.0, 0.0, 1.0);
	  glVertex3f(1.0, 1.0, 1.0);

	  glVertex3f(0.0, 0.0, 1.0);
	  glVertex3f(0.0, 1.0, 1.0);

	  // horizontala: dolje, naprijed
	  glVertex3f(0.0, 0.0, 0.0);
	  glVertex3f(1.0, 0.0, 0.0);

	  // horizontala: dolje, iza
	  glVertex3f(0.0, 0.0, 1.0);
	  glVertex3f(1.0, 0.0, 1.0);

	  // horizontala: gore, lijeva
	  glVertex3f(0.0, 1.0, 0.0);
	  glVertex3f(0.0, 1.0, 1.0);

	  // horizontala: gore, desna
	  glVertex3f(1.0, 1.0, 0.0);
	  glVertex3f(1.0, 1.0, 1.0);

	  // horizontala: gore, naprijed
	  glVertex3f(0.0, 1.0, 0.0);
	  glVertex3f(1.0, 1.0, 0.0);

	  // horizontala: gore, iza
	  glVertex3f(0.0, 1.0, 1.0);
	  glVertex3f(1.0, 1.0, 1.0);

	glEnd();

	/*
		nase tijelo treba rotirati od -90 stupnjeva do nule (0),
		tada zavrsavamo animaciju,
		vode vise nema
	*/

	if(currentAngle<0.0) {

		if( work == true ) currentAngle += 0.15f;

		glBegin(GL_POLYGON);
		glColor3f(0,0,1);
		glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
		glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
		glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
		glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
		glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
		glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
		glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
		glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
		glEnd();

		if(time_cnt <= 4.5) { // pocetak

			if(time_cnt==0){

				glBegin(GL_POLYGON);
				glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(0.0+0.02, 1.0-0.02, xwatch);
				glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 1.0-0.02, xwatch);
				glVertex3f(0.0+0.02, 1.0-0.02, xwatch);
				glVertex3f(1.0-0.02, 1.0-0.02, xwatch);
				glEnd();

				glBegin(GL_POLYGON);
				glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
				glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
				glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
				glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
				glEnd();

				glBegin(GL_POLYGON);
				glVertex3f(0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(0.02, 1.0-0.02, xwatch);
				glVertex3f(0.02, ywatch, 0.0+0.02);
				glVertex3f(0.02, 0.0+0.02, 0.0+0.02);
				glEnd();

			}
			else {

				if(work==true) xwatch=xwatch-0.0032;
				glBegin(GL_POLYGON);
				glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(0.0+0.02, 1.0-0.02, xwatch);
				glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(1.0-0.02, 1.0-0.02, xwatch);
				glVertex3f(0.0+0.02, 1.0-0.02, xwatch);
				glVertex3f(1.0-0.02, 1.0-0.02, xwatch);
				glEnd();

				glBegin(GL_POLYGON);
				glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
				glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
				glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
				glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
				glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
				glEnd();

				glBegin(GL_POLYGON);
				glVertex3f(0.02, 0.0+0.02, 1.0-0.02);
				glVertex3f(0.02, 1.0-0.02, xwatch);
				glVertex3f(0.02, ywatch, 0.0+0.02);
				glVertex3f(0.02, 0.0+0.02, 0.0+0.02);
				glEnd();

				glBegin(GL_POLYGON);
				glVertex3f(1.0-0.02, 1.0-0.02, xwatch);
				glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
				glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
				glEnd();

			}
		}

		if(time_cnt > 4.5 && time_cnt < 9.0){ // zavrsetak

			if(work==true) ywatch=ywatch-0.0032f;

			glBegin(GL_POLYGON);
			glVertex3f(0.0+0.02, 0.0+0.02, 1.0-0.02);
			glVertex3f(0.0+0.02, ywatch, xwatch);
			glVertex3f(1.0-0.02, 0.0+0.02, 1.0-0.02);
			glVertex3f(1.0-0.02, ywatch, xwatch);
			glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
			glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
			glEnd();

			glBegin(GL_POLYGON);
			glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
			glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
			glVertex3f(0.0+0.02, 0.0+0.02, 0.0+0.02);
			glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
			glVertex3f(1.0-0.02, 0.0+0.02, 0.0+0.02);
			glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
			glVertex3f(0.0+0.02, ywatch, 0.0+0.02);
			glVertex3f(1.0-0.02, ywatch, 0.0+0.02);
			glEnd();

			glBegin(GL_POLYGON);
			glVertex3f(0.02, 0.0+0.02, 1.0-0.02);
			glVertex3f(0.02, ywatch, 0.0+0.02);
			glVertex3f(0.02, 0.0+0.02, 0.0+0.02);
			glEnd();
			
		}

		glBegin(GL_LINES);
		glColor3f(0,1,0);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 0.0, 1.0);
		glVertex3f(0.0, 0.0, 1.0);
		glVertex3f(0.0, 1.0, 1.0);
		glVertex3f(0.0, 0.0, 1.0);
		glVertex3f(1.0, 0.0, 1.0);
		glEnd();

	}

	if(work==true) time_cnt=(time_cnt+0.015);
	glutSwapBuffers();
}

void controls (unsigned char k, int x, int y) {

	if (k == 27) exit(0);
	else if (k == 'e') work = false;
	else if (k == 's') work = true;

}

void timer(int value) {
   glutPostRedisplay();
   glutTimerFunc(refresh, timer, 0);
}

int main( int argc, char **argv )  {

	glutInit(&argc, argv);

	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );

	glutInitWindowSize( 800, 800 );

	glutCreateWindow("Racunalna grafika - Mario Novak");

	glClearColor( 1, 1, 1, 1 );
	glutDisplayFunc( myscene );

	glutKeyboardFunc( controls );
	glutTimerFunc( 0, timer, 0 );

	glutMainLoop();

	return 0;
}