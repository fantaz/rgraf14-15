Mario Novak

Racunalna grafika
	- ak. god. 2014./2015.
	- 5. semestar

Program sam kompajlirao preko komandne linije koristeci naredbu:
	g++ novak.cpp -lGL -lGLU -lglut -o novak
	(moguce koristenje make-a, no buduci da se radi o jednoj datoteci nije problem ni sa ovime ici)

Manipulaciju radite tipke koje su predlozene u zadatku:
	's' - start
	'e' - pauziranje
	ESC - izlaz

Princip rada
	Komentare vezane uz funkcioniranje programa postavio sam u .cpp datoteku radi lakseg snalazenja i pracenja koda.

Literatura
	Internet u punom smislu
	Od stackoverflowa do raznih foruma ukoliko ovaj prvi zakaze, sto je iznimno rijetko.
	Ponajvise gotovi primjeri sa Interneta koji prikazuju uporabu odredjenih naredbi i iscrtavanje jednostavnih oblika.