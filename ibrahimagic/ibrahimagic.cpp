#include <GL/glut.h>

GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 }; // Bijelo difuzno svjetlo
GLfloat light_ambient[] = { 0.2, 0.2, 0.2, 1.0 }; // Sivo Ambientalno svjetlo
GLfloat light_position[] = { 0.0, 4.0, 0.0, 1.0 }; // Lokacija beskonačnog svjetla

GLfloat radius = 1; //Radijus sfere i duljina stranice
float x = 0;
float y = 0;
int korak = 0;
bool animating = false;
float x_putanja[802];
float y_putanja[802];
float kut_rot[802];

void createTrajectory() {
    for (int i = 0; i < 802; i++) {
        x += 0.01;
        if(x < radius)
            y = 0;
        else
            y = -(x-radius)*(x-radius);
        float kut = 360 * x / (2 * 3.14 * radius);
        x_putanja[i] = x;
        y_putanja[i] = y;
        kut_rot[i] = kut;
    }
}

//Funckija crtanja putanje po točkama iz create patha

void drawTrajectory() {
    int i;
    glColor4f(0,0,0,1);
    glBegin(GL_LINES);
    for (i = 0; i < 802; i+=4) {
        glVertex3f(x_putanja[i], y_putanja[i], 0);
    }
    glEnd();
}

//CRTANJE KUTIJE

void drawCubeRed() {
    //vanjska strane kocke crvena
    glColor4f(1, 0, 0, 1);

    //prednja stranica
    glBegin(GL_POLYGON);
    glNormal3f(  0.0, 0.0, 1.0);
    glVertex3f(  radius, -radius, radius );
    glVertex3f(  radius,  radius, radius );
    glVertex3f( -radius,  radius, radius );
    glVertex3f( -radius, -radius, radius );
    glEnd();

    //lijeva stranica
    glBegin(GL_POLYGON);
    glNormal3f( -1.0, 0.0, 0.0);
    glVertex3f( -radius, -radius,  radius );
    glVertex3f( -radius,  radius,  radius );
    glVertex3f( -radius,  radius, -radius );
    glVertex3f( -radius, -radius, -radius );
    glEnd();

    //Gornja stranica
    glBegin(GL_POLYGON);
    glNormal3f( 0.0, 1.0, 0.0);
    glVertex3f(  radius,  radius,  radius );
    glVertex3f(  radius,  radius, -radius );
    glVertex3f( -radius,  radius, -radius );
    glVertex3f( -radius,  radius,  radius );
    glEnd();

    //Donja stranica
    glBegin(GL_POLYGON);
    glNormal3f(  0.0, -1.0, 0.0);
    glVertex3f(  radius, -radius, -radius );
    glVertex3f(  radius, -radius,  radius );
    glVertex3f( -radius, -radius,  radius );
    glVertex3f( -radius, -radius, -radius );
    glEnd();

    //Straznja stranica
    glBegin(GL_POLYGON);
    glNormal3f( 0.0, 0.0, -1.0);
    glVertex3f( radius,-radius,-radius);
    glVertex3f(-radius,-radius,-radius);
    glVertex3f(-radius, radius,-radius);
    glVertex3f( radius, radius,-radius);
    glEnd();
}

//plava kocka (unutrasnjost)
void drawCubeBlue() {
    glPushMatrix();
    //skalirana za 0.99
    glScalef(0.99, 0.99, 0.99);
    glColor4f(0, 0, 1, 1);
    //Prednja strana
    glBegin(GL_POLYGON);
    glNormal3f(  0.0, 0.0, 1.0);
    glVertex3f(  radius, -radius, radius );
    glVertex3f(  radius,  radius, radius );
    glVertex3f( -radius,  radius, radius );
    glVertex3f( -radius, -radius, radius );
    glEnd();

    //Lijeva strana
    glBegin(GL_POLYGON);
    glNormal3f( -1.0, 0.0, 0.0);
    glVertex3f( -radius, -radius,  radius );
    glVertex3f( -radius,  radius,  radius );
    glVertex3f( -radius,  radius, -radius );
    glVertex3f( -radius, -radius, -radius );
    glEnd();

    //Gornja strana
    glBegin(GL_POLYGON);
    glNormal3f( 0.0, 1.0, 0.0);
    glVertex3f(  radius,  radius,  radius );
    glVertex3f(  radius,  radius, -radius );
    glVertex3f( -radius,  radius, -radius );
    glVertex3f( -radius,  radius,  radius );
    glEnd();

    //Donja strana
    glBegin(GL_POLYGON);
    glNormal3f(  0.0, -1.0, 0.0);
    glVertex3f(  radius, -radius, -radius );
    glVertex3f(  radius, -radius,  radius );
    glVertex3f( -radius, -radius,  radius );
    glVertex3f( -radius, -radius, -radius );
    glEnd();

    //Stražnja strana
    glBegin(GL_POLYGON);
    glNormal3f( 0.0, 0.0, -1.0);
    glVertex3f( radius,-radius,-radius);
    glVertex3f(-radius,-radius,-radius);
    glVertex3f(-radius, radius,-radius);
    glVertex3f( radius, radius,-radius);
    glEnd();
    glPopMatrix();
}

//CRTANJE SFERE

void drawSphere() {
    glColor4f(1, 1, 0, 1);
    float sphereRad = radius;
    glutSolidSphere(sphereRad, 30, 30);
}

//POKRETANJE ANIMACIJE

void animacija(int t) {
    drawTrajectory();   //crtanje putanje
    glPushMatrix();
    drawCubeRed();
    drawCubeBlue();
    glPopMatrix();
    glPushMatrix();
    //pomicanje sfere(translacija)
    glTranslatef(x_putanja[t], y_putanja[t], 0);
    //pomicanje sfere(rotacija oko z osi)
    glRotatef(kut_rot[t], 0, 0, -1);
    drawSphere();
    glPopMatrix();
}

//DISPLAY

void display(void) {
    if(korak==800) animating=false;
    if(animating && korak<800)korak += 1;
    //pozadinska boja
    glClearColor(0.79,0.78, 0.78,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    animacija(korak);
    glutSwapBuffers();
}

//INICIJALIZACIJA SVIJETLA I POGLEDA

void init(void) {
    //računanje putanje
    createTrajectory();
    //Izvor svjetlosti
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    //koristi boju kao materijal
    glEnable(GL_COLOR_MATERIAL);
    //eliminiranje skrivenih povrsina
    glEnable(GL_DEPTH_TEST);
    //podesavanje pogleda
    glMatrixMode(GL_PROJECTION);
    gluPerspective(40.0, 1.0, 1.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
    gluLookAt(5.0, 2.0, 20.0, 4.4, -4.5, 0.0, 0.0, 1.0, 0.0);
}

void keyboard(unsigned char key, int x, int y) {

    if (key == 'g' || key == 'G')
        animating = true;
    else if (key == 's' || key == 'S')
        animating = false;
    else if (key == 27)
        exit(0);
    key=x;
    key=y;
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(700, 700);
    glutCreateWindow("Zadatak 2");
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutKeyboardFunc(keyboard);
    init();
    glutMainLoop();
    return 0;
}
