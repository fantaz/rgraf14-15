Objašnjenje realizacije zadatka 2:

Za prikaz scene odabrao sam sljedece vrijednosti:
	-Difuzno svijetlo rgb(1.0, 1.0, 1.0, 1.0)
	-Ambijentalno svijetlo rgb(0.2, 0.2, 0.2, 1.0)
	-Pozicija svijetlosti (0.0, 4.0, 0.0, 1.0)	
	-Window size (700x700)
	-Kamera je pomaknuta kako bi se kocka nalazila u gornjem lijevom kutu prozora
	-U funkciji animacija() se nalazi poziv funkcije drawTrajectory() koja iscrtava isprekidane linije na osnovu vrijednosti iz 		 polja za x i y koordinate parabolične putanje.


Iscrtavanje kocke i sfere:

	-Koristio sam varijablu radijus kao referentnu vrijednost za veličinu stranice kocke i sfere
	-Kocka je iscrtana pomocu glquads (plohu po plohu),na način da je unutrašnjost kocke realizirana skaliranjem druge kocke 		 (*0,99) koja je obojana u plavo
	-Sfera se translatira i rotira.Nakon što x koordinata njenog središta dosegne vrijednosti koja je definirana u varijabli 		 radijus,počinje se kretati paraboličnom putanjom.
	-Parabolična putanja je izdračunata povećavanjem vrijednosti x za 0,01 pri svakom koraku,a y vrijednost je izračunata y=-x*x 		 (gdje je x pomaknut za -1)
	
Upravljanje animacijom:

	-Tipka na tipkovnici "g" pokreće animaciju
	-Tipka na tipkovnici "s" zaustavlja animaciju
	-Tipka na tipkovnici "Esc" izlazi iz programa

Pokretanje programa:
	-program ne zahtjeva argumente prilikom pokretanja vec je dovoljno samo pokrenuti datoteku komandom ./ibrahimagic


Izvori:
Crtanje kocke:	 http://www.wikihow.com/Make-a-Cube-in-OpenGL
Osvjetljenje: http://www.glprogramming.com/red/chapter05.html
Upravljanje animacijom:	http://www.swiftless.com/tutorials/opengl/keyboard.html

	
	
