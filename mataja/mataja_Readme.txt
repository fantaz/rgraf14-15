Opis

Mateo Mataja

Zadatak 4

Osim onog što je zadano zadatkom, dodano je nekoliko dodatnih mogućnosti,
sve u svrhu lakšeg razvoja i testiranja:
 * mogućnost pomicanja (preciznije rotiranja) kamere oko objekta (lijevom tipkom miša)
 * mogućnost animiranja unazad (aktivira se i deaktivira tipkom 'r')
 * mogućnost izvođenja samo jednog koraka animacije (tipkom 't')

Preostalo za napraviti:
 * Uključiti osvjetljenje
 * Osjenčiti objekte (preciznije vodu)

Tipke:
 - ESC - izlaz
 - s - pokretanje animacije
 - e - zaustavljanje animacije
 - r - promjena smjera animacije
 - t - (samo) jedan korak animacije

Animacija se vrši pri brzini od 10 FPS. Korak animacije je jedan kutni stupanj.
Ove se vrijednosti izvučene kao globalne varijable, te je tako previđeno njihovo mijenjanje.

Animacija se bazira na rotaciji oko Z osi, a izvodi se mijenjanjem kuta rotacije od 0 do 90 stupnjeva.
Po pitanju vode, ova animacija ima dvije faze tj. crtanje ima dva slučaja:
  1) dok je kut između 0 i 45 - voda je nepravilna četverostrana prizma (baza je četverokut (preciznije trapez))
  2) dok je kut između 45 i 90 - voda je trostrana prizma (baza je pravokutni trokut)
Što se tiče izračuna koordinata vode, promatrajući ilustraciju u zadatku, zaključio sam da je kut kojeg zatvara površina vode s gornjom za prvi odnosno donjom stranom kocke za drugi slučaj sukladan kutu za koji je kocka rotirana.
Nadalje, ako promatramo kocku od naprijed, vidjeti ćemo da površina vode dijeli stranicu kocke (kvadrat) na dva dijela: jedan pravokutni trokut i jedan četverokut (točnije trapez, no to ovdje nije bitno).
Za navedeni pravokutni trokut znamo vrijednosti kuteva te duljinu kraće katete, a treba nam duljina duže katete.
Navedemo izračunavamo koristeći trigonometrijske funkcije tangens i kotangens.

Program ne prima (nikakve) parametre kao argumente komandne linije.

Literatura:
 - OpenGL - https://www.opengl.org/
 - OpenGL službena dokumentacija - https://www.opengl.org/documentation/
 - OpenGL wiki - https://www.opengl.org/wiki/Main_Page
 - Wikipedia
 - Wikibooks - http://en.wikibooks.org/wiki/OpenGL_Programming
 - http://www.glprogramming.com/red/
 - http://www.lighthouse3d.com/tutorials/glut-tutorial/
 - http://stackoverflow.com/questions/5033832/what-is-the-nicest-way-to-close-glut
 - http://stackoverflow.com/questions/17972275/how-to-close-glut-window-without-terminating-of-application
 - http://stackoverflow.com/questions/2146884/why-does-opengl-use-degrees-instead-of-radians
 - http://gamedev.stackexchange.com/questions/20758/how-can-i-orbit-a-camera-about-its-target-point
 - Stack Overflow
 - Stack Exchange
 - ...