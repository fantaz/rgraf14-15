#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut.h>

#define UNUSED(x)  (void)x

inline float rad2deg(float radians)
{
	static const float k = 180.0f / M_PI;
	return radians * k;
}

inline float deg2rad(float degrees)
{
	static const float k = M_PI / 180.0f;
	return degrees * k;
}

// Camera rotation / position
bool g_rotating = false;
int g_rotateLastX;
int g_rotateLastY;

double g_camera_distance = 5.0;
double g_camera_angle_h = +0.5;  // radians
double g_camera_angle_v = -0.5;  // radians
double g_camera_x = 0.0;
double g_camera_y = 0.0;
double g_camera_z = 0.0;

// Animation
void animation_timer_tick(int);
void animation_start();
void animation_step();
void animation_stop();

int g_timer_interval = 100;  // miliseconds
bool g_animating = false;
double g_anim_step = 1.0;
double g_angle = 0.0;  // degrees

void compute_camera_coordinates()
{
	// Reference: http://gamedev.stackexchange.com/questions/20758/how-can-i-orbit-a-camera-about-its-target-point
	g_camera_x = -1.0 * g_camera_distance * sin(g_camera_angle_h) * cos(g_camera_angle_v);
	g_camera_y = -1.0 * g_camera_distance * sin(g_camera_angle_v);
	g_camera_z = -1.0 * g_camera_distance * cos(g_camera_angle_h) * cos(g_camera_angle_v);
}

void init()
{
	compute_camera_coordinates();
}

void animation_timer_tick(int arg_value)
{
	// Unused parameter
	UNUSED(arg_value);

	// Do step of animation
	animation_step();

	// Invalidate i.e. request redraw
	glutPostRedisplay();

	// Schedule next timer tick unless animation is cancelled
	if (g_animating)
	{
		glutTimerFunc(g_timer_interval, animation_timer_tick, 0);
	}
}

void animation_start()
{
	if (!g_animating)
	{
		g_animating = true;

		if (g_angle <= 0.0 && g_anim_step < 0.0)
		{
			g_angle = 90.0;
		}
		else if (g_angle >= 90.0 && g_anim_step > 0.0)
		{
			g_angle = 0.0;
		}

		glutTimerFunc(g_timer_interval, animation_timer_tick, 0);
	}
}

void animation_step()
{
	// Increase angle
	g_angle += g_anim_step;

	// Stop animation if rotation angle outside range <0, 90>
	if (g_angle < 0.0 || g_angle > 90.0)
		g_animating = false;

	// Limit value of angle
	if (g_angle < 0.0)
	{
		g_angle = 0.0;
	}
	else if (g_angle > 90.0)
	{
		g_angle = 90.0;
	}
}

void animation_stop()
{
	g_animating = false;
}

void draw_axis()
{
	glBegin(GL_LINES);
		glVertex3f(0.0f, 0.0f, -2.5f);
		glVertex3f(0.0f, 0.0f, +2.5f);
	glEnd();
}

void draw_wire_cube()
{
	glBegin(GL_LINE_LOOP);
		glVertex3f( 0.0f,  0.0f, -0.5f);
		glVertex3f( 1.0f,  0.0f, -0.5f);
		glVertex3f( 1.0f, +1.0f, -0.5f);
		glVertex3f( 0.0f, +1.0f, -0.5f);
	glEnd();

	glBegin(GL_LINE_LOOP);
		glVertex3f( 0.0f,  0.0f, +0.5f);
		glVertex3f(+1.0f,  0.0f, +0.5f);
		glVertex3f(+1.0f, +1.0f, +0.5f);
		glVertex3f( 0.0f, +1.0f, +0.5f);
	glEnd();

	glBegin(GL_LINES);
		glVertex3f( 0.0f,  0.0f, -0.5f);
		glVertex3f( 0.0f,  0.0f, +0.5f);

		glVertex3f( 0.0f, +1.0f, -0.5f);
		glVertex3f( 0.0f, +1.0f, +0.5f);

		glVertex3f(+1.0f,  0.0f, -0.5f);
		glVertex3f(+1.0f,  0.0f, +0.5f);

		glVertex3f(+1.0f, +1.0f, -0.5f);
		glVertex3f(+1.0f, +1.0f, +0.5f);
	glEnd();
}

void draw_water_fill(double angle)
{
	if (angle >= 0.0f && angle <= 45.0f)
	{
		float d = 1.0f - tan(deg2rad(angle));  // Tangent

		glBegin(GL_QUADS);
			glVertex3f(1.0f,    d, -0.5f);
			glVertex3f(1.0f,    d, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);

			glVertex3f(1.0f,    d, -0.5f);
			glVertex3f(1.0f,    d, +0.5f);
			glVertex3f(1.0f, 0.0f, +0.5f);
			glVertex3f(1.0f, 0.0f, -0.5f);

			glVertex3f(1.0f,    d, -0.5f);
			glVertex3f(1.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);

			glVertex3f(1.0f,    d, +0.5f);
			glVertex3f(1.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);

			glVertex3f(0.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);

			glVertex3f(0.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(1.0f, 0.0f, +0.5f);
			glVertex3f(1.0f, 0.0f, -0.5f);
		glEnd();
	}
	else if (angle > 45.0f && angle < 90.0f)
	{
		float d = 1.0 / tan(deg2rad(angle));  // Cotangent

		glBegin(GL_TRIANGLES);
			glVertex3f(   d, 0.0f, -0.5f);
			glVertex3f(0.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);

			glVertex3f(   d, 0.0f, +0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);
		glEnd();

		glBegin(GL_QUADS);
			glVertex3f(   d, 0.0f, -0.5f);
			glVertex3f(   d, 0.0f, +0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 0.0f, -0.5f);

			glVertex3f(   d, 0.0f, -0.5f);
			glVertex3f(   d, 0.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);

			glVertex3f(0.0f, 0.0f, -0.5f);
			glVertex3f(0.0f, 0.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, +0.5f);
			glVertex3f(0.0f, 1.0f, -0.5f);
		glEnd();
	}
}

void on_display()
{
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(g_camera_x, g_camera_y, g_camera_z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glClearColor(0.5, 0.5, 0.5, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glRotated(g_angle, 0.0, 0.0, 1.0);

	glColor3f(0.0f, 0.0f, 0.0f);
	glLineWidth(2.0);
	draw_axis();

	glColor3f(0.0f, 1.0f, 0.0f);
	glLineWidth(4.0);
	draw_wire_cube();

	glColor3f(0.0f, 0.0f, 0.5f);
	draw_water_fill(g_angle);

	glFlush();
}

void on_reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, width, height);
	gluPerspective(45, (double) width / (double) height, 1.0, 1000.0);
}

void on_keyboard(unsigned char key, int x, int y)
{
	// Unused parameters
	UNUSED(x);
	UNUSED(y);

	if (key == 27)  // Escape key
	{
		// Exit
		glutLeaveMainLoop();
	}
	else if (key == 's')
	{
		// Start animation
		animation_start();
	}
	else if (key == 'e')
	{
		// Stop animation
		animation_stop();
	}
	else if (key == 'r')
	{
		// Reverse direction
		g_anim_step = -g_anim_step;
	}
	else if (key == 't')
	{
		// Do a single step of animation
		animation_step();

		// Request redraw
		glutPostRedisplay();
	}
}

void on_mouse_button(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			g_rotating = true;
			g_rotateLastX = x;
			g_rotateLastY = y;
		}
		else if (state == GLUT_UP)
		{
			g_rotating = false;
		}
	}
}

void on_mouse_move(int x, int y)
{
	if (g_rotating)
	{
		g_camera_angle_h += 0.004f * (g_rotateLastX - x);
		g_camera_angle_v += 0.004f * (g_rotateLastY - y);

		g_rotateLastX = x;
		g_rotateLastY = y;

		compute_camera_coordinates();

		glutPostRedisplay();
	}
}

int main(int argc, char* argv[])
{
	init();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH);
	glutCreateWindow("Zadatak 4");
	glutDisplayFunc(on_display);
	glutReshapeFunc(on_reshape);
	glutKeyboardFunc(on_keyboard);
	glutMouseFunc(on_mouse_button);
	glutMotionFunc(on_mouse_move);
	glutMainLoop();

	return 0;
}
