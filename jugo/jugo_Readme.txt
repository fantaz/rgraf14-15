Kratak opis programa koji simulira kuglu koja pada iz kocke parabolicnom putanjom.

U programskom rjesenju sam odvojio konfiguracijski dio od samog koda radi lakseg upravljanja i manipulacijom odredenih stavki u programu.

Objasnjenja pojedinih vrijednosti:

cubeSize=2.0;
Inicijalna velicina cijele stranice kocke, ogranicio sam je da bude izmedu 0.1 i 2.0 jer sve ispod bi bilo pre malo i kocka se nebi ni vidjela, isto tako iznad bi bilo pre veliko. Velicina kocke utjece i na velicinu kugle (sfere) jer moraju biti istih dimenzija.

Sljedecim vrijednostima sam odredio piramidu pogleda (frustum), vrijednosti su izabrane eksperimentiranjem dok nisu postignuti zadovoljavajuci rezultati.
Sami nazivi varijabli su jasni po sebi, za definiranje projekcije potrebno nam je...

vidno polje u stupnjevima
fov = 40.0;

near clip ravnina kao i far clip ravnina
nearZ = 1.0;
farZ = 100.0;

Jos jedan parametar je potreban a to je aspect ratio, njega nisam stavio da se moze konfigurirati rucno vec se on izracunava i njegova vrijednost je dinamicka u ovisnosti u velicini prozora, to sam napravio u reshape funkciji (changeSize), njegova vrijednost je u biti omjer sirina/visina.

Pogled je definiran sljedecim vrijednostima
polozaj ocista, malo odmaknuto u pozitivnom smjeru Z osi
eyeX=0.0, eyeY=0.0, eyeZ=15.0;

srediste, referentna tocka
centerX=0.0, centerY=0.0, centerZ=0.0;

smjer up vektora (u pozitivnom smjeru Y)
upX=0.0, upY=1.0, upZ=0.0;

Nadalje, definirane su tipke koje se koriste za upravljanje programom, moguca izmjena, kao sto u zadatku zadano sa g se pokrece animacija, sa s se pauzira animacija, a sa ESC (ascii vrijednost 27) se izlazi iz programa, uz zadane zadatke dodao sam jos i restartKey sa kojom se ponovno pokrece animacija.

startKey   = 'g';
stopKey    = 's';
restartKey = 'r';
endKey     =  27;

Sljedece vrijednosti su za definiranje kocke i njenog prikaza.
Moguc je odabir redoslijeda iscrtavanja odredenih ploha kocke (6 ploha, 0->5 iscrtavanje), indeksi se koriste za dinamicko popunjavanje arraya za plohe, vertexe i normale kocke u funkcijama predvidjenim za to

leftFace   0
topFace    1
rightFace  2
bottomFace 3
backFace   4
frontFace  5

array za plohe kocke, 6 ploha sa 4 indeksa vertexa svaki koji odreduju plohu
faces  [6][4];

array za normale kocke, 6 ploha sa 3 koordinate
normals[6][3];

array za vertexe
vertex [8][3];

Koristio sam dodatni array faces koji mi oznacava indeks vertexa na taj nacin sam izbjegao definiranje vertexa koji su zajednicki plohama, inace bez tog array bi trebao za crtanje kocke definirati 24 vertexa, ovako sam to napravio sa 8 vertexa, takoder tu je i array sa normalama vertexa radi osvjetljenja.

Sto se tice osvjetljenja, komponente koje sam realizirao su ambijentalno (sivkaste boje), difuzno i tockasto (specular) bijele boje, dok je pozicija svijetla pomaknuta za 5 u Y smjeru i beskonacno udaljen od scene (directional). Vrijednosti sam podesavao dok nisam dobio ugodni ugodjaj scene. Dodao sam i shininess (sjaj).

Za animaciju sam definirao trenutak animacije, bool varijablu s kojom kontroliram ako je animacija u tijeku ili nije, brzina kugle, sto je ta vrijednost manja kugla je brza, ogranicio sam je da ne ide ispod 20.0 jer se onda animacija odvije pre brzo.
Tu sam jos i inicijalizirao string stanje koji mi sluzi za prikaz stanja na ekranu pomocu funkcije za crtanje znakova.
Putanja parabolicnog pada moze i ne mora biti iscrtana sto se konfigurira podesavanjem drwPath varijable.

Te na poslijetku, inicijalzirao sam array za izvlacenje donje plohe iz piramide pogleda (view frustum), kao i definirao makro kako bi lakse indeksirao 1d array kao 2d.

Kratak opis funkcija koje su koristene,
U mainu sam ispisao tipke koje se koriste za manipulaciju programom korisniku u konzolu, inicijalizirao GLUT library, prozor, call back display funkcije i keyboard funckije kao i reshape funckije te pozvao funkciju za inicijalizaciju scene.

U initScene sam inicijalizirao plohe kocke na nacin da svaka ploha ima 4 vrijednosti indeksa vertexa koji je opisuje, inicijalizirao sam i normale koje su potrebne za osvjetljenje, kao i vertexa za crtanje kocke. Posto se kocka crta iz sredista znam da je lijeva ploha kocke ima sigurno x koordinatu negativnu, dok desna ima pozitivnu, gornja ploha ima y koordinatu pozitivnu, a donja negativnu, isto tako prednja ploha ima z pozitivnu, a zadnja negativnu. Na taj nacin sam u petlji popunio array za vertexe. 
Normale sam izracunao na nacin da mi je desna normala(1,0,0) lijeva(-1,0,0) gornja(0,1,0) donja (0,-1,0), straznja normala(0,0,-1) i prednja normala(0,0,1). U initScene sam takoder omogucio i osvjetljenje.

Animaciju za ovaj zadatak sam izradio na nacin da mi se ovisno u trenutku animacije t, x koordinata sredista kugle povecava konstantnim korakom, a y koordinata se ne mijenja sve dok kugla ne izadje izvan kocke, a onda se y mjenja na nacin y=-x^2 (parabola).
I to traje sve dok se ne pauzira animacija ili kugla ne izadje ispod ekrana. Kugla se translatira i rotira, kut rotacija kugle sam izracunao formulom angle=x/C*360 (C=2*PI*R). Takoder se iscrtava putanja ukoliko je tako konfigurirano. Za putanju sam koristio dva vektora za koordinate X i Y, te crtao tocke na odgovarajucim koordinatama u petlji. Kako animacija napreduje tako rastu vektori za X i Y koordinate (push back - dodajem na kraj), kada se stisne tipka za restarta pocistim vektora i sve vrijednosti u njima (clear).

Za crtanje kocke koristim Vertex3fv funckciju koja prima kao argument pointer na prvog clana pojedinog vertexa. Za bojanje dvijema bojama (unutra plavo, vani crveno) sam iskoristio face culing. Front side su mi poredani vertexi CCW (counter clock-wise), a back side su mi clock-wise. Takoder desnu plohu kocke ne iscrtavam. I kocki i kugli sam stavio smooth shading kako bi sjencanje bilo ugladjeno. Probao sam i sa flat shading iako je potrebno manje vrijeme za renderiranje, rezultati mi se nisu svidjeli.

Funkcija za ispisivanje teksta drawString kao ulazne parametre prima x, y, z kordinate iscrtavanja te string, a ono sto radi jest iscrtavanje znak po znak (bitmap) odredenim fontom na zadanoj raster poziciji pixela (3D pozicija u woorld coord). 

Algoritam koji sam koristio za izvlacenje donje plohe iz frustuma (frustum extraction) je baziran na "clip space" pristupu. Objasnjenje algoritma:

Kugla je definirana tockom sredista p(x, y, z, w=1) u 3D world coord. Postoje dvije matrice u OpenGL-u Modelview koja je kombinacija Model i View matrice (model transformacija iz object space u world space,a view transformacija iz world space u eye space). Kako bi dobio koordinate tocke u clip space (koordinate tocke koje su odsjecene usporedivanjem sa +- w) trebam pomnoziti Modelview matricu sa Projection matricom koja definira volumen pogleda (frustum), projekciju vertexa na ekranu. Za mnozenje matrica sam iskoristio OpenGL funkciju glloadMatrixf kako bi postavio trenutnu matricu i onda sa glMultMatrixf pomnozio matricu sa trenutnom (Modelview*Projection), sa push i pop sam ocuvao vrijednost matrice. Tako sam dobio tocku u clip spaceu pc(xc, yc, zc, wc). Ta tocke je unutar piramide pogleda ako vrijedi njene tocke:

-wc < xc < wc
-wc < yc < wc
-wc < zc < wc

Kako mene zanima samo y koordinate (provjera ako je tocka izasla ispod donje plohe view frustuma) onda za izvlacenje donje plohe imam uvjet da tocka nije ispod donje plohe ako:

-wc < yc

Iz pomnozenih matrica M*P= { [clip11, clip21, clip31, cip41],
			     [clip12, clip22, clip32, cip42],
			     [clip13, clip23, clip33, cip43],
			     [clip14, clip24, clip34, cip44]) }

i tocke p (x, y, z, 1) dobijem tocku pc (xc, yc, zc, wc) gdje vrijedi da je:

yc = x*clip21 + x*clip22 + x*clip23 + clip24 
wc = x*clip41 + y*clip42 + z*clip43 + clip44

uvrstavanjem u gore izraz dobijemo

-(x*clip41 + y*clip42 + z*clip43 + clip44) < x*clip21 + x*clip22 + x*clip23 + clip24

odnosno nakon manipulacije

(x*clip21 + x*clip22 + x*clip23 + clip24) + (x*clip41 + y*clip42 + z*clip43 + clip44) > 0

x(clip11+clip41) + y(clip12+clip42) + z(clip13+clip43) + clip14 + clip44 > 0

Dobijemo da je donja ploha definirana (Ax+By+Cz+D=0) 

A=clip21+clip41
B=clip22+clip42
C=clip23+clip43
D=clip24+clip44

(A,B,C,D)=col2+col4

Jos sto je na kraju ostalo posto se racuna udaljenost od centra kugle do ravnine jest normalizirat dobiveno, to je napravljeno u normalise funckiji formulom sqrt(A^2 + B^2 + C^2) sa kojom se dijele sve vrijednosti izvucene plohe. Te u funkciji isSphereInside provjera ako se kugla nalazi na drugoj strani plohe i ako je njena apsolutna udaljenost od sredista do plohe veca od radiusa sto znaci da je kugla izasla kompletno izvan frustuma. Udaljenost tocke i plohe se racuna
distance = A * X + B * Y + C * Z + D.
Algoritam se moze prosiriti i na provjeru za ostale plohe ali u ovom slucaju nije bilo potrebno nego samo za donju plohu.

Za pokretanje programe nisu potrebni nikakvi argumenti dosta je pokrenuti program sa ./jugo.

Sources:
Pokazni primjeri na mudrom
https://mudri.uniri.hr/mod/resource/view.php?id=65584
http://www.glprogramming.com/red/

Indeksiranje vertexa za crtanje kocke
http://stackoverflow.com/questions/5032334/how-many-vertices-needed-in-to-draw-a-cube-in-opengl-es
Normale kocke
http://stackoverflow.com/questions/4959654/what-are-the-normals-for-3d-cube-as-used-in-opengl-es
Face culling (dvobojna kocka)
https://www.opengl.org/wiki/Face_Culling
http://www.learnopengl.com/#!Advanced-OpenGL/Face-culling

Opcenito za animaciju
http://www.lighthouse3d.com/tutorials/glut-tutorial/animation/
Vektor
http://www.cplusplus.com/reference/vector/vector/

Osvjetljenje (chapter u gore navedenom doc)
http://glprogramming.com/red/chapter05.html

Iscrtavanje znakova na ekranu
http://stackoverflow.com/questions/8847899/opengl-how-to-draw-text-using-only-opengl-methods

Frustum extraction
http://www.lighthouse3d.com/tutorials/view-frustum-culling/clip-space-approach-extracting-the-planes/
http://www.songho.ca/opengl/gl_transform.html
http://linux.die.net/man/3/glmultmatrixf
