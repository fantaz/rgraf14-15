/*
 *   Racunalna grafika projekt - Zadatak 2
 *   Napravio: Samir Jugo <sjugo@riteh.hr>
 */

#include <GL/glut.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

/* ******************** KONFIGURACIJA ************************ */

/******************
 * VELICINA KOCKE *
 ******************/
float cubeSize=2.0;   // [0.1,2.0]

/******************
 *   PROJEKCIJA   *
 ******************/
// field of view (stupnjevi)
GLfloat fov = 40.0;
// near clip
GLfloat nearZ = 1.0;
// far clip
GLfloat farZ = 100.0;

/******************
 *     POGLED     *
 ******************/
// polozaj ocista
GLfloat eyeX=0.0, eyeY=0.0, eyeZ=15.0;
// srediste, referentna tocka
GLfloat centerX=0.0, centerY=0.0, centerZ=0.0;
// smjer up vektora (u pozitivnom smjeru Y)
GLfloat upX=0.0, upY=1.0, upZ=0.0;


/******************
 *     TIPKE      *
 ******************/
// keyevi koji se koriste
unsigned char startKey   = 'g';
unsigned char stopKey    = 's';
unsigned char restartKey = 'r';
unsigned int  endKey     = 27;


/******************
 *     KOCKA      *
 ******************/
// redoslijed iscrtavanja ploha (0 -> 5)
#define leftFace   0
#define topFace    1
#define rightFace  2
#define bottomFace 3
#define backFace   4
#define frontFace  5

// array za plohe
GLint   faces  [6][4];
// array za normale
GLfloat normals[6][3];
// array za vertexe
GLfloat vertex [8][3];

/********************
 *   OSVJETLJENJE   *
 ********************/
GLfloat ambientLight[]  = { 0.3, 0.3, 0.3, 1.0 };
GLfloat diffuseLight[]  = { 1.0, 1.0, 1.0, 1.0 };
GLfloat specularLight[] = { 1.0, 1.0, 1.0 };
GLfloat whiteSpecMate[] = { 1.0, 1.0, 1.0 };
GLfloat lightPosition[] = { 0.0, 5.0, 0.0, 1.0 };
GLfloat shininess[]     = { 50 };

/********************
 *     ANIMACIJA    *
 ********************/
// trenutak animacije
static int t = -1;

// animacija (false - pause, true - start)
bool animating = false;

// brzina kugle (manja vrijednost -> brze, i obrnuto)
float speed=80.0;

// prikaz stanja na ekranu, na pocetku inicijalno
char stanje[] = "Inicijalno stanje";

/******************
 *     PUTANJA    *
 ******************/
// vektori za crtanje putanje
vector<float> PATHX;
vector<float> PATHY;

// crtanje putanje (1-crtaj, 0-ne crtaj)
#define drwPath 1

/*********************
 *  FRUSTUM EXTRACT  *
 *********************/
// array za izvucenu donju plohu frustuma
GLfloat frustum[4];
// lakse indeksiranje 1d arraya kao 2d array
#define clip(row,col)  clip[col*4+row-5]


/* KORISTENE FUNCKIJE */
void initScene();
void changeSize(int, int);
void display();
void animation(int );
void initVertexes(GLfloat vertex[][3]);
void initNormals(GLfloat normals[][3]);
void initFaces(GLint faces[][4]);
void drawPath(float , float );
void drawCube();
void drawSphere();
void keys(unsigned char, int, int);
void drawString(void *, char *, float, float, float);
void extractFrustum();
void multiplyMatrixes(float *, float *, float *);
void normalise(GLfloat *);
bool isSphereInside(float, float, float, float);
bool existsInArray(GLint faces[][4], int, int);

/* ************************************************************** */


/* inicijalizacija GLUT library, prozora, scene,
 * callback display funkcije, keyboard funckije */
int main(int argc, char **argv) {

    cout << "***********************\n";
    cout << "Zadatak 2 - Samir Jugo \n";
    cout << "Tipke:\n";
    cout << "START       " << startKey   << "\n";
    cout << "STOP        " << stopKey    << "\n";
    cout << "RESTART     " << restartKey << "\n";
    cout << "***********************\n";

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    //u gornjem lijevom kutu
    glutInitWindowPosition(0,0);
    glutInitWindowSize(640, 640);
    glutCreateWindow("Samir Jugo - Racunalna grafika");

    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(changeSize);
    glutKeyboardFunc(keys);

    //inicijaliza scene
    initScene();

    //kontinuirani poziv display funckije
    glutMainLoop();

    return 0;
}



/* inicijalizacija scene */
void initScene() {

    //ogranicavanje velicine kocke
    if(cubeSize < 0.1) cubeSize=0.1;
    else if(cubeSize > 2.0) cubeSize=2.0;

    //ogranicavanje brzine kugle
    if(speed<20.0) speed=20.0;

    initFaces(faces);
    initNormals(normals);
    initVertexes(vertex);

    /* Omogucavanje svijetla */
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_AMBIENT,  ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  diffuseLight);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /* Udaljenost objekta utjece na njegovo osvjetljenje */
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.01);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    /* Boja kao materijal */
    glEnable(GL_COLOR_MATERIAL);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  whiteSpecMate);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

    /* Eliminacija skrivenih povrsina */
    glEnable(GL_DEPTH_TEST);

    //glShadeModel(GL_FLAT);
    glShadeModel(GL_SMOOTH);

}

/* reshape prilikom promjene velicine prozora */
void changeSize(int w, int h) {

    // Prevencija podjele sa 0 kad je velicina prozora pre mala
    if (h == 0)
        h = 1;

    float ratio =  (float)w / h;

    /* Projekcija */
    glMatrixMode(GL_PROJECTION);

    // reset matrix
    glLoadIdentity();

    // viewport cijeli prozor
    glViewport(0, 0, w, h);
    gluPerspective(fov, ratio, nearZ, farZ);

    /* Pogled */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(eyeX, eyeY, eyeZ,
              centerX, centerY, centerZ,
              upX, upY, upZ);

    // kocka se nalazi u gornjem lijevom kutu
    glTranslatef(centerX-3.8, centerY+4.0, centerZ+0.0);
    glRotatef(30, eyeX+1, eyeY-0.1, 0.0);

    /* Izvlacenje donje plohe frustuma */
    extractFrustum();
}

/* iscrtavanje scene */
void display() {
    if(animating) t += 1;
    glClearColor(0.1,0.1,0.1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    animation(t);

    /* prikaz trenutnog stanja animacije na ekranu, donjem lijevom kutu */
    drawString(GLUT_BITMAP_HELVETICA_18, stanje, centerX-3.5, centerY-12.0, 0);

    glutSwapBuffers();
}

/* animacija kugle */
void animation(int t) {

    /* Parabolican pad, y ne raste dok kugla ne izade iz kocke
     * onda se mijenja parabolicnom funkcijom y=-x^2 */
    float x = t / speed;
    float y = (x < cubeSize/2 ? 0 : -(x - cubeSize/2) * (x - cubeSize/2));
    float angle = x / (2 * 3.14 * (cubeSize/2))  * 360;

    if(!isSphereInside(x, y, 0, cubeSize/2)) {
        animating=false;
        sprintf(stanje, "Animacija zavrsena");
    }
    drawCube();

    if(drwPath)
        drawPath(x, y);

    /* translacija i rotacija kugle */
    glPushMatrix();
    glTranslatef(x, y, 0);
    glRotatef(angle, 0, 0, -1);
    drawSphere();
    glPopMatrix();

}

/* inicijalizacija ploha za kocku */
void initFaces(GLint faces[][4]) {
    //left face (v0, v1, v2, v3)
    faces[leftFace][0]=0; faces[leftFace][1]=1;
    faces[leftFace][2]=2; faces[leftFace][3]=3;

    //top face (v2, v3, v6, v7)
    faces[topFace][0]=3; faces[topFace][1]=2;
    faces[topFace][2]=6; faces[topFace][3]=7;

    //right face (v4, v5, v6, v7)
    faces[rightFace][0]=7; faces[rightFace][1]=6;
    faces[rightFace][2]=5; faces[rightFace][3]=4;

    //bottom face  (v0, v1, v4, v5)
    faces[bottomFace][0]=4; faces[bottomFace][1]=5;
    faces[bottomFace][2]=1; faces[bottomFace][3]=0;

    //back face (v1, v2, v5, v6)
    faces[backFace][0]=5; faces[backFace][1]=6;
    faces[backFace][2]=2; faces[backFace][3]=1;

    //front face  (v0, v3, v4, v7)
    faces[frontFace][0]=7; faces[frontFace][1]=4;
    faces[frontFace][2]=0; faces[frontFace][3]=3;

}

/* provjera postoji li vrijednost u arrayu */
bool existsInArray(GLint faces[][4], int value, int face)  {
    for(int i = 0; i < 4; i++) {
        if(faces[face][i] == value) {
            return true;
        }
    }
    return false;
}

/* inicijalizacija vertexa za kocku */
void initVertexes(GLfloat vertex[][3]) {
    for(int i=0; i<8; i++) {
        if(existsInArray(faces, i, frontFace))
            vertex[i][2] = cubeSize/2;  // z=1
        if(existsInArray(faces, i, backFace))
            vertex[i][2] = -cubeSize/2; // z=-1
        if(existsInArray(faces, i, leftFace))
            vertex[i][0] = -cubeSize/2; // x=-1
        if(existsInArray(faces, i, rightFace))
            vertex[i][0] = cubeSize/2;  // x=1
        if(existsInArray(faces, i, topFace))
            vertex[i][1] =  cubeSize/2; // y=1
        if(existsInArray(faces, i, bottomFace))
            vertex[i][1] = -cubeSize/2; // y=-1

    }
}

/* inicijalizacija normala */
void initNormals(GLfloat normals[][3]) {
    //left normal
    normals[leftFace][0]=-1.0;
    normals[leftFace][1]=0.0;
    normals[leftFace][2]=0.0;

    //top normal
    normals[topFace][0]=0.0;
    normals[topFace][1]=1.0;
    normals[topFace][2]=0.0;

    //right normal
    normals[rightFace][0]=1.0;
    normals[rightFace][1]=0.0;
    normals[rightFace][2]=0.0;

    //bottom normal
    normals[bottomFace][0]=0.0;
    normals[bottomFace][1]=-1.0;
    normals[bottomFace][2]=0.0;

    //back normal
    normals[backFace][0]=0.0;
    normals[backFace][1]=0.0;
    normals[backFace][2]=1.0;

    //front normal
    normals[frontFace][0]=0.0;
    normals[frontFace][1]=0.0;
    normals[frontFace][2]=-1.0;

}


/* crtanje putanje */
void drawPath(float x, float y) {
    /* dodavanje novih vrijednosti */
    PATHX.push_back (x);
    PATHY.push_back (y);

    /* iscrtavanje putanje */
    glColor4f(1, 1, 1, 1);
    glBegin(GL_POINTS);
    for (unsigned int i = 0; i < PATHX.size(); i++) {
        glVertex3f(PATHX[i], PATHY[i], 0);

    }
    glEnd();
}


/* crtanje kocke */
void drawCube() {
    int i;

    //enable culling
    glEnable(GL_CULL_FACE);
    for (i = 0; i < 6; i++) {
        if (i == rightFace)
            continue;
        glBegin(GL_QUADS);

        //kocka je unutra plava
        glNormal3fv(&normals[i][0]);
        glCullFace(GL_BACK);
        glColor4f(0, 0, 1, 1);
        glVertex3fv(&vertex[faces[i][0]][0]);
        glVertex3fv(&vertex[faces[i][1]][0]);
        glVertex3fv(&vertex[faces[i][2]][0]);
        glVertex3fv(&vertex[faces[i][3]][0]);

        //kocka je izvana crvena
        glCullFace(GL_FRONT);
        glColor4f(1, 0, 0, 1);
        glVertex3fv(&vertex[faces[i][3]][0]);
        glVertex3fv(&vertex[faces[i][2]][0]);
        glVertex3fv(&vertex[faces[i][1]][0]);
        glVertex3fv(&vertex[faces[i][0]][0]);

        glEnd();
    }
}


/* crtanje kugle */
void drawSphere() {
    glColor4f(1, 1, 0, 1);
    glutSolidSphere(cubeSize/2, 20, 20);
}

/* tipke za upravljanje */
void keys(unsigned char key, int /*x*/, int /*y*/) {

    /* promjena statusa */
    if (tolower(key) == startKey) {
        animating = true;
        sprintf(stanje, "Animacija zapoceta");
    }
    else if (tolower(key) == stopKey) {
        animating = false;
        sprintf(stanje, "Animacija pauzirana");
    }
    else if (tolower(key) == restartKey) {
        t=0;
        /* brisanje vektora */
        PATHX.clear();
        PATHY.clear();
    }
    else if (key == endKey) {
        exit(0);
    }
}


/* iscrtavanje znakova */
void drawString (void * font, char *s, float x, float y, float z){
    unsigned int i;
    glColor4f(0.2, 1, 0, 1);
    glRasterPos3f(x, y, z);

    for (i = 0; i < strlen (s); i++) {
        glutBitmapCharacter (font, s[i]);
    }
    return;
}


/* funkcija koja izvlaci donju plohu iz frustuma */
void extractFrustum()
{
    GLfloat   proj[16];
    GLfloat   modlview[16];
    GLfloat   clip[16];

    /* Trenutna PROJECTION matrica */
    glGetFloatv( GL_PROJECTION_MATRIX, proj );

    /* Trenutna MODELVIEW matrica */
    glGetFloatv( GL_MODELVIEW_MATRIX, modlview );

    /* Pretvorba iz 3D world space u clip space */
    /* Clip extraction (mnozenje matrica) CLIP = MODELW*PROJ (4x4) matrix*/
    multiplyMatrixes(clip, modlview, proj);

    /* Ax+By+Cz+D=0
     * A=clip(2,1)+clip(4,1)
     * B=clip(2,2)+clip(4,2)
     * C=clip(2,3)+clip(4,3)
     * D=clip(2,4)+clip(4,4)
     *
     * (A,B,C,D)=col2+col4  */

    /* Izvlacenje donje plohe frustuma */
    frustum[0] = clip(2,1) + clip(4,1);
    frustum[1] = clip(2,2) + clip(4,2);
    frustum[2] = clip(2,3) + clip(4,3);
    frustum[3] = clip(2,4) + clip(4,4);


    /* Normalizacija rezultata */
    normalise(frustum);

}

/* funckija koja mnozi dvije matrice */
void multiplyMatrixes(float *res, float *a, float *b) {

    glPushMatrix();

    glLoadMatrixf(b);
    glMultMatrixf(a);
    glGetFloatv(GL_MODELVIEW_MATRIX, res);

    glPopMatrix();
}

/* normalizacija furstum plohe */
void normalise(GLfloat frustum[]) {
    GLfloat n=0.0;

    for(int i=0; i<3; i++)
        n += pow(frustum[i], 2);

    n=sqrt(n);

    for(int i=0; i<4; i++)
        frustum[i] /= n;
}

/* funkcija koja provjerava ako je kugla izasla ispod ekrana */
bool isSphereInside(float x, float y, float z, float radius)
{
    /* D = A*x+B*y+C*z+D */
    float distance=frustum[0]*x + frustum[1]*y + frustum[2]*z + frustum[3];

    if(distance <= -radius )
        return false;
    return true;
}
