#include <GL/gl.h>

#include <GL/glu.h>
#include <GL/glut.h>

#include <math.h>
#include <iostream>

#define PI 3.14159265

float           angle           =   0.0f;
static float    to_angle        = -90.0f;
static float    size            =   2.0f;
static float    animation_speed =  0.05f;

bool paused = true;


void keypress(unsigned char key, int, int) {
    switch (key) {
        case 's':
            paused = false;
            break;
        case 'e':
            paused = true;
            break;
        case 27: //Esc
            exit(0);
        default:
            break;
    }
}


void windowSize(int w, int h) {
    float ratio =  w * 1.0 / h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, w, h);
    gluPerspective(45,ratio,1,size*10.0f);
    glMatrixMode(GL_MODELVIEW);
}


void drawAxis(void){
    glLineWidth(2.0f);
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, size*5.0f);
        glVertex3f(0.0f, 0.0f, -size*5.0f);
    glEnd();
}


void drawWater(void){
    glPushMatrix();
        glRotatef(angle, 0.0f, 0.0f, 1.0f);
        glTranslatef(-size/2, size/2, 0.0f);

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);

        glColor4f(0.0f, 0.0f, 1.0f, 0.3f);
        glutSolidCube(size);

        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
    glPopMatrix();
}


void drawCube(void){
    glPushMatrix();
        glRotatef(angle, 0.0f, 0.0f, 1.0f);
        glTranslatef(-size/2, size/2, 0.0f);
        glColor3f(0.0f, 1.0f, 0.0f);
        glLineWidth(4.0f);
        glutWireCube(size);
    glPopMatrix();
}


void updateAngle(void){
    if(angle<=to_angle)
        angle=to_angle;
    else if(!paused)
        angle-=animation_speed;
}


void capWater(float water_level){
    glPushMatrix();
        glBegin(GL_QUADS);
            glVertex3f( size, water_level, (size/2) + 0.1f );
            glVertex3f(-size, water_level, (size/2) + 0.1f );
            glVertex3f(-size, water_level, (-size/2) - 0.1f );
            glVertex3f( size, water_level, (-size/2) - 0.1f );
        glEnd();
    glPopMatrix();
}


void drawActors(void){
    float water_level = cos( (angle) *PI/180) * size;
    GLdouble equation[4] = {0.0f, -1.0f, 0.0f, water_level-0.001f};

   glEnable(GL_STENCIL_TEST);
        glStencilFunc(GL_ALWAYS, 1, 1);
        glStencilOp(GL_KEEP, GL_INVERT, GL_INVERT);

        glEnable(GL_CLIP_PLANE0);
        glClipPlane(GL_CLIP_PLANE0, equation);
            drawWater();
        glDisable(GL_CLIP_PLANE0);

        glStencilFunc(GL_EQUAL, 1, 1);
        glStencilOp(GL_KEEP, GL_ZERO, GL_ZERO);

        capWater(water_level);

    glDisable(GL_STENCIL_TEST);

    drawCube();

    updateAngle();
}


void renderScene(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor( 1, 1, 1, 1);
    glEnable(GL_BLEND);
    glEnable(GL_LIGHTING);

    glLoadIdentity();

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    gluLookAt(	size*4.0f, size*2.0f, size*2.0f,
                size, size/2, size/2,
                0.0f, 1.0f,  0.0f);

    drawAxis();
    drawActors();

    glutSwapBuffers();
}


int main(int argc, char **argv) {
    float specular[]  = {1.0f, 1.0f, 1.0f, 1.0f};
    float shininess[] = {60.0f};
    float position[]  = {size*2, 0.0f, -size*2, 0.0f};

    glutInit(&argc, argv);                                          //glut inicijalizacija
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_STENCIL);      // GLUT_DOUBLE - double buffer, za tecnu animaciju, GLUT_RGBA - kreiraj rgba prozor
    glutInitWindowPosition(100,100);                                //pozicija prozora na ekranu
    glutInitWindowSize(700,500);                                    //velicina prozora
    glutCreateWindow("Projekt - Aron Rescec");                      //kreiranje prozora sa parametrom 'title'

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glLightfv(GL_LIGHT0, GL_POSITION, position); //Svjetlo

    glutDisplayFunc(renderScene);
    glutReshapeFunc(windowSize);
    glutIdleFunc(renderScene);
    glutKeyboardFunc(keypress);

    glutMainLoop();

    return 1;
}
