Projekt iz računalne grafike, grupa 4
Autor: Aron Rečec


---------------------
Pokretanje: 
---------------------
	Za pokretanje nisu potrebni argumenti.


---------------------
Koritenje (tipke):
---------------------
	Esc 	- izlaz iz programa
	e	- pauziranje animacije	
	s	- pokretanje animacije


---------------------
Napomena:
---------------------
	Za podeavanje irine linija se koristi funkcija glLineWidth() koja ne radi sa Ubuntu 14.04 na VMware Player-u ukoliko je upaljena opcija "Hardware graphics acceleration".


---------------------
Opis programa: 
---------------------
	Za prikaz 'vode' unutar kocke sam korisito dvije kocke, jedna unutar druge. Prva kocka koja glumi spremnik (wireframe) se 
	rotira oko jednog svog brida zajedno sa drugom kockom koja imitira vodu (solid, prozirnost). Dojam pranjenja kocke-spremnika 
	se dobiva tako da se pomoću clipping ravnine koja predstavlja razinu vode odsjeca kocka-voda ovisno o rotaciji spremnika. 
	Tu se pojavljuje problem da glutSolidCube nije ispunjen, već se sastoji od stranica i praznine u sredini. Clipping
	tih stranica je prouzočio da se vidi prazna unutranjost kocke. Taj problem je rijeen sa koritenjem stencil buffer-a. 

	Stencil buffer je dodatni uz color i depth buffer, a koristi se za ograničavanje područja rendering-a. Stencil buffer 
	u slučaju ovog programa doputa da se ravnina koja poklapa rupu u kocki-vodi koja nastaje clippingom, a koja je veća 
	od kocke kako bi pokrila cijelo područje koje se mijenja uslijed rotacije, iscrtava samo na potrebnom području. 


---------------------
Reference:
---------------------

Stencil buffer: https://www.opengl.org/documentation/specs/version1.1/glspec1.1/node97.html
http://en.wikibooks.org/wiki/OpenGL_Programming/Stencil_buffer

Glut tutorial:
http://www.lighthouse3d.com/tutorials/glut-tutorial/?2

Osvjetljenje i materijali:
http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=11
https://www.cse.msu.edu/~cse872/tutorial3.html

