Programski zadatak 2

Za crtanje vanjske kocke sam koristio matricu sa kojom sam generirao sve strane. Za unutranju kocku sam takoder generirao novu matricu, sa umanjenim parametrima, jer sa pokusajem skaliranja postojece kocke nisam uspio dobiti zeljeni rezultat.

U display funkciji sam postavio boju scene u svijetlo plavu, pozicioniro scenu, postavio vanjsku i unutarnju boju kocke te sfere, te napravio animaciju.

U keyboard funkciji postavio tipku Escape za izlazak iz programa.

U init funkciji ukljucio osvjetljenja i postavio ih na zeljene pozicije.

U main-u postavio prozor u gornji lijevi kut i njegovu velicinu od 800x640 i nazvao ga PROJEKT-ZADATAK2, te pozvao sve potrebne funkcije.

Nedostatak programa je sto nema tipki za pokretanje i zaustavljanje animacije vec se animacija sama pokrece te je izlaz samo moguc prilikom pritiska tipke ESC.