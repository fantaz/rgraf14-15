#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
 
float cube[8][3] = {
	    {0, 0, 0},
        {0, 0, 1},
        {0, 1, 0},
        {0, 1, 1},
        {1, 0, 0},
        {1, 0, 1},
        {1, 1, 0},
    	{1, 1, 1}
};

float cube2[8][3] = {
        {0.1, 0.01, 0.1},
        {0.1, 0.01, 0.99},
        {0.1, 0.99, 0.1},
        {0.1, 0.99, 0.99},
        {0.99, 0.01, 0.1},
        {0.99, 0.01, 0.99},
        {0.99, 0.99, 0.1},
        {0.99, 0.99, 0.99}
};
 
void drawCube(){
	    glBegin(GL_QUADS);

	    glVertex3fv(cube[0]);
    	glVertex3fv(cube[1]);
    	glVertex3fv(cube[3]);
    	glVertex3fv(cube[2]);

        glVertex3fv(cube[0]);
        glVertex3fv(cube[1]);
	    glVertex3fv(cube[5]);
        glVertex3fv(cube[4]);

        glVertex3fv(cube[2]);
        glVertex3fv(cube[3]);
        glVertex3fv(cube[7]);
        glVertex3fv(cube[6]);
 
        glVertex3fv(cube[0]);
        glVertex3fv(cube[2]);
        glVertex3fv(cube[6]);
        glVertex3fv(cube[4]);
 
    	glVertex3fv(cube[1]);
    	glVertex3fv(cube[3]);
    	glVertex3fv(cube[7]);
    	glVertex3fv(cube[5]);
 
        glEnd();
}
 
void drawCube2(){
	    glBegin(GL_QUADS);
        
	    glVertex3fv(cube2[0]);
        glVertex3fv(cube2[1]);
        glVertex3fv(cube2[3]);
        glVertex3fv(cube2[2]);

        glVertex3fv(cube2[0]);
        glVertex3fv(cube2[1]);
        glVertex3fv(cube2[5]);
        glVertex3fv(cube2[4]);

        glVertex3fv(cube2[2]);
        glVertex3fv(cube2[3]);
        glVertex3fv(cube2[7]);
        glVertex3fv(cube2[6]);

        glVertex3fv(cube2[0]);
        glVertex3fv(cube2[2]);
        glVertex3fv(cube2[6]);
        glVertex3fv(cube2[4]);

        glVertex3fv(cube2[1]);
        glVertex3fv(cube2[3]);
        glVertex3fv(cube2[7]);
        glVertex3fv(cube2[5]);

        glEnd();
}

float angle = 0.0;
float sphere_position[3] = {0, 0, 0};

void display(){

		glClearColor(0.7f, 0.9f, 1.0f, 1.0f);
   		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
 
		glLoadIdentity(); 

    	glTranslatef(0, 0, -10);
    	glTranslatef(-3, 0, 0);
    	glTranslatef(0, 1, 0); 

    	glPushMatrix();
    	
		glColor4f(1.0, 0.0, 0.0, 1.0);
    	drawCube();
		
		glColor4f(0.0, 0.0, 1.0, 1.0);
		drawCube2();

    	glPopMatrix();
 
        glPushMatrix();
    	glColor4f(1.0, 1.0, 0.0, 1.0);
 
    	glTranslatef(   0.5 + sphere_position[0],
                        0.5 + sphere_position[1],
                        0.5 + sphere_position[2]
                    );
		glutSolidSphere(0.5, 20, 20);
     	glPopMatrix(); 

     	angle += 0.01;
     	sphere_position[0] += 0.005;

     	if(sphere_position[0] >= 0.65){
        	sphere_position[1] -= 0.0045;
			if(sphere_position[0] >= 0.85){
				sphere_position[1] -= 0.019;
			} 
		}

		glutSwapBuffers();
		glutPostRedisplay();

}
 
void keyboard(unsigned char key, int, int){
    
	switch (key)
        {
    	    case 27:
        	exit(0); 
        }
 
}

void resize(int w, int h){

	if (h == 0) { h = 1; }

    	float ratio = 1.0* w / h;
		glMatrixMode(GL_PROJECTION);
    	glLoadIdentity();
    	gluPerspective(45,ratio,1,1000);
    	glMatrixMode(GL_MODELVIEW);
    	glLoadIdentity();
}
 
void init(){

    	glEnable(GL_DEPTH_TEST);
    	glEnable(GL_LIGHTING); 
		glEnable(GL_LIGHT0);
    	glEnable(GL_COLOR_MATERIAL); 

    	float pos[] = {-3.0,2.0,-3.0, 1 };
    	glLightfv(GL_LIGHT0, GL_POSITION, pos);

    	float dif[] = {1.0, 1.0, 1.0};
    	glLightfv(GL_LIGHT0, GL_DIFFUSE, dif);

    	float amb[] = {0.2, 0.2, 0.2, 1.0};
    	glLightfv(GL_LIGHT0, GL_AMBIENT, amb);
}
 
int main(int argc, char** argv){

    	glutInit(&argc, argv);
    	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
		glutInitWindowPosition(0, 0);
    	glutInitWindowSize(800, 640);
    	glutCreateWindow("PROJEKT-ZADATAK2");
 
    	init();
    	
    	glutDisplayFunc(display);
    	glutKeyboardFunc(keyboard);
    	glutReshapeFunc(resize);
 
    	glutMainLoop();

    	return 0;
}
