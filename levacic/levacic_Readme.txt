Opis

Metoda rje�enja:
smanjivati sivu, odnosno pove�avati �utu kocku (u smjeru jedne osi)
a promjena koordinata (izra�un presjeka) je vo�ena trenutnom pozicijom
kruga (koordinata kruga u toj istoj osi)

Reference:
mudri.uniri resursi
https://www.opengl.org/sdk/docs/man/
http://www.videotutorialsrock.com/index.php
	

Upute za kori�tenje
-Prilikom pokretanja programa objekti su stacionarni i u po�etnoj poziciji
-pritiskom na tipku "s", program se pokre�e (animacija)
-pritiskom na tipku "x", program se pauzira
-daljnjim pritiskom na tipku "s", program nastavlja sa izvedbom
-kada se animacija izvrsi, pritiskom na tipku "x", objekti se postavljaju u po�etnu poziciju
-pritiskom na tipku "ESC",  izvedba programa se obustavlja

Tipke:
S - Start / Resume
X - Pause / Reset(after animation ends)
ESC - Exit program

dodatnih argumenata za izvedbu nema