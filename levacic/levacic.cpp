//Leon Levacic - zadatak broj 3

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>

using namespace std;
float circlePos= 4.0f; //pocetna pozicija kruga
float matCross = 1.0f; //sjeciste kocaka
float cubeYellow = -1.0f; // -1 za sivo iscrtavanje +1 za zuto
float cubeGray = -1.0f; // -1 za sivo iscrtavanje +1 za zuto
int seg=100; //broj segmenata kruga
int rad=2; //radijus kruga
int animation=0; //kontrola animacije

void initRendering() {
   glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // boja i prozirnost pozadine
   glClearDepth(1.0f);                   // dubina pozadine

   glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
   glEnable(GL_LIGHTING); //omoguceno osvjetljenje
   glEnable(GL_LIGHT0); //svjetlo
   glEnable(GL_NORMALIZE); //normale poligona
   glEnable(GL_COLOR_MATERIAL); //omoguceno iscrtavanje boja
   glDepthFunc(GL_LEQUAL);    // test dubljine
   glShadeModel(GL_SMOOTH);   // smooth shade model
   glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // korekcija perspektivnog pogleda

   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //omogucen blend i alfa kljuc za transparentnost
}

void handleResize(int w, int h) {//projekcija scene
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
}

void drawCubeG() {//iscrtavanje sive kocke
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBegin(GL_QUADS);
	
    // Top face
    glColor3f(0.3f, 0.3f, 0.3f);     
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f( matCross, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f,  1.0f);
    glVertex3f( matCross, 1.0f,  1.0f);
    // Bottom face
    glColor3f(0.3f, 0.3f, 0.3f);     
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f( matCross, -1.0f,  1.0f);
    glVertex3f(-1.0f, -1.0f,  1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f( matCross, -1.0f, -1.0f);
	// Left face
    glColor3f(0.3f, 0.3f, 0.3f);     
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f,  1.0f,  1.0f);
    glVertex3f(-1.0f,  1.0f, cubeGray);
    glVertex3f(-1.0f, -1.0f, cubeGray);
    glVertex3f(-1.0f, -1.0f,  1.0f);
    // Right face
    glColor3f(0.3f, 0.3f, 0.3f);     
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f,  1.0f, cubeYellow);
    glVertex3f(1.0f,  1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f, cubeYellow);
    // Front face
    glColor3f(0.3f, 0.3f, 0.3f);     
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( matCross,  1.0f, 1.0f);
    glVertex3f(-1.0f,  1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f( matCross, -1.0f, 1.0f);
    // Back face
    glColor3f(0.3f, 0.3f, 0.3f);    
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f,  1.0f, -1.0f);
    glVertex3f( 1.0f,  1.0f, -1.0f);

	glEnd();	
}

void drawCubeY() { //iscrtavanje zute kocke
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBegin(GL_QUADS);
	
	// Top face
    glColor3f(1.0f, 1.0f, 0.0f);
	glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f( 1.0f, 1.0f, -1.0f);
    glVertex3f(matCross, 1.0f, -1.0f);
    glVertex3f(matCross, 1.0f,  1.0f);
    glVertex3f( 1.0f, 1.0f,  1.0f);
    // Bottom face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f( 1.0f, -1.0f,  1.0f);
    glVertex3f(matCross, -1.0f,  1.0f);
    glVertex3f(matCross, -1.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);
    // Left face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f,  1.0f,  1.0f);
    glVertex3f(-1.0f,  1.0f, -cubeGray);
    glVertex3f(-1.0f, -1.0f, -cubeGray);
    glVertex3f(-1.0f, -1.0f,  1.0f);
    // Right face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f,  1.0f, -cubeYellow);
    glVertex3f(1.0f,  1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f,  1.0f);
    glVertex3f(1.0f, -1.0f, -cubeYellow);
    // Front face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( 1.0f,  1.0f, 1.0f);
    glVertex3f(matCross,  1.0f, 1.0f);
    glVertex3f(matCross, -1.0f, 1.0f);
    glVertex3f( 1.0f, -1.0f, 1.0f);
    // Back face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( 1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f,  1.0f, -1.0f);
    glVertex3f( 1.0f,  1.0f, -1.0f);

	glEnd();	
}

void drawCircle() { //iscrtavanje krugova
    glColor4f(1.0f, 1.0f, 1.0f, 0.6f);     // Transparentnost je na 60%
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin(GL_POLYGON);
        for(int i = 0; i < seg; i++)
        {
            float angle = 2.0f * 3.14159265359f * float(i) / float(seg); 
            float x = rad * cosf(angle);
            float y = rad * sinf(angle);
			glVertex3f(circlePos, y, x);
        }
    glEnd();

	glLineWidth(2.0f);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);     // Transparentnost je na 0%
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_POLYGON);
        for(int i = 0; i < seg; i++)
        {
            float angle = 2.0f * 3.14159265359f * float(i) / float(seg);
            float x = rad * cosf(angle);
            float y = rad * sinf(angle); 
            glVertex3f(circlePos, y, x); 
        }
    glEnd();
}

void matSwap() {//odredjuje "mjenjanje" boja, tj koordinate vertexa kocaka s obzirom na poziciju kruga
	if ((circlePos < 1.0f) && (circlePos >= -1.0f)) {
		matCross = circlePos;
        cubeYellow = 1.0f;
    } else if (circlePos < -1.0) 
        cubeGray = 1.0f;
}

void moveCircle(int) {//animacija pomaka kruga
	if (animation && circlePos > -4) {
		if ((circlePos < 1.0f) && (circlePos >= -1.0f)) //usporavanje kruga kod "pretvorbe" materijala
			circlePos -= 0.04; else circlePos -= 0.1; //cisto estetski
		matSwap();
		glutPostRedisplay();
		glutTimerFunc(25, moveCircle, 0);
	}
}

void handleKeypress(unsigned char key, int , int ) {
	switch (key) {
		case 's': //start/resume
			animation = 1; 
			moveCircle(0); 
			break;
		case 'x': //pause/reset
			if (circlePos <= -4) {	
				circlePos = 4.0;
				cubeYellow = -1.0f;
				cubeGray = -1.0f; 
				matCross= 1.0;
				glutPostRedisplay();
			}
			animation = 0;
			break;
		case 27: //"Escape" za terminaciju programa
			exit(0);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawScene() {//iscrtavanje scene
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(0.0f, 0.0f, -15.0f);
	
	GLfloat ambientLight[] = {0.3f, 0.3f, 0.3f, 1.0f}; //ambijentalno svijetlo
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight); 
	
	GLfloat lightColor[] = {0.7f, 0.7f, 0.7f, 1.0f}; //postavke boje i pozicije svijetla
	GLfloat lightPos[] = {-8.0f, 4.0f, 16.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	
	glRotatef(-45.0f, -45.0f, 45.0f, 0.0f); //rotacija scene (rotacijskom matricom)
	
	////////////////////////////////////
	drawCubeG();
	drawCubeY();
	drawCircle();
	glutSwapBuffers();
}

int main(int argc, char** argv) { //main
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Levacic Leon - zadatak 3.");
	initRendering();
	
	glutDisplayFunc(drawScene);
	glutKeyboardFunc(handleKeypress);
	glutReshapeFunc(handleResize);
	
	glutMainLoop();
	return 0;
}