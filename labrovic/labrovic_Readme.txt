Program se sastoji od Sive kocke, manje Zlatne kocke, kružnice portala te njegove prozirne unutrasnjosti.

U pocetku u sceni vidimo samo Sivu kocku te se Zlatna (jer je manja) nalazi unutar nje. Pokretanjem animacije
portal se pomice te tako "gura" sivu kocku koja na taj nacin otkriva onu zlatnu.
Dolazak portala na kraj kocke oznacava i kraj animacije.
Isto tako portal možemo pokretati i zaustavljati po volji.

Otkrivanje zlatne kocke postigao sam mjenjanjem koordinata verteksa sive kocke sto je vidljivo u programu.
Trudio sam se dobro komentirati program kako bi pokrio sve njegove dijelove.

Upravljanje programom:
S - Pokreni animaciju
X - Zaustavi animaciju
Esc - Izlazak

Koristio sam se s nekoliko referenci na internetu:
Kod za kocku (koja je u pocetku bila kutija bez gornje i donje strane) i template nasao sam u ovom tutorialu (+osvjetljenje): 
http://www.videotutorialsrock.com/opengl_tutorial/lighting/video.php
Ideju za puni krug unutar portala i kod uzeo sam sa ove stranice koju sam nasao preko Google-a: 
https://gist.github.com/strife25/803118
Prozirnost portala postigao sam gledajuci ovaj tutorial: 
https://www.youtube.com/watch?v=chbjxTmbQes
Kruznicu portala napravio sam pomocu ove funkcije: 
https://www.opengl.org/documentation/specs/glut/spec3/node84.html
Boja (RGB) zlata: 
https://www.opengl.org/discussion_boards/showthread.php/132502-Color-tables
Animacija pomocu timera: 
http://xoax.net/cpp/crs/opengl/lessons/Lesson7/
