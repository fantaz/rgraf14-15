#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

GLfloat gfPosX = 2.0; //Pocetna pozicija portala
GLfloat gfDeltaX = .01; //Pomak portala
GLfloat pomakX = 0; //Varijabla za micanje sive kocke (Kako bi otkrili zlatnu)
bool start=false;


//Funkcija za pracenje unosa na tipkovnici
void handleKeypress(unsigned char key, int /*x*/, int /*y*/) {
	switch (key) {
		case 27: //Escape tipka
			exit(0);
	
		case 's':case 'S':
			start=true; //Kreni
        break;
		 case 'x':case 'X':
			start=false; //Stani
        break;
        }
}

void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING); //Ukljuci osvjetljenje
	glEnable(GL_LIGHT0); //Koristimo LIGHT0 za usmjerenu svijetlost
	glEnable(GL_NORMALIZE); //Za normale
	glEnable( GL_BLEND ); //Omogucuje prozirnost
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//Resizanje prozora
void handleResize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (double)w / (double)h, 1.0, 200.0);
}

	
//Funkcija za crtanje 3D scene
void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(0.0f, 0.0f, -5.0f); //Namjestanje scene da imamo bolji pogled
	
	
	//Pozicionirano svijetlo usmjereno na prednju stranu kocke
	GLfloat lightColor0[] = {0.8f, 0.8f, 0.8f, 1.0f};
	GLfloat lightPos0[] = {-4.0f, -1.0f, 8.0f, 1.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
	
	//ZLATNA KOCKA
	glTranslatef(0.0, 0.0, -1.0);
	glRotatef(25, 1.0, 0.0, 0.0);
	glRotatef(-25, 0.0, 1.0, 0.0);
	glColor3f(0.8, 0.498039, 0.196078); //RGB za zlatnu boju
	
	glBegin(GL_QUADS);
	
	//Prednja stranica
	glNormal3f(0.01f, 0.01f, 0.99f);
	glVertex3f(-0.99f, -0.99f, 0.99f);
	glVertex3f(0.99f, -0.99f, 0.99f);
	glVertex3f(0.99f, 0.99f, 0.99f);
	glVertex3f(-0.99f, 0.99f, 0.99f);
	
	//Desna stranica
	glNormal3f(0.99f, 0.01f, 0.01f);
	glVertex3f(0.99f, -0.99f, -0.99f);
	glVertex3f(0.99f, 0.99f, -0.99f);
	glVertex3f(0.99f, 0.99f, 0.99f);
	glVertex3f(0.99f, -0.99f, 0.99f);
	
	//Straznja stranica
	glNormal3f(0.01f, 0.01f, -0.99f);
	glVertex3f(-0.99f, -0.99f, -0.99f);
	glVertex3f(-0.99f, 0.99f, -0.99f);
	glVertex3f(0.99f, 0.99f, -0.99f);
	glVertex3f(0.99f, -0.99f, -0.99f);
	
	//Lijeva stranica
	glNormal3f(-0.99f, 0.01f, 0.01f);
	glVertex3f(-0.99f, -0.99f, -0.99f);
	glVertex3f(-0.99f, -0.99f, 0.99f);
	glVertex3f(-0.99f, 0.99f, 0.99f);
	glVertex3f(-0.99f, 0.99f, -0.99f);
	
	
	//Gornja stranica
	glNormal3f(0.01f, 0.99f, 0.01f);
	glVertex3f(0.99f, 0.99f, 0.99f);
	glVertex3f(-0.99f, 0.99f, 0.99f);
	glVertex3f(-0.99f, 0.99f, -0.99f);
	glVertex3f(0.99f, 0.99f, -0.99f);
	
	
	//Donja stranica
	glNormal3f(0.01f, -0.99f, 0.01f);
	glVertex3f(0.99f, -0.99f, 0.99f);
	glVertex3f(-0.99f, -0.99f, 0.99f);
	glVertex3f(-0.99f, -0.99f, -0.99f);
	glVertex3f(0.99f, -0.99f, -0.99f);
	
	glEnd();
	
	// SIVA KOCKA
	
	glPushMatrix(); //Potrebno za ocuvanje nekih vrijednosti (zajedno sa Pop)
	
	glColor3f(0.30, 0.30, 0.30); //RGB za tamno sivu boju
	
	glBegin(GL_QUADS);
	
	//Prednja stranica
	glNormal3f(0.0f + pomakX, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f + pomakX, -1.0f, 1.0f);
	glVertex3f(1.0f + pomakX, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	
	//Desna stranica
	glNormal3f(1.0f + pomakX, 0.0f, 0.0f);
	glVertex3f(1.0f + pomakX, -1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, 1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, 1.0f, 1.0f);
	glVertex3f(1.0f + pomakX, -1.0f, 1.0f);
	
	//Straznja stranica
	glNormal3f(0.0 + pomakX, 0.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, 1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, -1.0f, -1.0f);
	
	//Lijeva stranica
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	
	
	//Gornja stranica
	glNormal3f(0.0f + pomakX, 1.0f, 0.0f);
	glVertex3f(1.0f + pomakX, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, 1.0f, -1.0f);
	
	
	//Donja stranica
	glNormal3f(0.0f + pomakX, -1.0f, 0.0f);
	glVertex3f(1.0f + pomakX, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f + pomakX, -1.0f, -1.0f);
	
	glEnd();
	glPopMatrix();
	
	//Namjestanje i pomicanje kruznice zajedno sa unutarnjim prozirnim dijelom
	glTranslatef(gfPosX, 0.0, 0.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	glColor3f(1, 1, 1);
	glutSolidTorus(0.04, 2, 10, 20); //Kruznica
	
	glColor4f(1, 1, 1, 0.5); //Bijeli, unutarnji, prozirni dio kruznice
	int i;
	//float x=0,y=0;
	//float PI=3.14;
	int triangleAmount = 20;
	
	GLfloat radius = 2.0f;
	GLfloat twicePi = 2.0f * 3.14;
	
	glBegin(GL_TRIANGLE_FAN); //Unutarnji djelomicno prozirni dio portala
		glVertex2f(0, 0);
		for(i = 0; i <= triangleAmount;i++) { 
			glVertex2f(
		        0 + (radius * cos(i *  twicePi / triangleAmount)), 
			    0 + (radius * sin(i * twicePi / triangleAmount))
			);
		}
	glEnd();
	
	
	if (start){
	gfPosX -= gfDeltaX; //Pomak (Ako je ukljucen preko tipkovnice)
	}
    if (gfPosX < -1) {
        gfDeltaX = 0; //Pomaka vise nema kada portal dode do kraja kocke
    }
    
    if (gfPosX <= 1) {
		if (start){
        pomakX -= gfDeltaX; //Otkrivanje zlatne kocke kada kruznica dode do njenog pocetka
		}
    }
   
    
	glutSwapBuffers();
}


void Timer(int /*iUnused*/) //Timer funkcija
{
    glutPostRedisplay();
    glutTimerFunc(30, Timer, 0);
}

int main(int argc, char** argv) {
	//Inicijalizacija GLUT-a
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	glutInitWindowSize(400, 400);
	
	//Ime prozora
	glutCreateWindow("Projekt iz Racunalne Grafike");
	initRendering();
	
	//Crtanje objekata
	glutDisplayFunc(drawScene);

	glutKeyboardFunc(handleKeypress);
	
	//Resizeanje windowa (U slucaju)
	glutReshapeFunc(handleResize);
	
	//Poziv Timera
	Timer(0);

	//Glavna petlja
	glutMainLoop();
	return 0;
}
