Računalna Grafika - Zadatak 2

Vanja Jansky

Pokretanje:
    Za pokretanje programa nisu potrebni nikakvi argumenti.
    
Opis:
    Na početku programa se inicijalizira GLUT library, a nakon toga se kreira prozor čije su
    dimenzije 800 * 800. U funkciji initialize se omogucuje svjetlo, test dubine da se
    ne iscrtavaju skrivene površine, odnosno iza, te face culling s kojim se određuje hoće
    li se vidjeti prednja ili zadnja strana, u ovom slučaju quad-a, ovisno o poretku točaka.
    U funkciji setupMaterials se postavljaju vrijednosti za ambijentalnu i spekularnu
    refleksiju materijala, poziciju svjetla te modela osvjetljenja. Sve vrijednosti su
    empirijske. 
    
    Računanje vektora normala se vrši u funkciji calulateNormals. Za izračun se koriste tri
    točke svake stranice iz kojih se izračunaju dva vektora te se potom napravi vektorski
    produkt i normalizacija da bi vektor bio jedinični.
    
    I zadnje što treba napraviti u main funkciji je definirati handler funkcije za iscrtavanje
    scene, pritisak tipke na tipkovnici i podešavanje viewport-a, te pozvati timer funkciju
    koja će svakih 15ms ponovno iscrtati scenu.
    
    U funkciji handleKeys ispitujemo je li pritisnuta tipka s za pokretanje animacije, g za
    zaustavljenje animacije ili escape za izlazak iz programa.
    
    U display funkciji se scena translatira kako bi kocka bila u gornjem lijevom kutu, rotira oko
    y-osi te se izračunava donja ravnina piramide pogleda. Nakon toga se iscrtavaju kocka i kugla
    te se, ukoliko nissmo stisnuli tipku s, poziva funkcija za animaciju. U njoj se povećava
    kut za koji se kugla rotira oko z-osi, povećava se x koordinata kugle i kada je njena
    vrijednost veća od polumjera kugle(rub kocke) y se počinje smanjivati po kvadratnoj funkciji.
    Parametar a se može mijenjati, te će ovisno o njemu kugla padati bliže ili dalje i brže ili
    sporije.
    
    Funkcija drawSphere crta kuglu na koordinatama postavljenim u strukturi sph na koje je dodan
    polumjer kugle. Crtanje kocke je riješeno s funkcijom drawCube u kojoj se ovisno o varijabli
    isInner poziva funkcija drawQuad koja crta pojedine stranice kocke. Ako je parametar innerCube
    u funkciji drawQuad true, prije crtanja stranice se smjer vektora normale stranice mora obrnuti
    jer su vektori normala izračunati za stranice crvene kocke.
    
    Za provjeru kada je kugla izašla izvan piramide pogleda se koriste funkcije getBottomPlane
    u kojoj se modelview matrica množi s projection matricom kako bi se dobilo clip prostor.
    Naposlijetku se iz clip prostora izvlači donja ploha, jer treba samo provjeriti ako je kugla
    ispod te plohe, te normalizirati dobivene vrijednosti. U funkciji isSphereInFrustum se zatim
    provjerava vidi li se kugla ili je otišla ispod donje ravnine. Ako je, animacija se zaustavlja.
    
    Vrijednost za brzinu rotacije i translacije kugle je empirijska te se može podešavati promjenom
    vrijednosti varijable speed.

Kontrole:
    s 	   - Zaustavi animaciju
    g 	   - Pokreni animaciju
    escape - Izlaz iz programa
    
Izvori:
    http://www.racer.nl/reference/vfc_markmorley.htm
    http://stackoverflow.com/questions/6799405/using-glloadmatrixf-with-my-own-matrix
    http://www.nondot.org/sabre/graphpro/3d2.html
    https://www.opengl.org/resources/libraries/glut/spec3/node64.html
    https://www.opengl.org/resources/libraries/glut/spec3/node81.html
    https://www.opengl.org/documentation/specs/glut/spec3/node20.html
    https://www.opengl.org/documentation/specs/glut/spec3/node48.html
    https://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml
    https://www.khronos.org/opengles/sdk/docs/man/xhtml/glCullFace.xml
    https://www.opengl.org/sdk/docs/man2/xhtml/glBegin.xml
    http://www.deannicholls.co.uk/tutorials/show/cpp_glut
    https://mudri.uniri.hr/mod/resource/view.php?id=65584
    https://mudri.uniri.hr/mod/resource/view.php?id=108882
    http://www.glprogramming.com/red/
    http://www.cplusplus.com/forum/general/77959/
    http://www.swiftless.com/tutorials/opengl/lighting.html
    http://www.swiftless.com/tutorials/opengl/material_lighting.html
    http://www.swiftless.com/tutorials/opengl/lighting_types.html