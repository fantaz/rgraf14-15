#include <iostream>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

struct sphere {
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
};

struct sphere sph;
bool animate = false;   // treba cekati s animacijom dok se ne stisne tipka 'g'
int millisec = 15;      // okidanje timera svakih 15 ms
float spin = 0.0;       // za rotaciju kugle
float diameter = 1.0;   // promjer kugle
float speed = 0.04;     // brzina kretanja kugle
float btm_plane[4];

float vertices[8][3] = {
    {0, 0, 0},
    {0, 0, diameter},
    {0, diameter, 0},
    {0, diameter, diameter},
    {diameter, 0, 0},
    {diameter, 0, diameter},
    {diameter, diameter, 0},
    {diameter, diameter, diameter}
};

int faces[6][4] = {
    {0, 2, 3, 1},   // lijeva ravnina
    {0, 1, 5, 4},   // donja ravnina
    {2, 6, 7, 3},   // gornja ravnina
    {0, 4, 6, 2},   // prednja ravnina
    {1, 3, 7, 5},   // zadnja ravnina
    {4, 5, 7, 6}    // desna ravnina
};

float normals[6][3];

/*
 * Funkcija za izracun donje ravnine piramide pogleda
 */
void getBottomPlane() {
   float projection[16];
   float modelview[16];
   float clip[16];
   float len;

   glGetFloatv(GL_PROJECTION_MATRIX, projection);   // dohvati matricu projekcije
   glGetFloatv(GL_MODELVIEW_MATRIX, modelview);     // dohvati model view matricu

   // mnozenje matrica modelview * projection = clip
   glPushMatrix();
   glLoadMatrixf(projection);
   glMultMatrixf(modelview);
   glGetFloatv(GL_MODELVIEW_MATRIX, clip);
   glPopMatrix();

   // treba nam samo donja ravnina piramide pogleda
   btm_plane[0] = clip[3] + clip[1];
   btm_plane[1] = clip[7] + clip[5];
   btm_plane[2] = clip[11] + clip[9];
   btm_plane[3] = clip[15] + clip[13];

   // normalizacija rezultata
   len = sqrt(btm_plane[0] * btm_plane[0] + btm_plane[1] * btm_plane[1] + btm_plane[2] * btm_plane[2]);
   btm_plane[0] /= len;
   btm_plane[1] /= len;
   btm_plane[2] /= len;
   btm_plane[3] /= len;
}

/*
 * Funkcija za provjeru je li kugla unutar piramide pogleda(samo za donju ravninu)
 */
bool isSphereInFrustum() {
  if((btm_plane[0] * sph.x + btm_plane[1] * sph.y + btm_plane[2] * sph.z + btm_plane[3]) <= -(diameter/2.0))
      return false;
  return true;
}

/*
 * Funkcija za racunanje vektora normale
 *
 * Izracun 2 vektora pomocu tri tocke ravnine,
 * vektorski produkt ta 2 vektora te normalizacija
 */
void calculateNormals() {
    float vector1[3], vector2[3], product[3];
    float len;

    for(int j=0; j<6; j++) {

        // za izracun normale ravnine dovoljne su tri tocke koje joj pripadaju
        int a = faces[j][0];
        int b = faces[j][1];
        int c = faces[j][2];

        // izracun 2 vektora pomocu tri tocke ravnine v1 = T3 - T1, v2 = T2 - T1
        for(int i=0; i<3; i++) {
            vector1[i] = vertices[c][i] - vertices[a][i];
            vector2[i] = vertices[b][i] - vertices[a][i];
        }

        // vektorski produkt rez = v1 x v2
        product[0] = (vector1[1] * vector2[2]) - (vector1[2] * vector2[1]);
        product[1] = (vector1[2] * vector2[0]) - (vector1[0] * vector2[2]);
        product[2] = (vector1[0] * vector2[1]) - (vector1[1] * vector2[0]);

        // normalizacija rezultantnog vektora
        len = sqrt(product[0] * product[0] + product[1] * product[1] + product[2] * product[2]);
        product[0] /= len;
        product[1] /= len;
        product[2] /= len;

        for(int k=0; k<3; k++) {
            normals[j][k] = product[k];
        }
    }
}

/*
 * Funkcija za iscrtavanje stranica kocke
 *
 * Parametri:
 * innerCube - smjer vektora normale ovisi koje stranice se iscrtavaju(crvene/plave)
 * n - redni broj stranice
 * a, b, c, d - verteksi stranice
 */
void drawQuad(bool innerCube, int n, int a, int b, int c, int d) {
    glBegin(GL_QUADS);

    float nX, nY, nZ;

    // crvene stranice kocke imaju vektore normale prema van, a plave prema unutra
    if(!innerCube) {
        nX = normals[n][0];
        nY = normals[n][1];
        nZ = normals[n][2];
    } else {
        nX = -normals[n][0];
        nY = -normals[n][1];
        nZ = -normals[n][2];
    }

    glNormal3f(nX, nY, nZ);
    glVertex3fv(vertices[a]);
    glVertex3fv(vertices[b]);
    glVertex3fv(vertices[c]);
    glVertex3fv(vertices[d]);

    glEnd();
}

/*
 * Funkcija za iscrtavanje kocke
 *
 * Parametri:
 * isInner - trebaju li se crtati stranice plave ili crvene kocke
 */
void drawCube(bool isInner) {
    for(int i=0; i<5; i++) {
        drawQuad(isInner, i, faces[i][0], faces[i][1], faces[i][2], faces[i][3]);
    }
}

/*
 * Funkcija za iscrtavanje kugle
 */
void drawSphere() {
    glTranslatef((0.5*diameter) + sph.x,
                 (0.5*diameter) + sph.y,
                 (0.5*diameter) + sph.z);
    glRotatef(spin, 0, 0, -1);
    glutSolidSphere((0.5*diameter), 30, 30);
}

/*
 * Funkcija za izracun parabolicne putanje
 * f(x) = a*(x^2) + b*x + c
 */
float parabolicTrajectory(float x, float a, float b, float c) {
    return (a * (x * x) + (b * x) + c);
}

/*
 * Funkcija za animaciju kretanja i rotacije kugle
 */
void animation() {
    // ako je kugla izvan piramide pogleda, zaustavi animaciju
    if(!isSphereInFrustum()) {
        animate = false;
        return;
    }

    spin += (speed*100.0);
    if(spin >= 360)
        spin = 0;

    // kugla pocinje padati po parabolicnoj putanji kada se pomakne po x-osi za
    // polovicu promjera
    if(sph.x >= diameter/2.0) {
        float t = (sph.x - (diameter*0.5));
        sph.y = parabolicTrajectory(t, -1.0, diameter, -(t*diameter));
    }
    sph.x += speed;
}

/*
 * Funkcija za okidanje timera svakih 15ms za iscrtavanje scene
 */
void timer(int time) {
    glutTimerFunc(time, timer, time);
    glutPostRedisplay();
}

/*
 * Funkcija za iscrtavanje scene
 */
void display() {
    glClearColor(0.4, 0.4, 0.4, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(-5*diameter, 0, 0);    // pomakni lijevo po x-osi
    glTranslatef(0, 4*diameter, 0);     // pomakni gore po y-osi
    glTranslatef(0, 0, -15*diameter);   // odmakni scenu po z-osi

    glRotatef(20, 0, -1, 0);            // rotacija oko y osi

    getBottomPlane();

    // kocka
    // crveno izvana
    glPushMatrix();
    glCullFace(GL_FRONT);
    glColor3f(1.0, 0.0, 0.0);
    drawCube(false);
    glPopMatrix();

    // plavo unutra
    glPushMatrix();
    glCullFace(GL_BACK);
    glColor3f(0.0, 0.0, 1.0);
    drawCube(true);
    glPopMatrix();

    // kugla
    glPushMatrix();
    glColor3f(1.0, 1.0, 0.0);
    drawSphere();
    glPopMatrix();

    // animacija
    if(animate)
        animation();

    glutSwapBuffers();
}

/*
 * Funkcija koja je pozvana kad se pritisne tipka
 * na tipkovnici koja ima ASCII vrijednost
 */
void handleKeys(unsigned char key, int, int) {
    switch (key) {
        case 'g':       // pokreni animaciju
        case 'G':
            animate = true;
            break;
        case 's':       // zaustavi animaciju
        case 'S':
            animate = false;
            break;
        case 27:        // escape
            exit(0);
            break;
        default:
            return;
    }
}

/*
 * Funkcija za postavljanje viewporta
 */
void resize(int w, int h) {
    if (h == 0)
        h = 1;

    float aspect_ratio = (float)w / (float)h;
    float z_near = 1.0;          // udaljenost od ocista do near clipping ravnine
    float z_far = 500.0;         // udaljenost od ocista do far clipping ravnina
    float field_view = 45.0;     // field of view u stupnjevima

    glViewport(0 ,0 ,w ,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(field_view, aspect_ratio, z_near, z_far);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

/*
 * Funkcija za postavljanje vrijednosti za osvjetljenje
 */
void setupMaterials() {
    GLfloat mat_ambient[] = {0.5, 0.5, 0.5, 0.5};
    GLfloat mat_specular[] = {1.0, 1.0, 1.0, 0.1};
    GLfloat mat_shininess[] = {70.0};
    GLfloat light_position[] = {2.0, 2.0, 5.0, 0.0};
    GLfloat model_ambient[] = {0.7, 0.5, 0.5, 1.0};

    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, model_ambient);
}

/*
 * Funkcija za inicijalizaciju osvjetljenja i face culling-a
 */
void initialize() {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);

    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);

    glutCreateWindow("Zadaca");

    initialize();
    setupMaterials();
    calculateNormals();

    glutDisplayFunc(display);
    glutKeyboardFunc(handleKeys);
    glutReshapeFunc(resize);
    glutTimerFunc(0, timer, millisec);    // inicijalni poziv timer funkcije
    glutMainLoop();
    return 0;
}
