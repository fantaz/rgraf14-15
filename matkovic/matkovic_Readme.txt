Animacija je po defaultu pokrenuta.
ZAUSTAVLJANJE: e
POKRETANJE: s
IZLAZAK: Esc

Nikakvi dodatni argumenti nisu potrebni kod otvaranja.

Spremnik je iscrtan kao wirebox - prazan cube sa vidljivim rubovima, a vodu sam izradio kao skaliranu verziju spremnika za 0.99, obojanog u plavu boju. Za čitavu scenu sam uključio ambijentno, difuzno i zrcalno osvjetljenje u funkciji init, a u mainu je određena veličina prozora (600*600), kao i pozivi potrebnih dodatnih funkcija.

Neki od promjenjivih parametara su FOV, duljina trajanja animacije, sve boje objekata na sceni kao i pogled na samu transformaciju/animaciju - koji se promjenom posljenja tri retka u init funkciji može prilagoditi po želji. Ja sam pokušao rekreirati animaciju sa slike zadane u popisu zadataka te je pogled na spremnik s vodom približan tome.

Kompletna scena u početku se nalazi približno u sredini novootvorenog prozora. Budući da je zadana rotacija spremnika s vodom oko zamišljene CRNE linije (koja predstavlja os), pozadina je preko glClearColor obojena u bijelo.

Iscrtavanje se vrši pozivima odgovarajućih funkcija (funkcija display poziva animaciju koja poziva funkcije okvir i voda - posljednje dvije rade transformacije odgovarajućih objekata). Rotacija spremnika je jednostavna, dok se kod vode istovremeno radi rotacija i "izlijevanje" odnosno ispadanje iz spremnika. Ta transformacija uzima u obzir kut do kojeg je rotacija došla (usporedba da li je kut veći ili manji od 45 stupnjeva) i ovisno o tome mijenja pozicije vrhova koji određuju volumen/površinu objekta koji predstavlja vodu u spremniku (detaljnije u funkciji voda).

Osim Googlea za sitne uspješno rješene probleme, primjera sa lab. vježbama na MudRI sustavu, te rješenih vježbi, koristio sam i razne izvore poput:
- https://www.opengl.org/sdk/docs/man2/xhtml/
- http://akshayanswers.org/wordpress/yingst/2014/12/13/create-a-chrome-cube-in-opengl-glut/
- http://www.wikihow.com/Make-a-Cube-in-OpenGL
- http://www.videotutorialsrock.com/opengl_tutorial/lighting/text.php


