#include <GL/glut.h>
#include <iostream>

//varijabla t predstavlja trenutnu poziciju animacije - potrebna kod kompletne transformacije
//animating služi kao informacija o kraju animacije - povezana je uz start/stop keyjeve
static int t = 0;
bool animating = true;

//tipke za upravljanje
void tipke(unsigned char key, int /*x*/, int /*y*/) {
	if (key=='s' || key=='S') animating = true; //tipka S nastavlja animaciju
	else if (key=='e' || key=='E') animating = false; //tipka E zaustavlja animaciju
	else if (key==27) exit(0); //ESC izlazi iz programa
}

//normale, indeksi, koordinate za vrhove i stranice objekata
//normale za 6 stranica spremnika
GLfloat normals[6][3] = {
	{-1.0, 0.0, 0.0}, {0.0, 1.0, 0.0},
	{1.0, 0.0, 0.0}, {0.0, -1.0, 0.0},
	{0.0, 0.0, 1.0}, {0.0, 0.0, -1.0}, 
};
        
//indeksi za 6 stranica spremnika
GLint indices[6][4] = {
	{0, 1, 2, 3}, {3, 2, 6, 7},
	{7, 6, 5, 4}, {4, 5, 1, 0},
	{5, 6, 2, 1}, {7, 4, 0, 3}, 
};

//XYZ koordinate za vrhove spremnika
GLfloat vertexes[8][3] = {
    {-1.0, -1.0, 1.0}, {-1.0, -1.0, -1.0},
    {-1.0, 1.0, -1.0}, {-1.0, 1.0, 1.0},
    {1.0, -1.0, 1.0}, {1.0, -1.0, -1.0},
    {1.0, 1.0, -1.0}, {1.0,  1.0,  1.0}, 
};

//ANIMACIJA
//wirebox za vanjsku kocku tj. spremnik - zelena boja i 4px okvir
void okvir(void) {
	int i;
	glColor4f(0, 1, 0, 1);
	for (i=0; i<6; i++) {
		glLineWidth(4);
		glBegin(GL_LINE_STRIP);
		glNormal3f(normals[i][0], normals[i][1], normals[i][2]);
		glVertex3f(vertexes[indices[i][0]][0], vertexes[indices[i][0]][1], vertexes[indices[i][0]][2]);
		glVertex3f(vertexes[indices[i][1]][0], vertexes[indices[i][1]][1], vertexes[indices[i][1]][2]);
		glVertex3f(vertexes[indices[i][2]][0], vertexes[indices[i][2]][1], vertexes[indices[i][2]][2]);
		glVertex3f(vertexes[indices[i][3]][0], vertexes[indices[i][3]][1], vertexes[indices[i][3]][2]);
		glEnd();
	}
}

//kocka sa vodom - plava boja, skalirana da "stane" u spremnik
void voda(float kut) {
	int i;
	glColor4f(0, 0.2, 1, 1);
		
	glPushMatrix();
	
	//skaliranje sa 0.99
	float scale = 0.99;
	glScalef(scale, scale, scale);
	
	//iscrtavanje 6 stranica vode
	for (i=0; i<6; i++) {
		glBegin(GL_QUADS);
        glNormal3fv(&normals[i][0]);
        
        //iscrtavanje 4 vrha čija se pozicija ažurira kod "izlijevanja" vode
		for (int k=0; k<4; k++) {
            float x = vertexes[indices[i][k]][0];
            float y = vertexes[indices[i][k]][1];
            
            //ažuriranje pozicije vrhova - utječe na volumen vode u spremniku
            
			//rotacija faza 1 - "izlijevanje" vode u smjeru JI
			//dok je 0 < kut < 45, mijenjaju se vrhovi stranica 0, 1, 4 i 5
			
			//vrhovi 2 i 3 stranice 0
			if (i==0 && k>=2) y = y - 2*(kut/45);
			//vrhovi 0 i 1 stranice 1
			if (i==1 && k<2) y = y - 2*(kut/45);
			//vrh 2 stranice 4
			if (i==4 && k==2) y = y - 2*(kut/45);
			//vrh 3 stranice 5
			if (i==5 && k==3) y = y - 2*(kut/45);
			if (y < -1) y = -1;
			
			//rotacija faza 2 - "izlijevanje" vode u smjeru J
			//dok je 45 < kut < 90, mijenjaju se vrhovi stranica 4, 5
			
			if (kut > 45) {
				float kut2 = kut - 45;
				//vrhovi 2 i 3 stranice 4
				if (i==4 && k>=2) x = x + 2*(kut2/45);
				//vrhovi 2 i 3 stranice 5
				if (i==5 && k>=2) x = x + 2*(kut2/45);
				//svi vrhovi stranice 0
				//vrhovi 0 i 1 stranice 1
				//vrhovi 2 i 3 stranice 3
				if ((i==0 && k>=0) || (i==1 && k<2) || (i==3 && k>=2)) {
					x = x + 2*(kut2/45);
				}
			}
            glVertex3f(x, y, vertexes[indices[i][k]][2]);
		}
		glEnd();
	}
	glPopMatrix();
}

//animacija transformacije
void animacija(int t) {
	//linija koja označava os (bijela boja)
	glColor3f(1, 1, 1);
	glBegin(GL_LINES);
	glVertex3f(0, 0, 3);
	glVertex3f(0, 0, -3);
	glEnd();
	
	glPushMatrix();
		//timer od 0 do 1000 određuje trajanje animacije
		//transformacija rotacije spremnika oko osi
		float kut = (int)((t/1000.0)*90);
		glRotatef(kut, 0, 0, -1);
		glTranslatef(-1, 1, 0);
		okvir();
		//iscrtavanje vode ovisno o kutu
		if(t<=1000) voda(kut);
	glPopMatrix();
}

//prozor za animaciju, varijabla t označava trenutnu vrijednost animacije
//promjena utječe na trajanje animacije
//postavljanje boje backgrounda u bijelu (radi crne linije koja predstavlja os - bila bi nevidljiva)
void display(void) {
	if(animating && t<=1000) t++;
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	animacija(t);
	glutSwapBuffers();
}

//osvjetljenje, setup scene
void init(void) {
	//ambijentno, difuzno i zrcalno osvjetljenje, inicijalizacija i dodjela
	GLfloat ambient_light[] = {0.1, 0.1, 0.1, 1.0};
	GLfloat diffuse_light[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat specular_light[] = {1.0, 0.0, 0.0, 1.0};
	GLfloat light_position[] = {2.0, 2.0, 2.0, 1.0};
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

	//pogled - FOV, aspect ratio, Z koo)
	glMatrixMode(GL_PROJECTION);
	gluPerspective(25.0, 1.0, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	
	//usmjerenje očišta - prve 3 očište - druge 3 pozicija centra, posljednje 3 smjer up
	gluLookAt(2.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	//određivanje kuta pogleda na objekte pomoću parametara samih transformacija
	glTranslatef(0.0, 0.0, -1.0);
	glRotatef(30, 1.0, 0.0, 0.0);
	glRotatef(-15, 0.0, 1.0, 0.0);	
}

//main funkcija
//pozicija i veličina prozora, inicijalizacija funkcija za upravljanje
int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);	
	
	glutInitWindowPosition(0,0);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Matkovic_zad4_rg");
	
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutKeyboardFunc(tipke);
	
	init();
	
	glutMainLoop();
	return 0;
}
