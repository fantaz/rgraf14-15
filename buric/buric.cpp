#include <iostream>
#include <GL/glut.h>
#include <stdio.h>
#include <vector>

struct _4{	
        float top_leftX, top_rightX, top_leftY, top_rightY;
	float bot_leftX, bot_rightX, bot_leftY, bot_rightY;
};
std::vector<_4>Quads;

struct _dot{
    float posX, posY, time;
};
std::vector<_dot>Points;

char step=0, end=0;
float x=0.0f, y=0.0f, totalTime=0.0f, r=0.5, offX=0.0f, offY=0.0f;
int key_index;

void keyboard(unsigned char key, int , int );
void timer(int );
void init();
void setColour(GLfloat color[]);
void sphere();
void display();
void shape_change(int width, int height);


//setting color
void setColor(GLfloat color[]){

        glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,color);
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,color);
}

//program
int main(int argc,char **argv){
	
	_dot tt;              	 //definiranje točke
        
    tt.time=0.0f;		 	 //start time
    tt.posX=3.0f*0.0f;		 //start position X
    tt.posY=3.0f*0.0f;		 //start position Y
    Points.push_back(tt); 	 //na kraj vektora Points dodamo novi element

    tt.time=1.0f;		 	 //next time
    tt.posX=3.0f*0.7f;		 //step position X
    tt.posY=3.0f*-0.3f;		 //step position X
    Points.push_back(tt);	 //new element

    tt.time=2.0f;
    tt.posX=3.0f*0.3f;
    tt.posY=3.0f*-0.7f;
    Points.push_back(tt);

    tt.time=3.0f;
    tt.posX=3.0f*1.0f;
    tt.posY=3.0f*-1.0f;
    Points.push_back(tt);

	key_index=1;
	
	_4 temp;
	
	temp.top_leftX=temp.top_rightX=Points[0].posX;
	temp.top_leftY=temp.top_rightY=Points[0].posY+r;
	temp.bot_leftX=temp.bot_rightX=Points[0].posX;
	temp.bot_leftY=temp.bot_rightY=Points[0].posY-r;
	Quads.push_back(temp);

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH|GLUT_ACCUM);
    glutInitWindowSize(800,800);
    glutInitWindowPosition(0,0);
    glutCreateWindow("matej_buric_zadatak-1");
    glutReshapeFunc(shape_change);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(0,timer,0);
    
    init();
    glutMainLoop();
    
    return 0;
}

//sphere position
void sphere(){
	
	GLfloat amb_green[]={0.0f,1.0f,0.0f,1.0f};
	setColor(amb_green);
	
    glPushMatrix();
    glColor3f(1.0,1.0,0.0);
    glutSolidSphere(r,50,50);
    glPopMatrix();
}

//process navigation
void keyboard(unsigned char key, int , int ){
	
	switch(key){	
					
		case 's': case 'S':
			step=1;
			break;
		case 'x': case 'X':
			step=0;
			break;
		case 27: 			//Esc = 27
			exit(0);
			break;
	}
}

//timer refresh (every 60ms)
void timer(int ){
	
    if(step && !end){
		
		offX=offX+0.1f;
        offY=offY-0.1f;
    }

    glutTimerFunc(60,timer,0);
    glutPostRedisplay();
    
    if(step) totalTime+=0.06f;
}

//init parameters and lightness
void init(){
			
	GLfloat mat_specular[]={0.0f,1.0f,0.0f,1.0f};
    GLfloat mat_shininess[]={69.0f};

    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininess);
		
    GLfloat light0_amb[]={0.5f,0.5f,0.5f,1.0f};
    GLfloat light0_diffuse[]={1.0f,1.0f,1.0f,1.0f};
    GLfloat light0_specular[]={1.0f,1.0f,1.0f,1.0f};
    GLfloat light0_pos[]={5.0f,5.0f,5.0f,0.0f};

    glLightfv(GL_LIGHT0,GL_AMBIENT,light0_amb);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
    glLightfv(GL_LIGHT0,GL_POSITION,light0_pos);
    glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,light0_amb);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//display setup
void display(){
	
	int i, index=-1;
        float interpol, inter_posX, inter_posY;
        
    for(i=0; i<(int)Points.size(); ++i){
		index=i;
        if(Points[i].time>totalTime) break;
    }

    interpol=(totalTime-Points[index-1].time)/(Points[index].time-Points[index-1].time);
    if(interpol>1) interpol=1.0;

    inter_posX=interpol*Points[index].posX+(1.0f-interpol)*Points[index-1].posX;
    inter_posY=interpol*Points[index].posY+(1.0f-interpol)*Points[index-1].posY;

	Quads[Quads.size()-1].top_rightX=inter_posX;
	Quads[Quads.size()-1].top_rightY=inter_posY+r;
	Quads[Quads.size()-1].bot_rightX=inter_posX;
	Quads[Quads.size()-1].bot_rightY=inter_posY-r;

	if(index!=key_index){
			
		key_index=index;

		Quads[Quads.size()-1].top_rightX=Points[index-1].posX;
		Quads[Quads.size()-1].top_rightY=Points[index-1].posY+r;
		Quads[Quads.size()-1].bot_rightX=Points[index-1].posX;
		Quads[Quads.size()-1].bot_rightY=Points[index-1].posY-r;

		_4 temp;
		
		temp.top_leftX=temp.top_rightX=Points[index-1].posX;
		temp.top_leftY=temp.top_rightY=Points[index-1].posY+r;
		temp.bot_leftX=temp.bot_rightX=Points[index-1].posX;
		temp.bot_leftY=temp.bot_rightY=Points[index-1].posY-r;
		Quads.push_back(temp);

	}

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
	glTranslatef(-1.5,1.5,0.0);
	
	glPushMatrix();
    glTranslatef(inter_posX,inter_posY,0.0f);
    sphere();
    glPopMatrix();
    
	glPushMatrix();
        GLfloat amb_white[]={1.0f,0.4f,0.2f,0.5f}; 					//transparent trail (alpha=0.5)
	setColor(amb_white); 

	for(i=0; i<(int)Quads.size(); ++i){
		
		glBegin(GL_QUADS); 										//producing trail by using quads
		glVertex2f(Quads[i].top_leftX, Quads[i].top_leftY);
		glVertex2f(Quads[i].top_rightX, Quads[i].top_rightY);
		glVertex2f(Quads[i].bot_rightX, Quads[i].bot_rightY);
		glVertex2f(Quads[i].bot_leftX, Quads[i].bot_leftY);
		glEnd();
	}
	glPopMatrix();
	
    glFlush();
    glutSwapBuffers();
}

//reshaping
void shape_change(int width, int height){
	
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity(); 
        
    if(width<=height)
    glOrtho(-2.0,2.0,-2.0*(GLfloat)height/(GLfloat)width,2.0*(GLfloat)height/(GLfloat)width,-10.0,10.0);
    else
    glOrtho(-2.0*(GLfloat)height/(GLfloat)width,2.0*(GLfloat)height/(GLfloat)width,-2.0,2.0,-10.0,10.0);
    
    glMatrixMode(GL_MODELVIEW);
    glClearAccum(0.0, 0.0, 0.0, 0.5);
    glClear(GL_ACCUM_BUFFER_BIT);
    glLoadIdentity();
}


