Zadatak 1.

Program inicijalno prikazuje zelenu sferu u gornjem lijevom kutu ekrana. Pritiskom tipke 's' se pokreće
animacija i traje dok se ne pritisne tipka 'x'. Pritiskom tipke 'Esc' potrebno izaći iz programa. Tijekom
animacije se sfera giba po putanji koja je nacrtana isprekidanom linijom prikazanom na Slici 4. Sfera
za sobom ostavlja trag tamnocrvene boje i visine jednake dijametru sfere. Animacija se zaustavlja kada
sfera dođe do donjeg desnog ruba ekrana.
Uključiti osvjetljenje te trag i sferu primjetno osjenčiti. Trag mora biti proziran, alpha = 0.5.




Opis mog rješenja

Program je realiziran pomoću vektora smjera u nekom određenom vremenskom trenutku.
Sferu sam inicijalizirao na početku i postavio je u gornji lijevi kut, kako je zadano u zadatku, a trag sfere
sam izradio koristeći funkciju glBefin(GL_QUADS). Za animaciju sfere sam koristio interpolaciju u translaciju
u određenom vremenskom trenutku prilikom koje se pomakne scena (X i Y) za neki korak(offset).
Veličina prozora OpenGL-a je određena na 800x800.


tipka s - pokretanje animacije
tipka x - zaustavljanje animacije
tipka Esc - izlazak iz programa


Reference:

https://www.opengl.org/resources/libraries/glut/spec3/node1.html
http://www.cplusplus.com/
http://www.cplusplus.com/reference/vector/vector/
http://en.wikibooks.org/wiki/OpenGL_Programming/Basics/2DObjects
http://wiki.delphigl.com/index.php/glShadeModel

