Program iscrtava sferu i trag iza nje. 


Način promjene smjera sfere:
	Budući da se sfera giba u dvije faze (povećanje x i z, pa smanjenje x i povećanje z), svoju metodu za izračun pozicije sfere sam podijelio na dva djela, a trenutnu fazu pratim varijablom currentSegment, dok ona ne dostigne broj segmenata koji je korisnik unesao sfera će se animirati.


Argumenti konstruktora sphereWithTrail:
	Sferu i njen trag predstavlja klasa sphereWithTrail čiji konstruktor prima argumente za njene početne (x, y i z) koordinate, njen radius, broj segmenata njene putanje koji je ujedno i broj segmenata traga, vrijednost frame1 koja predstavlja broj frame-ova unutar koje se prva faza traga mora obaviti i vrijednost frame2 koja ima istu svrhu za drugu fazu.


Način rada animacije:
	Public metoda klase sphereWithTrail draw poziva calculatePosition koji mjenja koordinate sfere po kubičnoj In/Out easing funkciji easeInOutCubic, u slučaju da je frame dostigao prikladnu vrijednost za promjenu smjera (faze) kretanja sfere poziva se funkcija nextSegment koja sprema koordinate traga, postavlja sljedeći segment i resetira vrijednost frame. Nakon calucluatePosition poziva se drawTrails koja iscrtava sve spremljene tragove iz vektora trailSegments u crvenoj boji sa 0.5 alpha. Segmenti se iscrtavaju kao QUAD-ovi i iz njihovih koordinata se računaju normale funkcijom getNormal. Nakon iscrtavanja svih segmenata tragova iz vektora iscrtava se još jedan trag ćija je početna točka na kraju posljednjeg segmenta traga iz vektroa, a završna na koordinatama sfere. Na kraju se crta sfera metodom drawSphere koja mjenja boju u zelenu sa 1.0 alpha, translatira za x, y i z (koordinate sfere) i crta sferu zadanog radiusa.


Tipke za kontrolu:
	Escape: Izlaz iz programa
	s:	Pokretanje animacije
	x: 	Pauziranje/prekid animacije
	r:	Reset animacije, reset metoda klase sphereWithTrail
	a:	Rotacija kamere oko y osi prema lijevo
	d:	Rotacija kamere oko y osi prema desno
	q:	Rotacija kamere oko x osi prema gore
	e:	Rotacija kamere oko x osi prema dolje
	+:	Zoom in, smanjivanje z koordinate kamere
	-:	Zoom out, povećanje z koordinate kamere
	Lijevo:	Pan ulijevo, smanjenje x koordinate kamere i središte pogleda
	Gore:	Pan ulijevo, povećanje y koordinate kamere i središte pogleda
	Desno:	Pan ulijevo, povećanje x koordinate kamere i središte pogleda
	Dolje:	Pan ulijevo, smanjenje y koordinate kamere i središte pogleda

Reference:
	Easing funkcija: gizma.com/easing/
	Računanje normala: https://www.opengl.org/discussion_boards/showthread.php/172697-Surface-Normal-Function-Not-really-sure