#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <vector>
#include <cmath>

#define DELTA_MS 5

float cameraX = 0.0f;   //
float cameraY = 5.0f;   // Pozicija kamere
float cameraZ = 7.0f;   //

float centerX = 0.0f;   //
float centerY = 0.0f;   // Koordinate u koje gleda kamera
float centerZ = 0.0f;   //

float angleY = 0.0f;    // Kut rotacije kamere oko y osi
float angleX = 0.0f;    // Kut rotacije kamere oko x osi

class sphereWithTrail {
    private:

        /************* Varijable ***********************/

        struct pointsPair {
            float x0, y0, z0;   // Struktura za spremanje koordinata traga
            float x1, y1, z1;   // x0, y0, z0 - početne koord. traga, x1, y1, z1 - krajnje koord.
        };

        float x;    //
        float y;    //  Trenutne koordinate sfere
        float z;    //

        float radius;   // Radius sfere

        int currentSegment;     // Trenutni segment koji se animira
        int trailSegmentsNum;   // Broj segmenata
        int firstSegFrames;     // Vrijeme trajanja prvog segmenta
        int secondSegFrames;    // Vrijeme trajanja drugog segmenta

        long frame;     // Trenutni frame

        std::vector<pointsPair> trailSegments;  // Vektor sa segmentima traga

        /************* METODE ***********************/

        void drawTrails();  // Metoda za crtanje traga
        void nextSegment(); // Metoda za sljedeći segment
        void calculatePosition();   // Metoda za animaciju sfere, kalkulacija nove pozicije
        void drawSphere();  // Metoda za crtanje sfere
        void getNormal(pointsPair, float*); // Metoda za izračunavanje normale
        float easeInOutCubic(float, float, long);   // Metoda za easing kretanja sfere

    public:
        /************* Varijable ***********************/
        bool running;   // Pauziranje i pokretanje animacije

        /************* METODE ***********************/

        sphereWithTrail(float, float, float, float , int , int , int ); // Konstruktor
        void draw();    // Metoda za isctavanje animacije
        void reset();   // Metoda za reset animacije
};

/**
 * @brief sphereWithTrail::sphereWithTrail Konstruktor klase sphereWithTrail.
 * @param x Početna x koordinata sfere.
 * @param y Početna y koordinata sfere.
 * @param z Početna z koordinata sfere.
 * @param radius Radius sfere.
 * @param trailSegmentsNum Broj segmenata
 * @param frames1 Vrijeme iscrtavanja prvog segmenta (U frames-ima).
 * @param frames2 Vrijeme iscrtavanja drugog segmenta (U frames-ima).
 */
sphereWithTrail::sphereWithTrail(float x, float y, float z, float radius, int trailSegmentsNum, int frames1, int frames2) {
    this->x = x;
    this->y = y;
    this->z = z;
    this->trailSegmentsNum = trailSegmentsNum;
    this->radius = radius;
    this->firstSegFrames = frames1;
    this->secondSegFrames = frames2;

    frame = 0;
    running = false;
    currentSegment = 0;

    pointsPair tmp = {x, y, z, x, y, z};    // Početni trag širine 0 koji služi za praćenje početne točke animacije

    trailSegments.push_back(tmp);
}

/**
 * @brief sphereWithTrail::easeInOutCubic Easing metoda za kretanje sfere.
 * @param startVal Vrijednost koordinate sa koje je započelo kretanje.
 * @param dVal Vrijednost za koju će se promjeniti početna kooridinata.
 * @param dFrame Vrijeme za koje se startVal mora promjeniti za dVal.
 * @return Float vrijednost koordinate u trenutnom frame-u kojeg prati klasa.
 */
float sphereWithTrail::easeInOutCubic(float startVal, float dVal, long dFrame) {
    float newVal;

    float tmpFrame = frame;

    tmpFrame /= dFrame/2.0f;

    if (tmpFrame < 1) newVal = dVal/2.0f*pow(tmpFrame, 3.0f) + startVal;
    else {
        tmpFrame -= 2;
        newVal = dVal/2.0f*(pow(tmpFrame, 3.0f) + 2) + startVal;
    }

    return newVal;
}

/**
 * @brief sphereWithTrail::nextSegment Spremaju se koordinate trenutnog traga, resetira se frame i prelazi se na sljedeći segment.
 */
void sphereWithTrail::nextSegment() {
    currentSegment++;
    frame = 0;

    pointsPair tmp = {trailSegments.back().x1, trailSegments.back().y1, trailSegments.back().z1, x, y, z};

    trailSegments.push_back(tmp);
}

/**
 * @brief sphereWithTrail::getNormal Metoda za računanje normale na površinu.
 * @param vertices Struktura koja sadrži koordinate površine.
 * @param normal Array u koji se sprema normalizirane vektor normale.
 */
void sphereWithTrail::getNormal(pointsPair vertices, float normal[]) {

    float vector1[3] = {vertices.x1-vertices.x0, (vertices.y1-radius)-(vertices.y0-radius), vertices.z1-vertices.z0};   //
    float vector2[3] = {vertices.x0-vertices.x0, (vertices.y0+radius)-(vertices.y0-radius), vertices.z0-vertices.z0};   // Izračun vektora

    normal[0] = vector1[1]*vector2[2] - vector2[1]*vector1[2];  //
    normal[1] = vector1[2]*vector2[0] - vector2[2]*vector1[0];  // Cross produkt vektora
    normal[2] = vector1[0]*vector2[1] - vector2[0]*vector1[0];  //

    float length = sqrt((normal[0]*normal[0])+(normal[1]*normal[1])+(normal[2]*normal[2])); // Duljina normale

    if(length == 0.0f) {
        normal[0] = 0.0f;
        normal[1] = 0.0f;
        normal[2] = 0.0f;
    }else {
        normal[0] /= length;    //
        normal[1] /= length;    // Normalizacija
        normal[2] /= length;    //
    }

}

/**
 * @brief sphereWithTrail::drawTrails Crtanje tragova iza sfere, koristi getNormal metodu za izračun normale za svaki segment traga.
 */
void sphereWithTrail::drawTrails() {
    glColor4f(1.0f, 0.0f, 0.0f, 0.5f);   // Crvena boja, prozirnost 50%
    float normal[3];

    for(std::vector<pointsPair>::iterator it = trailSegments.begin(); it != trailSegments.end(); it++) {        

        glBegin(GL_QUADS);
            getNormal(*it, normal);

            glNormal3f(normal[0], normal[1], normal[2]);

            glVertex3f(it->x0, it->y0-radius, it->z0);  //v1
            glVertex3f(it->x1, it->y1-radius, it->z1);  //v2
            glVertex3f(it->x1, it->y1+radius, it->z1);  //Trokuta
            glVertex3f(it->x0, it->y0+radius, it->z0);  //v3
        glEnd();

    }

    pointsPair currentPoints = {trailSegments.back().x1, trailSegments.back().y1, trailSegments.back().z1, x, y, z};

    glBegin(GL_QUADS);
        getNormal(currentPoints, normal);

        glNormal3f(normal[0], normal[1], normal[2]);

        glVertex3f(currentPoints.x0, currentPoints.y0-radius, currentPoints.z0);
        glVertex3f(currentPoints.x1, currentPoints.y1-radius, currentPoints.z1);
        glVertex3f(currentPoints.x1, currentPoints.y1+radius, currentPoints.z1);
        glVertex3f(currentPoints.x0, currentPoints.y0+radius, currentPoints.z0);
    glEnd();

}

/**
 * @brief sphereWithTrail::calculatePosition Izračun nove pozicije sfere, koristi easeInOutCubic.
 */
void sphereWithTrail::calculatePosition() {
    if(running && currentSegment != trailSegmentsNum) {

        switch (currentSegment%2) {
            case 0:
                // prva faza

                // Pomak z od koordinate kraja posljednjeg zapamčenog segmenta traga za 3.0f u firstSegFrames frame-ova
                z = easeInOutCubic(trailSegments.back().z1, 3.0f, firstSegFrames);

                // Pomak x od koordinate kraja posljednjeg zapamčenog segmenta traga za 6.0f u firstSegFrames frame-ova
                x = easeInOutCubic(trailSegments.back().x1, 6.0f, firstSegFrames);

                if(frame == firstSegFrames) nextSegment();
            break;

            case 1:
                // druga faza

                // Pomak z od koordinate kraja posljednjeg zapamčenog segmenta traga za 3.0f u secondSegFrames frame-ova
                z = easeInOutCubic(trailSegments.back().z1, 3.0f, secondSegFrames);

                // Pomak x od koordinate kraja posljednjeg zapamčenog segmenta traga za -2.0f u secondSegFrames frame-ova
                x = easeInOutCubic(trailSegments.back().x1, -2.0f, secondSegFrames);

                if(frame == secondSegFrames) nextSegment();
             break;
        }
        frame++;

    }

}

/**
 * @brief sphereWithTrail::drawSphere Crtanje sfere.
 */
void sphereWithTrail::drawSphere() {
    glTranslatef(x, y, z);

    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);  // Zelena boja, prozirnost 0%

    glutSolidSphere(radius, 30, 30);
}

/**
 * @brief sphereWithTrail::reset Reset metoda, prazni se vektor sa tragovima, a koordinate sfere se postavljaju početne koordinate prvog segmetna traga.
 */
void sphereWithTrail::reset() {

    x = trailSegments.front().x0;   //
    y = trailSegments.front().y0;   // Početna koordinata prvog traga je početna koordinata sfere
    z = trailSegments.front().z0;   //

    frame = 0;
    running = false;
    currentSegment = 0;
    trailSegments.clear();  // Ukloni spremnjene segmente traga

    pointsPair tmp = {x, y, z, x, y, z};    // Spremi trag širine 0 na početnim koordinatama sfere u vektor

    trailSegments.push_back(tmp);
}

/**
 * @brief sphereWithTrail::draw Public metoda za poziv iscrtavanja animacije, koristi calculatePosition, drawTrails i drawSphere.
 */
void sphereWithTrail::draw() {

    calculatePosition();

    drawTrails();

    drawSphere();

}

// Globalni objekt sphere čiju metodu draw čemo koristiti
sphereWithTrail sphere(-8.0f, 1.0f, -6.0f, 0.5f, 3, 150, 75);

void processNormalKeys(unsigned char key, int /*x*/, int /*y*/) {

    switch (key) {
        case 27:    // Escape tipka
            exit(0);
        break;

        case 's':   // s tipka, pokretanje animacije
            sphere.running = true;
        break;

        case 'x':   // x tipka, zaustavljanje animacije
            sphere.running = false;
        break;

        case 'r':   // r tipka, reset animacije
            sphere.reset();
        break;

        case 'a':   // a tipka, rotacija oko y osi premo lijevo
            angleY -= 5.0f;
            if(angleY > 360.0f) angleY = angleY-360.0f;
        break;

        case 'd':   // d tipka, rotacija oko y osi prema desno
            angleY += 5.0f;
            if(angleY < 0.0f) angleY = angleY+360.0f;
        break;

        case 'q':   // q tipka, rotacija oko x osi prema gore
            angleX -= 5.0f;
            if(angleX > 360.0f) angleX = angleX-360.0f;
        break;

        case 'e':   // e tipka, rotacija oko x osi prema dolje
            angleX += 5.0f;
            if(angleX < 0.0f) angleX = angleX+360.0f;
        break;

        case '+':   // + tipka, zoom in
            cameraZ -= 0.1f;
        break;

        case '-':   // - tipka, zoom out
            cameraZ += 0.1f;
        break;

    }
}

void processSpecialKeys(int key, int /*x*/, int /*y*/) {

    switch (key) {
        case GLUT_KEY_LEFT:     // tipka lijevo, pan ulijevo
            cameraX -= 0.1f;
            centerX -= 0.1f;
        break;

        case GLUT_KEY_UP:       // tipka gore, pan prema gore
            cameraY += 0.1f;
            centerY += 0.1f;
        break;

        case GLUT_KEY_RIGHT:    // tipka desno, pan udesno
            cameraX += 0.1f;
            centerX += 0.1f;
        break;

        case GLUT_KEY_DOWN:     // tipka dolje, pan prema dolje
            cameraY -= 0.1f;
            centerY -= 0.1f;
        break;

    }
}

void init(void) {
    glEnable(GL_DEPTH_TEST);

    GLfloat light_position[] = {0.0, 8.0, 0.0, 1.0};    // Pozicija svijetla
    glShadeModel(GL_SMOOTH);    // Shading model

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);  // Postavke svijetla LIGHT0

    glEnable(GL_LIGHTING);  // Omogučavanje svijetla
    glEnable(GL_LIGHT0);    // Omogučavanje svijetla LIGHT0

    glEnable(GL_COLOR_MATERIAL);    //Omogučavanje boje

    glEnable(GL_BLEND); // Omogučavanje prozirnosti, blend
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // Blend funkcija
}

void display() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();   // Jedinična matrica

    gluLookAt(cameraX, cameraY, cameraZ, //
            centerX, centerY, centerZ,  // Postavljanje kamere
            0.0f, 1.0f, 0.0f);          //

    glRotatef(angleY, 0.0f, 1.0f, 0.0f);    // Rotacija animacije oko Y osi

    glRotatef(angleX, 1.0f, 0.0f, 0.0f);    // Rotacija animacije oko X osi

    sphere.draw();  // Crtanje animacije

    glutSwapBuffers();


}

void reshape(int w, int h) {
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    gluPerspective(45, (GLfloat)w / (GLfloat)h, 1.0f, 100.0f);

    glMatrixMode(GL_MODELVIEW);
}

void timer(int /*value*/) {
    glutPostRedisplay();
    glutTimerFunc(DELTA_MS, timer, 0);
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

    glutInitWindowSize(960, 540);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Zadaca - Matko Burul");

    init();

    glutKeyboardFunc(processNormalKeys);
    glutSpecialFunc(processSpecialKeys);

    glutDisplayFunc(display);
    glutTimerFunc(DELTA_MS, timer, 0);
    glutReshapeFunc(reshape);

    glutMainLoop();

    return 0;
}
