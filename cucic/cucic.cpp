#include <iostream>
#include <GL/glut.h>
#include <stdlib.h>

using namespace std;
bool animacija = false; //u pocetku animacija nije pokrenuta



void initRendering(){
	//Makes 3d drawing work when something is infront of something else
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL); //omogucimo rad sa bojama
	glEnable(GL_LIGHTING); //omogucimo rad sa svjetlima
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glEnable(GL_BLEND);//Za transparentnost
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glClearColor(0.5f, 0.5f, 0.5f, 1.0f); //OTKOMENTIRATI za drugaciju boju pozadine
}

void handleResize(int w, int h){
	//Tell opengl how to convert from coordinates to pixels values
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective

	//Set the camera perspective
	glLoadIdentity();//reset the camera
	gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
	// camera angle, width to height ratio, near z clipping, far z clipping
}


float pomx = 0.2f;
float pomy = -0.2f;
bool inicijalno = true;  // pocetno stanje
bool prviput = false; // kugla prvi put mijenja smjer
bool drugiput = false; //kugla drugi put mijenja smjer
bool kraj = false; // kugla je dosla do kraja

void update(int /*value*/){

	//Gledamo ako animacija treba krenuti
	if (animacija){

		// Pomak za x os
		if (inicijalno){
			if (pomx < 1.3f){
				pomx += 0.02f;
				prviput = false;
			}
			else{
				prviput = true;
				inicijalno = false;
			}
		}

		if (prviput){
			if (pomx > 0.7f){
				pomx -= 0.03f;
			}
			else{
				prviput = false;
				drugiput = true;
			}
		}

		if (drugiput){
			pomx += 0.02f;
		}



		//Pomak za y os
		if (!kraj){
			if (!prviput){
				pomy -= 0.01f;
			}
			else{
				pomy -= 0.02f;
			}
		}

		//uvjet ako je na kraju
		if (pomx > 2.0f){
			drugiput = false;
			kraj = true;
		}

		glutPostRedisplay(); //Tell glut that the scene has changed
		//tell glut to call update again in 30 miliseconds
		glutTimerFunc(30, update, 0);
	}

}


void handleKeypress(unsigned char key, int /*x*/, int /*y*/){ // Key that was pressed, current mouse cordinates
	switch (key) {
	case 27: //escape key
		exit(0); //Exit the program
	case 's':
		animacija = true; //pokreni animaciju
		update(0); //pozivamo update funkciju kako bi registrirala da je animacija pokrenuta
		break;
	case 'x':
		animacija = false;
		break;
	}
}


void drawScene(){
	//clear information from last draw
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); //switch to drawing perspective
	glLoadIdentity(); //reset the drawing perspective

	glTranslatef(0.0f, 0.0f, -3.0f); // Pomak scene u daljinu
	
	//add ambient light
	GLfloat ambientColor[] = { 0.2f, 0.2f, 0.2f, 1.0f }; //color 0.2 0.2 0.2
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

	
	//add positioned light
	GLfloat lightColor0[] = { 0.6f, 0.6f, 0.6f }; //color 0.6 0.6 0.6
	GLfloat lightPos0[] = { 1.0f, 0.0f, 0.0f, 1.0f }; // poistioned at (1,0,0)
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
	
	
	//add directed light
	GLfloat lightColor1[] = { 0.5f, 0.2f, 0.2f, 1.0f }; //color 0.5, 0.2, 0.2
	//coming from direction 1.0 , 0.0, 0.5
	GLfloat lightPos1[] = { 1.0f, 0.0f, 0.5f, 0.0f };
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
	
	//Ispod vrsimo crtanje nase sfere i putanje

	glTranslatef(-1.0f, 1.0f, 0.0f); // Pomak sfere u gorni lijevi kut
	glPushMatrix();
	glTranslatef(pomx, pomy, 0.0f); // Pomak sfere - parametri se mijenjaju u update funkciji
	glColor3f(0.0f, 0.8f, 0.0f);
	glutSolidSphere(0.1, 100, 100); //nacrtaj sferu

	glPopMatrix();

	glBegin(GL_QUADS); //Trag koji kugla ostavlja


	if (inicijalno){  //Prva traka dok se kugla mice
		glColor4f(0.5f, 0.1f, 0.1f, 0.5f);// prozirnost gdje 4 argument je jednak alfi
		glNormal3f(0.0f, 0.0f, 1.0f);//
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(pomx, pomy + 0.1f, 0.0f);
		glVertex3f(pomx, pomy - 0.1f, 0.0f);
		glVertex3f(0.0f, -0.2f, 0.0f);
	}
	else{ //prva traka kad je kugla promjenila smjer
		glColor4f(0.5f, 0.1f, 0.1f, 0.5f);//
		glNormal3f(0.0f, 0.0f, 1.0f);//
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.3f, -0.62f, 0.0f);
		glVertex3f(1.3f, -0.82f, 0.0f);
		glVertex3f(0.0f, -0.2f, 0.0f);
		if (prviput){ // druga traka dok se kugla mice
			glColor4f(0.5f, 0.1f, 0.1f, 0.5f);//
			glNormal3f(1.0f, 0.0f, 0.01f);//
			glVertex3f(1.3f, -0.62f, 0.01f);
			glVertex3f(pomx, pomy + 0.1f, 0.01f);
			glVertex3f(pomx, pomy - 0.1f, 0.01f);
			glVertex3f(1.3f, -0.82f, 0.01f);
		}
		else{ //Druga traka kad je kugla promjenila smjer
			glColor4f(0.5f, 0.1f, 0.1f, 0.5f);//
			glNormal3f(1.0f, 0.0f, 0.01f);//
			glVertex3f(1.3f, -0.62f, 0.01f);
			glVertex3f(0.7f, -1.1f, 0.01f);
			glVertex3f(0.7f, -1.3f, 0.01f);
			glVertex3f(1.3f, -0.82f, 0.01f);
			if (drugiput){ //zadnja traka dok se kugla mice
				glColor4f(0.5f, 0.1f, 0.1f, 0.5f);//
				glNormal3f(0.0f, 0.0f, 1.0f);//
				glVertex3f(0.7f, -1.1f, 0.02f);
				glVertex3f(pomx, pomy + 0.1f, 0.02f);
				glVertex3f(pomx, pomy - 0.1f, 0.02f);
				glVertex3f(0.7f, -1.3f, 0.02f);
			}
			else{ // zadnja traka kada je animacija stala i kugla dosla do kraja
				glColor4f(0.5f, 0.1f, 0.1f, 0.5f);//
				glNormal3f(0.0f, 0.0f, 1.0f);//
				glVertex3f(0.7f, -1.1f, 0.02f);
				glVertex3f(2.0f, pomy + 0.1f, 0.0f);
				glVertex3f(2.0f, pomy - 0.1f, 0.0f);
				glVertex3f(0.7f, -1.3f, 0.02f);
			}
		}
	}




	glEnd();


	glutSwapBuffers(); //send the 3D scene to the screen
}

int main(int argc, char** argv){
	//initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500); //set the window size

	//create the window
	glutCreateWindow("Dejan Cucic Zadatak - 1");
	initRendering(); //initialize rendering

	//Set the handler functions for drawing keypresses and window resizes
	glutDisplayFunc(drawScene);
	glutKeyboardFunc(handleKeypress);
	glutReshapeFunc(handleResize);
	glutTimerFunc(30, update, 0); //ponovno pozivaj update funkciju svakih 30ms

	glutMainLoop(); //starts the main loop. glutMainLoop dosen't return.
	return 0; //this line is never reached
}