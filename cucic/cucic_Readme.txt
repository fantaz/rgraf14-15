Program se inicijalno otvara u 500x500 prostoru. 
- handleResize funkcija se pobrinjava da nam aspect ratio ostane zadrzan u slucaju da dodje do promjene veličine prozora. 

- U funkciji initRendering sam omogucio sa naredbama glEnable() potrebne komponente openGl-a kako bi ih mogao koristiti u crtanju. 

- Inicijalno je program postavljem u stanje cekanja odnosno animacija nije pokrenuta. Pritiskom tipke s animacija se pokrece, hvatanje tipke koja je pritisnuta je odrađeno u funkciji handleKeypress. Funkciji smo morali i proslijediti pozicije mouse-a ali ih nismo morali koristiti. Pritiskom tipke s animacija se stavlja u cekanje. Izvedeno je sa if uvjetom u update funkciji. 

- Update funkcija prvo gleda ukoliko animacija treba biti pokrenuta, ako da onda gleda u kojem je dijelu animacija (mogli smo je zaustaviti bilo kada tokom izvođenja) i zatim radi odgovarajući pomak, odnosno dali se pomice lijevo ili desno. Ono sto ta funkcija zapravo radi je samo računa vrijednost varijabli koje smo proslijedili u Translate naredbu kod crtanja. Ima tri dijela, prije nego je kugla dosla do zavoja, kada je prosla prvi zavoj, te kada je prosla drugi zavoj. U funkciji drawScene obavlja se vecina posla naseg programa.
 - Najprije pomicemo scenu u daljinu kako nam nebi bila "na ocima". Zatim dodajemo ambijentno svjetlo koje zapravo simbolizira generalno osvjetljenje u svijetu. Zatim pozicionirano svjetlo u gornjem desnom kutu, ciji utjecaj se vidi i na sferi kako se ona giba. I zatim direktno svjetlo kako bi bolje vidjeli trag i utjecaj prozirnosti traga.
Vrijedno je napomenuti da nismo zapravo određivali boju sa argumentima vec smo određivali Intenzitet tog svjetla.
Nakon toga pomicemo sferu u gornji lijevi kut sa translacijom. Potom kreće pomak (Translacija) sfere te njezino crtanje u tom mjestu. Vidimo da pomx i pomy varijable su zapravo vrijednosti za koliko cemo translatirati kuglu. A sa glColor3f određujemo boju. 

- Trag kugle je iveden na nacin da ima tri dijela kako je i ranije napomenuto. Prvo crta trag od pocetnih tocaka pa do tocaka gdje se kugla trenutno nalazi. Izvedeno je sa Quadovima (kao da crtamo jednu stranu kvadra). Kada dodje do promjene smjera, u potpunosti se nacrta od pocetnih do krajnjih tocaka tog dijela (u prvom slucaju od pocetnih kordinata kugle pa do trena kad je skrenula) i krece se crtat novi trag sa novim pocetnim tockama do tocaka gdje se kugla trenutno nalazi. To se obavlja 3 puta kako imamo 3 dijela traga. Prozirnost traga je izvedeno postavljanjem gColor4f 4.tog argumenta na 0.5 na taj nacin smo dobili od nas trazenu prozirnost (0.5)

- U main funkciji smo inicijalizirali GLUT, stvorili prozor, postavili timer za update naredbu kako bi imali prikaz animacije. 

Napomena:
	-u initRendering //glClearColor(0.5f, 0.5f, 0.5f, 1.0f); se moze otkomentirati za drugaciju pozadinu.
	-Za ubrzavanje animacije, odnosno povećanje brzine kojom se kugla pomice, pritisnite više puta tipku s.
	
Materijale koje sam koristio za realizaciju:
	- http://www.videotutorialsrock.com/index.php
		Iz navedene stranice sam koristio i video i tekstualne tutorijale za smjernice i zatim sam adaptirao nauceno za moj projekt.
		Vecina se temelji na iznad navedenim tutorialima, ostatak je osnovno znanje programiranja nauceno na ranijim godinama studija.
	
