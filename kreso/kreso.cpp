#include <iostream>
#include <GL/glut.h>
#include <math.h>

using namespace std;

//inicijalizacija svih varijabli
bool kreni=false;

float xlijevosivo=-1.0f;
float xdesnosivo=1.0f;
float xlijevozlatno=xdesnosivo;
float xdesnozlatno=xdesnosivo;

float ydolje=-1.0f;
float ygore=1.0f;
float zispred=-7.0f;
float ziza=-5.0f;

float xkrug=xdesnosivo + 0.5f;

float ykrug1,ykrug2, zkrug1, zkrug2;
float kut;
float radius=2.0f;  //promjer 4px
float alfa = 0.3f;  //prozirnost




void stisnuta_tipka(unsigned char key, int /*x*/, int /*y*/){
    switch (key) {
        case 27: //Escape key
            exit(0);
        case 's':
            kreni=true;
            break;
        case 'S':
            kreni=true;
            break;
        case 'x':
            kreni=false;
            break;
        case 'X':
            kreni=false;
            break;
    }
}


void initRendering() {

    GLfloat svjetlo_boja[] = {1.0f, 1.0f, 1.0f, 1.0f};   //bijela svjetlost
    GLfloat svjetlo_pozicija[] = {xdesnozlatno+3.0f, ygore+2.0f, zispred-3.0f, 1.0f};


    glLightfv(GL_LIGHT0, GL_DIFFUSE, svjetlo_boja);
    glLightfv(GL_LIGHT0, GL_POSITION, svjetlo_pozicija);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);

}


void resize_prozora(int w, int h) {

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);


    glLoadIdentity();
    gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
}

void nacrtaj_krug(){

    //pocetne koordinate kruga
    ykrug1 = (ygore+ydolje)/2;
    zkrug1 = (zispred+ziza)/2;

    //unutrasnjost poluprozirnog kruga
    glColor4f(1.0f,1.0f,1.0f,alfa);

    glBegin(GL_POLYGON);

    for (kut=0.0f;kut<=2*3.141593f;kut+=0.01){   //od 0 do 2*pi
        ykrug2 = ykrug1+sin(kut)*radius;
        zkrug2 = zkrug1+cos(kut)*radius;
        glVertex3f(xkrug,ykrug2, zkrug2);   //koordinate "vrhova"

    }

    glEnd();

    //vanjska bijela kruznica
    glColor3f(1.0,1.0,1.0);
    glBegin(GL_LINE_LOOP);

    for (kut=0.0f;kut<=2*3.141593f;kut+=0.1){
        ykrug2 = ykrug1+sin(kut)*radius;
        zkrug2 = zkrug1+cos(kut)*radius;
        glVertex3f(xkrug,ykrug2, zkrug2);   //koordinate "vrhova"

    }

    glEnd();
}


void nacrtaj_sivu_kocku(){
    glColor3f(0.2f, 0.2f, 0.2f);    //tamno siva boja

    //prednja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(xlijevosivo, ydolje, zispred);
    glVertex3f(xlijevosivo, ygore, zispred);
    glVertex3f(xdesnosivo, ygore, zispred);
    glVertex3f(xdesnosivo, ydolje, zispred);
    glEnd();

    //gornja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(xlijevosivo, ygore, zispred);
    glVertex3f(xdesnosivo, ygore, zispred);
    glVertex3f(xdesnosivo, ygore, ziza);
    glVertex3f(xlijevosivo, ygore, ziza);
    glEnd();

    //desna strana
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(xdesnosivo, ydolje, zispred);
    glVertex3f(xdesnosivo, ydolje, ziza);
    glVertex3f(xdesnosivo, ygore, ziza);
    glVertex3f(xdesnosivo, ygore, zispred);
    glEnd();

    //lijeva strana
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(xlijevosivo, ydolje, zispred);
    glVertex3f(xlijevosivo, ydolje, ziza);
    glVertex3f(xlijevosivo, ygore, ziza);
    glVertex3f(xlijevosivo, ygore, zispred);
    glEnd();

    //donja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(xlijevosivo, ydolje, zispred);
    glVertex3f(xdesnosivo, ydolje, zispred);
    glVertex3f(xdesnosivo, ydolje, ziza);
    glVertex3f(xlijevosivo, ydolje, ziza);
    glEnd();

    //straznja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(xlijevosivo, ydolje, ziza);
    glVertex3f(xlijevosivo, ygore, ziza);
    glVertex3f(xdesnosivo, ygore, ziza);
    glVertex3f(xdesnosivo, ydolje, ziza);
    glEnd();

}


void nacrtaj_zlatnu_kocku(){
    glColor3f(1.0f, 0.8745f, 0.0f);    //zlatna boja

    //prednja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(xlijevozlatno, ydolje, zispred);
    glVertex3f(xlijevozlatno, ygore, zispred);
    glVertex3f(xdesnozlatno, ygore, zispred);
    glVertex3f(xdesnozlatno, ydolje, zispred);
    glEnd();

    //gornja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(xlijevozlatno, ygore, zispred);
    glVertex3f(xdesnozlatno, ygore, zispred);
    glVertex3f(xdesnozlatno, ygore, ziza);
    glVertex3f(xlijevozlatno, ygore, ziza);
    glEnd();

    //desna strana
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(xdesnozlatno, ydolje, zispred);
    glVertex3f(xdesnozlatno, ydolje, ziza);
    glVertex3f(xdesnozlatno, ygore, ziza);
    glVertex3f(xdesnozlatno, ygore, zispred);
    glEnd();

    //lijeva strana
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(xlijevozlatno, ydolje, zispred);
    glVertex3f(xlijevozlatno, ydolje, ziza);
    glVertex3f(xlijevozlatno, ygore, ziza);
    glVertex3f(xlijevozlatno, ygore, zispred);
    glEnd();

    //donja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(xlijevozlatno, ydolje, zispred);
    glVertex3f(xdesnozlatno, ydolje, zispred);
    glVertex3f(xdesnozlatno, ydolje, ziza);
    glVertex3f(xlijevozlatno, ydolje, ziza);
    glEnd();

    //straznja strana
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(xlijevozlatno, ydolje, ziza);
    glVertex3f(xlijevozlatno, ygore, ziza);
    glVertex3f(xdesnozlatno, ygore, ziza);
    glVertex3f(xdesnozlatno, ydolje, ziza);
    glEnd();


}


void iscrtaj_scenu() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //pozicija kamere relativna na koordinate kocke, uvijek gleda u srediste kocke
    gluLookAt(xdesnozlatno+4,ygore+2,zispred+8,(xlijevosivo+xdesnozlatno)/2 ,(ydolje+ygore)/2 ,(zispred+ziza)/2 ,0,1,0);


    if(xkrug>=xlijevosivo){ //ne iscrtava ako je krug prosao cijelu kocku
        nacrtaj_sivu_kocku();
    }

    if(xkrug<=xdesnosivo){  //ne iscrtava ako krug nije jos dosao do kocke
        nacrtaj_zlatnu_kocku();
    }

        nacrtaj_krug();

        glutSwapBuffers();
}


void update(int /*value*/){
    if(kreni==true && xkrug>xlijevozlatno-0.5){
        xkrug-=0.02; //krug putuje prema lijevo
    }
    if(xkrug<=xdesnosivo && xkrug>=xlijevosivo){    //dok je kocka u krugu
        xdesnosivo=xkrug;   //smanjuj sivu kocku
        xlijevozlatno=xkrug;    //i povecavaj zlatnu

    }

    glutPostRedisplay();
    glutTimerFunc(20, update,0); //updejtaj opet za 20 milisekundi

}


int main(int argc, char** argv){

    //inicijalizacija GLUT-a
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 500);
    glutInitWindowPosition(100,100);

    //stvori prozor
    glutCreateWindow("Zadatak 3 - Kreso David");
    initRendering();

    //funkcije za crtanje, tipkanje i promjenu velicine prozora
    glutDisplayFunc(iscrtaj_scenu);
    glutKeyboardFunc(stisnuta_tipka);
    glutReshapeFunc(resize_prozora);
    glutTimerFunc(20, update,0);

    glutMainLoop();
    return 0;
}
