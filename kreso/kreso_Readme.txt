3. zadatak

U programu sam napravio sivu i zlatnu kocku, pa kako se portal pomiče, tako se siva kocka smanjuje, a zlatna povećava. To sam napravio u update funkciji koja se okida svakih 20 milisekundi, te mijenja x koordinatu portala, sive i zlatne kocke.

Sve varijable su inicijalizirane na početku programa, pa za promijeniti veličinu ili poziciju kocke/portala, potrebno je promijeniti samo nekoliko vrijednosti.

Pozicija kruga, osvjetljenja i kamere su relativne na koordinate kocke, tako da kad se recimo promijeni pozicija kocke, automatski se promijeni i pozicija kamere itd.

Program se pokreće bez ikakvih argumenata. Pritiskom na tipku 's' pokreće se animacija, tipka 'x' zaustavlja animaciju, a tipkom 'Esc' se izlazi iz programa.

Reference:
Za pomoć kod izrade ovog programa koristio sam sljedeće internetske stranice:
http://www.videotutorialsrock.com/
http://stackoverflow.com/
https://www.opengl.org/


