#include <iostream>
#include <GL/glut.h>

#define TORSO_HEIGHT 5.0
#define TORSO_SIRINA 1.0
#define TORSO_DEBLJINA 3.0
#define UPPER_ARM_HEIGHT 2.4
#define LOWER_ARM_HEIGHT 2.0
#define UPPER_LEG_RADIUS  0.4
#define LOWER_LEG_RADIUS  0.4
#define LOWER_LEG_HEIGHT 2.5
#define UPPER_LEG_HEIGHT 2.5
#define UPPER_LEG_RADIUS  0.4
#define TORSO_RADIUS 1.0
#define UPPER_ARM_RADIUS  0.4
#define LOWER_ARM_RADIUS  0.4
#define HEAD_HEIGHT 1.5
#define HEAD_RADIUS 1.0
#define VRAT_VISINA 0.5
#define VRAT_RADIUS 0.5
#define RAME_RADIUS 0.6
#define LAKAT_RADIUS 0.6
#define KUK_RADIUS 0.6
#define KOLJENO_RADIUS 0.6
#define ZAPESCE_RADIUS 0.4

//početni kutevi za sve dijelove robota
static GLfloat zglob[24] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            180.0, 0.0, 180.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};


GLUquadricObj *t, *g, *lgr, *ldr, *dgr, *ddr, *ldn, *ddn, *dgn, *lgn, *v, *lr, *ll, *dr, *dl, *lkuk, *lkoljeno, *dkuk, *dkoljeno,
              *lzap, *dzap, *lzglob, *dzglob, *s, *stop;

int rame_smjer = 1;
int smjer = 1;
void glava()
{
   glPushMatrix();
   glTranslatef(0.0, 0.5*HEAD_HEIGHT,0.0);
   gluSphere(g,1.0,10,10);
   glPopMatrix();
}

void vrat()
{
    glPushMatrix();
    glRotatef(-90.0, 1.0, 0.0, 0.0);
    gluCylinder(v,VRAT_RADIUS, VRAT_RADIUS, VRAT_VISINA,10,10);
    glPopMatrix();
}

void torzo() {
    glPushMatrix();
        glBegin(GL_QUADS);
        //Prednja
        glNormal3f(0.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        //Desna
        glNormal3f(1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        //Lijeva
        glNormal3f(0.0f, 0.0f, -1.0f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3, 5.0f, -1.5f);
        //Straznja
        glNormal3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        //Gornja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        //Donja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);

        glEnd();
        glPopMatrix();
}

void lijevo_rame()
{

    glPushMatrix();
    glTranslatef(0.0, 0.7,0.0);
    gluSphere(lr,RAME_RADIUS,10,10);
    glPopMatrix();
}

void lijeva_gornja_ruka()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(lgr,UPPER_ARM_RADIUS, UPPER_ARM_RADIUS, UPPER_ARM_HEIGHT,10,10);
   glPopMatrix();
}

void lijevi_lakat()
{
    glPushMatrix();
    gluSphere(ll,LAKAT_RADIUS,10,10);
    glPopMatrix();
}

void lijeva_donja_ruka()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(ldr,LOWER_ARM_RADIUS, LOWER_ARM_RADIUS, LOWER_ARM_HEIGHT,10,10);
   glPopMatrix();
}

void lijevo_zapesce(){
    glPushMatrix();
    gluSphere(lzap,ZAPESCE_RADIUS,10,10);
    glPopMatrix();
}

void desno_rame()
{
    glPushMatrix();
    glTranslatef(0.0, 0.7,0.0);
    gluSphere(lr,RAME_RADIUS,10,10);
    glPopMatrix();
}

void desna_gornja_ruka()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(dgr,UPPER_ARM_RADIUS, UPPER_ARM_RADIUS, UPPER_ARM_HEIGHT,10,10);
   glPopMatrix();
}

void desni_lakat()
{
    glPushMatrix();
    gluSphere(ll,LAKAT_RADIUS,10,10);
    glPopMatrix();
}

void desna_donja_ruka()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(ddr,LOWER_ARM_RADIUS, LOWER_ARM_RADIUS, LOWER_ARM_HEIGHT,10,10);
   glPopMatrix();
}

void desno_zapesce(){
    glPushMatrix();
    gluSphere(dzap,ZAPESCE_RADIUS,10,10);
    glPopMatrix();
}

void lijevi_kuk(){
    glPushMatrix();
    gluSphere(lkuk,KUK_RADIUS,10,10);
    glPopMatrix();
}

void lijeva_gornja_noga()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(lgn,UPPER_LEG_RADIUS, UPPER_LEG_RADIUS, UPPER_LEG_HEIGHT,10,10);
   glPopMatrix();
}

void lijevo_koljeno(){
    glPushMatrix();
    gluSphere(lkoljeno,KOLJENO_RADIUS,10,10);
    glPopMatrix();
}

void lijeva_donja_noga()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(ldn,LOWER_LEG_RADIUS, LOWER_LEG_RADIUS, LOWER_LEG_HEIGHT,10,10);
   glPopMatrix();
}

void lijevi_zglob(){
    glPushMatrix();
    gluSphere(lzglob,ZAPESCE_RADIUS,10,10);
    glPopMatrix();
}

void desni_kuk(){
    glPushMatrix();
    gluSphere(dkuk,KUK_RADIUS,10,10);
    glPopMatrix();
}

void desna_gornja_noga()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(dgn,UPPER_LEG_RADIUS, UPPER_LEG_RADIUS, UPPER_LEG_HEIGHT,10,10);
   glPopMatrix();
}

void desno_koljeno(){
    glPushMatrix();
    gluSphere(dkoljeno,KOLJENO_RADIUS,10,10);
    glPopMatrix();
}

void desna_donja_noga()
{
   glPushMatrix();
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(ddn,LOWER_LEG_RADIUS, LOWER_LEG_RADIUS, LOWER_LEG_HEIGHT,10,10);
   glPopMatrix();
}

void desni_zglob(){
    glPushMatrix();
    gluSphere(dzglob,ZAPESCE_RADIUS,10,10);
    glPopMatrix();
}

void saka(){
    glPushMatrix();
    glScalef(0.3f, 0.15f, 0.25f);
        glBegin(GL_QUADS);
        //Prednja
        glNormal3f(0.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        //Desna
        glNormal3f(1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        //Lijeva
        glNormal3f(0.0f, 0.0f, -1.0f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3, 5.0f, -1.5f);
        //Straznja
        glNormal3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        //Gornja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        //Donja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glEnd();
        glPopMatrix();
}

void stopalo(){
    glPushMatrix();
    glScalef(0.3f, 0.1f, 0.4f);
        glBegin(GL_QUADS);
        //Prednja
        glNormal3f(0.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        //Desna
        glNormal3f(1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        //Lijeva
        glNormal3f(0.0f, 0.0f, -1.0f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(-1.3, 5.0f, -1.5f);
        //Straznja
        glNormal3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        //Gornja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, 1.5f);
        glVertex3f(1.3f, 5.0f, -1.5f);
        glVertex3f(-1.3f, 5.0f, -1.5f);
        //Donja
        glNormal3f(-1.0f, 5.0f, 1.5f);
        glVertex3f(-1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, 1.5f);
        glVertex3f(1.3f, 0.0f, -1.5f);
        glVertex3f(-1.3f, 0.0f, -1.5f);

        glEnd();
        glPopMatrix();
}
//Funkcija koja se poziva kako bi se prikazali objekti
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    //Namještanje ambijentalnog svjetla
    GLfloat ambientColor[] = {0.2f, 0.2f, 0.2f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
    //Namještanje pozicijskog osvjetljenja
    GLfloat lightColor0[] = {0.5f, 0.5f, 0.5f, 1.0f};
    GLfloat lightPos0[] = {4.0f, 0.0f, 8.0f, 1.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    //Namještanje direktnog osvjetljenja
    GLfloat lightColor1[] = {0.5f, 0.2f, 0.2f, 1.0f};
    //Pozicija svijetla
    GLfloat lightPos1[] = {-0.4f, 0.4f, 0.5f, 0.0f};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
    glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
    //Torzo
    glColor3f(1.0f, 0.7f, 0.0f);
    glRotatef(zglob[0], 0.0, 1.0, 0.0);
    glRotatef(zglob[11], 1.0, 0.0, 0.0);
    torzo();
    //Glava
    glColor3f(1.0f, 0.0f, 0.0f);
    glPushMatrix();
    glTranslatef(0.0, TORSO_HEIGHT+0.9*HEAD_HEIGHT, 0.0);
    glRotatef(zglob[1], 1.0, 0.0, 0.0);
    glRotatef(zglob[2], 0.0, 1.0, 0.0);
    glTranslatef(0.0, -0.5*HEAD_HEIGHT, 0.0);
    glava();
    glColor3f(1.0f, 0.3f, 0.0f);
    glPopMatrix();
    //Vrat
    glPushMatrix();
    glTranslatef(0.0, TORSO_HEIGHT, 0.0);
    vrat();
    glPopMatrix();
    //Lijeva ruka
    //Lijevo rame
    glPushMatrix();
    glColor3f(265.0f, 1.0f, 0.0f);
    glTranslatef(-(TORSO_SIRINA+0.3), 0.8*TORSO_HEIGHT, 0.0);
    glRotatef(zglob[3], 1.0, 0.0, 0.0);
    lijevo_rame();
    //Lijeva nadlaktica
    glColor3f(65.0f, 65.0f, 0.0f);
    glTranslatef(0, RAME_RADIUS, 0.0);
    glRotatef(80, 0.0, 0.0, 1.0);
    lijeva_gornja_ruka();
    //Lijevi lakat
    glColor3f(0.0f, 160.0f, 1.0f);
    glTranslatef(0.0, UPPER_ARM_HEIGHT, 0.0);
    glRotatef(zglob[4], -1.0, 0.0, 0.0);
    lijevi_lakat();
    //Lijeva podlaktica
    glColor3f(0.3f, 50.0f, 0.0f);
    glTranslatef(0.0, LAKAT_RADIUS-0.2, 0.0);
    lijeva_donja_ruka();
    //Lijevo zapešće
    glColor3f(0.3f, 1.0f, 0.0f);
    glTranslatef(0.0, LOWER_ARM_HEIGHT, 0.0);
    lijevo_zapesce();
    //Lijeva šaka
    glColor3f(0.0f, 1.0f, 0.0f);
    glTranslatef(0.0, ZAPESCE_RADIUS-0.2, 0.0);
    saka();
    glPopMatrix();
    //Desna ruka
    //Desno rame
    glPushMatrix();
    glColor3f(265.0f, 265.f, 0.0f);
    glTranslatef(TORSO_SIRINA+0.3, 0.8*TORSO_HEIGHT, 0.0);
    glRotatef(zglob[5], 1.0, 0.0, 0.0);
    desno_rame();
    //Desna nadlaktica
    glColor3f(0.3f, 50.f, 0.3f);
    glTranslatef(0, RAME_RADIUS, 0.0);
    glRotatef(-80, 0.0, 0.0, 1.0);
    desna_gornja_ruka();
    //Desni lakat
    glColor3f(0.0f, 2.5f, 0.7f);
    glTranslatef(0.0, UPPER_ARM_HEIGHT, 0.0);
    glRotatef(zglob[6], 1.0, 0.0, 0.0);
    desni_lakat();
    //Desna podlaktica
    glColor3f(0.0f, 0.7f, 0.5f);
    glTranslatef(0.0, LAKAT_RADIUS-0.2, 0.0);
    desna_donja_ruka();
    //Desno zapešće
    glColor3f(0.0f, 0.5f, 0.5f);
    glTranslatef(0.0, LOWER_ARM_HEIGHT, 0.0);
    desno_zapesce();
    //Desna šaka
    glColor3f(0.0f, 0.5f, 1.5f);
    glTranslatef(0.0, ZAPESCE_RADIUS-0.2, 0.0);
    saka();
    glPopMatrix();
    //Lijeva noga
    //Lijevi kuk
    glPushMatrix();
    glColor3f(0.7f, 0.1f, 0.1f);
    glTranslatef(-(TORSO_SIRINA+0.1), 0.2, 0.0);
    glRotatef(zglob[7], 1.0, 0.0, 0.0);
    lijevi_kuk();
    //Lijevo bedro
    glColor3f(0.7f, 0.0f, 0.3f);
    glTranslatef(0.0, (KUK_RADIUS-0.2), 0.0);
    lijeva_gornja_noga();
    //Lijevo koljeno
    glColor3f(0.7f, 0.0f, 0.6f);
    glTranslatef(0.0, UPPER_LEG_HEIGHT, 0.0);
    glRotatef(zglob[8], 1.0, 0.0, 0.0);
    lijevo_koljeno();
    //Lijeva potkoljenica
    glColor3f(0.4f, 0.0f, 0.7f);
    glTranslatef(0.0, KOLJENO_RADIUS-0.2, 0.0);
    lijeva_donja_noga();
    //Lijevi zglob
    glColor3f(0.2f, 0.0f, 0.6f);
    glTranslatef(0.0, LOWER_LEG_HEIGHT, 0.0);
    glRotatef(zglob[12], 0.0, 1.0, 0.0);
    lijevi_zglob();
    //Lijevo stopalo
    glColor3f(0.2f, 0.0f, 0.8f);
    glTranslatef(0.0, ZAPESCE_RADIUS-0.2, -0.4);
    stopalo();
    glPopMatrix();
    //Desna noga
    //Desni kuk
    glPushMatrix();
    glColor3f(0.7f, 0.0f, 0.5f);
    glTranslatef(TORSO_SIRINA+0.1, 0.2, 0.0);
    glRotatef(zglob[9], 1.0, 0.0, 0.0);
    desni_kuk();
    //Desno bedro
    glColor3f(0.1f, 0.1f, 0.2f);
    glTranslatef(0.0, (KUK_RADIUS-0.2), 0.0);
    desna_gornja_noga();
    //Desno koljeno
    glColor3f(0.1f, 0.1f, 0.5f);
    glTranslatef(0.0, UPPER_LEG_HEIGHT, 0.0);
    glRotatef(zglob[10], 1.0, 0.0, 0.0);
    desno_koljeno();
    //Desna potkoljenica
    glColor3f(0.1f, 0.1f, 0.1f);
    glTranslatef(0.0, KOLJENO_RADIUS-0.2, 0.0);
    desna_donja_noga();
    //Desni zglob
    glColor3f(0.1f, 0.2f, 0.1f);
    glTranslatef(0.0, LOWER_LEG_HEIGHT, 0.0);
    glRotatef(zglob[13], 0.0, 1.0, 0.0);
    desni_zglob();
    //Desno stopalo
    glColor3f(0.1f, 0.3f, 0.1f);
    glTranslatef(0.0, ZAPESCE_RADIUS-0.2, -0.4);
    stopalo();
    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}
//Funkcija za renderiranje objekata
void initRendering() {

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glColor3f(0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST); //Bez ovoga se ne može nacrtati Kvadar
    glEnable(GL_COLOR_MATERIAL); //Omogućimo postavljanje boje materijala
    glEnable(GL_LIGHTING); //Omogućavamo osvjetljenje
    glEnable(GL_LIGHT0); //Omogući svijetlo
    glEnable(GL_LIGHT1); //Omogući svijetlo
    glEnable(GL_NORMALIZE); //Automatsko normaliziranje normala
    glShadeModel(GL_SMOOTH);
    // Crtanje objekata
            g=gluNewQuadric();
            gluQuadricDrawStyle(g, GLU_FILL);
            t=gluNewQuadric();
            gluQuadricDrawStyle(t, GLU_FILL);
            lgr=gluNewQuadric();
            gluQuadricDrawStyle(lgr, GLU_FILL);
            ldr=gluNewQuadric();
            gluQuadricDrawStyle(ldr, GLU_FILL);
            dgr=gluNewQuadric();
            gluQuadricDrawStyle(dgr, GLU_FILL);
            ddr=gluNewQuadric();
            gluQuadricDrawStyle(ddr, GLU_FILL);
            lgn=gluNewQuadric();
            gluQuadricDrawStyle(lgn, GLU_FILL);
            ldn=gluNewQuadric();
            gluQuadricDrawStyle(ldn, GLU_FILL);
            dgn=gluNewQuadric();
            gluQuadricDrawStyle(dgn, GLU_FILL);
            ddn=gluNewQuadric();
            gluQuadricDrawStyle(ddn, GLU_FILL);
            v=gluNewQuadric();
            gluQuadricDrawStyle(v, GLU_FILL);
            lr=gluNewQuadric();
            gluQuadricDrawStyle(lr, GLU_FILL);
            ll=gluNewQuadric();
            gluQuadricDrawStyle(ll, GLU_FILL);
            dr=gluNewQuadric();
            gluQuadricDrawStyle(dr, GLU_FILL);
            dl=gluNewQuadric();
            gluQuadricDrawStyle(dl, GLU_FILL);
            lkuk=gluNewQuadric();
            gluQuadricDrawStyle(lkuk, GLU_FILL);
            lkoljeno=gluNewQuadric();
            gluQuadricDrawStyle(lkoljeno, GLU_FILL);
            dkuk=gluNewQuadric();
            gluQuadricDrawStyle(dkuk, GLU_FILL);
            dkoljeno=gluNewQuadric();
            gluQuadricDrawStyle(dkoljeno, GLU_FILL);
            lzap=gluNewQuadric();
            gluQuadricDrawStyle(lzap, GLU_FILL);
            dzap=gluNewQuadric();
            gluQuadricDrawStyle(dzap, GLU_FILL);
            lzglob=gluNewQuadric();
            gluQuadricDrawStyle(lzglob, GLU_FILL);
            dzglob=gluNewQuadric();
            gluQuadricDrawStyle(dzglob, GLU_FILL);
            s=gluNewQuadric();
            gluQuadricDrawStyle(s, GLU_FILL);
}
//Funkcija pomoću koje se odvija animacija
void idle(void)
{
        if (rame_smjer == 1){
            while( zglob[3] <= 100.0){
                if ( zglob[5] != 70){
                    zglob[5] += 5;
                }
                if (zglob[0] <= 30.0){
                    if ( (zglob[0] >= 20.0) & (zglob[0] <= 30.0)){
                        zglob[11] += 5;
                    }

                        zglob[7] += 7;
                        zglob[8] = 10;

                        zglob[9] -= 7;
                        zglob[10] += 5;
                        zglob[12] -= 3;
                        zglob[13] -= 3;

                    zglob[0] += 6;
                    display();
                }
                zglob[3] += 5;
                if ( zglob[4] <= 65.0){
                    zglob[4] += 2;
                    zglob[6] += 2;
                }
                if ( zglob[3] == 100.0){
                    rame_smjer = 0;
                }
                display();

            }
        }
        if (rame_smjer == 0){
            while( zglob[5] >= -40.0){
              if ( zglob[3] != -70){
                  zglob[3] -= 5;
              }
              if (zglob[0] >= -30.0){
                  if ((zglob[0] <= -20.0) & (zglob[0] >= -30.0)){
                      zglob[11] -= 5;
                  }

                      zglob[7] -= 7;
                      zglob[8] += 5;

                      zglob[9] += 7;
                      zglob[10] = 10;

                      zglob[12] += 3;
                      zglob[13] += 3;

                  zglob[0] -= 6;
                  display();
              }
              zglob[5] -= 5;
              if ( zglob[4] >= -65.0){
                  zglob[4] -= 2;
                  zglob[6] -= 2;
              }
              if (zglob[5] == -40.0){
                  rame_smjer = 1;
              }
              display();
            }
        }
 }

void resize(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (w <= h)
        glOrtho(-10.0, 10.0, -10.0 * (GLfloat) h / (GLfloat) w,
            10.0 * (GLfloat) h / (GLfloat) w, -10.0, 10.0);
    else
        glOrtho(-10.0 * (GLfloat) w / (GLfloat) h,
            10.0 * (GLfloat) w / (GLfloat) h, 0.0, 10.0, -10.0, 10.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(700, 700);
    glutCreateWindow("Dancing robot");
    initRendering();
    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);

    glutMainLoop();
    return 0;
}
