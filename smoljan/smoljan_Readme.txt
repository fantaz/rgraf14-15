Kratak opis projekta i korištenih funkcija:

Projekt je rađen pomoću openGL i glut-a.

Imena funkcija za iscrtavanje dijelova tijela su dana na hrvatskom jeziku radi lakšeg snalaženja u kodu. 
Dodatno, svaka funkcija ima komentar radi dodatnog pojašnjenja.

Model robota je napravljen hijerarhijski, a za "sastavljanje" robota sam koristio sljedeće funkcije:
	
	glTranslatef(x, y, z); - vrši translaciju po koordinatnim osima
	glRotatef(kut, x, y, z); - vrši rotaciju oko koordinatnih osi
	glScalef(x, y, z); - skalira model sukladno definiranim parametrima

Vrijednosti varijabli su unaprijed određene, no većina ih je ipak u konačnici određena metodom pokušaja-pogreške.

Animacija robota se odvija u idle(void) funkciji koja se poziva odmah pri pokretanju programa i odvija se
dok se program ne zatvori.
Animiranje robota se temelji na promjenama kutova (angles), koji su određeni pri inicijalizaciji programa, a
šalju se u glRotatef() funkciju koja odrađuje rotaciju i ponovo poziva funkciju za iscrtavanje robota.

Korištene reference:
https://mudri.uniri.hr/course/view.php?id=1790
http://www.experts-exchange.com/Programming/Game/Q_21162435.html



