Kutija sa vodom

Zelenu �i�anu kutiju predstavlja spremnik vode. odabrana je veli�ina 3x3x3 zbog izgleda pravilne kocke. 
Unutar spremnika nalazi se voda.

Animacija 
Temeljem funkcije glutGet(GLUT_ELAPSED_TIME) znamo vrijeme izvr�avanja animacije i kako vrijeme prolazi, 
kut animacije se pove�ava sve do 90 stupnjeva kada animacija staje.

Voda
Voda je predstavljena pomo�u dva poligona. Izme�u kuta od 0-45 stupnjeva smanjuje se y koordinata gornjeg lijevog poligona, 
dok donji desni poligon miruje. Od 45-90 stupnjeva smanjuje se x koordinata donjeg poligona i na taj na�in imamo
efekt izlijevanja vode iz spremnika. To uspijeva pomo�u matemati�kog izraza za izra�un koordinate to�ke, oblika 
"tan((45 - kut) * PI / 180", dok se za drugi poligon koristi izraz sa 90 stupnjeva.

Svijetlo
Tri su tipa osvjetljenja, i to ambijentalno, difuzno i zrcalno.

Tipke
Pritiskom na tipku "s" animacija zapo�inje. 
Tipka "e" animacija se prestaje izvr�avati, odnosno pauzira.
Pritiskom na Esc tipku izlazi se iz programa. 

Razvoj
Koristio sam Visual Studio 2013 na Win7.
Tako�er i freeGLUT knji�nice.

Izvori i tutoriali:
http://www.opengl-tutorial.org/
http://nehe.gamedev.net/
http://stackoverflow.com/
