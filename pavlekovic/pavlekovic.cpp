#include <iostream>
#include <math.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

using namespace std;

const double PI = 4.*atan(1); //defining pi value

GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };

GLfloat water_ambient[] ={ 0., 0., 0.3, 1. },
water_diffuse[] ={ 0., 0., 1., 1. },
water_specular[] ={ 0., 0., 1., 1. },
water_shininess = 12.8;

GLfloat light_position[] = { 1., 3., 1., 0.};

double angle = 0; //first value is 0, cube in start position
bool isAnimating = false; //initial value is false, no animation
int win_id;
int glut_lastUpdate;

void initGL(){
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE); //normal vector enable

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f); //white background
    glLightfv(GL_LIGHT0, GL_AMBIENT, white_light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);
    glShadeModel(GL_FLAT);//nice constant color on sides
    glDepthFunc(GL_LEQUAL); //depth test
    glEnable(GL_SMOOTH);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
}

//done when window is resized
void resize(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-6.*width / height, 6.*width / height, -6, 6, -20, 20);
    gluLookAt(1, 1, 3, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

//function for keyboard input
void keyboard(unsigned char key, int, int){
    switch (key){
    case 27: exit(0);
             break;
    case 's': glut_lastUpdate = glutGet(GLUT_ELAPSED_TIME); //timer begins at start, value in glut_lastUpdate
            isAnimating = true;
            break;
    case 'e': isAnimating = false;
              break;
    }
}

//
void update(){
    if (isAnimating){
        int lastUpdate = glutGet(GLUT_ELAPSED_TIME); //value of time passed in lastUpdate
        double dt = lastUpdate - glut_lastUpdate;//checking passed time
        if (dt >= 0){
            angle += 0.2;//increment of angle
            if (angle >= 90){
                angle = 90;
                isAnimating = false;
            }
            glutPostRedisplay();
        }
    }
}

//drawing water, getting arguments from render function depending on angle
void renderWater(double z, double x0, double y0, double x1, double y1, double x2, double y2){
    glBegin(GL_TRIANGLES);
    glNormal3f(0, 0, 1);
    glVertex3f(x0, y0, 0);
    glVertex3f(x2, y2, 0);
    glVertex3f(x1, y1, 0);

    glNormal3f(0, 0, -1);
    glVertex3f(x0, y0, -z);
    glVertex3f(x1, y1, -z);
    glVertex3f(x2, y2, -z);
    glEnd();

    glBegin(GL_QUAD_STRIP);
    glNormal3f(x0, y0, 0);
    glVertex3f(x0, y0, 0);
    glVertex3f(x0, y0, -z);

    glNormal3f(x1, y1, 0);
    glVertex3f(x1, y1, 0);
    glVertex3f(x1, y1, -z);

    glNormal3f(x2, y2, 0);
    glVertex3f(x2, y2, 0);
    glVertex3f(x2, y2, -z);
    glEnd();
}

void render(void){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor3f(0., 0., 0.);
    glEnable(GL_LIGHTING);
    glDisable(GL_COLOR_MATERIAL);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, water_ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, water_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, water_specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, water_shininess);

    //water
    glPushMatrix();
    glRotatef(angle, 0, 0, -1); //around -z axis
    //changing y0 when angle between 0 and 45
    if (angle <= 45){
        renderWater(3, -3, 0, 0, 3, 0, 0);
        renderWater(3, -3, 3 * tan((45 - angle) * PI / 180), 0, 3, -3, 0);
    }
    //changing x0 when angle between 45 and 90
    else if (angle<90){
        renderWater(3, -3 * tan((90 - angle) * PI / 180), 0, 0, 3, 0, 0);
    }
    glPopMatrix();

    //black line
    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glColor3f(0., 0., 0.); //black
    glLineWidth(2.); //2px
    glBegin(GL_LINES);
    glVertex3f(-1, -1, 0);//start
    glVertex3f(2, 2, 0);//end
    glEnd();

    //wired cube
    glColor3f(0., 1., 0.);//green
    glLineWidth(4.);//4px
    glPushMatrix();
    glRotatef(angle, 0, 0, -1);
    glTranslatef(-1.5, 1.5, -1.5);
    glutWireCube(3.);
    glPopMatrix();
    glutSwapBuffers();
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowPosition(100, 100);//position
    glutInitWindowSize(800, 600);//size
    win_id = glutCreateWindow("Zadatak4 - Pavlekovic");
    glutDisplayFunc(render);
    glutReshapeFunc(resize);
    glutIdleFunc(update);
    glutKeyboardFunc(keyboard);
    initGL();
    glutMainLoop();
    return 0;
}
