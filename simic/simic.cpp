#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
double z; //inicijalizacija varijable z koja se kasnije koristi za pokrete
GLUquadricObj* quadricO;    //inicijalizacija objekta koji se koristi kod cilindara
GLfloat light[] = {0.8, 0.9, 0.7, 1.0};  /* inicijalizacija svjetla */
GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};  /* inicijalizacija pozicije svjetla na infinite place */


void svjetlo(){ //funkcija za svjetlo
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}

void inicijalizacija(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_NORMALIZE);
    glClearColor(0.4, 0.6, 0.9, 1);//boja pozadine
    quadricO = gluNewQuadric();
    svjetlo();
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
}

void zglob(){ //funkcija koju cu koristiti za iscrtavanja zglobova , laktova, ramena i koljena
    //zglob
       glutSolidSphere(1, 30, 30);
}
void kocka(){
    glutSolidCube(1);//kreiranje kocke
}

void torzo()
{
    // Torzo
    glColor3ub(0, 100, 255); //boja torza
    glPushMatrix();
    glScaled(5, 10, 4);//skaliranje kocke u kvadar
    kocka();//poziv kocke
    glPopMatrix();
}

void glava(){
    //vrat
    glColor3ub(110, 150, 100);//boja vrata
    glTranslated(0, 4.5, 0);//translacija
    glPushMatrix();
    glRotated(-90, 1, 0, 0); //rotacija po x osi
    gluCylinder(quadricO, 2, 1, 2, 10, 10); //kreiranje cilindra

    //glava
    glColor3ub(255, 0, 0);  //boja glave
    glTranslatef(0, 0, 4); //translacija na vrat
    glutSolidSphere(3, 20, 20); //velicina glave
    glPopMatrix();
}
void lijevaRuka(){
       //rame
       glPushMatrix();
       glColor3ub(190, 100, 130); //boja ramena
       glTranslated(3,-1,0); //translacija na lijevi dio torza
       zglob(); //pozivanje funkcije za izradu ramena ( sfera )
       //nadlaktica
       glColor3ub(130, 180, 80); //boja nadlaktica
       glRotated(90, 0, 1, 0);  //rotacija po y osi za pocetnu poziciju
       glRotated(15*sin(z), 1, 0, 0); // animacija pokreta po X osi
       glRotated(25*sin(z), 0, 1, 0); //animacija pokreta po Y osi
       gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);  // kreiranje cilindra
       //lakat
       glColor3ub(180, 80, 80); //boja lakta
       glTranslated(0, 0, 4);   //translacija na dno nadlaktice
       zglob(); //pozivanje funkcije za izradu lakta kao i kod ramena ( sfera )
       //podlaktica
       glColor3ub(140, 180, 180); //boja podlaktice
       glRotated(-45, 1, 1, 0); //rotacija po x,y osi za pocetnu poziciju
       glRotated(2*sin(z), 1, 1, 0);    //animacija pokreta po x,y osi
       gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);  // kreiranje cilindra
       //zapesce
       glColor3ub(0, 0, 255);   //boja zapesca
       glTranslated(0, 0, 4);   //translacija na dno podlaktice
       zglob(); //pozivanje funkcije za izradu zapesca kao i kod lakta i ramena (sfera)
       //saka
       glColor3ub(0, 255, 0);   //boja sake
       glTranslated(0,0,1);     //translacija po zglobu
       glScaled(1, 2, 2);   //skaliranje kocke
       kocka();    //iscrtavanje kocke
       glPopMatrix();
}
void desnaRuka(){
       //rame
       glPushMatrix();
       glColor3ub(255, 100, 0); //boja ramena
       glTranslated(-3,-1,0);  //translacija na desni dio torza
       zglob();     //pozivanje funkcije za izradu ramena ( sfera )
       //nadlaktica
       glColor3ub(255, 150, 0); //boja nadlaktica
       glRotated(15*sin(z)-25, 0, 1, 0);    //animacija pokreta po y osi
       glRotated(25*sin(z), 1, 0, 0);   //animacija pokreta po x osi
       gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);  //kreiranje cilindra
       //lakat
       glColor3ub(255, 0, 150); //boja lakta
       glTranslated(0, 0, 4);   //translacija lakta na dno nadlaktice
       zglob(); //pozivanje funkcije za izradu lakta ( sfera )
       //podlaktica
       glColor3ub(0, 255, 150); //boja podlaktice
       glRotated(90, 0, 1, 0);  //rotacija podlaktice po y osi
       glRotated(30*sin(z)-10, 0, 1, 0);
       gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);  //kreiranje cilindra
       //zapesce
       glColor3ub(0, 150, 255); //boja zapesca
       glTranslated(0, 0, 4);   //translacija na dno podlaktice
       zglob(); //pozivanje funkcije za iscrtavanje zgloba (sfera)
       //saka
       glColor3ub(180, 150, 255);   //boja sake
       glTranslated(0,0,1); //translacija na zglobu
       glScaled(1, 2, 2);   //skaliranje kocke
       kocka();    //iscrtavanje kocke
       glPopMatrix();
}
void lijevaNoga(){
      glPushMatrix();
      //bok
      glColor3ub(60, 160, 110); //boja boka
      glTranslated(2,-9,0);  //translacija na lijevi dio torza i na zeljenu poziciju(dno)
      zglob();  //pozivanje funkcije za iscrtavanja boka ( sfera ) kao i kod lakta,zapesca i ramena
      //natkoljenica
      glColor3ub(160, 60, 110); //boja natkoljenice
      glRotatef(45*sin(z)+30, 0, 1, 0); //animacije pokreta po y osi
      glRotatef(10*sin(z)+50, 1, 0, 0);   //rotacija po x osi
      gluCylinder(quadricO, 0.8, 0.8, 5, 10, 10);   //kreiranje cilindra
      //koljeno
      glColor3ub(160, 110, 60);  //boja koljena
      glTranslated(0,0,5);  //translacija na dno natkoljenice
      zglob();  //pozivanje funkcije za iscrtavanje koljena ( sfera )
      //potkoljenica
      glColor3ub(30, 110, 160); //boja potkoljenice
      glRotated(10*sin(z)+30, 1, 0, 0);   //rotacija po x osi
      gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);   //kreiranje cilindra
      //skocni zglob
      glColor3ub(180, 120, 80); //boja zgloba
      glTranslated(0,0,4);  //translacija na dno potkoljenice
      zglob();  //pozivanje funkcije za iscrtavanje koljena (sfera)
      //stopalo
      glColor3ub(120, 180, 80); //boja stopala
      glRotated(80, 1, 0, 0);   //rotacija po x osi
      glTranslated(0,1,-1);     //translacija po zglobu
      glScaled(2, 2, 4);        //skaliranje kocke
      glutSolidCube(1);         //iscrtavanje kocke
      glPopMatrix();
}
void desnaNoga(){
      glPushMatrix();
      //bok
      glColor3ub(190, 120, 180);    //boja boka
      glTranslatef(-2,-9,0);     //translacija na desni dio torza te postavljanje na zeljenu poziciju ( dno )
      zglob();      //pozivanje funkcije za iscrtavanje boka(sfera)
      //natkoljenica
      glColor3ub(0, 120, 80);   //boja natkoljenice
      glRotatef(45*sin(z)-30, 0, 1, 0); //animacija pokreta po y osi
      glRotatef(20*sin(z), 0, 0, 1);    //animacija pokreta po z osi
      glRotatef(20*sin(z)+10, 1, 0, 0);    //animacija pokreta po x osi
      glRotatef(70, 1, 0, 0);       //rotacija po x osi za "pocetnu" poziciju
      gluCylinder(quadricO, 0.8, 0.8, 5, 10, 10);   //iscrtavanje cilindra
      //koljeno
      glColor3ub(0, 80, 120);   //boja koljena
      glTranslatef(0,0,5);  //translacija na dno natkoljenice
      zglob();      //pozivanje funkcije za iscrtavanje koljena (sfera)
      //potkoljenica
      glColor3ub(120, 80, 120); //boja potkoljenice
      glRotated(10*sin(z)+30, 1, 0, 0);   //rotacija po x osi
      gluCylinder(quadricO, 0.8, 0.8, 4, 10, 10);   //iscrtavanje cilindra
      //skocni zglob
      glColor3ub(40, 80, 80);   //boja zgloba
      glTranslatef(0,0,4);  //translacija na dno potkoljenice
      zglob();  //pozivanje funkcije za iscrtavanje zgloba ( sfera )
      //stopalo
      glColor3ub(160, 80, 80);  //boja stopala
      glRotated(80, 1, 0, 0);   //rotacija po x osi
      glTranslatef(0,1,-1); //translacija po zglobu
      glScaled(2, 2, 4);    //skaliranje kocke
      glutSolidCube(1);     //iscrtavanje kocke
      glPopMatrix();
}
void dance(){   //funkcija za "plesanje" robota.. uz animacije pokreta ruku
                //i nogu koristio sam ovu funkciju radi pokretanja cijelog robota
        GLfloat z = glutGet(GLUT_ELAPSED_TIME)*1000;   //timer varijabla koja se koristi pri pokretima i rotacijama
        glTranslatef(sin(1*z*0.000002),0,0); //pomak robota lijevo-desno (x-os )
        glRotatef(sin(2*z*0.000002),0,0,1);//naginjanje robota po z-osi
        glRotatef(90*z*0.000001,0,1,0); //rotacija cijelog robota za 360 (y-os)
}

void display(void){

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(50, 1, 0.1, 50);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glTranslatef(0, 0, -40); // zoom (-40) da bi se vidio cijeli robot
        dance(); //poziv funkcije dance da se pokrene robot
        torzo(); //poziv funkcije torzo da se iscrta torzo
        glava(); //poziv funkcije glava da se iscrtaju glava i vrat
        lijevaRuka();   //poziv funkcije za iscrtavanje lijeve ruke
        desnaRuka();    //poziv funkcije za iscrtavanje desne ruke
        lijevaNoga();   //poziv funkcije za iscrtavanje lijeve noge
        desnaNoga();    //poziv funkcije za iscrtavanje desne noge
        z+=0.05;        //povecanje varijable z zbog pomaka

        glutSwapBuffers();
        glutPostRedisplay();
}



int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(600, 600);
    glutCreateWindow("The Robot!");
    inicijalizacija();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
