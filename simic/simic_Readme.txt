Opis

Scena:
Na sceni se prikazuje robot kojem je svaki dio tijela obojan u različitu boju. Robot se sastoji od torza(kvadar), vrata("cilindar"),glave(sfera), lijeve i desne ruke [ redom sastavljene: rame(sfera),nadlaktica(cilindar),lakat(sfera),podlaktica(cilindar),zapesce(sfera),saka(kvadar)] te lijeve i desne noge [ redom sastavljene: bok(sfera),natkoljenica(cilindar),koljeno(sfera),potkoljenica(cilindar),skocni zglob(sfera) te stopalo(kvadar)].	Boja pozadine je (0.4, 0.6, 0.9, 1).

Animacija se automatski pokreće, uključuje pokrete ruku,nogu te pomake cijelog robota.

Korištena svjetlost je boje{0.8, 0.9, 0.7, 1.0} na infinite poziciji.


Reference:
www.openGL.org
www.stackoverflow.com
