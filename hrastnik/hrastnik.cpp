#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <cmath>

/**
 * @brief The MyCube class
 *      An empty cube with one side missing.
 *
 * The class MyCube provides an easy way of creating a cube
 * with user defined size, position and angle. It can then
 * easily be rendered with the draw method.
 */
class MyCube
{
private:
    float size;
    float position[3];
    float angle;
    float cube_points[8][3];
    void generateCubePoints();
    void computeNormal(float [3], float [3], float [3], float [3]);
    void drawFace(float [3], float [3], float [3], float [3]);
    void drawCube();
public:
    MyCube (float cube_size);
    void setSize(float cube_size);
    void setPosition3f(float cube_pos_x, float cube_pos_y, float cube_pos_z);
    void draw();
};

/**
 * @brief MyCube::generateCubePoints
 *      Generates the 8 vertices of a cube centered around 0.
 *
 * The numbers from 0 to 7 in binary are:
 *  000, 001, 010, 011, 100, 101, 110, 111
 * If the bits are read as if they were xyz coordinates
 * they can be considered the 8 points of the cube.
 * We use this to generate the cube points.
 * First we extract the bit at the relevant position,
 * then multiply with the size of the cube, then subtract
 * half of the size to center the cube around (0, 0, 0)
 */
void MyCube::generateCubePoints()
{
    float size_half = size*.5;
    for (int i = 0;i < 8;i++) {
        cube_points[i][2] = size * ((i >> 0) & 1) - size_half;
        cube_points[i][1] = size * ((i >> 1) & 1) - size_half;
        cube_points[i][0] = size * ((i >> 2) & 1) - size_half;
    }
}

/**
 * @brief MyCube::MyCube
 *      Constructor for the MyCube class
 * @param cube_size
 *      Size of the cube to be generated.
 *
 * Sets the size of the cube to cube_size and sets the
 * initial position and angle of the cube to 0. Then it
 * generates the cube points.
 */
MyCube::MyCube(float cube_size)
{
    size = cube_size;
    position[0] = position[1] = position[2] = angle = 0.0f;
    generateCubePoints();
}

/**
 * @brief MyCube::setSize
 *      Sets the size of the cube
 * @param cube_size
 *      The new size of the cube.
 */
void MyCube::setSize(float cube_size)
{
    size = cube_size;
    generateCubePoints();
}

/**
 * @brief MyCube::setPosition3f
 *      Sets the new position of the cube
 * @param cube_pos_x, cube_pos_y, cube_pos_z
 *      Floats with the new x,y,z position of the cube
 *
 * Sets the new position of the cube. Note that the cube has to be
 * redrawn to actually see the difference.
 */
void MyCube::setPosition3f(float cube_pos_x, float cube_pos_y, float cube_pos_z)
{
    position[0] = cube_pos_x;
    position[1] = cube_pos_y;
    position[2] = cube_pos_z;
}

/**
 * @brief MyCube::computeNormal
 *      Calculates and normalizes the normal vector for the plane defined with p0, p1 and p3.
 * @param p0
 *      Array of floats holding the x,y,z values of a vertice on the plane
 * @param p1
 *      Array of floats holding the x,y,z values of a vertice on the plane
 * @param p2
 *      Array of floats holding the x,y,z values of a vertice on the plane
 * @param n
 *      The output array where the normal vector will be written to
 *
 * Given three vertices defining a plane, calculates the normal vector
 * using the cross product of two vectors from p1->p0 and p1->p2.
 * The normal vector is then normalized and stored in n[3].
 */
void MyCube::computeNormal(float p0[3], float p1[3], float p2[3], float n[3])
{
    float v0[3], v1[3];
    v0[0] = p2[0]-p1[0];
    v0[1] = p2[1]-p1[1];
    v0[2] = p2[2]-p1[2];

    v1[0] = p0[0]-p1[0];
    v1[1] = p0[1]-p1[1];
    v1[2] = p0[2]-p1[2];

    // Cross product
    n[0] = v0[1]*v1[2] - v0[2]*v1[1];
    n[1] = v0[2]*v1[0] - v0[0]*v1[2];
    n[2] = v0[0]*v1[1] - v0[1]*v1[0];

    // Normalize vector
    float l = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
    n[0] /= l;
    n[1] /= l;
    n[2] /= l;
}

/**
 * @brief MyCube::drawFace
 *      Draws a single face of the cube
 * @param p0-3
 *      Vertices of the face
 *
 * Draws a quad defined with the points p0-3, and computes and sets
 * the normal vector on that face.
 */
void MyCube::drawFace(float p0[3], float p1[3], float p2[3], float p3[3])
{
    float normal[3];
    computeNormal(p0, p1, p2, normal);
    glBegin(GL_QUADS);
    glNormal3fv(normal);
    glVertex3fv(p0);
    glVertex3fv(p1);
    glVertex3fv(p2);
    glVertex3fv(p3);
    glEnd();
}

/**
 * @brief MyCube::drawCube
 *      Draws the 5 faces of the cube
 *
 * Draws the cube and leaving the front face "open".
 */
void MyCube::drawCube()
{
    drawFace(cube_points[0], cube_points[1], cube_points[3], cube_points[2]); // Left
    drawFace(cube_points[4], cube_points[6], cube_points[7], cube_points[5]); // Right
    drawFace(cube_points[0], cube_points[4], cube_points[5], cube_points[1]); // Bottom
    drawFace(cube_points[2], cube_points[3], cube_points[7], cube_points[6]); // Top
    drawFace(cube_points[0], cube_points[2], cube_points[6], cube_points[4]); // Back
//    drawFace(cube_points[1], cube_points[5], cube_points[7], cube_points[3]); // Front
}

/**
 * @brief MyCube::draw
 *      Draws the cube on scene
 *
 * Translates and rotates to get to cube position and then calls drawCube()
 * in order to render the cube.
 */
void MyCube::draw()
{
    glTranslatef(position[0], position[1], position[2]);
    glRotatef(angle, 1.0f, 0.0f, 0.0f);
    drawCube();
}

/**
 * @brief The MySphere class
 *
 * The class MySphere provides an easy way of creating a sphere
 * with user defined radius, position, angle and animation function.
 * It can then easily be rendered with the draw method.
 */
class MySphere
{
private:
    float radius;
    float angle;
    float position[3];
    float camera_position[3]; // This is used to detect if sphere is inside the view frustum
    float camera_angle;
    float camera_ratio;
    void (*anim_func)(int t, float sphere_position[], float *sphere_angle);
    int timer_offset;
public:
    MySphere (float sphere_radius);
    bool isInFrustum();
    void setAnimFunc(void (*)(int t, float sphere_position[], float *sphere_angle));
    void setRadius(float sphere_radius);
    void setPosition3f(float sphere_pos_x, float sphere_pos_y, float sphere_pos_z);
    void setCameraPosition3f(float cam_pos_x, float cam_pos_y, float cam_pos_z);
    void setCameraAngle(float cam_angle);
    void setCameraRatio(float cam_ratio);
    void resetTimer();
    void animate();
    void draw();
};

/**
 * @brief MySphere::MySphere
 *      Constructor for the MySphere class
 * @param sphere_radius
 *      The radius for the generated sphere
 */
MySphere::MySphere(float sphere_radius)
{
    radius = sphere_radius;
    position[0] = position[1] = position[2] = angle = 0.0f;
    anim_func = NULL;
    timer_offset = glutGet(GLUT_ELAPSED_TIME);
}

/**
 * @brief MySphere::setRadius
 *      Sets the radius of the sphere
 * @param sphere_radius
 *      The new radius of the sphere.
 */
void MySphere::setRadius(float sphere_radius)
{
    radius = sphere_radius;
}

/**
 * @brief MySphere::setPosition3f
 *      Sets the new position of the sphere
 * @param sphere_pos_x, sphere_pos_y, sphere_pos_z
 *      Floats with the new x,y,z position of the sphere
 *
 * Sets the new position of the sphere. Note that the sphere has to be
 * redrawn to actually see the difference.
 */
void MySphere::setPosition3f(float sphere_pos_x, float sphere_pos_y, float sphere_pos_z)
{
    position[0] = sphere_pos_x;
    position[1] = sphere_pos_y;
    position[2] = sphere_pos_z;
}

/**
 * @brief MySphere::setCameraPosition3f
 *      Sets the internal camera_position that is used in detecting
 *      if sphere is inside the view frustum
 * @param cam_pos_x cam_pos_y cam_pos_z
 *      Camera xyz coordinates
 */
void MySphere::setCameraPosition3f(float cam_pos_x, float cam_pos_y, float cam_pos_z)
{
    camera_position[0] = cam_pos_x;
    camera_position[1] = cam_pos_y;
    camera_position[2] = cam_pos_z;
}

/**
 * @brief MySphere::setCameraAngle
 *      Sets the internal camera_angle that is used in detecting
 *      if sphere is inside the view frustum
 * @param cam_angle
 *      Camera angle
 */
void MySphere::setCameraAngle(float cam_angle)
{
    camera_angle = cam_angle;
}

/**
 * @brief MySphere::setCameraRatio
 *      Sets the internal camera_ratio that is used in detecting
 *      if sphere is inside the view frustum
 * @param cam_ratio
 *      Camera ratio
 */
void MySphere::setCameraRatio(float cam_ratio)
{
    camera_ratio = cam_ratio;
}

/**
 * @brief MySphere::setAnimFunc
 *      Sets the animation function for the sphere
 * @param animation_func
 *      Pointer to the animation function
 *
 * When MySphere::animate() is called it calls this animation function
 * to calculate the position and angle of the sphere. The animation function
 * should be a function of time. It is passed parameters:
 *      t -> time (this is fetched with glutGet(GLUT_ELAPSED_TIME))
 *      sphere_position -> Array of floats holding the position of the sphere in x,y,z
 *      sphere_angle -> Pointer to the angle of the sphere
 */
void MySphere::setAnimFunc(void (*animation_func) (int t, float sphere_position[3], float *sphere_angle))
{
    anim_func = animation_func;
}

/**
 * @brief MySphere::isInFrustum
 *      Returns a boolean value signaling that the sphere is inside or outside
 *      of the view frustum
 * @return
 *      true - the sphere is inside the frustum
 *      flase - the sphere is outside the frustum
 */
bool MySphere::isInFrustum()
{
    float v[3]; // Camera eye to sphere vector
    v[0] = position[0] - camera_position[0];
    v[1] = position[1] - camera_position[1];
    v[2] = position[2] - camera_position[2];

    float tang = (float) tan(M_PI/180.0 * camera_angle * 0.5);
    return fabs(v[1])-radius < fabs(v[0] * tang) && // Top and bottom
           fabs(v[2])-radius < fabs(v[0] * tang * camera_ratio); // Left and rigth
}

/**
 * @brief MySphere::resetTimer
 *      Resets the timer
 *
 * Resets the timer_offset variable that is passed to the animation function.
 *
 * Use this to reset the animation.
 */
void MySphere::resetTimer()
{
    timer_offset = glutGet(GLUT_ELAPSED_TIME);
}

/**
 * @brief MySphere::animate
 *      Animates the sphere
 *
 * Calls the animation function and passes it the current time - timer_offset.
 * The animation function should update the position and angle of the sphere.
 *
 * Note that the draw() method should be called to acutally see the updated
 * position of the sphere.
 */
void MySphere::animate()
{
    if (anim_func == NULL) return;
    anim_func(glutGet(GLUT_ELAPSED_TIME)-timer_offset, position, &angle);
}

/**
 * @brief MySphere::draw
 *      Draws the sphere on the scene
 *
 * Translates and rotates the sphere to its position and renders it on screen.
 */
void MySphere::draw()
{
    glTranslatef(position[0], position[1], position[2]);
    glRotatef(angle, 1.0f, 0.0f, 0.0f);
    glutSolidSphere(radius, 12, 12);
}

///////////////////////////////////////
//// END OF CLASS DEFINITIONS
///////////////////////////////////////

///////////////////////////////////////
//// GLOBAL VARIABLES
///////////////////////////////////////

float cam[3]; // Camera position
bool animation_started = false;

MyCube *cube = new MyCube(1.0);   // MyCube object with size 1.0
MySphere *sphere = new MySphere(0.5); // MySphere object with radius 0.5

const float red[]     = {1.f, 0.f, 0.f, 1.f}; // RGBA values for the color red
const float blue[]    = {0.f, 0.f, 1.f, 1.f}; // RGBA values for the color blue
const float yellow[]  = {1.f, 1.f, 0.f, 1.f}; // RGBA values for the color yellow


///////////////////////////////////////
//// END OF GLOBAL VARIABLES
///////////////////////////////////////

///////////////////////////////////////
//// FUNCTIONS
///////////////////////////////////////

void handleKeypress(unsigned char key, int, int);
void handleArrowKeypress(int key, int, int);
void handleResize(int w, int h);
void animationFunc1(int ti, float sphere_pos[3], float *sphere_ang);
void animationFunc2(int ti, float sphere_pos[3], float *sphere_ang);
void animationFunc3(int ti, float sphere_pos[3], float *sphere_ang);
void animationFunc4(int ti, float sphere_pos[3], float *sphere_ang);
void render(int ms);
void initRendering();
void drawScene();

/**
 * @brief handleKeypress
 *      Function handling keyboard control
 * @param key
 *      The ascii value of the key pressed
 * @param x, y
 *      X and Y coordinates of the mouse location
 *
 * When a key is pressed this function is called and handles the keypress.
 */
void handleKeypress(unsigned char key, int, int)
{
    switch (key) {
    case 27: // ESC key
        exit(0);
    case 'g': // Begin animation;
        if (!animation_started) {
            sphere->resetTimer();
            animation_started = true;
        }
        break;
    case 's': // Stops animation
        animation_started = false;
        break;
    case 'r': // Reset animation
        sphere->resetTimer();
        sphere->setPosition3f(0, 0, 0);
        break;
    case '1':
        sphere->setAnimFunc(animationFunc1);
        break;
    case '2':
        sphere->setAnimFunc(animationFunc2);
        break;
    case '3':
        sphere->setAnimFunc(animationFunc3);
        break;
    case '4':
        sphere->setAnimFunc(animationFunc4);
        break;
    case '9': // Switch shade model to flat
        glShadeModel(GL_FLAT);
        break;
    case '0': // Switch shade model to smooth
        glShadeModel(GL_SMOOTH);
        break;
    default:
        return;
    }
    glutPostRedisplay(); // Force redisplay
}

/**
 * @brief handleArrowKeypress
 *      Function handling keyboard arrow keys
 * @param key
 *      Int describing the special key pressed
 * @param x, y
 *      X and Y coordinates of the mouse location
 *
 * When a special key is pressed this function is called and handles the keypress.
 */
void handleArrowKeypress(int key, int, int)
{
    switch (key) {
    case GLUT_KEY_LEFT:
        cam[2] += .1;
        break;
    case GLUT_KEY_RIGHT:
        cam[2] -= .1;
        break;
    case GLUT_KEY_UP:
        cam[1] -= .1;
        break;
    case GLUT_KEY_DOWN:
        cam[1] += .1;
        break;
    default:
        return;
    }
    sphere->setCameraPosition3f(cam[0], cam[1], cam[2]); // Update sphere internal camera pos
    glutPostRedisplay(); // Force redisplay
}

/**
 * @brief handleResize
 *      Handles resizing of the window
 * @param w
 *      Width of the window
 * @param h
 *      Height of the window
 *
 * Sets the view port to whole window and calculates new ratio.
 */
void handleResize(int w, int h)
{
    if (h == 0) // Prevent division by 0
        h = 1;
    float ratio = (float) w / h;
    float view_angle = 45.0;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h); // Set viewport to whole window
    gluPerspective(view_angle, ratio, 1.0, 20.0); // Set view angle, aspect, near, far
    sphere->setCameraAngle(view_angle); // This is later used to calculate if sphere is in view frustum
    sphere->setCameraRatio(ratio);

    glMatrixMode(GL_MODELVIEW);
}

/**
 * @brief animationFunc1
 *      The animation function that calculates sphere position
 * @param ti
 *      Elapsed time
 * @param sphere_pos
 *      Array of float values holding the position of the sphere
 * @param sphere_ang
 *      Pointer to the angle of the sphere
 *
 * This function is registered with a MySphere object to update the position
 * of the sphere. The time is converted to float and used in to calculate
 * spheres position on the parabola.
 *
 * This function should be changed to get different (parabolic) trajectories.
 */
void animationFunc1(int ti, float sphere_pos[3], float *sphere_ang)
{
    double t = (float) ti / 1000; // Convert miliseconds to seconds
    *sphere_ang = (360.0 * t)/2;
    if (*sphere_ang > 360.0) *sphere_ang -= 360.0;

    if (sphere_pos[2] < .5) { // Inside the cube
        sphere_pos[0] = 0.0;
        sphere_pos[1] = 0.0;
        sphere_pos[2] = t * 1.;
    }
    else { // Outside the cube
        sphere_pos[0] = (float) 0.0f;
        sphere_pos[1] = (float) - pow(t - 0.5, 2.5) * 0.2; // Parabolic fall
        sphere_pos[2] = (float) t * 1.;
    }
}

/**
 * @brief animationFunc2
 *      Same as animationFunc1()
 */
void animationFunc2(int ti, float sphere_pos[3], float */*sphere_ang*/)
{
    double t = (float) ti / 1000; // Convert miliseconds to seconds

    sphere_pos[0] = cos((t-0.5)*3) * 1.5f;
    sphere_pos[1] = sin((t-0.5)*3) * 1.5f; // Revolve around cube
    sphere_pos[2] = cos((t-0.5)*3) * 1.5f;
}

/**
 * @brief animationFunc3
 *      Same as animationFunc1()
 */
void animationFunc3(int ti, float sphere_pos[3], float */*sphere_ang*/)
{
    double t = (float) ti / 1000; // Convert miliseconds to seconds
    sphere_pos[0] = sin((t-0.5)*3) * 1.5f;
    sphere_pos[1] = cos((t-0.5)*3) * 1.5f;  // Spiral out
    sphere_pos[2] = t * 1.0f;
}

/**
 * @brief animationFunc4
 *      Same as animationFunc1()
 */
void animationFunc4(int ti, float sphere_pos[3], float */*sphere_ang*/)
{
    double t = (float) ti / 1000; // Convert miliseconds to seconds
    if (sphere_pos[2] < .5) { // Inside the cube
        sphere_pos[0] = 0.0;
        sphere_pos[1] = 0.0;
        sphere_pos[2] = t * 1.;
    }
    else { // Outside of the cube
        sphere_pos[0] = 0.0;
        sphere_pos[1] = -1 + fabs((t-.25 - floor(t-.25))-0.5)*4; // Triangle wave
        sphere_pos[2] = t * 1.;
    }
}

/**
 * @brief drawScene
 *      Draws the current scene
 *
 * drawScene is registered to glutDisplayFunc() and redraws the scene
 * when glutPostRedisplay() is called.
 * It handles the translation and rotation of matrices and coloring
 * of the scene.
 */
void drawScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear screen for next frame
    glLoadIdentity(); // Load indentity matrix

    // Set camera position
    gluLookAt(cam[0], cam[1], cam[2],
                  0., cam[1], cam[2],
                  0., 1., 0.);

    // Cube
    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, red); // Set front material color to red
    glMaterialfv(GL_BACK, GL_AMBIENT_AND_DIFFUSE, blue); // Set back material color to blue
    cube->draw();       // Draw the cube in red and blue
    glPopMatrix();

    // Sphere
    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, yellow); // Set front material color to yellow
    if (sphere->isInFrustum())
        sphere->draw();     // Draw the yellow sphere
    if (animation_started)
        sphere->animate();  // Animate if needed
    glPopMatrix();

    glutSwapBuffers();
}

/**
 * @brief initRendering
 *      Initializes rendering
 *
 * Sets sphere animation function, enables lighting, depth test,
 * sets lighting and material properties. Called only once.
 */
void initRendering()
{
    sphere->setAnimFunc(animationFunc1); // Set initial animation function for the sphere

    glEnable(GL_DEPTH_TEST);

    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1); // Enable faces to have different front and back colors
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    float ambient_light[]   = { 0.5f, 0.5f,  0.5f, 1.0f}; // Intensitiy of ambient light
    float diffuse_light[]   = { 0.5f, 0.5f,  0.5f, 1.0f}; // Intensity of diffuse light
    float specular_light[]  = { 1.0f, 1.0f,  1.0f, 1.0f}; // Intensity of specular light
    float position_light[]  = {-3.0f, 1.0f, -2.0f, 1.0f}; // Position of light source

    // Set light properties
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
    glLightfv(GL_LIGHT0, GL_POSITION, position_light);

    float specular_material[] = {1.0f, 1.0f, 1.0f, 1.0f};
    float shine[] = {40};

    // Set material properties
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material); // Make the sphere shiny
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shine);   // Make it shine white

    // Set camera position
    cam[0] = -10;
    cam[1] = -3;
    cam[2] = 4;
    // Set same values in sphere
    // this is later used to detect if sphere is out of view frustum
    sphere->setCameraPosition3f(cam[0], cam[1], cam[2]);

    glClearColor(0.7f, 0.7f, 0.7f, 1.0f); // Background color
}

/**
 * @brief render
 *      Renders the scene
 * @param ms
 *      time in miliseconds until next render
 *
 * Render is first called by glutTimerFunc and after that periodically called
 * every ms miliseconds.
 */
void render(int ms)
{
    glutTimerFunc(ms, render, ms);
    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // Enable double buffering, true colors and depth buffer
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(20, 20);

    // Create the window
    glutCreateWindow("Hrastnik Mateo - RG - Zadatak 2");
    initRendering();

    // Set handler functions
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutSpecialFunc(handleArrowKeypress);
    glutReshapeFunc(handleResize);

    // Call render() every 16ms;
    // This controls the fps of the animation
    // Smaller number -> More fps
    glutTimerFunc(0, render, 16);

    glutMainLoop();

    return 0;
}

