Mateo Hrastnik

Računalna Grafika - Zadatak 2

Zadatak:
Program inicijalno, u gornjem lijevom kutu ekrana, prikazuje crvenu kocku kojoj nedostaje desna strana
i žutu sferu u njoj (slika 2a). Promjer sfere je jednak duljini stranice kocke. Pritiskom tipke g se
pokreće animacija i traje dok se ne pritisne tipka s. Pritiskom tipke Esc potrebno izaći iz programa.
Tijekom animacije se sfera kotrlja po stranici kocke na kojoj leži dok joj centar ne izađe iz kocke. U
tom trenutko počinje padati po paraboličnoj putanju. Putanja je predstavljena isprekidanom linijom.
Animacija se zaustavlja kada sfera izađe izvan ekrana. Uključiti osvjetljenje, sferu i kocku primjetno
osjenčiti. Unutrašnjost kocke obojiti u plavo.

****************************
Opis rada programa:

	U programu se stvara jedan objekt klase MyCube i jedan objekt klase MySphere. U main funkciji 
programa najprije se inicjalizira GLUT knjižnica funkcija, te se stvara prozor dimenzija 800x600
koji koristi double buffer, Z-bufferom i podržava RGB boje.

	Funkcija initRendering() namješta neke postavke i priprema animaciju. "Zakači" se prva 
funkcija za animiranje na sferu, uključuje se korištenje Z-buffera, podešavaju se prametri
svijetla te materijala koji će vrijediti za sve renderirane objekte. Uključuje se i mogućnost
da materijali imaju dvije boje, tj. da poligoni imaju različitu prednju i stražnju boju.
Također se podešava inicjalna pozicija kamere. Ova funkcija se poziva samo jednom.

	Nakon vraćanja iz funkcije postavljaju se funkcije za display, reshape, i funkcije za upravljanje
tipkovnicom.

	Sa glutTimerFunc() pokreće se timer koji, nakon što izbroji određeno vrijeme, poziva neku funkciju
i šalje joj jedan parametar.

	Ta se funkcija koristi za kontroliranje FPS-a animacije. Nakon poziva glutMainLoop() funkcije 
pokreče se timer koji poziva funkciju render() periodički svakih 16 ms. Funkcija render() zove
glutPostRedisplay() koji zove funkciju za crtanje scene.

	Funkcija drawScene() crta scenu tako što namjesti prednju boju poligona na crvenu a stražnju na plavu,
tada nacrta kocku. Zatim namjesti prednju boju na žutu i iscrta sferu, međutim sfera se iscrtava samo
ukoliko je u piramidi pogleda, a sfera se animira samo ako je globalna varijabla animation_started 
postavljena na 1.


*************************	
Kontroliranje programa:
	Program se može kontrolirati tipkovnicom.
		G - Pokretanje animacije
		S - Zaustavljanje animacije
		R - Reset animacije
		(1-4) - Promjena animacijske funkcije sfere
		9 - Postavljanje shade modela na GL_FLAT
		0 - Postavljanje shade modela na GL_SMOOTH
		Arrow Keys - Pomicanje scene u smjeru pretisnute strelice 
	 
