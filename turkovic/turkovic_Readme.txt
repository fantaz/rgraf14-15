--------------------Dokumentacija--------------------
----------Deni Turković------------------------------


Zadatak:
Hijerarhijskim modeliranjem kreirati robota koji se sastoji od torza (kvadar), vrata(cilindar), glave
(sfera), dvije nadlaktice (cilindar), podlaktice(cilindar), šake (kvadar), natkoljenice (cilindar), podkoljenice
(cilindar) i stopala (kvadar), kao i pripadajućih zglobova (sfere). Svaki dio robota obojati različitom
bojom.
Animirati jedan plesni pokret, odnosno pokret koji uključuje pomake svih dijelova tijela robota.


Popis korištenih funkcija (ime svake funkcije oznacava za sta se koristi):
	void Crtaj_torzo(void)
	void Crtaj_vrat_glava(void)
	void Crtaj_ruku(int side)
	void Crtaj_nogu(int side)
	void Crtaj_sipku(void)
	void prikazi(void)
	void osvjezi (int , int )

Opis funkcionalnosti:
Program prikazuje animaciju robota koji radi zgibove. Pokretanjem animacije kamera gleda u robota s leđa te se automatski pokreću animacija i pomak kuta kamere.
Animacija je dobivena tako da se mijenjaju kutovi lakta i ramena te podizanjem i spustanjem cijelog robota.
