#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

void init(void );
void Crtaj_torzo(void);
void Crtaj_vrat_glava(void);
void Crtaj_ruku(int side);
void Crtaj_nogu(int side);
void Crtaj_sipku(void);
void prikazi(void);
void osvjezi (int , int );

float L_rame = 180.0;   //Kut lijevog ramena koristen za animaciju
float L_lakat = 45.0;   //Kut lijevog lakta koristen za animaciju
float R_rame = 0.0;     //Kut desnog ramena koristen za animaciju
float R_lakat = -45.0;  //Kut desnog lakta koristen za animaciju

float zgib = 0.0;
float rotacija = 0.0;   //Kut koristen za rotaciju pogleda kamere
int top = 1;            //Pomoćna varijabla za određivalje da li se robot podiže ili spušta


int main(int argc, char** argv){
       glutInit(&argc, argv);
       glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
       glutInitWindowSize (1000, 800);
       glutInitWindowPosition (100, 100);
       glutCreateWindow (argv[0]);

       init ();

       glutDisplayFunc(prikazi);
       glutReshapeFunc(osvjezi);
       glutMainLoop();

       return 0;
}

void init(void){
        glClearColor (0.0, 0.0, 0.0, 0.0);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_COLOR_MATERIAL);
        glDepthFunc(GL_LEQUAL);
        glShadeModel (GL_FLAT);
        glShadeModel (GL_SMOOTH);

        //komponente osvjetljenja, pozicija kamere
        GLfloat mat_specular[] = { 1.0, 1.0, 0.0, 1.0 };
        GLfloat mat_diffuse[] = { 0.8, 0.8, 0.8, 1.0 };
        GLfloat mat_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
        GLfloat mat_shininess[] = { 10.0 };
        GLfloat light_position[] = { 0.0, 10.0, 0.0, 2.0 };
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
        glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}

void Crtaj_sipku(){
    GLUquadricObj *obj = gluNewQuadric();
    glPushMatrix();

    //dva cilindra koji nam predstavljaju sipke za koje se robot drzi
    glColor3f(0.0,255.0,0.0);
    glRotatef(90, 0.0, 1.0, 0.0);
    glTranslatef(0, 0.0 - zgib, 0.75);
    gluCylinder(obj, 0.1, 0.1, 2, 100, 100);

    glTranslatef(0, 0.0, 5.5);
    gluCylinder(obj, 0.1, 0.1, 2, 100, 100);
    glPopMatrix();
}

void Crtaj_torzo(void){
        glPushMatrix();

        //torso
        glColor3f(1.0,1.0,0.0);
        glScalef(1.0,2.0,0.5);
        glTranslatef(0.0,0.25,0.0);
        glutSolidCube(2.5);
        glPopMatrix();

}

void Crtaj_vrat_glava(void){
        GLUquadricObj *obj = gluNewQuadric();
        glPushMatrix();

        //vrat - crveni cilindar
        glColor3f(255.0,0.0,0.0);
        glRotatef(90, 1.0, 0.0, 0.0);
        gluCylinder(obj, 0.3, 0.3, 1, 100, 100);
        glPushMatrix();

        //glava - plava sfera
        glColor3f(0.0,0.0,255.0);
        glTranslatef(0.0, 0.0,-0.5);
        glutSolidSphere(1,100,100);
        glPopMatrix();
}

void Crtaj_ruku(int side){
        GLUquadricObj *obj = gluNewQuadric();
        glPushMatrix();

        //rame - zelena sfera
        glColor3f(0.0,255.0,0.0);
        glTranslatef(0.0, 0.0, 0.0);
        glutSolidSphere(0.4,100,100);

        //nadlaktica - crveni cilindar
        glColor3f(255.0,0.0,0.0);
        glRotatef(90, 0.0, 1.0, 0.0);
        //koristimo kako ne bi imali dvije funkcije(za svaku ruku posebno)
        //0 za lijevu, 1 za desnu ruku
        if(side == 0)
        glRotatef(L_rame, 1.0, 0.0, 0.0);
        if(side == 1)
        glRotatef(R_rame, 1.0, 0.0, 0.0);
        gluCylinder(obj, 0.4, 0.2, 2, 100, 100);

        //lakat - zelena sfera
        glColor3f(0.0,255.0,0.0);
        glTranslatef(0.0, 0.0, 2);
        glutSolidSphere(0.2,100,100);

        //podlaktica - plavi cilindar
        glColor3f(0.0,0.0,255.0);
        //provjera za lijevu ili desnu podlakticu, isto kao kod nadlaktice
        if(side == 0)
        glRotatef(L_lakat, 1.0, 0.0, 0.0);
        if(side == 1)
        glRotatef(R_lakat, 1.0, 0.0, 0.0);
        gluCylinder(obj, 0.2, 0.125, 2, 100, 100);

        //rucni zglob - zelena sfera
        glTranslatef(0.0, 0.0, 2);
        glColor3f(0.0,255.0,0.0);
        glutSolidSphere(0.125,100,100);

        //saka - crveni kvadar
        glColor3f(255.0,0.0,0.0);
        glTranslatef(0.0, 0.0, 0.5);
        glutSolidCube(0.5);
        glTranslatef(0.0, 0.0, 0.5);
        glutSolidCube(0.5);

        glPopMatrix();
}

void Crtaj_nogu(int side){
        GLUquadricObj *obj = gluNewQuadric();

        glTranslatef(0.0, 0.0, 0.0);
        glPushMatrix();

        //kukovi - zelena sfera
        glColor3f(0.0,255.0,0.0);
        glTranslatef(0.0, 0.0, 0.0);
        glutSolidSphere(0.6,100,100);

        //nadkoljenica - crveni cilindar
        glColor3f(255.0,0.0,0.0);

        //provjera za lijevu ili desnu nadkoljenicu
        if(side == 0)
        glRotatef(90, 1.0, 0.0, 0.0);
        if(side == 1)
        glRotatef(90, 1.0, 0.0, 0.0);
        gluCylinder(obj, 0.6, 0.4, 2.5, 100, 100);

        //koljeno - zelena sfera
        glColor3f(0.0,255.0,0.0);
        glTranslatef(0.0, 0.0, 2.5);
        glutSolidSphere(0.4,100,100);

        //podkoljenica - plavi cilindar
        glColor3f(0.0,0.0,255.0);

        //provjera za lijevu ili desnu podkoljenicu
        if(side == 0)
        glRotatef(0, 1.0, 0.0, 0.0);
        if(side == 1)
        glRotatef(0, 1.0, 0.0, 0.0);
        gluCylinder(obj, 0.4, 0.2, 2, 100, 100);

        //nozni zglob - zelena sfera
        glColor3f(0.0,255.0,0.0);
        glTranslatef(0.0, 0.0, 2);
        glutSolidSphere(0.2,100,100);

        //stopalo - crveni kvadar
        glColor3f(255.0,0.0,0.0);
        glTranslatef(0.0, 0.0,  0.5);
        glutSolidCube(0.5);
        glTranslatef(0.0, 0.5, 0.0);
        glutSolidCube(0.5);
        glTranslatef(0.0, 0.5, 0.0);
        glutSolidCube(0.5);

        glPopMatrix();
}

void prikazi(void){
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glColor3f (0.0, 0.0, 0.0);
        glLoadIdentity ();
        gluLookAt (0.0, 0.0, 17.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

        //rotacije
        glRotatef(rotacija,0,1.0,0.0);

        //koristimo za animaciju
        glTranslatef(0.0, zgib,0.0);
        glPushMatrix();

        //desna ruka
        glPushMatrix();
        glTranslatef(1.35, 3.25, 0.0);
        glRotatef(45,0.0,0.0,1.0);
        Crtaj_ruku(1);
        glPopMatrix();

        //lijeva ruka
        glPushMatrix();
        glTranslatef(-1.35, 3.25, 0.0);
        glRotatef(-45,0.0,0.0,1.0);
        Crtaj_ruku(0);
        glPopMatrix();

        //desna noga
        glPushMatrix();
        glTranslatef(0.75,-2.5,0.0);
        Crtaj_nogu(1);
        glPopMatrix();

        //lijeva noga
        glPushMatrix();
        glTranslatef(-0.75,-2.5,0.0);
        Crtaj_nogu(0);
        glPopMatrix();

        //torzo
        glPushMatrix();
        Crtaj_torzo();
        glPopMatrix();

        //vrat i glava
        glPushMatrix();
        glTranslatef(0.0,4.2,0.0);
        Crtaj_vrat_glava();
        glPopMatrix();

        //sipka
        glPushMatrix();
        glTranslatef(-4.5, 7.2, 0.0);
        Crtaj_sipku();

        glPopMatrix();

        glFlush ();

        if(L_lakat > 140) top = 0;
        if(L_lakat < 30)  top = 1;
        if(top){
            L_rame -= 1;
            L_lakat += 1;
            R_rame += 1;
            R_lakat -= 1;
            zgib += 0.03;
            rotacija += 0.5;
        }
        else{
            L_rame += 4;
            L_lakat -= 4;
            R_rame -= 4;
            R_lakat += 4;
            zgib -= 0.12;
            rotacija += 0.5;
        }
        glutPostRedisplay();
}

void osvjezi (int width, int height){
       glViewport (0, 0, (GLsizei) width, (GLsizei) height);
       glMatrixMode (GL_PROJECTION);
       glLoadIdentity ();
       glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
       glMatrixMode (GL_MODELVIEW);
}