**********Dokumentacija*Assigment1*************************************************************************
***********************************************************************************************************
***************************************************Dominik Beževan*****************************************

NAPOMENA---------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
Objasnjenja funkcionalnosti metoda, varijabli i klasa dana su u komentarima unutar .cpp datoteke
radi lakšeg iščitavanja.

ZADATAK----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
Program inicijalno prikazuje zelenu sferu u gornjem lijevom kutu ekrana. Pritiskom tipke 's' se pokreće
animacija i traje dok se ne pritisne tipka 'x'. Pritiskom tipke 'Esc' potrebno izaći iz programa. 

Tijekom animacije se sfera giba po putanji koja je nacrtana isprekidanom linijom prikazanom na Slici 4. 
Sfera za sobom ostavlja trag tamnocrvene boje i visine jednake dijametru sfere. Animacija se zaustavlja 
kada sfera dođe do donjeg desnog ruba ekrana. 

Uključiti osvjetljenje te trag i sferu primjetno osjenčiti. Trag mora biti proziran, alfa=0.5

***********************************************************************************************************

POPIS KORIŠTENIH FUNKCIJA----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

	void init()
	void display()
	void reshape()
	void processNormalKeys(unsigned char key, int, int)
	void processSpecialKeys()
	
	sphereWithTrail::sphereWithTrail -->konstruktor istoimene klase

	sphereWithTrail::easeInOutExpo()
	void sphereWithTrail::nextSegment()
	void sphereWithTrail::drawTrails()
	void sphereWithTrail::calculateCoords()
	void sphereWithTrail::draw()
	void sphereWithTrail::drawObject()

OPIS FUNCKIONALNOSTI---------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
Na samom pocetku koda definirane su sve globalne varijable koje se koriste unutar gore navedenih funkcija 
(izuzevši lokalnih pomoćnih unutar pojedinih funkcija).

Pozivamo konstruktor koji nam služi za pripremu upotrebe varijabli (kao članskih) unutar stvorenog objekta.

___________________________________________________________________________________________________________
Funkcionalnost se dijeli na 4 koraka: 	
	*računanja koordinata kretanja
	*crtanja isprekidane linije kretanja
	*crtanja traga 
	*crtanja objekta tj. sfere.


*******************
*Metoda drawTrails*
*******************

pozivima na metode glLineStipple, glEnable i glBegin, 
uz izračun koordinata putanja kretanja sfere (glVertex3f), vrsi iscrtavanje isprekidane linije.

U metodi se također redom pozivaju i metode calculateCoords, drawTrails i drawObject() kojima se potom izvršava animacija.

Metoda calculateCoords računa vrijednosti koordinata po kojima se sfera krece. Put se sastoji od 2 segmenta kretanja, na početku je
funkcije postavljena IF petlja koja zaustavlja animaciju nakon završetka 2. segmenta.

U metodi mjerimo vrijeme početka segmenta, koje se sprema u varijablu time_since_begin. Također biljezimo razliku između trenutnog i prošlog vremena.
Iteracijom if(D_time > neki_broj) možemo regulirati brzinu kretanja sfere odnosno izvođenja animacije.
Veća vrijednost varijable neki_broj usporava animaciju dok manja ubrzava.

Switch() petlja sa caseovima koordinira operacije ovisno o trenutnom segmentu. Poziva se metoda easeInOutExpo() koja brine o 'glatkoći' prolaska sfere. Može se
reći da stilzira način prolaska kugle tako da regulira brzinu kretanja sfere (primjetno kod izmjene prvog i drugog smjera kretanja s međuprijelazom)

Na kraju se iteracijom provjerava frames_2 te u true slučaju  prelazi na idući segment.

*******************
*Metoda drawTrails*
*******************

Metoda za crtanje traga. Na samom početku se definira boja i prozirnost funkcijom glColor4f(). GlBegin(GL_QUADS) delimitira vertekse, odabiremo GL_QUADS jer
tretira svaku grupu od 4 verteksa neovisno. For petlju postavljamo kako se trag ne bi izgubio nakon svake promjene smjera, da bi bio cjelokupan.

Slijed operacija nakon for petlje osigurava da se trag iscrtava u realnom vremenu.  
 
*******************
*Metoda drawObject*
*******************

Metoda za iscrtavanje sfere. Metodom glTranslatef() trenutnu matricu mnozimo sa translacijskom matricom. Definiramo boju sfere na zelenu, 
i pozivamo glutSolidSphere() kojom crtamo sferu.

sphereWithTrail sphere(-8.0f, 1.0f, -6.0f, 0.5f, 5, 150, 75) predstavlja objekt sfere. Vrijednosti koje sadrži kao argumente predstavljaju varijable definirane
u konstruktoru kao članske. Uloga svake od pojedinih varijabli nadopisana je u komentaru unutar .cpp datoteke
















