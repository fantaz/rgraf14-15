#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>
#include <iostream>
#include <vector>

//položaj kamere
float cam_x = 0.0f;
float cam_y = 6.0f;
float cam_z = 8.0f;

//rotacija kamere oko x i y-osi
float angle_x = 0.0f;
float angle_y = 0.0f;

//koordinate točke u koju je kamera usmjerena
float center_x = 0.0f;
float center_y = 0.0f;
float center_z = 0.0f;


class sphereWithTrail{
    private:
        //spremanje pocetnih i krajnjih koordinata traga
        struct coordPairs{
            float x0, y0, z0;
            float x1, y1, z1;
        };

       float x;//trenutne koordinate objekta
       float y;//
       float z;//
       float start_x;
       float start_z;
       float radius; //polumjer sfere
       int now_segment; //trenutni segment animacije
       int trail_segments_num; //broj pojedinih segmenata

       int frames_1; //vrijeme trajanja prvog i drugog segmenta
       int frames_2; //

       long frame; //trenutni frame
       int old_time_since_begin; //vrijeme koje služi za kontrolu FPS-a

        std::vector<coordPairs> trailSegments; //vektor koji sadrži segmente traga

        void drawTrails(); //metoda crtanja traga
        void nextSegment(); //metoda sljedeceg segmenta
        void calculateCoords();//računanje koordinata kretanja sfere
        void drawObject(); //prikaz sfere
        float easeInOutExpo(float, float, long); //metoda određivanja brzine animacije na pojedinim dijelovima

    public:
        bool go; //bool varijable za zaustavljanje i pokretanje animacije
        void draw(); // metoda crtanja animacije
        sphereWithTrail(float, float, float, float, int, int, int); //constructor
};

/*sphereWithTrail::sphereWithTrail()
*
* konstruktor klase sphereWithTrail
*
* start_val - koordinata s koje je zapocelo kretanje
* d_value - vrijednost za koju se mijenja pocetna koordinata
* d_frame - vrijeme za koje se start_val mijenja u d_value
*
*/
sphereWithTrail::sphereWithTrail(float x, float y, float z, float radius, int trail_segments_num, int frames_1, int frames_2){
    this -> x = x; //koordinate sfere
    this -> y = y; //
    this -> z = z; //

    this -> frames_1 = frames_1;//vrijeme iscrtavanja segmenata 1 i 2
    this -> frames_2 = frames_2;//
    this -> trail_segments_num = trail_segments_num; //trail_segments_num - kolicina segmenata
    this -> radius = radius;//polumjer sfere

    coordPairs tmp = {x, y, z, x, y, z}; //početni trag koji služi za praćenje početne točke animacije sfere

    start_x = x;
    start_z = z;
    frame = 0;
    old_time_since_begin = 0;

    now_segment = 0;
    go = false;

    trailSegments.push_back(tmp);
}

/*float sphereWithTrail::easeInOutExpo()
* kontrola ubrzanja odnosno ponašanja objekta u animaciji
* metoda za kretanje sfere
*
* start_val - koordinata s koje je zapocelo kretanje
* d_value - vrijednost za koju se mijenja pocetna koordinata
* d_frame - vrijeme za koje se start_val mijenja u d_value
*
*/
float sphereWithTrail::easeInOutExpo(float start_val, float d_value, long d_frame){

    float new_val;
    float tmp_frame = frame;

    tmp_frame /= d_frame/2.0f;

    if(tmp_frame < 1) new_val = d_value/2.0f * pow(tmp_frame, 3.0f) + start_val;

    else{
        tmp_frame -= 2;
        new_val = d_value/2.0f * (pow(tmp_frame, 3.0f) + 2) + start_val;
    }

    return new_val; //vrijednost koordinate u trenutnom frame-u
}

/*void sphereWithTrail::nextSegment()
*
* metoda u kojoj se spremaju koordinate traga, postavlja frame na 0 i radi push na idući segment
*
*/
void sphereWithTrail::nextSegment(){

    now_segment++;

    frame=0;
    start_x = x;
    start_z = z;

    coordPairs tmp = {trailSegments.back().x1,
                      trailSegments.back().y1,
                      trailSegments.back().z1,
                      x, y, z};

    trailSegments.push_back(tmp);
}

/*void sphereWithTrail::drawTrails()
*
* metoda za iscrtavanje tragova
*
*/
void sphereWithTrail::drawTrails(){

    glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
    coordPairs last_points;

    for(std::vector<coordPairs>::iterator it = trailSegments.begin(); it != trailSegments.end(); it++) {

           glBegin(GL_QUADS);

               glVertex3f(it->x0, it->y0-radius, it->z0);
               glVertex3f(it->x1, it->y1-radius, it->z1);
               glVertex3f(it->x1, it->y1+radius, it->z1);
               glVertex3f(it->x0, it->y0+radius, it->z0);

           glEnd();
           last_points = *it;
    }

    glBegin(GL_QUADS);

        glVertex3f(last_points.x1, last_points.y1-radius, last_points.z1);
        glVertex3f(x, y-radius, z);
        glVertex3f(x, y+radius, z);
        glVertex3f(last_points.x1, last_points.y1+radius, last_points.z1);

    glEnd();

}

/*void sphereWithTrail::calculateCoords()
*
* metoda za izračun nove pozicije sfere, implementiran easeInOutExpo
*
*/
void sphereWithTrail::calculateCoords(){
    if(now_segment<3){ //IF petlja koja zaustavlja kretanje sfere odnosno animaciju u trenutku kada sfera dodje do donjeg desnog ruba
        if(go && now_segment != trail_segments_num){
            int time_since_begin = glutGet(GLUT_ELAPSED_TIME);
            int D_time = time_since_begin - old_time_since_begin;

            if(D_time > 10){
                old_time_since_begin = time_since_begin;

                switch (now_segment % 2){
                    case 0:
                        z = easeInOutExpo(start_z, 3.5f, frames_1);
                        x = easeInOutExpo(start_x, 7.0f, frames_1);

                        frame++;
                        if(frame == frames_1) nextSegment();
                    break;

                    case 1:
                        z = easeInOutExpo(start_z, 3.5f, frames_2);
                        x = easeInOutExpo(start_x, -2.5f, frames_2);

                        frame++;
                        if(frame == frames_2) nextSegment();
                    break;

                }
            }
        }
    }
}

/*void sphereWithTrail()::draw()
*
* metoda za poziv iscrtavanja animacije
* Ubaceni dodatak: pozivi na metode za iscrtavanje putanje(iscrtkane linije) po kojoj se sfera krece
*
*/
void sphereWithTrail::draw() {

    glLineStipple(5, 0xAAAA);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);

    glVertex3f(-8.0,1.0,-6.0);
    glVertex3f(-1.00001,1,-2.5);

    glVertex3f(-1.00001,1,-2.5);
    glVertex3f(-3.49998,1,0.999963);

    glVertex3f(-3.49998,1,0.999963);
    glVertex3f(3.50001,1,4.49996);
    glEnd();

    calculateCoords();
    drawTrails();
    drawObject();
}

/*void sphereWithTrail::drawObject()
*
* metoda za crtanje sfere
*
*/
void sphereWithTrail::drawObject(){

    glTranslatef(x,y,z);
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glutSolidSphere(radius, 30, 30);
}

sphereWithTrail sphere(-8.0f, 1.0f, -6.0f, 0.5f, 5, 150, 75); //objekt (sfere) koji sadrži metodu koju se koristi


/* void processNormalKeys()
 *
 * osnovni eventi:::
 *
 * start - s, stop -x, restart -r, esc - exit
 *
 */
void processNormalKeys(unsigned char key, int, int){
    switch(key){
        case 27:
            exit(0);
        break;

        case 'r':
            sphere = sphereWithTrail(-8.0, 1.0, -6.0f, 0.5f, 3, 300, 150);
        break;

        case 's':
            sphere.go = true;
        break;

        case 'x':
            sphere.go = false;
        break;
    }
}

/* VOID processSpecialKeys()
 *
 * interakcija s tipkovnicom (kamera gore, dolje, lijevo, desno)
 *
 */
void processSpecialKeys(int key, int, int){
    switch (key) {
           case GLUT_KEY_LEFT:
                cam_x -= 0.1f;
                center_x -= 0.1f;
           break;
           case GLUT_KEY_UP:
                cam_y += 0.1f;
                center_y += 0.1f;
           break;
            case GLUT_KEY_RIGHT:
                 cam_x += 0.1f;
                 center_x += 0.1f;
            break;
            case GLUT_KEY_DOWN:
                 cam_y -= 0.1f;
                 center_y -= 0.1f;
            break;

    }
}


void init(void) {

    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH); //sjena
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND); //blendanje/prozirnost (primjena - boja traga sferer ide u tamno crvenu
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLfloat light_coord[] = {2.0, 7.0, 0.0, 1.0}; //koordinate izvora svjetla
    glLightfv(GL_LIGHT0, GL_POSITION, light_coord); //postavke LIGHT0
    glEnable(GL_LIGHTING); //enable komponente svjetlosti
    glEnable(GL_LIGHT0); //enable svjetla LIGHT0
}


void display(void){
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //boja prozora(alpha)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //sfera se vidi

    glLoadIdentity();

    gluLookAt(cam_x, cam_y, cam_z, center_x, center_y, center_z, 0.0f, 0.5f, 0.0f); //transformacija pogleda - promjena upX - obrnuta perspektiva

    glRotatef(angle_y, 0.0f, 1.0f, 1.0f); //moze se mijenjati - rotacija objekta oko y-osi
    glRotatef(angle_x, 1.0f, 0.0f, 1.0f); // -||- oko x-osi

    sphere.draw();
    glutSwapBuffers();
}

/* void reshape()
 *
 * poziva se jednom kod inicijalizacije programa i svaki put kada se prozor pomakne ili promjeni veličina
 *
 */
void reshape(int w, int h){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(41, (GLfloat)w / (GLfloat)h, 1.0f, 50.0f); //perspektiva
    glMatrixMode(GL_MODELVIEW);
}

int main (int argc, char **argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

    glutInitWindowSize(800, 500); //inicijalna vrijednost
    glutInitWindowPosition(200, 200); //pozicija prozora
    glutCreateWindow("Assignment_1 - Bezevan Dominik");

    init();

    glutKeyboardFunc(processNormalKeys);
    glutSpecialFunc(processSpecialKeys);

    glutDisplayFunc(display);
    glutIdleFunc(display);

    glutReshapeFunc(reshape);

    glutMainLoop();

    return 0;
}

