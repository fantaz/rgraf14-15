#include <GL/glut.h>

//for dancing
float danceMove=-20;
int help=1;

GLUquadricObj *obj;

//used function in program
void keyboard(unsigned char key, int, int)
{
    switch (key) {
    case 27://ascii code for esc
        exit(0);
        break;
    case 'q'://or press 'q' 
		exit(0);
        break;
    }
}
void torso()
{
glPushMatrix();
glTranslatef(0.0,2.3,0.0);//x,y,z coordinates
glScalef(1.7, 2.9, 0.7);
glColor3f(0.0, 0.0, 0.0);//black
glutSolidCube(2.0);
glPopMatrix();
}
void joint()
{
glPushMatrix();
glScalef(0.85, 0.85, 0.85);
glColor3ub(255, 0, 0);//red
gluSphere(obj,1.0,32,32);//quadric, radius, 3rd and 4th specify smoothness and detailed
glPopMatrix();
}
void left_upper_arm()
{
glPushMatrix();
glColor3ub(0, 255, 196);//light blue
gluCylinder(obj,0.45, 0.45, 3.0,32,32);//obj, base radius, top radius, specify smoothness and detailed
glPopMatrix();
}
void left_forearm()
{
glPushMatrix();
glColor3ub(30, 185, 149);//some kind of blue
gluCylinder(obj,0.4, 0.40, 2.0,10,10);
glPopMatrix();
}
void right_upper_arm()
{
glPushMatrix();
glRotatef(-90.0, 1.0, 0.0, 0.0);
glColor3ub(130, 185, 29);
gluCylinder(obj,0.45, 0.45, 3.0,32,32);
glPopMatrix();
}
void right_forearm()
{
glPushMatrix();
glRotatef(-90.0, 1.0, 0.0, 0.0);
glColor3ub(173, 215, 91);
gluCylinder(obj,0.4, 0.40, 2.0,32,32);
glPopMatrix();
}
void neck()
{
glPushMatrix();
glScalef(1.3, 0.7, 1.3);
glRotatef(-90.0, 1.0, 0.0, 0.0);
glColor3ub(255, 156, 156);//skin color
gluCylinder(obj,0.5, 0.5, 2, 32, 32);
glPopMatrix();
}
void head()
{
glPushMatrix();
glTranslatef(0.0, 2.20,0.075);//position of head relative to neck
glScalef(1.5, 1.5, 1.5);
glColor3ub(255, 97, 97);//skin color
gluSphere(obj,1.0,32,32);
glPopMatrix();
}
void left_thigh()
{
glPushMatrix();
glRotatef(-120.0, 1.0, 0.0, 0.0);//rotate -120 degrees around x ax
glColor3ub(95, 191, 115);
gluCylinder(obj,0.7, 0.7, 3.0,32,32);
glPopMatrix();
}
void left_lower_leg()
{
glPushMatrix();
glTranslatef(0.0,-0.2,-1.5);
glRotatef(-70.0, 1.0, 0.0, 0.0);
glColor3ub(45, 42, 146);
gluCylinder(obj,0.5, 0.5, 3.0, 32,32);
glPopMatrix();
}
void right_thigh()
{
glPushMatrix();
glRotatef(-120.0, 1.0, 0.0, 0.0);
glColor3ub(255, 0, 215);
gluCylinder(obj,0.7, 0.7, 3.0, 32,32);
glPopMatrix();
}
void right_lower_leg()
{
glPushMatrix();
glTranslatef(0.0,-0.75,-1.35);
glRotatef(-70.0, 1.0, 0.0, 0.0);
glColor3ub(139, 22, 116);
gluCylinder(obj,0.5, 0.5, 3.0, 32,32);
glPopMatrix();
}
void fingers()
{
glPushMatrix();
glScalef(0.5, 1.4, 0.7);
glColor3ub(219, 70, 242);//purpleissh
glutSolidCube(1.0);
glPopMatrix();
}

void display(void)
{
glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
glLoadIdentity();	//reset matrix to default values, its used because all the transalte and rotate options
gluLookAt(0.0,0.0,8.0,0.0,0.0,0.0,0.0,1.0,0.0);
glTranslatef(0.0, -3.0, 10);
glRotatef(danceMove, 0.0, 1.0, 0.0);//dance move all body

//torso
glPushMatrix();
torso();
glPopMatrix();

//neck
glPushMatrix();
glTranslatef(0.0, 5.95, 0.0);//putting neck next to body,torso..connecting those two
glRotatef(danceMove/2, 0.0, 0.0, 1.0);//dance move
glTranslatef(0.0, -1, 0.0);
neck();
head();
glPopMatrix();

//left arm
glPushMatrix();
glTranslatef(-1.9, 4.5, 0.0);
glRotatef(danceMove, 0.5, 0.5, 0.0);//dance moves
left_upper_arm();
glTranslatef(0.0, 0.0, 3.0);
joint();

glRotatef(danceMove, 1.0, 0.0, 0.0);
left_forearm();
glTranslatef(0.0, 0.0,2.0);
fingers();
glPopMatrix();

//right arm
glPushMatrix();
glTranslatef(1.9, 4.5, 0.0);
glRotatef(20.0, 0.0, 0.0, -1.0);//move away of head
glRotatef(danceMove, 0.5, -0.3, 0.0);// dance moves
right_upper_arm();
glTranslatef(0.0, 3.0, 0.0);
joint();
glRotatef(danceMove, 1.0, 0.0, 0.0);//dance move
right_forearm();
glTranslatef(0.0, 2.0, 0.0);
glRotatef(-90.0, 1.0, 0.0, 0.0);//rotation for a fist because of initialization of fingers
fingers(); 
glPopMatrix();

//moving/dancing with increasing/decreasing the variable
	if(danceMove<=50.0 && danceMove>=-31 && help==1)
	{
	danceMove+=1;
	}
	else
		help=0;
if(help==0)
	danceMove=danceMove-1;
if(danceMove<-30)
	help=1;

//joint
glPushMatrix();
glTranslatef(-1.9, 4.5, 0.0);//move shoulder to the left
joint();
glTranslatef(4.0, 0.0, 0.0);
joint();
glPopMatrix();

//joint
glPushMatrix();
glTranslatef(1.4, 0.0, 0.0);
joint();
glTranslatef(-2.8, 0.0, 0.0);
joint();
glPopMatrix();

//left leg
glPushMatrix();
glTranslatef(-1.3, 0.3, 0.0);

glRotatef(180, 1.0, 0.0, 0.0);
glRotatef(danceMove, 1.0, 0.0, 0.0);//dance moves
left_thigh();
glTranslatef(0.0, 3.0, -1.5);
joint();
glTranslatef(0.0, 0.0, 1.5);
glRotatef(danceMove, 1.0, 0.0, 0.0);//dance moves
left_lower_leg();
glTranslatef(0.0, 2.7, -1.0);//position of foot
glRotatef(-90.0, 1.0, 0.0, 0.0);//rotation for a foot
fingers(); 
glPopMatrix();

//right leg
glPushMatrix();
glTranslatef((1.3), 0.3, 0.0);
glRotatef(220, 1.0, 0.0, 0.0);//so it can be little bit further than other leg
glRotatef(-danceMove, 1.0, 0.0, 0.0);//dance move
right_thigh();
glTranslatef(0.0, 3.0, -1.5);
joint();
glTranslatef(0.0, 0.0, 1.5);
glRotatef(danceMove, 1.0, 0.0, 0.0);//dance move
right_lower_leg();
glTranslatef(0.0, 2.2, -1.0);
glRotatef(-90.0, 1.0, 0.0, 0.0);//rotation for a foot
fingers(); 
glPopMatrix();

glutSwapBuffers();
}

void reshape(int w, int h)
{
glViewport(0, 0, w, h);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
//when risizing windows its gonna resize robot
glOrtho(-10.0, 10.0, -10.0 * (GLfloat) h / (GLfloat) w,10.0 * (GLfloat) h / (GLfloat) w, -10.0, 10.0);
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
}
void initializations()
{
glShadeModel(GL_SMOOTH);
glDepthFunc(GL_LEQUAL);
glEnable(GL_DEPTH_TEST);
//initialization of quadric
obj=gluNewQuadric();
gluQuadricDrawStyle(obj, GLU_FILL);
gluQuadricNormals(obj, GLU_SMOOTH);
glClearColor(0.2f, 0.3f, 0.6f, 0.2f);//background color
}
int main(int argc, char **argv)
{
glutInit(&argc, argv);
glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
glutInitWindowSize(480, 620);
glutCreateWindow(" \"Dancing\" robot! WOO-HOO Party! ");

initializations();
glutDisplayFunc(display);
glutReshapeFunc(reshape);
glutIdleFunc(display);
glutKeyboardFunc(keyboard);
glutMainLoop();

return 0;
}
