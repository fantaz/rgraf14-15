Kratko objasnjenje programa koji simulira robota koji "plese".

Program sam pisao na engleskom jeziku, kako nazive funkcija tako i komentare, jer sam se vodio prema primjerima s Mudrog koji su bili na engleskom jeziku.

VRIJEDNOSTI POJEDINIH VARIJABLI:

float danceMove=-20;
int help=1;
Pomocne varijable koje omogucuju kretanje dijelova tijela robota. DanceMove je varijable koja ide od -20 do 30 i to je ubiti domena kretanja robota dok "help" varijabla sluzi za kretanje dijelova unazad.

glTranslatef(0.0,2.3,0.0);
Funckija koja prima input koordinate matricnog sustava: x,y,z. Mjenjanjem vrijednosti tih varijabli pomicnemo mjesto,poziciju na kojoj cemo vrsiti neke radnje(crtanje kvadra,valjka,sfere...). Logicnim zakljuccima i ponekad "trial and error" metodom sam podesavao te vrijednosti.

glScalef(1.7, 2.9, 0.7);
Kao i sto sam naziv kaze funkcija sluzi za skaliranje odredenih dijelova na kojima vrsimo radnju. Povecavamo ili smanjujemo dubinu, visinu ili sirinu objekta(sfere,valjka,kvadra...). Vrijednosti su koordinate "x,y,z" te se prema unesenim vrijednostima objekt skalira.

glColor3f(0.0, 0.0, 0.0);
glColor3ub(255, 0, 0);
Funkcije za mjenjanje boje objekta. Prva funckija prima tri vrijednosti kao unos. Vrijednosti su RGB(Red Green Blue) vrijednosti te su u domeni od 0 do 1 te se prema tome obracunavaju.
Druga funkcija prima isto RGB vrijednosti ali su te vrijednosti od 0 do 255 te se takvo obiljezavaje cesce koristi u racunarstvu.

glRotatef(-120.0, 1.0, 0.0, 0.0);
Funkcija za rotiranje mjesta i pozicije kasnije navedenog objekta. Ova odredena funkcija rotira za -120 stupnjeva oko x osi i samo oko nje jer su y i z postavljeni na 0.

gluSphere(obj,1.0,32,32);
glutSolidCube(2.0);
gluCylinder(obj,0.45, 0.45, 3.0,10,10);

Funkcija "gluSphere" sluzi za stvaranje kugle,sfere ili tome slicno, ovisi o parametrima. Prvi parametar je parametar za quadric object dok sljedeci specifira radijus te sfere. Zadnja dva sluze da odrede cistocu i glatkost povrsine sfere. Istrazivajuci na internetu saznao sam da je najbolje i najefikasnije staviti vrijednost 32 te se to pokazalo kao dobra odluka.
Funkcija "glutSolidCube" sluzi za stavranje kocke ili kvadra t prima samo jedan parametar, sirinu te prema tome stvara nas objekt. Vrijednost 2 nije toliko bitna s obzirom da s funckijom "glScalef" se moze prilagodit.
Funkcija "gluCylinder" sluzi za stvaranje valjka ili tome slicno sto naravno ovisi o unesenim parametrima. Parametri osim quadric objekta jesu radijus baze i radijus druge baze valjka a njihove vrijednosti sam prilagodavao zglobovima. Zadnja dva parametra sluze da odrede cistocu i glatkost povrsine.

KORISTENJE PROGRAMA:
Kada se program pokrene nema nikakvih opcija osim tipke ESC ili slova 'q' koji prekidaju izvrsavanje programa.
ARGUMENTI: Nije potrebno specifirati nikakve argumente pri pokretanju programa. Program se pokrece ./ime_programa .

Reference
https://mudri.uniri.hr/course/view.php?id=1790
https://www.opengl.org/sdk/docs/man2/xhtml/
https://social.msdn.microsoft.com/Search/en-US/windows/desktop?query=glscalef&Refinement=181&emptyWatermark=true&ac=4
