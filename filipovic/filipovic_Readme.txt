/*
 *	Projekt Računalna Grafika 2014/15
 *	Student: Filipović Bojan
 *	Zadatak 2
 *
 */

--> Pokretanje programa: 
	QtCreator: File --> Open File or Project --> navigacija do direktorija
		gdje se nalazi naš glavni CMakeLists.txt
		te odabiremo tu CMakeLists.txt datoteku --> Configure Project
	Program se pokreće buildanjem (Ctrl + B) te pokretanjem (Ctrl + R)

	Iz terminala (bez CMake): opcionalno, program se može pokrenuti i 
		bez korištenja CMake-a, na način da se u terminalu smjestimo 
		u direktorij gdje se nalazi .cpp datoteka te upišemo sljedeću
		komandu:
		    g++ <imeprograma>.cpp -lGL -lGLU -lglut  -o <imeizvrsnedatoteke>
		te onda program pokrećemo sa naredbom:
		    ./imeizvrsnedatoteke

--> Korištenje programa:
	Nakon pokretanja, koriste se sljedeće tipke za upravljanje programom:
		Tipka 'Esc' = prekida program;
		Tipka 'g'   = pokreće animaciju;
		Tipka 's'   = zaustavlja animaciju;
	Također, nakon što sfera izađe iz frustuma, program se automatski zatvara.

--> Vrijednosti u programu:
	Varijable su definirane pri vrhu koda, radi lakše izmjene i eventualnih 
		promjena. Ukratko ćemo proći kroz definirane vrijednosti.
	(Napomena: pošto je program pisan generalno engleskim jezikom te su imena
		varijabli dodijeljena na engleskom nazivu, sukladno tome su 
		napisani i engleski komentari.)
	spherePosition -> definiramo početnu poziciju naše sfere
	xParabolic,yParabolic -> koriste se za određivanje brzine pada sfere i 
		paraboličnosti same putanje pada.
	angle -> određuje kut rotacije sfere
	startAnimation -> kontrolna varijabla za startanje/zaustavljanje animacije.
	xWindow, yWindow, xWindowPos, yWindowPos -> veličina prozora i početni 
		smještaj prozora po x i y osi, respektivno.
	frustum -> koristi se za dohvaćanje ravnina frustuma
	glFloat dlr,dlg,dlb,alr,alg,alb,lx,ly,lz,lw -> varijable za definiranje
		difuzne i ambijentalne komponente osvijetljenja te poziciju 
		svijetla.
	cube[][], normal[][] -> definiranje pozicije verteksa i normala na plohe 			naše kocke. Prikaz kocke i verteksa dan ispod:

	  3---7   <- sukladno ovim vrijednostima definirani su i verteksi kocke.
	 /|  /|
	2---6 |
	| 1-|-5
	|/  |/ 
	0---4 
	
	drawCube -> crtamo našu kocku bez desne plohe, GL_QUADS nam govori da radimo
		sa pravokutnicima (umjesto trokuta ili poligona), dok nam 
		glNormal3fv te glVertex3fv omogućavaju da pošaljemo cijeli redak 
		matrice u kojoj je definirana kocka/normala te se tako dodijeljuju 
		x,y,z vrijednosti.
	drawSphere -> puno jednostavniji iz razloga što imamo gotovu funkciju 
		glutSolidSphere koja kao argumente prima radijus te brojeve 
		paralela i ordinata pomoću kojih se konstruira sfera. Zadnje 
		dvije vrijednosti su 20, po osobnom mišljenju optimalne vrijednosti
		koje daju sferi željeni oblik a nisu "prenaporne" za render.
		U funkciju je odmah dodana i boja, te transformacija i rotacija 
		sfere.
	ExtractFrustum -> dohvaćamo projection i modelview matrice iz OpenGl-a
		te nakon množenja tih matrica izvlačimo 6 potrebnih ravnina koje 
		sačinjavaju frustum. Korišten je ovaj pristup kako bi bili neovisni
		o veličini prozora, te ovakav pristup ima više primjena od klasičnog
		hardkodiranja koordinata sfere te uspoređivanja sa veličinom 			prozora.
	SphereInFrustum -> funkcija koja računa jeli naša sfera u frustumu te 
		pomoću vrijednosti returna određujemo stanje sfere u odnosu na 			frustum. 
	display -> postavljena sivkasta pozadinska boja da ne bude "dosadna" crna.
		Postavljaju se osvjetljenja te se pomoću matričnog stoga crtaju 
		objekti te vrše animacije nad njima. Ukoliko se animacija pokrenula,
		računa se parabolična funkcija pada sfere te provjera za odnos sfere
		i frustuma.
	keyboard -> funkcija za provjeru korisničkog unosa tipki za pokretanje 			ili zaustavljanje animacije.
	resize, init, main -> "standardne" funkcije, u resize-u postavljamo pogled
		i poziciju očišta. Valja napomenuti da se bez 
		glEnable(GL_DEPTH_TEST) sfera crta preko kocke te je ova 
		naredba apsolutno nužna za uspješno izvršavanje zadatka.

Reference korištene tokom izrade zadaće:

 -->Normale i lica kocke : 
  http://goworldwind.org/developers-guide/how-to-build-a-custom-renderable/
  http://stackoverflow.com/questions/13149524/understanding-glnormal3f
  https://www.opengl.org/discussion_boards/showthread.php/138521-glNormal3f

  -> Drugi najbolji odgovor na postavljeno pitanje:
   http://stackoverflow.com/questions/8142388/in-what-order-should-i-send-my-  vertices-to-opengl-for-culling

 -->Pogledi i kamere:
  http://stackoverflow.com/questions/17539132/what-is-the-use-of-reshape-function-in-glut

 -->Osvijetljenje:
  https://www.cse.msu.edu/~cse872/tutorial3.html

 -->Frustum test:
  http://www.crownandcutlass.com/features/technicaldetails/frustum.html

 -->Datoteke sa Mudri sustava te template-i sa laboratorijskih vježbi
		


	
		
