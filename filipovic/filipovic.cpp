#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>
#include <math.h>

/* Initial sphere position */
float spherePosition[3] = {0, 0, 0};

/* For parabolic falling */
float xParabolic = 0.003;
float yParabolic = 0.0;

float angle = 0.0;

/* Start/stop animation */
bool startAnimation = false;

/* Window size */
int xWindow = 600, yWindow = 600;
/* Window position */
int xWindowPos = 150, yWindowPos = 100;

/* For frustum extraction */
float frustum[6][4];

/* Diffuse light color variables */
GLfloat dlr = 1.0;
GLfloat dlg = 1.0;
GLfloat dlb = 1.0;

/* Ambient light color variables */
GLfloat alr = 0.2;
GLfloat alg = 0.2;
GLfloat alb = 0.2;

/* Light position variables */
GLfloat lx = 0.0;
GLfloat ly = 5.0;
GLfloat lz = 1.0;
GLfloat lw = 1.0;

float cube[8][3] = {
        {-0.5, -0.5, 0.5},
        {-0.5, -0.5, -0.5},
        {-0.5, 0.5, 0.5},
        {-0.5, 0.5, -0.5},
        {0.5, -0.5, 0.5},
        {0.5, -0.5, -0.5},
        {0.5, 0.5, 0.5},
        {0.5, 0.5, -0.5}
};

float normal[5][3] = {
    {0, 0, 1}, 		//front face
    {0, 1, 0},		//upper face
    {0, -1, 0},		//bottom face
    {-1, 0, 0},     //left face
    {0, 0, -1},		//back face
};

void drawCube()
{

    /* Front face */
    glBegin(GL_QUADS);
    glNormal3fv(normal[0]);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[2]);
    glVertex3fv(cube[6]);
    glVertex3fv(cube[4]);
    glEnd();

    /* Back face */
    glBegin(GL_QUADS);
    glNormal3fv(normal[4]);
    glVertex3fv(cube[1]);
    glVertex3fv(cube[3]);
    glVertex3fv(cube[7]);
    glVertex3fv(cube[5]);
    glEnd();

    /* Left face */
    glBegin(GL_QUADS);
    glNormal3fv(normal[3]);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[1]);
    glVertex3fv(cube[3]);
    glVertex3fv(cube[2]);
    glEnd();

    /* Top face */
    glBegin(GL_QUADS);
    glNormal3fv(normal[1]);
    glVertex3fv(cube[2]);
    glVertex3fv(cube[3]);
    glVertex3fv(cube[7]);
    glVertex3fv(cube[6]);
    glEnd();

    /* Bottom face */
    glBegin(GL_QUADS);
    glNormal3fv(normal[2]);
    glVertex3fv(cube[0]);
    glVertex3fv(cube[1]);
    glVertex3fv(cube[5]);
    glVertex3fv(cube[4]);
    glEnd();

}

void drawSphere()
{
    glColor3f(1.0, 1.0, 0.0);
    glTranslatef(spherePosition[0],
                 spherePosition[1],
                 spherePosition[2]);
    glRotatef(angle,0,0,1);              //sphere rotation around its own axis (rolling)
    glutSolidSphere(0.5, 20, 20);
}

void ExtractFrustum()
{
   float   proj[16];
   float   modl[16];
   float   clip[16];
   float   t;

   /* Get the current PROJECTION matrix from OpenGL */
   glGetFloatv( GL_PROJECTION_MATRIX, proj );

   /* Get the current MODELVIEW matrix from OpenGL */
   glGetFloatv( GL_MODELVIEW_MATRIX, modl );

   /* Combine the two matrices (multiply projection by modelview) */
   clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
   clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
   clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
   clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

   clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
   clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
   clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
   clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

   clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
   clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
   clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
   clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

   clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
   clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
   clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
   clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

   /* Extract the numbers for the RIGHT plane */
   frustum[0][0] = clip[ 3] - clip[ 0];
   frustum[0][1] = clip[ 7] - clip[ 4];
   frustum[0][2] = clip[11] - clip[ 8];
   frustum[0][3] = clip[15] - clip[12];

   /* Normalize the result */
   t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
   frustum[0][0] /= t;
   frustum[0][1] /= t;
   frustum[0][2] /= t;
   frustum[0][3] /= t;

   /* Extract the numbers for the LEFT plane */
   frustum[1][0] = clip[ 3] + clip[ 0];
   frustum[1][1] = clip[ 7] + clip[ 4];
   frustum[1][2] = clip[11] + clip[ 8];
   frustum[1][3] = clip[15] + clip[12];

   /* Normalize the result */
   t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
   frustum[1][0] /= t;
   frustum[1][1] /= t;
   frustum[1][2] /= t;
   frustum[1][3] /= t;

   /* Extract the BOTTOM plane */
   frustum[2][0] = clip[ 3] + clip[ 1];
   frustum[2][1] = clip[ 7] + clip[ 5];
   frustum[2][2] = clip[11] + clip[ 9];
   frustum[2][3] = clip[15] + clip[13];

   /* Normalize the result */
   t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
   frustum[2][0] /= t;
   frustum[2][1] /= t;
   frustum[2][2] /= t;
   frustum[2][3] /= t;

   /* Extract the TOP plane */
   frustum[3][0] = clip[ 3] - clip[ 1];
   frustum[3][1] = clip[ 7] - clip[ 5];
   frustum[3][2] = clip[11] - clip[ 9];
   frustum[3][3] = clip[15] - clip[13];

   /* Normalize the result */
   t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
   frustum[3][0] /= t;
   frustum[3][1] /= t;
   frustum[3][2] /= t;
   frustum[3][3] /= t;

   /* Extract the FAR plane */
   frustum[4][0] = clip[ 3] - clip[ 2];
   frustum[4][1] = clip[ 7] - clip[ 6];
   frustum[4][2] = clip[11] - clip[10];
   frustum[4][3] = clip[15] - clip[14];

   /* Normalize the result */
   t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
   frustum[4][0] /= t;
   frustum[4][1] /= t;
   frustum[4][2] /= t;
   frustum[4][3] /= t;

   /* Extract the NEAR plane */
   frustum[5][0] = clip[ 3] + clip[ 2];
   frustum[5][1] = clip[ 7] + clip[ 6];
   frustum[5][2] = clip[11] + clip[10];
   frustum[5][3] = clip[15] + clip[14];

   /* Normalize the result */
   t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
   frustum[5][0] /= t;
   frustum[5][1] /= t;
   frustum[5][2] /= t;
   frustum[5][3] /= t;
}

/*
 * distance = A * X + B * Y + C * Z + D
 * Where A, B, C, and D are the four numbers that define the plane and X, Y, and Z are the sphere's center coordinates.
 * If sphere is outside of any of 6 planes, function returns false.
 */
bool SphereInFrustum( float x, float y, float z, float radius )
{
   int p;

   for( p = 0; p < 6; p++ )
       if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= -radius )
         return false;
   return true;
}

void display()
{
    glClearColor(0.2, 0.2, 0.2, 1.0f);                  //grey background color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    GLfloat DiffuseLight[] = {dlr, dlg, dlb};           //set DiffuseLight [] to the specified values
    GLfloat AmbientLight[] = {alr, alg, alb};           //set AmbientLight [] to the specified values
    glLightfv (GL_LIGHT0, GL_DIFFUSE, DiffuseLight);    //change  the light accordingly
    glLightfv (GL_LIGHT0, GL_AMBIENT, AmbientLight);    //change  the light accordingly
    GLfloat LightPosition[] = {lx, ly, lz, lw};         //set the  LightPosition to the specified values
    glLightfv (GL_LIGHT0, GL_POSITION, LightPosition);  //change the light accordingly

        /* Drawing outer cube */
        glPushMatrix();
            glColor3f(1.0, 0.0, 0.0);
            drawCube();
        glPopMatrix();

        /* Drawing inner cube */
        glPushMatrix();
            glColor3f(0.0,0.0,1.0);
            glScalef(0.99, 0.99, 0.99);
            drawCube();
        glPopMatrix();

        /* Draw and animate sphere */
        glPushMatrix();
            drawSphere();
        glPopMatrix();

        if (startAnimation==true){

            angle -= 0.1;

            /* When sphere passes center of cube start falling parabollicaly */
            if (spherePosition[0] > 0.5) {
                spherePosition[0] += xParabolic;
                yParabolic = (spherePosition[0]-0.5) * (spherePosition[0]-0.5);   //parabolic function for falling
                spherePosition[1] = -yParabolic;
                ExtractFrustum();

                /* Exit program when sphere leaves frustum */
                if ((SphereInFrustum(spherePosition[0],spherePosition[1],spherePosition[2],0.5))==0) exit(0);
            }

            spherePosition[0] += xParabolic;        //speed of sphere movement

        }

    glutSwapBuffers();
    glutPostRedisplay();
}

void keyboard(unsigned char key, int , int )
{
    switch (key)
    {
    case 27:        //Escape key
        exit(0);
    case 103:       //g key, start
        startAnimation = true;
        break;
    case 115:       //s key,stop
        startAnimation = false;
        break;
    }
}
void resize(int w, int h)
{
    if (h == 0) { h = 1; }
    float ratio = 1.0* w / h;
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective(70.0, ratio, 1.0, 20.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt (2.0, 0.0, 10.0,
               4.0, -4.0, 0.0,
               0.0, 1.0, 0.0);

    glRotatef(-30, 0, 1, 0);
}

void init() {

    glEnable(GL_DEPTH_TEST);        // Without this, the sphere gets drawn over the cube
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);            // Turn on light0

    float pos[] = {0.0, 5, 0.0, 1}; // Position of the light
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

}

int main(int argc, char** argv)
{

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(xWindow, yWindow);
    glutInitWindowPosition (xWindowPos, yWindowPos);

    /* Create the window */
    glutCreateWindow("rokaaaajjjj");

    init();
    /* Set handler functions */
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);

    glutMainLoop();

    return 0;

}
