#include <glew/glew.h>
#include <freeglut/freeglut.h>
#include <math.h>

#define M_PI       3.14159265358979323846

float angle = 0.0;
float angle1 = 0.0;
char pomak = 0;
int smijer = 1;
int flag = 1;
int rotacija = 0;
GLUquadric *quad = gluNewQuadric();

void drawCylinder(float radius) {
	glBegin(GL_ELEMENT_ARRAY_POINTER_ATI);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f(cos(i) * radius, sin(i) * radius, 0.0);
	glEnd();
	glBegin(GL_POLYGON);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f(cos(i) * radius, sin(i) * radius, 0.0);
	glEnd();
}
void display()
{
	glClearColor(0, 0, 0, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	// Cijeli robot
	glTranslatef(0, 0, -11);   
	glRotatef(angle1, 0, 1, 0); // Rotiranje cijelog robota


	glPushMatrix(); // Torzo
	glColor3f(1.0f, 0.0f, 0.0f);
	glPushMatrix(); // Torzo shape 
	glScalef(1.4f, 2, 0.5f);
	glutSolidCube(1.);
	glPopMatrix();
	     
	glPushMatrix(); // Neck
	glColor3f(1.0, 1.0, 0.0);
	glTranslatef(0, 1.2f, 0);
	glPushMatrix(); // Neck Shaping
	glRotatef(90, 1, 0, 0);
	gluCylinder(quad, 0.1, 0.1, 0.2, 10, 10);
	glPopMatrix(); // end neck shape
	glPushMatrix(); // Head
	glColor3f(1.0, 0.0, 1.0);
	glTranslatef(0, 0.4f, 0);
	glutSolidSphere(0.4, 12, 12);
	glPopMatrix(); // end head
	glPopMatrix(); // end neck

	glPushMatrix();				//Zglob ramena
	glColor3f(0.0, 1.0, 0.0);
	glTranslatef(0.7f, 1, 0);
	
	glRotatef(0, -1, 0, 0);
	glutSolidSphere(0.2, 12, 12);

	glPushMatrix(); // Lijeva nadlaktica
	glColor3f(0.0, 0.8f, 0.0);
	glTranslatef(0, 0, 0);
	glRotatef(angle, 1, 0, 0);
	glRotatef(angle, 0, 1, 0);
	glRotatef(angle, 0, 1, 0); 
	gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);

	
	glPushMatrix();				//Zglob
	glColor3f(0.0, 0.6f, 0.0);
	glTranslatef(0, 0, 1.5);
	glRotatef(0, -1, 0, 0);
	glutSolidSphere(0.2,12, 12);

	glPushMatrix(); // Podlaktica
	glColor3f(0.0, 0.4f, 0.0);
	glTranslatef(0, 0, 0);
	glRotatef(angle, -1, 0, 0);			
	gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);

	glPushMatrix();				//Zglob sake
	glColor3f(0.0, 0.4f, 0.0);
	glTranslatef(0.0, 0, 1.5);
	glRotatef(angle, 0, 0, 1);
	glutSolidSphere(0.15, 12, 12);



	glPushMatrix(); // Saka 
	glColor3f(0, 0.2f, 0);
	glScalef(0.5f, 1, 2.5f);
	glTranslatef(0, 0, 0.1f);
	glutSolidCube(0.2);


	
	glPopMatrix(); // End saka
	glPopMatrix(); // End podlaktica
	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob
	glPopMatrix(); // End nadlaktica
	glPopMatrix(); // End zglob

	glPushMatrix();				//Zglob desnog ramena
	glColor3f(0.3f, 0.4f, 0.0);
	glTranslatef(-0.7f, 1, 0);
	glRotatef(0, -1, 0, 0);
	glutSolidSphere(0.2, 12, 12);

	glPushMatrix(); // Desna nadlaktica
	glColor3f(0.9f, 0.5f, 0.0);
	glTranslatef(0, 0, 0);
	glRotatef(angle, 1, 0, 0);
	glRotatef(-angle, 0, 1, 0);
	glRotatef(-angle, 0, 1, 0);
	gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);

	glPushMatrix();				//Zglob lakta
	glColor3f(0.6f, 0.3, 0.0);
	glTranslatef(0, 0, 1.5);
	glRotatef(angle, -1, 0, 0);
	glutSolidSphere(0.2, 12, 12);

	glPushMatrix(); // Podlaktica
	glColor3f(0.8f, 0.6f, 0.1f);
	glTranslatef(0, 0, 0);
	gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);

	glPushMatrix();				//Zglob sake
	glColor3f(0.6f, 0.4f, 0.0);
	glTranslatef(0.0, 0, 1.5);
	glRotatef(angle, 0, 1, 0);
	glutSolidSphere(0.15, 12, 12);


	glPushMatrix(); // Saka 
	glColor3f(0.4f, 0.2f, 0);
	glScalef(0.5, 1, 2.5);
	glTranslatef(0, 0, 0.1f);
	glutSolidCube(0.2);

	glPopMatrix(); // End saka
	glPopMatrix(); // End podlaktica
	glPopMatrix(); // End Nadlaktica
	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob


	glPushMatrix();				//Zglob lijeve natkoljenice
	glColor3f(0.9, 0.5f, 0);
	glTranslatef(-0.7f, -1, 0);
	glRotatef(angle+80, 1, 0, 0);
	glutSolidSphere(0.3, 12, 12);

	glPushMatrix();				// Lijeva natkoljenica
	glColor3f(0.8, 0.3f, 0);
	gluCylinder(quad, 0.3, 0.3, 1.5, 10, 1);

	glPushMatrix();				//Zglob koljena
	glColor3f(0.7f, 0.3f, 0);
	glTranslatef(0, 0, 1.5f);
	glRotatef(angle, 1, 0, 0);
	glutSolidSphere(0.3, 12, 12);

	glPushMatrix(); // Podkoljenica
	glColor3f(0.6, 0.2f, 0.1f);
	gluCylinder(quad, 0.25, 0.25, 1.5, 10, 1);

	glPushMatrix();				//Zglob stopala
	glColor3f(0.3f, 0.1f, 0.3f);
	glTranslatef(0, 0, 1.5);
	glRotatef(angle+90, 1, 0, 0);
	glutSolidSphere(0.15, 12, 12);
	

	glPushMatrix(); // Stopalo
	glColor3f(0.6f, 0.2f, 0);
	glScalef(0.4f, 0.2f, 0.7f);
	glTranslatef(0, 0, -0.5f);
	glutSolidCube(1);
	
	glPopMatrix(); // End stopalo 
	glPopMatrix(); // End podkoljenica
	glPopMatrix(); // End nadkoljenica
	glPopMatrix(); // End zglob
 	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob

	glPushMatrix();				//Zglob desne natkoljenice
	glColor3f(0,0 , 0.7f);
	glTranslatef(0.7f, -1, 0);
	glRotatef(angle, 1, 0, 0);
	glRotatef(angle, 0, 1, 0);
	glutSolidSphere(0.3, 12, 12);

	glPushMatrix();				// Desna natkoljenica
	glColor3f(0, 0.4f, 0.6f);
	glTranslatef(0.0, 0, 0);
	gluCylinder(quad, 0.3, 0.3, 1.5, 10, 1);

	glPushMatrix();				//Zglob koljena
	glColor3f(0.0, 0.6f, 0.6f);
	glTranslatef(0, 0, 1.5);
	glRotatef(angle * 2, 1, 0, 0);
	glutSolidSphere(0.3, 12, 12);

	glPushMatrix(); // Podkoljenica
	glColor3f(0.15f, 0.35f, 0.75f);
	gluCylinder(quad, 0.25, 0.25, 1.5, 10, 1);
	
	glPushMatrix();				//Zglob stopala
	glColor3f(0.0, 0.3f, 0.4f);
	glTranslatef(0, 0, 1.5);
	glRotatef(angle+70, 1, 0, 0);
	glutSolidSphere(0.2, 12, 12);
	
	glPushMatrix(); // Stopalo 
	glScalef(0.4f, 0.2f, 0.7f);
	glTranslatef(0, 0, -0.5);
	glutSolidCube(1);

	
	glPopMatrix(); // End stopalo
	glPopMatrix(); // End podkoljenica
	glPopMatrix(); // End nadkoljenica
	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob
	glPopMatrix(); // End zglob

	glPopMatrix(); // End Torzo

	
	glutSwapBuffers();

}

void resize(int w, int h)
{
	if (h == 0) { h = 1; }
	float ratio = 1.0f* w / h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, ratio, 1, 1000); 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}    

void init() {
	glEnable(GL_DEPTH_TEST);		// Bez ovoga, kugla se crta preko kocke
	glEnable(GL_LIGHTING);			// Osvijetljenje
	glEnable(GL_COLOR_MATERIAL);	// Boja materijala
	glEnable(GL_LIGHT0);			// Ukljucimo svijetlo0 i pozicioniramo ga na (0.5,0.5,0.5)
	float pos[] = { 0.5, 0.5, 0.5, 1 };
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
}


void keyboard(unsigned char key, int ,int )
{
	switch (key)
	{
	case 's': case 'S':
		if (pomak)
			pomak = 0;
		else
			pomak = 1;
		break;
	case 'y': case 'Y':
		if (rotacija)
			rotacija = 0;
		else
			rotacija = 1;
		break;
	case 27:			//ESCAPE
		exit(0);
		break;
	}
}


//Definiranje timera 
void timer(int value)
{



	if (pomak){
		if (angle >= 0 && angle <= 40 && flag){		//ograničenje pomaka
			angle += 0.5f;

		}
		else
			flag = 0;

		if (!flag && angle > 0){
			angle -= 0.5f;
		}
		else
			flag = 1;
	}
	if (rotacija)
		angle1 += 1;
	    

	glutPostRedisplay();
	glutTimerFunc(60, timer, 0);
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 800);

	//Create the window
	glutCreateWindow("Zadatak 5 -Lovro Spetić");

	
	//Set handler functions
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(resize);
	glutTimerFunc(0, timer, 0);
	init();
    glutMainLoop();

	return 0;
}
