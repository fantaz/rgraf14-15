OPIS:
Program sam realizirao vođen idejom o dvije kocke, jedna unutar druge. Vanjska kocka je siva dok je unutarnja samo malo manja zlatna kocka. Kada portal dođe u dodir sa kockom tada se mijenja cubeOffset te se dobiva efekt da siva kocka nestaje dok te zlatna kocka dolazi na vidjelo.
Za kocke sam koristio GL_QUADS metodu dok sam za portal koristio SolidTorus za vanjski dio (kružnicu) te sam je "popunio" pomoću triangle fan funkcije. Kod je kompletno iskomentiran te su ovdje u prilogu upute za korištenje programa.

UPUTE:
S - Pokreće animaciju
X - Pauzira animaciju
Esc - Izlazi iz programa

REFERENCE:
https://mudri.uniri.hr  - Resursi predmeta Računalna Grafika
http://www.wikihow.com/Make-a-Cube-in-OpenGL  -  Moja početna točka projekta
http://xoax.net/cpp/crs/opengl/index.php  -  odličan niz tutoriala koji pokrivaju velik dio područja potrebnih za izradu projekta


