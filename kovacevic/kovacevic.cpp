#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

#define GL_PI 3.141592654 // pi
#define GL_2PI GL_PI*2 // 2*pi

GLfloat portalPos = 2.0; // Portal starting position
GLfloat portalOffset = .01; // Offset portal position
GLfloat cubeOffset = 0; // Grey cube offset
GLfloat lightColor[] = {1.0f, 1.0f, 1.0f, 1.0f};//color of the lighting
GLfloat lightPos[] = {-0.5, 1.0, 2.0, 1.0}; // position of the lighting
float cubeColor1[]={0.2,0.2,0.2}; //grey color for the first cube
float cubeColor2[]={1.0,1.0,0.0};// golden color for the second cube
float portalColor[]={1.0, 1.0, 1.0};//white color for the portal
float torusSize[]={0.010, 2.5, 10, 70};//size of portal, r=2.5
bool run=false; // variable for starting the animation

void control(unsigned char key, int /*x*/, int /*y*/){ // controls for the animation
    switch (key){
        case 's': run=true;          //start animation
        break;
        case 'x': run=false;         //stop animation
        break;
        case 27: exit(0);            //exit program -> ESC button
    }
}

void timer(int /*time*/) // timer function for adjusting the speed of refresh rate of animation
{
    glutPostRedisplay();
    glutTimerFunc(30, timer, 0);
}

void init() {
    glEnable(GL_COLOR_MATERIAL); //use of color as material enabled
    glEnable( GL_BLEND ); // transparency enabled
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //blend and alpha key enabled
    glClearColor (0.0, 0.0, 0.0, 0.0); //set background color and transparency
    glShadeModel (GL_SMOOTH); //enable smooth shade model
    glEnable(GL_NORMALIZE); //enable depth testing for z-culling
    glEnable(GL_DEPTH_TEST);//depth test enable
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);// enable lighting
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);//use light0

}

void display() {  //function for setting up objects on the screen and animation
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW); // use model view matrix mode
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -6.0f); // position
    glRotatef(10, 1, 1, 1); //  angle

    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor); //lighting settings color and position
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);;

    glTranslatef(0.0, 0.0, -2.0); //positioning of the cube
    glRotatef(50, 1.5, -1.5, 0.0);// rotation of the cube
    glColor3f(cubeColor2[0], cubeColor2[1], cubeColor2[2]); //cube color

    glBegin(GL_QUADS);  // slightly smaller golden cube positioned inside of the grey one
    // Front golden
    glNormal3f(0.01f, 0.01f, 0.99f);
    glVertex3f(-0.99f, -0.99f, 0.99f);
    glVertex3f(0.99f, -0.99f, 0.99f);
    glVertex3f(0.99f, 0.99f, 0.99f);
    glVertex3f(-0.99f, 0.99f, 0.99f);

    // Right golden
    glNormal3f(0.99f, 0.01f, 0.01f);
    glVertex3f(0.99f, -0.99f, -0.99f);
    glVertex3f(0.99f, 0.99f, -0.99f);
    glVertex3f(0.99f, 0.99f, 0.99f);
    glVertex3f(0.99f, -0.99f, 0.99f);

    // Back golden
    glNormal3f(0.01f, 0.01f, -0.99f);
    glVertex3f(-0.99f, -0.99f, -0.99f);
    glVertex3f(-0.99f, 0.99f, -0.99f);
    glVertex3f(0.99f, 0.99f, -0.99f);
    glVertex3f(0.99f, -0.99f, -0.99f);

    // Left golden
    glNormal3f(-0.99f, 0.01f, 0.01f);
    glVertex3f(-0.99f, -0.99f, -0.99f);
    glVertex3f(-0.99f, -0.99f, 0.99f);
    glVertex3f(-0.99f, 0.99f, 0.99f);
    glVertex3f(-0.99f, 0.99f, -0.99f);

    // Top golden
    glNormal3f(0.01f, 0.99f, 0.01f);
    glVertex3f(0.99f, 0.99f, 0.99f);
    glVertex3f(-0.99f, 0.99f, 0.99f);
    glVertex3f(-0.99f, 0.99f, -0.99f);
    glVertex3f(0.99f, 0.99f, -0.99f);

    // Bottom golden
    glNormal3f(0.01f, -0.99f, 0.01f);
    glVertex3f(0.99f, -0.99f, 0.99f);
    glVertex3f(-0.99f, -0.99f, 0.99f);
    glVertex3f(-0.99f, -0.99f, -0.99f);
    glVertex3f(0.99f, -0.99f, -0.99f);

    glEnd();
    glPushMatrix();

    glColor3f(cubeColor1[0], cubeColor1[1], cubeColor1[2]);
    glBegin(GL_QUADS);  // full size grey cube covers the golden one

    // Front grey
    glNormal3f(0.0f + cubeOffset, 0.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, 1.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);

    // Right grey
    glNormal3f(1.0f + cubeOffset, 0.0f, 0.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, 1.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, 1.0f);

    // Back grey
    glNormal3f(0.0 + cubeOffset, 0.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, -1.0f);

    // Left grey
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);


    // Top grey
    glNormal3f(0.0f + cubeOffset, 1.0f, 0.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, 1.0f, -1.0f);


    // Bottom grey
    glNormal3f(0.0f + cubeOffset, -1.0f, 0.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(1.0f + cubeOffset, -1.0f, -1.0f);

    glEnd();
    glPopMatrix();

    glTranslatef(portalPos, 0.0, 0.0);
    glRotatef(90, 0.0, -1.0, 0.0); // portal position adjustment
    glColor3f(portalColor[0],portalColor[1],portalColor[2]); //portal outside color (torus)
    glutSolidTorus(torusSize[0],torusSize[1],torusSize[2],torusSize[3]); //torus is used to create our "portal"
    glColor4f(portalColor[0],portalColor[1],portalColor[2],0.5 ); //portal inside color, transparency @ 50%
    int i;
    int triangles = 50; // number of triangles used in triangle fan for filling the torus inside
    GLfloat r = 2.5f;
    glBegin(GL_TRIANGLE_FAN); //triangle fan function for filling the torus
    glVertex2f(0, 0);
    for(i = 0; i <= triangles;i++) {
        glVertex2f(r * cos(i *  GL_2PI / triangles),r * sin(i * GL_2PI / triangles));
    }
    glEnd();

    if (run) portalPos -= portalOffset; //movement of the portal
    if (portalPos <= -1) portalOffset = 0; //movement stops when portal gets to the end of the grey cube
    if (portalPos <= 1) {
        if (run) cubeOffset -= portalOffset; //golden cube starts to reveal when portal gets to the cube
    }
    glutSwapBuffers(); // swap front and back buffers (double buffering)
}

void resize(int w, int h) { // auto-resize based on changes of window
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
   // glOrtho(-2.0*w/h, 2.0*w/h, -2.0, 2.0, -3.0, 3.0);
    gluPerspective(60.0, (double)w / (double)h, 1.0, 200.0);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv); //glut initialiazation
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);// set window size 800*600
    glutCreateWindow("Luka Kovacevic - 3. zadatak");
    init(); // initial settings applied
    glutDisplayFunc(display); //calling display function for rendering objects
    glutKeyboardFunc(control);//calling control function for animation
    glutReshapeFunc(resize);//window resize function
    glutTimerFunc(0,timer,0);//calling timer function
    glutMainLoop();
    return 0;
}


