Opis

Scena:
Prikazuje se zelena zicana kocka koja predstavlja spremnik vode, crna linija na z osi pricvrscena za desni doljni rub kocke. Unutar spremnika je plava kocka koja predstavlja vodu.

Animacija:
Animacija traje dok se kocka ne zarotira za 90 stupnjeva. Pocetna vrijednost je postavljena na 0 te timer okida svakih 20ms i povecava kut za 0.2 stupnja.
Voda je napravljena pomocu dva poligona. Dok je kut manji od 45 stupnjeva mijenja se y vrijednost gornjeg lijevog ruba gornjeg poligona u ovisnosti o kutu.
Kada je kut veci od 45 stupnjeva mijenja se x vrijednost doljnjeg desnog ruba doljnjeg poligona. 

Svijetlost:
Koristena je svijetlost light0 pozicionirana na (-1,1,1).

Upravljanje animacijom:
s - pokretanje animacije
e - zaustavljanje animacije
Esc - izlazak iz programa

Reference:
laboratorijske vijezbe
http://www.opengl-tutorial.org/
https://www.opengl.org/sdk/docs/
http://www.glprogramming.com/
Google
Stack Overflow