#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>

//polje vrhova kocke
float cube[8][3] = {
    {0, 0, 0},  //0
    {0, 0, 1},  //1
    {0, 1, 0},  //2
    {0, 1, 1},  //3
    {1, 0, 0},  //4
    {1, 0, 1},  //5
    {1, 1, 0},  //6
    {1, 1, 1}   //7
};

//pocetni kut
float angle = 0.0;
//refresh interval 20ms
int refresh = 20;
//u pocetku animacija zaustavljena
bool animating = false;

//funkcija za crtanje zicane kocke(spremnik vode)
void drawCubeGrid() {
    glBegin(GL_LINES);

    glVertex3fv(cube[0]);
    glVertex3fv(cube[4]);

    glVertex3fv(cube[0]);
    glVertex3fv(cube[2]);

    glVertex3fv(cube[0]);
    glVertex3fv(cube[1]);

    glVertex3fv(cube[4]);
    glVertex3fv(cube[5]);

    glVertex3fv(cube[4]);
    glVertex3fv(cube[6]);

    glVertex3fv(cube[2]);
    glVertex3fv(cube[3]);

    glVertex3fv(cube[2]);
    glVertex3fv(cube[6]);

    glVertex3fv(cube[3]);
    glVertex3fv(cube[1]);

    glVertex3fv(cube[3]);
    glVertex3fv(cube[7]);

    glVertex3fv(cube[6]);
    glVertex3fv(cube[7]);

    glVertex3fv(cube[5]);
    glVertex3fv(cube[1]);

    glVertex3fv(cube[5]);
    glVertex3fv(cube[7]);

    glEnd();

}


//funkcija za crtanje vode
void drawWater(float x, float y, float x1){
    glBegin(GL_QUAD_STRIP);
        glVertex3f(x,y,0);
        glVertex3f(x,y,-1);

        glVertex3f(0,1,0);
        glVertex3f(0,1,-1);

        glVertex3f(x1,0,0);
        glVertex3f(x1,0,-1);
    glEnd();

    glBegin(GL_POLYGON);
        glVertex3f(x,y,0);
        glVertex3f(x1,0,0);
        glVertex3f(0,1,0);
        glNormal3f(0,0,-1);
        glVertex3f(x1,0,-1);
    glEnd();


    }

//funkcija za crtanje crne linije
void drawLine(){
    glBegin(GL_LINES);
    glVertex3f(1.0, 0.0, -1.0);
    glVertex3f(1.0, 0.0, 2.0);
    glEnd();


}


//funkcija za crtanje scene
void display(){
    //postavljanje pozadinske boje, color buffera...
    glClearColor(255, 255, 255, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


    //crtanje kocke
    glPushMatrix();
    //rotacija oko z osi
    glTranslatef(1, 0, 0);
    glRotatef(angle, 0, 0, -1);
    glTranslatef(-1, 0, 0);
    //debljina linije 4px
    glLineWidth(4.0);
    //zelena boja
    glColor3f(0.0, 1.0, 0.0);
    drawCubeGrid();
    glPopMatrix();



    //crtanje vode
    glPushMatrix();
    //rotacija oko z osi
    glTranslatef(1, 0, 0);
    glRotatef(angle,0,0,-1);
    glTranslatef(-1, 0, 0);
    //plava boja
    glColor3f(0.0, 0.0, 1.0);
    glTranslatef(1, 0, 1);

    //ako je kut manji od 45
    if(angle <= 45){
        //racunanje y tocke o ovisnosti o kutu
        float noviY = tan((45-angle) * 3.14 / 180);
        drawWater(-1,0,0);
        drawWater(-1,noviY,-1);
    }
    //ako je kut veci od 45
    else if (angle > 45 && angle != 90){
        //racunanje x tocke o ovisnosti o kutu
        float noviX = tan((90-angle) * 3.14 / 180);
        drawWater(-noviX,0,0);
    }
    else if (angle == 90){
        //ne crtaj vodu
    }
    glPopMatrix();


    //crtanje linije
    glPushMatrix();
    //debljina linije 2px
    glLineWidth(2.0);
    //crna boja
    glColor3f(0.0, 0.0, 0.0);
    drawLine();
    glPopMatrix();

    glutSwapBuffers();
}


//inicijalizacija
void init(){
    //omogucavanje depth bufferinga
    glEnable(GL_DEPTH_TEST);
    // osvjetljenje
    glEnable(GL_LIGHTING);
    //koristenje boje kao materijala
    glEnable(GL_COLOR_MATERIAL);

    //palimo svijetlo0 i pozicioniramo ga na (-1,1,1)
    glEnable(GL_LIGHT0);
    float pos[] = {-1, 1, 1, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

    //postavka pogleda
    glMatrixMode(GL_PROJECTION);
    gluPerspective(40.0, 1.0, 1.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
    gluLookAt(2.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.);

    //namjestanje scene
    glTranslatef(0.0, 0.0, 8.0);
    glRotatef(20, 1.0, 0.0, 0.0);
    glRotatef(-10, 0.0, 5.0, 0.0);
}


//timer funkcija
void timer(int) {
    if(animating==true){
        //granica animacije(90 stupnjeva)
        if(angle >= 90){
            //zaustavi animaciju, postavi kut na 90
            animating = false;
            angle = 90;
            //re-paint display
            glutPostRedisplay();
        }else{
            //povecaj kut
            angle += 0.25;
            //pozovi timer funkicju svakih 20ms
            glutTimerFunc(refresh, timer, 0);
            //re-paint display
            glutPostRedisplay();
        }
    }
}


//funkcija za pokretanje i zaustavljanje animacije tipkama s i e
void keys(unsigned char key, int, int){
    if (key == 's' || key == 'S'){
           animating = true;
           glutTimerFunc(0, timer, 0);
         }
       else if (key == 'e' || key == 'E')
           animating = false;
       else if (key == 27 )
           exit(0);

}


int main(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    //postavljanje prozora u gornji lijevi kut velicine 600x600
    glutInitWindowPosition(0,0);
    glutInitWindowSize(600, 600);

    //kreiranje prozora
    glutCreateWindow("Muskovic - Zadatak 4");

    //pozivanje funkcija
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keys);
    glutTimerFunc(0, timer, 0);

    glutMainLoop();
    return 0;
}
