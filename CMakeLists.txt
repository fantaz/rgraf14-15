cmake_minimum_required(VERSION 2.6)
project(zadaca)

find_package(GLUT)
find_package(OpenGL)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY
      ${PROJECT_BINARY_DIR}/bin CACHE PATH "Single Directory for all Executables."
  )

#idemo postaviti compiler flags za razne kompjalere
if(CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
    add_definitions(/W4)
    add_definitions(/MP)
elseif (CMAKE_COMPILER_IS_GNUCXX)# OR CMAKE_COMPILER_IS_GNUC)
  add_definitions(-std=c++0x)
  add_definitions(-Wall)
  add_definitions(-Wextra)
  add_definitions(-pedantic)
  else (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
  message(STATUS "Unknown build tool, cannot set warning flags for your")
endif (CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")

# include all dirs
include_directories(${GLUT_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR} ${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})

set(studenti
    babic
    baraba
    bezevan
    brlas
    buric
    burul
    butkovic
    cucic
    duhovic
    djuga
    fafandjel
    filipovic
    franc
    hrastnik
    hrzic
    ibrahimagic
    jakus
    jansky
    jugo
    juresa
    klobucar
    kovacevic
    kreso
    krizak
    krizanec
    krmpotic
    krsnik
    labrovic
    lackovic
    levacic
    licul
    mance
    mataja
    matkovic
    muskovic
    #novak
    pavlekovic
    pincic
    poljancic
    rescec
    ruzic
    smoljan
    #spetic
    simic
    svast
    terlevic
    terzic
    ticak
    tomic
    turkovic
    vlasic
)
foreach(subdir ${studenti})
    add_subdirectory(${subdir})
endforeach()
