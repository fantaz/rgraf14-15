#include <GL/glut.h>
#include <stdlib.h>

float kut_kuk_L=90.0;
float kut_koljeno_L=0.0;

float kut_kuk_R=90.0;
float kut_koljeno_R=0.0;

float kut_rame_L=180.0;
float kut_lakat_L=45.0;

float kut_rame_R=0.0;
float kut_lakat_R=-45.0;

float spustanje=0.0;
float rotacija_robota=0.0;

float visina_robota=1.0;

bool animacija=false;
bool cucanj=true;


void init(void) 
{
		//cistimo buffer za boju i dubinu
		glClearColor (0.0, 0.0, 0.0, 0.0);
		glClearDepth(1.0f);
		
		//omogucavanje osvjetljenja, dubine i sjencanja
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
	    glEnable(GL_LIGHT0);
	    glEnable(GL_COLOR_MATERIAL);
		glDepthFunc(GL_LEQUAL);
		glShadeModel (GL_FLAT);
		glShadeModel (GL_SMOOTH);
		
		//komponente osvjetljenja, pozicija svjetla
		GLfloat mat_specular[] = { 1.0, 1.0, 0.0, 1.0 };
		GLfloat mat_diffuse[] = { 0.8, 0.8, 0.8, 1.0 };
		GLfloat mat_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat mat_shininess[] = { 40.0 };
		GLfloat light_position[] = { 0.0, 5.0, 0.0, 1.0 };

		//dodjeljivanje materijalima parametre za model osvjetljanja te pozicioniranje svjetla
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
		glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
		glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}

void ruka(int R)
{
		//R==1 za lijevu ruku R==2 za desnu ruku
		//crta robotsku ruku u ishodistu
		float xx=0.0;
		float yy=0.0;
		float zz=0.0;
		GLUquadricObj *obj = gluNewQuadric();

		//ruka		
		glTranslatef(0.0, 0.0, 0.0);
		glPushMatrix();
		//rame sfera
		glColor3f(1.0,0.0,0.0);
		glTranslatef(xx, yy, zz);
		glutSolidSphere(0.25,100,100);
			glPushMatrix();
			//nadlaktica cilindar
			glColor3f(0.0,1.0,0.0);
			glRotatef(90, 0.0, 1.0, 0.0);
			//provjere dali je smo zvali funkciju za lijevu ili desnu ruku
			if(R==1)
			glRotatef(kut_rame_L, 1.0, 0.0, 0.0);
			if(R==2)
			glRotatef(kut_rame_R, 1.0, 0.0, 0.0);
			gluCylinder(obj, 0.125, 0.125, 2, 100, 100);
				glPushMatrix();
				//lakat sfera
				glColor3f(1.0,0.0,0.0);
				glTranslatef(xx, yy, zz+2);
				glutSolidSphere(0.25,100,100);
					glPushMatrix();
					//podlaktica cilindar
					glColor3f(0.0,1.0,0.0);
					//provjere dali je smo zvali funkciju za lijevu ili desnu ruku
					if(R==1)
					glRotatef(kut_lakat_L, 1.0, 0.0, 0.0);
					if(R==2)
					glRotatef(kut_lakat_R, 1.0, 0.0, 0.0);
					gluCylinder(obj, 0.125, 0.125, 2, 100, 100);
						glPushMatrix();
						//rucni zglob sfera
						glTranslatef(xx, yy, zz+2);
						glColor3f(1.0,0.0,0.0);
						glutSolidSphere(0.25,100,100);
							glPushMatrix();
							//saka kvadar
							glColor3f(0.0,0.0,1.0);
							glTranslatef(xx, yy, zz+0.5);
							glutSolidCube(0.5);
							glTranslatef(xx, yy, zz+0.5);
							glutSolidCube(0.5);
							glPopMatrix();
						glPopMatrix();
					glPopMatrix();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
}

void noga(int N)
{
		//N==1 za lijevu N==2 za desnu
		//crta robotsku nogu u ishodistu
		float xx=0.0;
		float yy=0.0;
		float zz=0.0;
		GLUquadricObj *obj = gluNewQuadric();

		//noga		
		glTranslatef(0.0, 0.0, 0.0);
		glPushMatrix();
		//kukovi sfera
		glColor3f(1.0,0.0,0.0);
		glTranslatef(xx, yy, zz);
		glutSolidSphere(0.5,100,100);
			glPushMatrix();
			//nadkoljenica cilindar
			glColor3f(0.0,1.0,0.0); 
			//provjera dali smo zvali funkciju za lijevu ili desnu nogu
			glRotatef(90, 0.0, 1.0, 0.0);
			if(N==1)
			glRotatef(kut_kuk_L, 1.0, 0.0, 0.0);
			if(N==2)
			glRotatef(kut_kuk_R, 1.0, 0.0, 0.0);
			gluCylinder(obj, 0.25, 0.25, 2, 100, 100);
				glPushMatrix();
				//koljeno sfera
				glColor3f(1.0,0.0,0.0);
				glTranslatef(xx, yy, zz+2);
				glutSolidSphere(0.5,100,100);
					glPushMatrix();
					//podkoljenica cilindar
					glColor3f(0.0,1.0,0.0);
					//provjera dali smo zvali funkciju za lijevu ili desnu nogu
					if(N==1)
					glRotatef(kut_koljeno_L, 1.0, 0.0, 0.0);
					if(N==2)
					glRotatef(kut_koljeno_R, 1.0, 0.0, 0.0);
					gluCylinder(obj, 0.25, 0.25, 2, 100, 100);
						glPushMatrix();
						//nozni zglob sfera
						glColor3f(255.0,0.0,0.0);
						glTranslatef(xx, yy, zz+2);
						glutSolidSphere(0.5,100,100);
							glPushMatrix();
							//stopalo kvadar
							glColor3f(0.0,0.0,1.0);
							glTranslatef(xx, yy, zz+1);
							glutSolidCube(1.0);
							glTranslatef(xx-1, yy, zz);
							glutSolidCube(1.0);
							glPopMatrix();
						glPopMatrix();
					glPopMatrix();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
}

void torso(void)
{
		//crta torso u ishodistu
		glPushMatrix();
		//torsi
		glColor3f(1.0,1.0,0.0);
		glScalef(1.0,2.0,0.5);
		glTranslatef(0.0,0.75,0.0);
		glutSolidCube(3.0);
		glPopMatrix();
           
}

void vrat_i_glava(void)
{		
		//crta glavu i vrat u ishodistu
		GLUquadricObj *obj = gluNewQuadric();
		
		glPushMatrix();
		//vrat cilindar
		glColor3f(0.0,1.0,0.0);
		glRotatef(90, 1.0, 0.0, 0.0);
		gluCylinder(obj, 0.5, 0.5, 1, 100, 100);
			glPushMatrix();
			//glava sfera
			glColor3f(1.0,0.0,0.0);
			glTranslatef(0.0, 0.0,-0.5);
			glutSolidSphere(1,100,100);
			glPopMatrix();
		glPopMatrix();
}

void model_robot(void)
{
		//rotacije robota
	    glRotatef(rotacija_robota,0,1.0,0.0);
	    //koriatimo za animaciju cucnja robota
	    glTranslatef(0.0,spustanje,0.0);
	   	glPushMatrix();
	   	
			//desna ruka
			glPushMatrix();
			glTranslatef(1.75, 4.5, 0.0);
			glRotatef(45,0.0,0.0,1.0);	
			ruka(2);
			glPopMatrix();
			
			//lijeva ruka
			glPushMatrix();
			glTranslatef(-1.75, 4.5, 0.0);	
			glRotatef(-45,0.0,0.0,1.0);
			ruka(1);
			glPopMatrix();
			
			//desna noga
			glPushMatrix();
			glTranslatef(1.0,-2.0,0.0);
			noga(2);
			glPopMatrix();
			
			//lijeva noga
			glPushMatrix();
			glTranslatef(-1.0,-2.0,0.0);
			noga(1);
			glPopMatrix();
							
			//torso
			glPushMatrix();
			torso();
			glPopMatrix();
			
			//vrat i glava
			glPushMatrix();
			glTranslatef(0.0,5.5,0.0);
			vrat_i_glava();
			glPopMatrix();
					
		glPopMatrix();
		glFlush ();
}

void animacija_cucnja(void)
{
	if(animacija)
		{
			if(kut_kuk_L>140)
				cucanj=false;
			if(kut_kuk_L<90)
				cucanj=true;
			if(cucanj)
			{
					kut_kuk_L=kut_kuk_L+0.5;
					kut_koljeno_L=kut_koljeno_L-0.5;
					
					kut_kuk_R=kut_kuk_R-0.5;
					kut_koljeno_R=kut_koljeno_R+0.5;
					
					kut_rame_L=kut_rame_L-1;
					kut_lakat_L=kut_lakat_L-0.5;
					
					kut_rame_R=kut_rame_R+1;
					kut_lakat_R=kut_lakat_R+0.5;
					
					spustanje=spustanje-0.01;
			}
			else
			{
					kut_kuk_L=kut_kuk_L-2;
					kut_koljeno_L=kut_koljeno_L+2;				
					
					kut_kuk_R=kut_kuk_R+2;
					kut_koljeno_R=kut_koljeno_R-2;
					
					kut_rame_L=kut_rame_L+4;
					kut_lakat_L=kut_lakat_L+2;
					
					kut_rame_R=kut_rame_R-4;
					kut_lakat_R=kut_lakat_R-2;
					
					spustanje=spustanje+0.04;
			}
			glutPostRedisplay();
		}
	
	
	
}

void display(void)
{
			glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glColor3f (1.0, 1.0, 1.0);
			glLoadIdentity ();             
			gluLookAt (0.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
			
			glScalef(visina_robota,visina_robota,visina_robota);
			model_robot();
			animacija_cucnja();
}

void reshape (int w, int h)
{
		   glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
		   glMatrixMode (GL_PROJECTION);
		   glLoadIdentity ();
		   glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
		   glMatrixMode (GL_MODELVIEW);
	}

void keyboard(unsigned char key, int x, int y)
{	
		//ove dvije linije ispod su samo zato da mi compiler ne javlja gresku o unused variables
		x=y;
		y=x;

	   switch (key) {
		  case 27:
			 exit(0);
			 break;
		  
		  case 'a':
			 animacija=true;
			 display();
			 break;
			 
		  case 'b':
			 animacija=false;
			 display();
			 break;
			 
		  case 'c':
			 rotacija_robota++;
			 display();
			 break;
			 
		  case 'd':
			 rotacija_robota--;
			 display();
			 break;
		}
}

int main(int argc, char** argv)
{
	   //inicijalizacija parametara prozora i scene
	   glutInit(&argc, argv);
	   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	   glutInitWindowSize (800, 600); 
	   glutInitWindowPosition (100, 100);
	   glutCreateWindow (argv[0]);
	   
	   init ();
	   
	   //inicijalizacija funkcija 
	   glutDisplayFunc(display); 
	   glutReshapeFunc(reshape);
	   glutKeyboardFunc(keyboard);
	   glutMainLoop();
	   
	   return 0;
}
