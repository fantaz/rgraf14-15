#include<GL/glut.h>
#include<stdio.h>
#include<iostream>
#include<math.h>

/// Inicijalizacija potrebnih parametara: radiusa sfere, pocetne pozicije, brzine padanja
float radius = 0.5f, radius_of_outer_cube = radius+0.01;
float x_begin = -1.0f, y_begin = 1.0f, z_begin = -5.0f;
float speedx = 0.015f, speedy = 0.0f, speedy_increase = 0.0009;
float rotate_cubex = -10.0f, rotate_cubey = -25.0f;
bool animate = false;

/// Funkcija u kojoj se inicijaliziraju svijetlosti i omogućavaju sjene
void myinit(){
    // Podesavanje svijetla
    GLfloat LightModelAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightModelAmbient);

    GLfloat LightAmbient[] = {0.33f, 0.33f, 0.33f, 1.0f};
    glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);

    GLfloat LightDiffuse[] = {0.67f, 0.67f, 0.67f, 1.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);

    // Podesavanje svijetla materijala
    GLfloat MaterialAmbient[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, MaterialAmbient);

    GLfloat MaterialDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, MaterialDiffuse);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glShadeModel(GL_FLAT);
    glEnable(GL_NORMALIZE);
}

/// Funkcija za crtanje crvene sfere(kugle)
void sphere(){
    glColor3f(1.0f, 1.0f, 0.0f);
    glPushMatrix();
    glutSolidSphere(radius,100,100);
    glPopMatrix();
}


/// Funkcija za crtanje unutarnje plave kocke
void innerCube(){
    glColor3f(0.0f, 0.0f, 1.0f);

    glPushMatrix();

    // Gornja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f( radius, radius,  radius);
    glVertex3f(-radius, radius,  radius);
    glVertex3f(-radius, radius, -radius);
    glVertex3f( radius, radius, -radius);
    glEnd();

    // Donja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f( radius, -radius, -radius);
    glVertex3f(-radius, -radius, -radius);
    glVertex3f(-radius, -radius, radius);
    glVertex3f( radius, -radius, radius);
    glEnd();

    // Prednja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( radius, -radius, radius);
    glVertex3f(-radius, -radius, radius);
    glVertex3f(-radius,  radius, radius);
    glVertex3f( radius,  radius, radius);
    glEnd();


    // Straznja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( radius, radius, -radius);
    glVertex3f(-radius, radius, -radius);
    glVertex3f(-radius, -radius, -radius);
    glVertex3f( radius,  -radius, -radius);
    glEnd();

    // Lijeva ploha
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(-radius,-radius, radius);
    glVertex3f(-radius,-radius,-radius);
    glVertex3f(-radius, radius,-radius);
    glVertex3f(-radius, radius, radius);
    glEnd();
    glPopMatrix();
}

/// Funkcija koja crta vanjsku crvenu kocku
void outerCube(){
    glColor3f(1.0f, 0.0f, 0.0f);
    glPushMatrix();

    // Gornja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f,1.0f,0.0f);
    glVertex3f( radius_of_outer_cube, radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, radius_of_outer_cube,  radius_of_outer_cube);
    glVertex3f( radius_of_outer_cube, radius_of_outer_cube,  radius_of_outer_cube);
    glEnd();

    // Donja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f,-1.0f,0.0f);
    glVertex3f( radius_of_outer_cube, -radius_of_outer_cube,  radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube,  radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f( radius_of_outer_cube, -radius_of_outer_cube, -radius_of_outer_cube);
    glEnd();

    // Prednja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f( radius_of_outer_cube,  radius_of_outer_cube, radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube,  radius_of_outer_cube, radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube, radius_of_outer_cube);
    glVertex3f( radius_of_outer_cube, -radius_of_outer_cube, radius_of_outer_cube);
    glEnd();

    // Straznja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f( radius_of_outer_cube, -radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube,  radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f( radius_of_outer_cube,  radius_of_outer_cube, -radius_of_outer_cube);
    glEnd();

    // Lijeva ploha
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-radius_of_outer_cube,  radius_of_outer_cube,  radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube,  radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube, -radius_of_outer_cube);
    glVertex3f(-radius_of_outer_cube, -radius_of_outer_cube,  radius_of_outer_cube);
    glEnd();

    glPopMatrix();
}

/// Funkcija koja kreira frustum kako bi mogli ustanoviti ako je sfera izvan pogleda
float frustum[6][4];
void ExtractFrustum()
{
    float   proj[16];
    float   modl[16];
    float   clip[16];
    float   t;

    glGetFloatv( GL_PROJECTION_MATRIX, proj );
    glGetFloatv( GL_MODELVIEW_MATRIX, modl );

    // Kombiniramo dvije matrice (multiply projection by modelview)
    clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
    clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
    clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
    clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

    clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
    clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
    clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
    clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

    clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
    clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
    clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
    clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

    clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
    clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
    clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
    clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

    // Podaci za desnu plohu
    frustum[0][0] = clip[ 3] - clip[ 0];
    frustum[0][1] = clip[ 7] - clip[ 4];
    frustum[0][2] = clip[11] - clip[ 8];
    frustum[0][3] = clip[15] - clip[12];

    // Normalizacija
    t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
    frustum[0][0] /= t;
    frustum[0][1] /= t;
    frustum[0][2] /= t;
    frustum[0][3] /= t;

    // Podaci za lijevu plohu
    frustum[1][0] = clip[ 3] + clip[ 0];
    frustum[1][1] = clip[ 7] + clip[ 4];
    frustum[1][2] = clip[11] + clip[ 8];
    frustum[1][3] = clip[15] + clip[12];

    // Normalizacija
    t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
    frustum[1][0] /= t;
    frustum[1][1] /= t;
    frustum[1][2] /= t;
    frustum[1][3] /= t;

    // Podaci za donju plohu
    frustum[2][0] = clip[ 3] + clip[ 1];
    frustum[2][1] = clip[ 7] + clip[ 5];
    frustum[2][2] = clip[11] + clip[ 9];
    frustum[2][3] = clip[15] + clip[13];

    // Normalizacija
    t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
    frustum[2][0] /= t;
    frustum[2][1] /= t;
    frustum[2][2] /= t;
    frustum[2][3] /= t;

    // Podaci za gornju plohu
    frustum[3][0] = clip[ 3] - clip[ 1];
    frustum[3][1] = clip[ 7] - clip[ 5];
    frustum[3][2] = clip[11] - clip[ 9];
    frustum[3][3] = clip[15] - clip[13];

    // Normalizacija
    t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
    frustum[3][0] /= t;
    frustum[3][1] /= t;
    frustum[3][2] /= t;
    frustum[3][3] /= t;

    // Podaci za far-plane plohu
    frustum[4][0] = clip[ 3] - clip[ 2];
    frustum[4][1] = clip[ 7] - clip[ 6];
    frustum[4][2] = clip[11] - clip[10];
    frustum[4][3] = clip[15] - clip[14];

    // Normalizacija
    t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
    frustum[4][0] /= t;
    frustum[4][1] /= t;
    frustum[4][2] /= t;
    frustum[4][3] /= t;

    // Podaci za near-plane plohu
    frustum[5][0] = clip[ 3] + clip[ 2];
    frustum[5][1] = clip[ 7] + clip[ 6];
    frustum[5][2] = clip[11] + clip[10];
    frustum[5][3] = clip[15] + clip[14];

    // Normalizacija
    t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
    frustum[5][0] /= t;
    frustum[5][1] /= t;
    frustum[5][2] /= t;
    frustum[5][3] /= t;
}


/// Funkcija koja testira ako je sfera u frustumu
bool SphereInFrustum( float x, float y, float z, float radius )
{
    int p;
    for( p = 0; p < 6; p++ )
        if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= -radius )
            return false;
    return true;
}


static GLfloat theta[]={0.0f,0.0f,0.0f};
static GLint axis=2.0f;
static GLfloat translate[]={0.0f,0.0f,0.0f};

/// Funkcija koja rotira i pomice sferu
void spinsphere(){

    // Uvijet koji sluzi za provjeru ako je pauzirana animacija, ako je pauzirana, tada se sfera ne pomice
    if (animate){

        // Racunanje brzine pomaka
        if (translate[0] > radius){
            speedy += speedy_increase;
        }
        translate[0] += speedx;
        translate[1] -= speedy;
        translate[2] += 0.0f;

        // Racunanje rotacije
        theta[axis]-=10.0f;
        if(theta[axis]>360.0f)
            theta[axis]-=360.0f;
        if (!SphereInFrustum(translate[0]+x_begin, translate[1]+y_begin, translate[2]+z_begin, radius)){exit(1);}
    }
    glutPostRedisplay();

}

/// Funkcija koja prikazuje i crta scenu
void display(){
    // Izracunavanje rotacije
    spinsphere();

    // Pozicija svijetla
    static float LightPosition[] = {1.0f, -1.0f, -4.0f, 1.0f};
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);

    // Izracunavanje frustuma
    ExtractFrustum();

    // Translacija u pocetni polozaj
    glTranslatef(x_begin, y_begin, z_begin );
    glRotatef( rotate_cubey, 0.0, 1.0, 0.0 );
    glRotatef( rotate_cubex, 1.0, 0.0, 0.0);

    // Cratanje kocki
    innerCube();
    outerCube();

    // Translacija i rotacija sfere
    glTranslatef(translate[0],translate[1],translate[2]);
    glRotatef(theta[2],0.0f,0.0f,1.0f);

    // Crtanje sfere
    sphere();

    glutSwapBuffers();

}

/// Postavljanje timera za interval precrtavanja
void timer(int time) {
    glutTimerFunc(time, timer, 500);
    glutPostRedisplay();
}


/// Postavljanje viewporta
void myreshape(int w,int h){
    // Testiranje visine prozora( prozor ne smije biti visok 0
    if(h == 0)
        h = 1;
    float ratio = 1.0* w / h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Postavljanje viewporta da bude cijeli prozor
    glViewport(0, 0, w, h);

    // Postavljanje perspektive
    gluPerspective(45,ratio,1,1000);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

/// Funkcija koja se poziva kada se pritisne odgovarajuca tipka
void keyboard(unsigned char key, int , int ){
    switch (key) {
    case 27:{exit(0); break;}
    case 103:{animate = true; break;}
    case 115:{animate = false; break;}
    default : break;
    }
}

/// Main funkcija
int main(int argc,char **argv){
    // Testni primjeri
    if (argc >1){
        if ( *argv[1]=='t' ){
            if ( *argv[2]=='0' ){
                radius = 0.1f;
                radius_of_outer_cube = radius+0.01;
                x_begin = -1.0f, y_begin = 1.0f, z_begin = -3.0f;
                rotate_cubex = -5.0f;
                rotate_cubey = -30.0f;
            }
            if ( *argv[2]=='1' ){
                radius = 0.7f;
                radius_of_outer_cube = radius+0.01;
                x_begin = -1.25f, y_begin = 1.25f, z_begin = -6.0f;
                rotate_cubex = 0.0f;
                rotate_cubey = -20.0f;
            }
            if ( *argv[2]=='2'){
                radius = 1.2f;
                radius_of_outer_cube = radius+0.01;
                x_begin = -1.25f, y_begin = 1.25f, z_begin = -7.5f;
                rotate_cubex = 5.0f;
                rotate_cubey = -45.0f;
            }
        }
        if (*argv[1] == 'r'){
            radius = atof(argv[2]);;
            radius_of_outer_cube = radius+0.01;
        }
    }
    // Incijalizacija prozora i potrebnih funkcija
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(500,500);
    glutInitWindowPosition(500,150);
    glutCreateWindow("TEST");

    glutReshapeFunc(myreshape);
    glutDisplayFunc(display);
    //glutIdleFunc(spinsphere);
    glutKeyboardFunc(keyboard);

    myinit();
    glutTimerFunc(0,timer, 500);
    glutMainLoop();
    return 0;
}
