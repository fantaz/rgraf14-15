Drugi zadatak

Ovdje se nalaze reference, dok je kod komentiran u cpp-u.

1) Pokretanje:
	Projekt se moze pokrenuti uz nekoliko opcija iz comand line-a: "t 0", "t 1", "t 2" daju 3 primjera programa dok "r n" pri cemu je n broj tipa float moze mijenjati radijus kugle

2) Izvedba i stvaranje programa:
	1) Temeljna ideju sam dobio iz dva koda koji su mi posluzili kao ogledni primjeri za moj kod, jedan je kod 6-te vjezbe iz kolegija racunalna grafika, a drugi
	   se nalazi na linku : https://www.opengl.org/discussion_boards/showthread.php/167622-Rotate-sphere-and-move-it
 
        2) Nakon što sam proučio vjezbu 1 i 2 koje se nalaze unutar kolegija, krenuo sam jos podrobnije prouciti kordinatne sustave u opengl-u pri tome mi je pomogao sadrzaj
	   na stranici: http://www.falloutsoftware.com/tutorials/gl/gl0.htm, http://www.matrix44.net/cms/notes/opengl-3d-graphics/coordinate-systems-in-opengl

	3) Zatim sam naisao na problem frustuma-odnosno detekcije izlaska kugle iz pogleda. Nakon trazenja adekvatnog rjesenja, a istovremeno meni razumljivog, pronasao sam sljedeci sadrzaj
	   koji sam implementirao u svoj kod: http://www.matrix44.net/cms/notes/opengl-3d-graphics/coordinate-systems-in-opengl

        4) Buduci da sam htio koristiti glviewport, a ne glortho nacin pogleda, proucio sam sljedeci tutorijal: http://content.gpwiki.org/index.php/OpenGL:Tutorials:Tutorial_Framework:First_Polygon
           Razliku izmedju ta dva pristupa sam nasao objasnjenu u postu na sljedecoj stranici: http://stackoverflow.com/questions/2099211/gluortho2d-and-glviewport

3) Ispravak greske-->ukazali su mi kolege da bi trebala dodat timer funkciju radi razlicitih hardware-skih okruzenje pa sam to i ucinio.
