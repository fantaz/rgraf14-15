Kori�teni kod: 
	https://www.opengl.org/discussion_boards/showthread.php/167622-Rotate-sphere-and-move-it

Varijable:
	speed ->veli�ina koraka kod pomaka sfere
	radius -> radius sfere

	milisec -> svakih koliko milisekundi timer okida

	translate[] -> x, y, z vrijednosti za translaciju 

	i -> broj koraka 
	flag -> 1- animacija se izvr�ava; 0- animacija stoji
	x1; x2; x0; x1_old; x1_new; y1_new -> varijable za odre�ivanje pozicija to�aka kod crtanja traga sfere

Tipke za kontrolu animacije:
	s - start
	x - pause
	Esc - exit
	*izlaz iz programa kada sfera iza�e iz vidljivog dijela

Rje�enje:

	Sfera se iscrtava pa translatira odre�eni broj puta po x osi, zatim po y osi i onda opet po x osi za 'speed' svakih 'milisec'.
	Trag se iscrtava pomo�u 3 dijela: prvi od po�etne to�ke do to�ke gdje se sfera po�ela gibati po y osi, drugi uzima krajnju to�ku
	prvoga kao po�etnu i crta se dok se sfera ne po�ne opet gibati po x osi. Tre�i trag uzima krajnju to�ku drugoga kao po�etnu i 
	prati sferu do kraja animacije.

	Prije translacije sfere provjerava se 'flag', pa se ovisno o tome je li varijabla 1 ili 0 animacija izvr�ava ili stoji. 

	