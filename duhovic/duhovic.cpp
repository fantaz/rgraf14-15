#include <GL/glut.h>
#include <stdio.h>
#include <iostream>
 
float speed=0.005;
float radius= 0.3;
int milisec=10;
static GLfloat translate[]={0.0,0.0,0.0};
int i=0;
int flag=0;
float x1=0;
float x2=0;
float x0 = 0.0, x1_old = 0;
float x1_new, y1_new;
 
void myinit(){
    GLfloat mat_ambient[]={0.3,0.3,0.3,1.0};
    GLfloat mat_diffuse[]={0.6,0.6,0.6,1.0};
    GLfloat mat_specular[]={0.9,0.9,0.9,1.0};
    GLfloat mat_shininess[]={100.0};
 
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,mat_ambient);
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,mat_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininess);
 
    GLfloat light0_ambient[]={0.5,0.5,0.5,1.0};
    GLfloat light0_diffuse[]={1.0,1.0,1.0,1.0};
    GLfloat light0_specular[]={1.0,1.0,1.0,1.0};
    GLfloat light0_position[]={5.0,5.0,2.0,0.0};
 
    glLightfv(GL_LIGHT0,GL_AMBIENT,light0_ambient);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
    glLightfv(GL_LIGHT0,GL_POSITION,light0_position);
    glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,light0_ambient);
 
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
 
void sphere(){
    glColor3f(0.1f,1.0f,0.1f);
    glPushMatrix();
    glTranslatef( translate[0], translate[1], translate[2] );
    glutSolidSphere(radius,100,100);
    glPopMatrix();
}
 
 
void trace1(){
    if (i<1000){
        x1_old = x1;
        }
    glColor4f(1.0f,0.0f,0.0f, 0.5f);		
    glPushMatrix();
    glBegin(GL_POLYGON);                                    
    glVertex3f(x0, -radius, 0.0f);   	 // The bottom left corner
    glVertex3f(x1_old, -radius, 0.0f); 	 // The bottom right corner translate[1]-radius
    glVertex3f(x1_old, radius, 0.0f);    // The top right corner translate[1]+radius
    glVertex3f(x0, radius, 0.0f);        // The top left corner
    glEnd();
    glPopMatrix();
}
 
void trace2(){
    if ( i>1000 && i<2000){
		x1_new=translate[0]; 
		y1_new=translate[1];
        }
    glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(x1_old, -radius, 0.0f);       	// The bottom left corner
    glVertex3f(x1_new, y1_new-radius, 0.0f);    // The bottom right corner
    glVertex3f(x1_new, y1_new+radius, 0.0f);    // The top right corner
    glVertex3f(x1_old, radius, 0.0f);            // The top left corner
    glEnd();
    glPopMatrix();
}
 
void trace3(){
    if (i>2000){
                glColor4f(1.0f,0.0f,0.0f, 0.5f);
                glPushMatrix();
                glBegin(GL_POLYGON);                            
                glVertex3f(x1_new, y1_new+radius, 0.0f);        // The bottom left corner
                glVertex3f(x2, y1_new+radius, 0.0f);   		    // The bottom right corner
                glVertex3f(x2, y1_new-radius, 0.0f);    		// The top right corner 
                glVertex3f(x1_new, y1_new-radius, 0.0f);        // The top left corner
                glEnd();
                glPopMatrix();
    }
}
 
void translatesphere(){
   if (flag==true) {
       i+=2;
       if (i<1000){
           translate[0] += speed;
           x1=translate[0];
           flag = 1;
       }
       else if ((i>999) && (i<2000)){
           translate[0] -= speed/2;
           translate[1]-= speed;
           
       }
       else {
           translate[0]+=speed;
           x2=translate[0];
       }
    }              
}
 
void display(){
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt (2.0, 2.0, 2.0, 1.5, -3.0, 0.0, 0.0, 1.0, 0.0);
    glScalef (1.0, 1.0, 1.0);
    glRotatef(-10,0.0f,1.0f,0.0f);
    sphere();
    trace1();
    trace2();
    trace3();
    translatesphere();
    if (i>=3170) exit(0);
    glutSwapBuffers();
}
 
 
 
 
void myreshape(int w,int h){
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(w<=h)
        glOrtho(-2.0,2.0,-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-10.0,10.0);
    else
        glOrtho(-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-2.0,2.0,-10.0,10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
 
void keypressed(unsigned char key, int, int){
    switch(key){
    case 27:{exit(0); break;}
    case 120:{flag = false; break;}
    case 115:{flag = true; break;}
    default : break;
    }
}
 
 
void timer(int time){
    glutTimerFunc(time, timer, time);
    glutPostRedisplay();
}
 
int main(int argc,char **argv){
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);
    glutInitWindowSize(700,700);
    glutInitWindowPosition(30,30);
    glutCreateWindow("RAC_GRAF");
    glutReshapeFunc(myreshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(keypressed);
    myinit();
    glutTimerFunc(0,timer, milisec);
    glutMainLoop();
    return 0;
}