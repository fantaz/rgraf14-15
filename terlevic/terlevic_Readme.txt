Opis
Plesac

gluSolidSphere
glutSolidCube
gluCylinder

Funkcija "gluSolidSphere" sluzi za stvaranje kugle
Funkcija "glutSolidCube" sluzi za stavranje kocke ili kvadra
Funkcija "gluCylinder" sluzi za stvaranje valjka 


Kod ima komentare za svaki pojedini dio robota.

KORISTENJE PROGRAMA:
Tipka ESC za zaustavljanje programa nakon pokretanja


Reference
https://mudri.uniri.hr
https://www.opengl.org
http://stackoverflow.com/questions/9415761/using-cmake-with-qt-creator
http://qt-project.org/wiki/Introduction-to-Qt3D
http://qt-project.org/forums/viewthread/17529
http://qt-project.org/doc/qt-4.8/qdeclarativeanimation.html