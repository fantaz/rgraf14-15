#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
GLUquadric *quad = gluNewQuadric();
void drawCylinder(float radius) {
    glBegin(GL_ELEMENT_ARRAY_POINTER_ATI);
    for(double i = 0; i < 2 * M_PI; i += M_PI / 6)
        glVertex3f(cos(i) * radius, sin(i) * radius, 0.0);
    glEnd();
    glBegin(GL_POLYGON);
    for(double i = 0; i < 2 * M_PI; i += M_PI / 6)
        glVertex3f(cos(i) * radius, sin(i) * radius, 0.0);
    glEnd();
}
float angle = 0.0;
void display()
{
    glClearColor(0, 0, 0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // Robot svi djelovi
    glTranslatef(0, 0, -10); // Scena
    glRotatef(angle, 0, 1, 0); // Rotacija robota
    glPushMatrix(); // Torzo
        glPushMatrix();
            glScalef(1.4, 2, 0.5);
            glutSolidCube(1.);
        glPopMatrix();
        glPushMatrix(); // Vrat
            glTranslatef(0, 1.2, 0);
            glPushMatrix();
                glRotatef(90, 1, 0, 0);
                gluCylinder(quad, 0.1, 0.1, .2, 10, 10);
            glPopMatrix();
            glPushMatrix(); // Glava
                glTranslatef(0, 0.4, 0);
                glutSolidSphere(.4, 12, 12);
            glPopMatrix(); // Kraj glave
            glPopMatrix(); // Kraj vrata
        glPushMatrix(); // Lijeva nadlaktica
            glTranslatef(0.7, 1, 0);
            glRotatef(90, 0, 1, 0);
            glRotatef(45, 1, 0, 0);
            glRotatef(angle, 0, 0, 1); // Animacija lijeve ruke
            gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);
            glPushMatrix(); // Lijeva podlaktica
                glTranslatef(0, 0, 1.5);
                glRotatef(45, 1, 0, 0);
                gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);
                glPushMatrix(); // Lijeva saka
                    glTranslatef(0, 0, 1.5);
                    glPushMatrix();
                        glScalef(0.5, 1, 2.5);
                        glTranslatef(0,0,.1);
                        glutSolidCube(0.2);
                    glPopMatrix();
                glPopMatrix(); // Kraj lijeve sake
            glPopMatrix(); // Kraj lijeve podlaktice
        glPopMatrix(); // Kraj lijeve nadlaktice
        glPushMatrix(); // Desna nadlaktica
            glTranslatef(-0.7, 1, 0);
            glRotatef(-90, 0, 1, 0);
            glRotatef(45, 1, 0, 0);
            glRotatef(angle, 0, 0, 1); // Animacija desne ruke
            gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);
            glPushMatrix(); // Desna podlaktica
                glTranslatef(0, 0, 1.5);
                glRotatef(45, 1, 0, 0);
                gluCylinder(quad, 0.1, 0.1, 1.5, 10, 1);
                glPushMatrix(); // Desna saka
                    glTranslatef(0, 0, 1.5);
                    glRotatef(30, 0, 1, 1);
                    glPushMatrix();
                        glScalef(0.5, 1, 2.5);
                        glTranslatef(0,0,.1);
                        glutSolidCube(0.2);
                    glPopMatrix();
                glPopMatrix(); //Kraj desne sake
                glPopMatrix(); //  Kraj desne podlaktica
        glPopMatrix(); // Kraj desne nadlaktice
        glPushMatrix(); // Lijeva natkoljenica
            glTranslatef(-0.7, -1, 0);
            glRotatef(70, 1, 0, 0);
             glRotatef(angle, 0, 0, 1);
            gluCylinder(quad, 0.3, 0.3, 1.5, 10, 1);
            glPushMatrix(); // Lijeva podkoljenica
                glTranslatef(0, 0, 1.5);
                glRotatef(45, 1, 0, 0);
                gluCylinder(quad, 0.25, 0.25, 1.5, 10, 1);
                glPushMatrix(); // Lijevo stopalo
                    glTranslatef(0, 0, 1.5);
                    glRotatef(90, 1,0,0);
                    glPushMatrix();
                        glScalef(0.4, 0.2, .7);
                        glTranslatef(0,0,-.5);
                        glutSolidCube(1);
                    glPopMatrix();
                glPopMatrix(); // Kraj lijevog stopala
            glPopMatrix(); // Kraj lijeve podkoljenice
        glPopMatrix(); // Kraj lijeve nadkoljenice
        glPushMatrix(); // Desna natkoljenica
            glTranslatef(0.7, -1, 0);
            glRotatef(90, 1, 0, 0);
             glRotatef(angle, 0, 0, 2);
            gluCylinder(quad, 0.3, 0.3, 1.5, 10, 1);
            glPushMatrix(); // Desna podkoljenica
                glTranslatef(0, 0, 1.5);
                glRotatef(45, 1, 0, 0);
                gluCylinder(quad, 0.25, 0.25, 1.5, 10, 1);
                glPushMatrix(); // Desno stopalo
                    glTranslatef(0, 0, 1.5);
                    glRotatef(90, 1,0,0);
                    glPushMatrix();
                    glScalef(0.4, 0.2, .7);
                        glTranslatef(0,0,-.5);
                        glutSolidCube(1);
                    glPopMatrix();
                glPopMatrix(); // Kraj desnog stopala
            glPopMatrix(); // Kraj desne podkoljenice
        glPopMatrix(); // Kraj desne nadkoljenice
    glPopMatrix(); // Kraj Torza
    // Animation
    angle += 0.05;
    glutSwapBuffers();
    glutPostRedisplay();
}
void keyboard(unsigned char key, int, int)
{
    switch (key)
    {
    case 27: //Escape key
        exit(0);
    }
}
void resize(int w, int h)
{
    if (h == 0) { h = 1; }
    float ratio = 1.0* w / h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,ratio,1,1000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
void init() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);
    float pos[] = {0.5, 0.5, 0.5, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
}
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);
    glutCreateWindow("Robot");
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);
    glutMainLoop();
    return 0;
}