#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <map>
#include <set>
#include <vector>
#include <queue>
using namespace std;

const double M__PI = 4.*atan(1);

//Rotation angle of the ball
float angle = 0;
//Boolean indicating if the animation is currently running or not
bool isAnimating = false;
int win_id;
int glut_lastUpdate;
//ligting
GLfloat white_light[] = {1.0, 1.0, 1.0, 1.0};
GLfloat white_light_amb[] = {.3,.3,.3, 1.0};
GLfloat light_position[] = {.5,.5,-3.5,1.};
//animating parameter
float sphere_position[3] = {0, 0, 0};
GLfloat BALL_SPEED = 0.4;
GLfloat gravity = 0.1;
GLfloat ball_y_speed = 0;

void init() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glShadeModel(GL_FLAT);
    glDepthFunc(GL_LEQUAL);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);// set the light source position
    glLightfv(GL_LIGHT0, GL_AMBIENT, white_light_amb);// set the ambient color
    glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);// set the diffuse color
    glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);// set the specular color
}

void resize(int w, int h)
{
    glViewport(0, 0, w, h);
    if (h == 0) {
        h = 1;
    }
    float ratio = 1.0* w / h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,ratio,1,1000); // view angle u y, aspect, near, far
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void keyboard(unsigned char key, int, int) {
    switch(key) {
    case 27:
        glutDestroyWindow(win_id);
        break;
    case 'g':
        glut_lastUpdate=glutGet(GLUT_ELAPSED_TIME);
        isAnimating=true;
        break;
    case 's':
        isAnimating=false;
        break;
    }
}

//GLUT idle function: this function is called when glut processed all events in queue and become idle
void update() {
    if(isAnimating) { //if the animation is on going
//Calculate the eslaped time since the last update to sync the animation to real timing
        int lastUpdate=glutGet(GLUT_ELAPSED_TIME);
        double dt = 1e-3 * (lastUpdate - glut_lastUpdate);
//dt is in seconds.
        if(dt>=0) {
            double spx = BALL_SPEED * dt;
            angle += spx / (M_PI) * 360.; //rotation angle is calculated by delta X
            sphere_position[0] += spx;
            if(sphere_position[0]>0.5) {
// if the center of sphere passed the box's bottom border,
// start gravity related animations.
                ball_y_speed -= gravity * dt;//speed of the ball on Oy is now changed by gravity
                sphere_position[1] += ball_y_speed * dt; // update position according to speed
            }
            glutPostRedisplay();
        }
        glut_lastUpdate=lastUpdate;
    }
}

//draw the cube
void renderCube(int flip=1) {
    static float cube[8][3] = {
        {0, 0, 0},
        {0, 0, 1},
        {0, 1, 0},
        {0, 1, 1},
        {1, 0, 0},
        {1, 0, 1},
        {1, 1, 0},
        {1, 1, 1}
    };
    static float normal[][3] = {
        {-1,0,0},
        {0,-1,0},
        {0,-1,0},
        {0,0, 1},
        {0,0, 1},
    };
    static float color_red[] = {1,0,0,1};
    static float color_blue[] = {0,0,1,1};
    static float* colors[] = {
        color_red,
        color_red,
        color_blue,
        color_blue,
        color_red,
    };
    static int indicators[]= {0,1,3,2, 0,1,5,4, 2,3,7,6, 0,2,6,4, 1,3,7,5};
    static int ind_size = sizeof(indicators) / sizeof(*indicators);
    glBegin(GL_QUADS);
    for(int i=0; i<ind_size; i+=4)
        glColor3fv(colors[i/4]),
                   glNormal3f(normal[i/4][0]*flip,normal[i/4][1]*flip,normal[i/4][2]*flip),
                   glVertex3fv(cube[indicators[i]]), glVertex3fv(cube[indicators[i+1]]),
                   glVertex3fv(cube[indicators[i+2]]), glVertex3fv(cube[indicators[i+3]]);
    glEnd();
}

void render(void) {
    glClearColor(1, 1, 1, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

// Position scene
    glTranslatef(0, 0, -10); // Step back to see the scene
    glTranslatef(-3, 0, 0); // Set the scene to the right
    glTranslatef(0, 1, 0); // Set the scene up

//first render the cube
    glPushMatrix();
    renderCube();
    glPopMatrix();

//render the ball
    glPushMatrix();
    glColor3f(1.0, 1.0, 0.0);

//translate the ball to its position
    glTranslatef( 0.5 + sphere_position[0],
                  0.5 + sphere_position[1],
                  0.5 + sphere_position[2]
                );
//apply rotation
    glRotatef(-angle,0,0,1);
    glutSolidSphere(0.5, 20, 20);
    glPopMatrix();

//update the display
    glutSwapBuffers();
    glutPostRedisplay();
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(800,800);
    win_id = glutCreateWindow("2.zadatak");
    glutDisplayFunc(render);
    glutReshapeFunc(resize);
    glutIdleFunc(update);
    glutKeyboardFunc(keyboard);
    init();
    glutMainLoop();
    return 0;
}
