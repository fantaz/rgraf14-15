Vrijednosti za prikaz scene:
	-Difuzno svijetlo {1.0, 1.0, 1.0, 1.0}
	-Ambijentalno svijetlo {.3,.3,.3, 1.0}
	-Pozicija svijetlosti {.5,.5,-3.5,1.}
	
Kontroliranje programa:
	Program se može kontrolirati tipkovnicom.
		G - pokreće animaciju
		S - zaustavlja animaciju
		Esc - izlazi se iz programa

Pomoću glTranslatef funkcije pomičen sferu, dok sa glRotatef rotiram sferu.
Veličina prozora OpenGL-a je određena na 800x800.

Razvojna okolina
	koristen je QtCreator 5.2.1 na Kubuntu 14.10
	koristeni su glut i gl paket

Reference
	
	Stack Overflow
	http://www.gamedev.net/page/resources/_/technical/opengl/lighting-and-normals-r1682
	http://www.swiftless.com/tutorials/opengl/rotation.html