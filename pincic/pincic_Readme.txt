Zadatak 4.

NAPOMENA: novi kod (odnosno 2. commit) je preraden da radi na Linuxu, ali na linuxu mi ne radi funkcija koja definira debljinu linija
          kocke i podloge. Na Windowsu sve radi kako treba.

1.)Kako koristiti program
	- pokretanje iz nekog od IDE-a, npr Visual C++ 2010 Express
	
2.)Tipke za navigaciju i kontrolu
	- tipka 's' pokrece animaciju
	- tipka 'e' zaustavlja (pauzira) animaciju
	- tipa 'Esc' prekida i izlazi iz programa
	
3.)Reference
	- 6. lab vjezba
	- sluzbena dokumentacija OpenGL-a, www.opengl.org/sdk/docs/man2
	- razni online tutoriali povezani s osvjetljenjem, npr. http://www.sjbaker.org/steve/omniv/opengl_lighting.html
	- Stack Overflow
	
4.)Odabir vrijednosti varijabli
	- varijabla 'refresh' - interval renderiranja scene - 0.015sec
		- ukupno trajanje animacije = 9sec (kako animacija ne bi bila prekratka niti preduga), 4.5sec po dijelu animacije
		- uz otprilike ~65 rendera po sekundi (kako bi animacija izgledala fluidno) -> 9/(9*65)~=0.015 
	- vrijednosti argumenata unutar glVertex3f() funkcija za kocku koja aproksimira vodu - 
	  uzeo sam vrijednost pomaka od 0.02f kako bi se dobio dojam da spremnik doista sadrzi vodu,
	  odnosno time sam definirao debljinu spremnika (zelene zicane kocke) od 0.02f 
	- definirana su 3 tipa osvijetljenja: ambijentalno, zrcalno i difuzno, svi pomaknuti 
	  od sredista kugle kako bi se prikazala razlika u osvijetljenju kako se kugla priblizava
	  izvorima svijetla
	- varijabla 'proslovrijeme' sluzi za odabir rendera ovisno o tome koji dio animacije se treba prikazat
	- dodatni komentari nalaze se u samom kodu
	
5.)Razvojna okolina
	- koristen je Microsoft Visual C++ 2010 express na Windows 8.1 OS-u
	- koristeni su glut i glew paketi
	
