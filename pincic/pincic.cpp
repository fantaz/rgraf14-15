#include <stdlib.h>
#include <GL/glut.h>

#include <iostream>
using  namespace std;

float xx=1.0f-0.02f;
float proslovrijeme=0;
float yy=1.0f-0.02f;
float rotationAngle=-90.0f;//rotira se za 90 stupnjeva (-90->0)
int refresh=15;
int animacija=0;

void Keys(unsigned char key,int,int)
{
    /*key=x;
    key=y;*/
    switch (key) {
        case 27:
            exit(0);//izadi iz programa
        break;
        case 's':
            animacija=1;//pokreni animaciju
        break;
        case 'e':
            animacija=0;//zaustavi animaciju
        break;
    }
}

void drawScene()
{
    //postavke projekcije i scene
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,1.0,1.0f,6.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(1.0f, 1.0f, 5.0f, // pozicija ocista
              1.0f, 1.0f, 1.0f, // pozicija referentne tocke odnosno sredista scene
              0.0f,1.0f,0.0f);

    glRotatef(5.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(65.0f, 0.0f, 1.0f, 0.0f);
    glRotatef(0.0f, 0.0f, 0.0f, 1.0f);

        GLfloat Ka[]={0.0f,0.0f,0.0f, 0.3f};//ambijentalno osvijetljenje, postavljeno na 30% inteziteta
        glLightfv(GL_LIGHT0, GL_AMBIENT, Ka);

        GLfloat Ks[] = {0.0f,0.0f,0.0f, 1.0f};//zrcalno osvijetljenje, 100%
        glLightfv(GL_LIGHT0, GL_SPECULAR, Ks);

        GLfloat Kd[] = {1.0f,1.0f,1.0f, 1.0f};//difuzno osvijetljenje, 100%
        glLightfv(GL_LIGHT0, GL_DIFFUSE, Kd);

    //omogucimo osvijetljenje
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    float black[4]={0,0,0,0};
    glMaterialfv(GL_FRONT,GL_AMBIENT,black);
    glMaterialfv(GL_FRONT,GL_SPECULAR,black);

    glTranslatef(0, 0, 1.0f);
    glRotatef(rotationAngle,1.0f,0.0f,0.0f);

    //podloga, crna linija oko koje se spremnik vode rotira
    glLineWidth(2);
    glBegin(GL_LINES);
        glColor3f(0,0,0);
        glVertex3f(2.0f,0.0f,0.0f);
        glVertex3f(-1.0f,0.0f,0.0f);
    glEnd();

    //spremnik vode (zelena zicana kocka)
    glLineWidth(4);
    glBegin(GL_LINES);
      glColor3f(0,1,0);

      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, 1.0f, 0.0f);

      glVertex3f(1.0f, 0.0f, 0.0f);
      glVertex3f(1.0f, 1.0f, 0.0f);

      //11
      glVertex3f(1.0f, 0.0f, 1.0f);
      glVertex3f(1.0f, 1.0f, 1.0f);

      //9
      glVertex3f(0.0f, 0.0f, 1.0f);
      glVertex3f(0.0f, 1.0f, 1.0f);

      //donja ploha
      //1
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, 0.0f, 1.0f);

      glVertex3f(1.0f, 0.0f, 0.0f);
      glVertex3f(1.0f, 0.0f, 1.0f);

      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(1.0f, 0.0f, 0.0f);

      //10
      glVertex3f(0.0f, 0.0f, 1.0f);
      glVertex3f(1.0f, 0.0f, 1.0f);

      //gornja ploha
      glVertex3f(0.0f, 1.0f, 0.0f);
      glVertex3f(0.0f, 1.0f, 1.0f);

      glVertex3f(1.0f, 1.0f, 0.0f);
      glVertex3f(1.0f, 1.0f, 1.0f);

      glVertex3f(0.0f, 1.0f, 0.0f);
      glVertex3f(1.0f, 1.0f, 0.0f);

      glVertex3f(0.0f, 1.0f, 1.0f);
      glVertex3f(1.0f, 1.0f, 1.0f);

    glEnd();

    if(rotationAngle<0.0f){//ako spremnik nije u finalnom stanju (prazan i zarotiran za 90 stupnjeva od pocetne tocke)
        if(animacija==1) rotationAngle += 0.15f;//ako je animacija omogucena pomakni spremnik

        glBegin(GL_POLYGON);
        glColor3f(0,0,1);
            //1
            glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
            glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f);
            //2
            glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);
            glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);
            //6
            glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
            glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);
            //10
            glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f); //stranica preko kojeg se preljeva voda
            glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);
        glEnd();

        if(proslovrijeme<=4.5f){ //prvi dio animacije
            if(proslovrijeme==0){ //prvi render
                glBegin(GL_POLYGON);
                  //9
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f);
                  glVertex3f(0.0f+0.02f, 1.0f-0.02f, xx);

                  //10
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f); //stranica preko kojeg se preljeva voda
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);

                  //11
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);//ostaje fiksan
                  glVertex3f(1.0f-0.02f, 1.0f-0.02f, xx);

                  //12
                  glVertex3f(0.0f+0.02f, 1.0f-0.02f, xx);
                  glVertex3f(1.0f-0.02f, 1.0f-0.02f, xx);
                glEnd();

                glBegin(GL_POLYGON);
                  //5
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);

                  //6
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);

                  //7
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);

                  //8
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);
                glEnd();

                glBegin(GL_POLYGON);
                  glVertex3f(0.02f, 0.0f+0.02f, 1.0f-0.02f);//10 dio
                  glVertex3f(0.02f, 1.0f-0.02f, xx);//12 dio
                  glVertex3f(0.02f, yy, 0.0f+0.02f);//8 dio
                  glVertex3f(0.02f, 0.0f+0.02f, 0.0f+0.02f);//6 dio
                glEnd();


            }else{ //ostali renderi
                if(animacija==1) xx=xx-0.0032f;
                glBegin(GL_POLYGON);
                  //9
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f);
                  glVertex3f(0.0f+0.02f, 1.0f-0.02f, xx);

                  //10
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f); //stranica preko kojeg se preljeva voda
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);

                  //11
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);//ostaje fiksan
                  glVertex3f(1.0f-0.02f, 1.0f-0.02f, xx);

                  //12
                  glVertex3f(0.0f+0.02f, 1.0f-0.02f, xx);
                  glVertex3f(1.0f-0.02f, 1.0f-0.02f, xx);
                glEnd();

                glBegin(GL_POLYGON);
                  //5
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);

                  //6
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);

                  //7
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);

                  //8
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);
                glEnd();

                glBegin(GL_POLYGON);
                  glVertex3f(0.02f, 0.0f+0.02f, 1.0f-0.02f);//10 dio
                  glVertex3f(0.02f, 1.0f-0.02f, xx);//12 dio
                  glVertex3f(0.02f, yy, 0.0f+0.02f);//8 dio
                  glVertex3f(0.02f, 0.0f+0.02f, 0.0f+0.02f);//6 dio
                glEnd();

                glBegin(GL_POLYGON);
                  glVertex3f(1.0f-0.02f, 1.0f-0.02f, xx);//12 dio
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);//8 dio
                glEnd();
            }
        }

        if(proslovrijeme>4.5f){ //drugi dio animacije
            if(proslovrijeme<9.0f){
                if(animacija==1) yy=yy-0.0032f;
                glBegin(GL_POLYGON);
                  //9_2
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 1.0f-0.02f);
                  glVertex3f(0.0f+0.02f, yy, xx);

                  //11_2
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 1.0f-0.02f);//ostaje fiksan
                  glVertex3f(1.0f-0.02f, yy, xx);

                  //12_2
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);
                glEnd();

                glBegin(GL_POLYGON);
                  //5
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);

                  //6
                  glVertex3f(0.0f+0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);

                  //7
                  glVertex3f(1.0f-0.02f, 0.0f+0.02f, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);

                  //8_2
                  glVertex3f(0.0f+0.02f, yy, 0.0f+0.02f);
                  glVertex3f(1.0f-0.02f, yy, 0.0f+0.02f);
                glEnd();

                glBegin(GL_POLYGON);
                  glVertex3f(0.02f, 0.0f+0.02f, 1.0f-0.02f);//10 dio
                  glVertex3f(0.02f, yy, 0.0f+0.02f);//8 dio
                  glVertex3f(0.02f, 0.0f+0.02f, 0.0f+0.02f);//6 dio
                glEnd();
            }else{

            }
        }

        glBegin(GL_LINES);
        glColor3f(0,1,0);
              //1
              glVertex3f(0.0f, 0.0f, 0.0f);
              glVertex3f(0.0f, 0.0f, 1.0f);
              //9
              glVertex3f(0.0f, 0.0f, 1.0f);
              glVertex3f(0.0f, 1.0f, 1.0f);
              //10
              glVertex3f(0.0f, 0.0f, 1.0f);
              glVertex3f(1.0f, 0.0f, 1.0f);
        glEnd();

    }

    if(animacija==1) proslovrijeme=(proslovrijeme+0.015);//sluzi za pracenje u kojem smo dijelu animacije
    //ukupno trajanje animacije je 9sec, svaki dio traje po 4.5sec
    glutSwapBuffers();
}

void timer(int) {
   glutPostRedisplay();
   glutTimerFunc(refresh, timer, 0);
}


int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);
    glutCreateWindow("Zadaca - 4. zadatak");
    glutDisplayFunc(drawScene);
    glClearColor( 1, 1, 1, 1);//pozadinska boja = bijela
    glutKeyboardFunc(Keys);
    glutTimerFunc(0, timer, 0);
    glutMainLoop();
    return 0;
}