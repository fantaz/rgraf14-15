#include <GL/glut.h>
#include <GL/gl.h>
#include <math.h>

#define PI 3.14159265358979323846

using namespace std;

/* Global variables */
char title[] = "Zadatak 4 - Enzo Licul";
GLfloat cubeAngle = 0.0f;     // Rotational angle for cube
int refreshMills = 15;        // refresh interval in milliseconds
bool animation = false;
float green = 0.725f;

GLfloat light[] = {1.0, 1.0, 1.0, 1.0};

GLfloat ambient[] = {0.1, 0.2, 0.5, 1.0};
GLfloat diffuse[] = {0.1, 0.5, 0.8, 1.0};
GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};
GLfloat shininess = 7.0;

GLfloat light_position[] = {5.0,8.0,-1.0,1.0};

/* Initialize OpenGL Graphics */
void initGL() {

    glLightfv(GL_LIGHT0, GL_AMBIENT, light);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // Set background color to white
    glClearDepth(1.0f);                   // Set background depth to farthest
    glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
    glEnable(GL_NORMALIZE);    // Enable normal vector scaling after transformation
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
    glShadeModel(GL_SMOOTH);   // Enable smooth shading
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
}

/* Function to draw the cube with green lines */
void draw_cube(float size){

    glLineWidth(4.0);
    glColor3f(0.0f, green, 0.0f);     // Green
    float point = size/2.0;

    glBegin(GL_LINE_LOOP);                // Begin drawing the color cube with lines
        // Top face
        glVertex3f( point, point, -point);
        glVertex3f(-point, point, -point);
        glVertex3f(-point, point,  point);
        glVertex3f( point, point,  point);
    glEnd();

    glBegin(GL_LINE_LOOP);
        // Bottom face
        glVertex3f( point, -point,  point);
        glVertex3f(-point, -point,  point);
        glVertex3f(-point, -point, -point);
        glVertex3f( point, -point, -point);
    glEnd();
    glBegin(GL_LINE_LOOP);
        // Front face
        glVertex3f( point,  point, point);
        glVertex3f(-point,  point, point);
        glVertex3f(-point, -point, point);
        glVertex3f( point, -point, point);
    glEnd();
    glBegin(GL_LINE_LOOP);
        // Back face
        glVertex3f( point, -point, -point);
        glVertex3f(-point, -point, -point);
        glVertex3f(-point,  point, -point);
        glVertex3f( point,  point, -point);
    glEnd();
    glBegin(GL_LINE_LOOP);
        // Left face
        glVertex3f(-point,  point,  point);
        glVertex3f(-point,  point, -point);
        glVertex3f(-point, -point, -point);
        glVertex3f(-point, -point,  point);
    glEnd();
    glBegin(GL_LINE_LOOP);
        // Right face
        glVertex3f(point,  point, -point);
        glVertex3f(point,  point,  point);
        glVertex3f(point, -point,  point);
        glVertex3f(point, -point, -point);
    glEnd();
}

/* Function to draw the black z-axis line */
void draw_line(float size){
    glLineWidth(2.0);
    glBegin(GL_LINES);
        glColor3f(0.0f, 0.0f, 0.0f);     // Black
        glVertex3f(-size, -size, 0.0f);
        glVertex3f(size, size, 0.0f);
    glEnd();
}

/* Function to draw water */
void draw_water(float z, float x0, float y0, float x1, float y1, float x2, float y2, float r, float g, float b){
    glBegin(GL_POLYGON);
        glColor3f(r, g, b);
        glNormal3f(0,0,1);
        glVertex3f(x0,y0,0);
        glVertex3f(x2,y2,0);
        glVertex3f(x1,y1,0);
        glNormal3f(0,0,-1);
        glVertex3f(x0,y0,-z);
        glVertex3f(x1,y1,-z);
        glVertex3f(x2,y2,-z);
    glEnd();
    glBegin(GL_QUAD_STRIP);
        glColor3f(r, g, b);
        glNormal3f(x0,y0,0);
        glVertex3f(x0,y0,0);
        glVertex3f(x0,y0,-z);

        glNormal3f(x1,y1,0);
        glVertex3f(x1,y1,0);
        glVertex3f(x1,y1,-z);

        glNormal3f(x2,y2,0);
        glVertex3f(x2,y2,0);
        glVertex3f(x2,y2,-z);
    glEnd();
}

/* Handler for window-repaint event. Called back when the window first appears and
   whenever the window needs to be re-painted. */
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
    glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix
    glLoadIdentity();                 // Reset the model-view matrix

    glColor3f(0.,0.,0.);
    glEnable(GL_LIGHTING);
    glDisable(GL_COLOR_MATERIAL);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);

    glPushMatrix();     // Drawing water
        glRotatef(cubeAngle,0,0,-1);    // Set the water rotation over z os
        if(cubeAngle <= 45){
            draw_water(1,-1,0,0,1,0,0,0,0,1);
            draw_water(1,-1,1*tan((45-cubeAngle) * PI / 180),0,1,-1,0,0,0,1);
        }else if(cubeAngle == 90){
        }else{
            draw_water(1,-1*tan((90-cubeAngle) * PI / 180),0,0,1,0,0,0,0,1);
        }
    glPopMatrix();

    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

    draw_line(1.0);

    glPushMatrix();     // Drawing the green cube over water
        glRotatef(cubeAngle,0,0,-1);    // Set the water rotation over z os
        glTranslatef(-0.5,0.5,-0.5);
        draw_cube(1.0);
    glPopMatrix();

    glutSwapBuffers();  // Swap the front and back frame buffers (double buffering)

}

/* Called back when timer expired */
void timer(int ) {

    if(animation==1){
        if(cubeAngle>=90){
            cubeAngle = 90;     //Rotation limit
            animation = false;  //Turn off the animation
            glutPostRedisplay();    // Post re-paint request to activate display()
        }else{
            cubeAngle += 0.1f;  //Icrement the cube angle by 0.1
            glutTimerFunc(refreshMills, timer, 0); // next timer call milliseconds later
            glutPostRedisplay();    // Post re-paint request to activate display()
        }
    }
}

void handleKeypress(unsigned char key, int, int)
{
    switch (key) {
    case 27: // ESC key
        exit(0);
    case 's': // Begin animation;
        animation = true;
        glutTimerFunc(0, timer, 0);
        break;
    case 'e': // Stops animation
        animation = false;
        break;
    case 'w':
        if(green <= 1.0){
        green += 0.010;
        glutPostRedisplay();
        }
        break;
    case 'a':
        if(green >= 0.5){
        green -= 0.010;
        glutPostRedisplay();
        }
        break;
    default:
        return;
    }
}

/* Handler for window re-size event. Called back when the window first appears and
   whenever the window is re-sized with its new width and height */
void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer

   // Compute aspect ratio of the new window
   if (height == 0) height = 1;                // To prevent divide by 0
   GLfloat aspect = (GLfloat)width / (GLfloat)height;
   // Set the viewport to cover the new window
   glViewport(0, 0, width, height);
   // Set the aspect ratio of the clipping volume to match the viewport
   glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
   glLoadIdentity();             // Reset
   // Enable perspective projection with fovy, aspect, zNear and zFar
   gluPerspective(45.0f, aspect, 0.1f, 500.0f);
   gluLookAt(2,2,3,1,1,1,0,1,0); // To change the camera vision eye/center/up vector
   glMatrixMode(GL_MODELVIEW);   // To operate on the Projection matrix
   glLoadIdentity();             // Reset
}

/* Main function: GLUT runs as a console application starting at main() */
int main(int argc, char** argv) {
   glutInit(&argc, argv);            // Initialize GLUT
   glutInitDisplayMode(GLUT_DOUBLE); // Enable double buffered mode
   glutInitWindowSize(640, 480);   // Set the window's initial width & height
   glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
   glutCreateWindow(title);          // Create window with the given title
   glutDisplayFunc(display);       // Register callback handler for window re-paint event
   glutReshapeFunc(reshape);       // Register callback handler for window re-size event
   glutKeyboardFunc(handleKeypress); //Call keyboard funtion to handle pressed keys
   initGL();                       // Our own OpenGL initialization
   glutTimerFunc(0, timer, 0);     // First timer call immediately
   glutMainLoop();                 // Enter the infinite event-processing loop
   return 0;
}
