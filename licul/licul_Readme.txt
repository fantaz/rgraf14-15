Opis

Tipke za kontrolu
	tipka 's' služi za pokretanje animacije i traje dok se sva "voda" ne izlije ili dok se ne stisne tipa 'e'
	tipka 'e' zaustavlja animaciju
	tipkama 'a' i 'w' moguće je podesiti nijansu zelene boje okvira kocke
	tipa 'Esc' služi za izlaz iz programa

Način rada animacije
	animacija radi pomocu timer funkcije, te se pregledava kut nagiba kocke. Kocka će se rotirati dok ne dođe do 90 stupnjeva rotacije

Nacin rada vode
	smanjivanje vode se dobije smanjivanjem jedne tocke polygona po y-koordinati, dva su polygona potrebna jer nakon što se dođe do 45 stupnjeva nagiba jedan se polygon gubi pa se koristi drugi za doc do 90 stupnjeva
	smanjuje se svakih 15 milisekundi za kut od tan(45-kut rotacije kocke) stupnjeva
	
Svjetlo
	definirana su 3 tipa osvijetljenja: ambijentalno, zrcalno i difuzno, svi pomaknuti desno od sredista kocke kako bi se prikazala razlika u osvijetljenju prilikom rotacije
	
Razvojna okolina
	koristen je QtCreator 5.2.1 na Kubuntu 14.10
	koristeni su glut i gl paketi 

Reference
	6. Lab vježba
	većinom sam koristio ovu stranicu za učenje openGL-a http://www.glprogramming.com/red/
	Stack Overflow


