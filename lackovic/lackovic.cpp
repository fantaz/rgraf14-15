
#include <GL/glut.h>
#include "math.h"

//osnovno osvjetljenje
GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 }; 
GLfloat light_position[] = { 2.0, 2.0, 2.0, 1.0 }; 

//normale, lica i polje za vertexe kocke
GLfloat n[6][3] = { 
{ -1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 1.0, 0.0, 0.0 },
{ 0.0, -1.0, 0.0 }, { 0.0, 0.0, 1.0 }, { 0.0, 0.0, -1.0 } };
 
GLint faces[6][4] = { 
{ 0, 1, 2, 3 }, { 3, 2, 6, 7 }, { 7, 6, 5, 4 }, 
{ 4, 5, 1, 0 }, { 5, 6, 2, 1 }, { 7, 4, 0, 3 } };

GLfloat v[8][3]; 

static int t = 0;
bool animating = false;


void drawPortal() {
	glColor4f(1, 1, 1, 1);
	glutSolidTorus(0.01,3,4,80);
	
	float x1,y1,x2,y2;
	float angle;
	double radius=3;
	glColor4f(1, 1, 0, 0.1);//transparentna boja
	x1 = 0, y1 = 0;
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(x1,y1);
	/*iscrtavanje ispunjenog kruga pomocu triangle_fan
	x1 i y1 su srediste, a ostale vertexe dobijam
	mjenjajuci kut i tako za 360 stupnjeva */
	for (angle=1.0f;angle<363.0f;angle+=3)
	{
	    x2 = x1+sin(angle*3.14/180)*radius;
	    y2 = y1+cos(angle*3.14/180)*radius;
	    glVertex2f(x2,y2);
	}
	glEnd();
}


void drawBox(float rate) {
	int i;
	//siva kutija
	glColor4f(0.2, 0.2, 0.2, 1);
	glPushMatrix();
	glTranslatef(-rate,0,0);
	glScalef(1-rate,1,1); 
	//kad se portal pomice s desna na lijevo otkriva se manja zuta kocka
	for (i = 0; i < 6; i++) {
		glBegin(GL_QUADS);
		glNormal3fv(&n[i][0]);
		glVertex3fv(&v[faces[i][0]][0]);
		glVertex3fv(&v[faces[i][1]][0]);
		glVertex3fv(&v[faces[i][2]][0]);
		glVertex3fv(&v[faces[i][3]][0]);
		glEnd();
	}
	glPopMatrix();
	//zuta (unutarnja) kocka
	glPushMatrix();
	glScalef(0.99,0.99,0.99);
	glColor4f(1,1, 0, 1);
	for (i = 0; i < 6; i++) {
		glBegin(GL_QUADS);
		glNormal3fv(&n[i][0]);
		glVertex3fv(&v[faces[i][0]][0]);
		glVertex3fv(&v[faces[i][1]][0]);
		glVertex3fv(&v[faces[i][2]][0]);
		glVertex3fv(&v[faces[i][3]][0]);
		glEnd();
	}
	glPopMatrix();
}


//animacija portala
void animation(int t) {
	//omogucava prozirnost portala
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	//crtanje kocke
	glPushMatrix();
	drawBox(t/400.0);
	glPopMatrix();
	glPushMatrix();
	glRotatef(90,0,1,0);
	//pomak portala
	glTranslatef(0,0,1-t/201.0);
	drawPortal();
	glPopMatrix();

}


void display(void) {
	if(animating && t<=400) t+= 1;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		animation(t);
	glutSwapBuffers();
}


void init(void) {
	//vertexi za kocku
	v[0][0] = v[1][0] = v[2][0] = v[3][0] = -1;
	v[4][0] = v[5][0] = v[6][0] = v[7][0] = 1;
	v[0][1] = v[1][1] = v[4][1] = v[5][1] = -1;
	v[2][1] = v[3][1] = v[6][1] = v[7][1] = 1;
	v[0][2] = v[3][2] = v[4][2] = v[7][2] = 1;
	v[1][2] = v[2][2] = v[5][2] = v[6][2] = -1;

	//postavljanje jednog openGL svjetla
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.01);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	
	//upotrebljavam boju kao materijal
	glEnable(GL_COLOR_MATERIAL);

	//omogucava depth buffering za micanje skrivenih povrsina
	glEnable(GL_DEPTH_TEST);

	//postavka pogleda 
	glMatrixMode(GL_PROJECTION);
	gluPerspective(40.0, 1.0, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	gluLookAt(2.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.);

	//ovo je za lijepi pogled na kocku
	glTranslatef(0.0, 0.0, -1.0);
	glRotatef(15, 1.0, 0.0, 0.0);
	glRotatef(-10, 0.0, 1.0, 0.0);
}


void processNormalKeys(unsigned char key, int /*x*/, int /*y*/) {
	 if (key == 's' || key == 'S')
		animating = true;
	else if (key == 'x' || key == 'X')
		animating = false;
	else if (key == 27 )
		exit(0);
}


int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//postavljanje prozora
	glutInitWindowPosition(0,0);
	glutInitWindowSize(640, 640);
	glutCreateWindow("Portal");
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutKeyboardFunc(processNormalKeys);
	init();
	glutMainLoop();
	return 0;
}
