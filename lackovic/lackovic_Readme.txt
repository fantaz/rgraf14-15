Animacija portala radi tako da imam 2 kocke, jednu sivu
i jednu malo manju zutu te napocetku vidimo samo
sivu kocku, kako se portal pomice tako se siva kocka
translatira te se postepeno otkriva zuta kocka.

Transparentnost portala sam dobio tako da sam portal
ispunio sa prozirnijom plohom (alpha=0.1) koju sam
napravio pomocu GL_TRIANGLE_FAN, a vertexe za njega
sam napravio definirajuci pocetno x1,y1 kao srediste
te zatim petljom napravio puni krug s korakom od
3 stupnja.

Program se upravlja iskljucivo preko 3 tipke zadane
u zadatku, tipka s za pokretanje animacije,
tipka x za zaustavljanje animacije te tipka escape
za izlaz iz programa.

Za realizaciju ovog projekta koristio sam primjer
izrade osnovne kocke koju sam nasao ovdje
www0.cs.ucl.ac.uk/teaching/GMV/Exercises/lab3/glut_examples/cube.c

