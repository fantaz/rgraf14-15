﻿Vrijednosti za prikaz scene:
	
	-Difuzno svijetlo {1.0, 1.0, 1.0, 1.0}
	
	-Pozicija svijetlosti {3.0, 0.0, 0.0, 1.0}
	


Kontroliranje programa:
	Program se može kontrolirati tipkama na tipkovnici.
		
	S - pokreće animaciju
		
	X - zaustavlja animaciju
		
	Esc - izlazi se iz programa



Program se otvara u 400x400 prostoru. 



Funkcija initRendering nam omogucava rad sa naredbama glEnable() kako bi te komponente mogli koristiti u crtanju. 

Inicijalno je program postavljem u stanje cekanja, odnosno animacija nije pokrenuta.



Funkcija timerEvent se poziva svakih 30 ms i obnavlja scenu.



Inicijalizacija gluta, stvaranje prozora i postavljanje timera je omoguceno u main funkciji.



Razvojna okolina
	
	-koristen je QtCreator 5.3.1 na Ubuntu 14.04
	
	-koristeni su glut i gl paket



Reference: 
Za izradu ovog programa koristio sam sljedece reference:

http://stackoverflow.com/

http://www.videotutorialsrock.com/

http://math.hws.edu/graphicsnotes/c3/s2.html



