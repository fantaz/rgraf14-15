#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace std;

#define START_ANIMATION 1
#define PAUSE_ANIMATION 0
#define SPHERE_RADIUS 0.05

float sphere_x,sphere_z;
int state_animation;
float window_width=400,window_height=400;

std::vector<GLfloat*> track;

//Ova funkcija crta ono sto je potrebno
void drawScene()
{
    GLfloat sphereColor[] = {0.0f,1.0f,0.0f,1.0f};
    GLfloat trackColor[] = {1.0f,0.0f,0.0f,0.5f};
    glClearColor(0, 0, 0, 0);//Boja pozadine
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//Cisti sve buffere

    GLUquadric *quadric;
    quadric = gluNewQuadric();
    gluQuadricDrawStyle(quadric, GLU_FILL );
    //Spremanje transformacije
    glPushMatrix();
    //Translacija na zadanu poziciju
    glTranslatef(sphere_x,0.0f,sphere_z);
    //Postavljanje boje
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,sphereColor);
    //crtanje sfere
    gluSphere( quadric ,SPHERE_RADIUS, 36 , 18 );
    glPopMatrix();
    //Crtanje trake
    if(track.size()>1){
        //Postavljanje boje
        glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,trackColor);
        //Traka u obliku cetverokuta
        glBegin(GL_QUAD_STRIP);
            for(unsigned int i=0;i<track.size();i++){
                    glVertex3f(track[i][0],-SPHERE_RADIUS,track[i][1]);
                    glVertex3f(track[i][0],SPHERE_RADIUS,track[i][1]);
            }
        glEnd();
    }
    //Oslobađanje memorije
    gluDeleteQuadric(quadric);
    //Zamijeni buffere
    glutSwapBuffers();
    //Izvrsi sve GL komande
    glFlush();
}

//Ova funkcija se poziva kada je neka tipka pritisnuta
void handleKeypress(unsigned char key, int /*x*/, int /*y*/){
    if(key=='s'||key=='S'){
        state_animation=START_ANIMATION;
    }else if(key=='x' || key=='X'){
        state_animation=PAUSE_ANIMATION;
    }else if(key==27){//Pritisnuta je ESC tipka
        exit(0);
    }
}

void setupPerspective(float /*width*/, float /*height*/){
    //Changing the dimensions of the view port
    glViewport(0,0,window_width,window_height);
    //Enable depth test so that objects behind another wouldn't be visible
    glEnable(GL_DEPTH_TEST);
    //Setting up projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective( /* field of view in degree */ 40.0,
                    /* aspect ratio */ 1.0,
                    /* Z near */ 1.0, /* Z far */ 10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(1.0, 1.0, 1.0,  // eye position
              0.0, 0.0, 0.0,      // center
              0.0, 1.0, 0.0);      // up vector
}

void handleResize(int width, int height){
    window_width=width;
    window_height=height;
	//Nakon promjene dimenzija prozora
    setupPerspective(window_width,window_height);
    //Ponovno prikazicanje trenutnog prozora
    glutPostRedisplay();
}

//Ova funkcija se poziva samo jednom tijekom izvođenja programa
void initRendering()
{
    setupPerspective(window_width,window_height);
    //Postavljanje svjetla
    GLfloat light_diffuse[]={1.000f,1.000f,1.000f,1.0f};
    GLfloat light_position[]={3.0f,0.0f,0.0f,1.0f};
    //Funkcija za upotrebu prozirnosti
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    //Omogucavanje svjetla. 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    //Postavljanje inicijalnih vrijednosti za animaciju
    sphere_x=-1.0,sphere_z=-0.25;//Pozicija sfere u world koordinatama
    state_animation=PAUSE_ANIMATION;//Na pocetku je animacija zaustavljena
}

//Ova funkcija se poziva svakih 30 ms da bi se scena obnavljala
void timerEvent(int /*value*/){
    GLfloat *temp=new GLfloat[2];
    //Ako animacija ide dalje
    if(state_animation==START_ANIMATION){
        temp[0]=sphere_x;
        temp[1]=sphere_z;
        track.push_back(temp);//Pohranjuje koordinate trake 
        if(sphere_x<0.0){//Prvi zavoj
            sphere_x+=0.01;
        }else if(sphere_z<0.0){//Drugi zavoj
            sphere_z+=0.01;
        }else if(sphere_x<0.6){//Dohvacanje konacne pozicije
            sphere_x+=0.01;
        }
    }
    drawScene();//Crtanje svih promjena
    glutTimerFunc(30, timerEvent, 1);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(100, 100);

    //Kreiranje prozora
    glutCreateWindow("Zadatak 1 - Babic Sandro");
    initRendering();
    glutTimerFunc(30,timerEvent, 1);
    //Postavljanje handler funkcija
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutReshapeFunc(handleResize);

    glutMainLoop();
    return 0;
}
