Opis

Zadatak 1

Program sam realizirao koristeći izradom vektora smjera u određenom vremenu.
Inicijalizacija animacije se pokreće pritiskom na tipku 's', zaustavljanje animacije pritiskom
na tipku 'x' i izlaz iz programa pritiskom na tipku 'ESC'. Sfera je inicijalizirana na početku
i postavljena u gornjem lijevom kutu.Trag sam izradio koristeći glBegin(GL_QUADS).
Animaciju i pomak sfere sam izradio koristeći interpolaciju i translaciju u određenom 
vremenskom trenutku prilikom koje se pomakne X i Y za neki korak(offset). 
Veličina prozora OpenGL-a je određena na 600x600.

Reference:

Vector - http://www.cplusplus.com/reference/vector/vector/
glBegin(GL_QUADS) - https://www.opengl.org/sdk/docs/man2/xhtml/glBegin.xm-buffer-in-opengl/
