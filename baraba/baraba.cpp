#include <iostream>
#include <GL/glut.h>
#include <stdio.h>
#include <vector>


char pomak = 0;
float x=0.0f, y=0.0f;
float radius=0.5;

struct Cell
{
        float t, posx, posy;
};

std::vector<Cell> Points;
float totalTime=0.0f;

struct Quad
{
	float top_leftX, top_rightX, bottom_leftX, bottom_rightX;
	float top_leftY, top_rightY, bottom_leftY, bottom_rightY;
};

std::vector<Quad>Quads;
int current_animation_key_index;

//Definiranje funkcije za manipulaciju sa tipkovnicom
void keyboard( unsigned char key, int , int  )
{
        switch( key )
        {
        case 's': case 'S':
                pomak = 1;
                break;
        case 'x': case 'X':
                pomak = 0;
                break;
        case 27: //ESCAPE
                exit(0);
      break;
        }
}


float offX = 0.0f;
float offY = 0.0f;
char end=0;


//Definiranje timera koji refresha sliku svakih 60ms
void timer( int  )
{
    if( pomak && !end )
    {
                offX= offX + 0.1f;
                offY= offY - 0.1f;
    }

    glutTimerFunc( 60, timer, 0 );
    glutPostRedisplay();
        if(pomak) totalTime+=0.06f;
}

//Definiranje inicijalizacijskih parametara i svijetline
void init()
{
			
        GLfloat mat_specular[]={0.0f,1.0f,0.0f,1.0f};
        GLfloat mat_shininess[]={75.0f};

        glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specular);
        glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininess);
		
        GLfloat light0_ambient[]={0.5f,0.5f,0.5f,1.0f};
        GLfloat light0_diffuse[]={1.0f,1.0f,1.0f,1.0f};
        GLfloat light0_specular[]={1.0f,1.0f,1.0f,1.0f};
        GLfloat light0_position[]={5.0f,5.0f,5.0f,0.0f};

        glLightfv(GL_LIGHT0,GL_AMBIENT,light0_ambient);
        glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffuse);
        glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
        glLightfv(GL_LIGHT0,GL_POSITION,light0_position);
        glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,light0_ambient);

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_DEPTH_TEST);
        glShadeModel(GL_SMOOTH);
        glEnable(GL_NORMALIZE);

		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//funkcija za podesavanje boje
void setMaterialColor(GLfloat color[])
{
        glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,color);
        glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,color);
}

//definiranje sfere i njene pocetne pozicije
void sfera()
{
		GLfloat mat_ambient_green[]={0.0f,1.0f,0.0f,1.0f};
		setMaterialColor(mat_ambient_green);
        glPushMatrix();
        glColor3f(1.0, 1.0, 0.0);
        glutSolidSphere(radius,50,50);
        glPopMatrix();
}


//definiranje prikaza
void display()
{
        int index=-1;
        for(int i=0; i < (int)Points.size();++i)
		{
                index=i;
                if(Points[i].t > totalTime)
                {
                break;
                }
		}

        float interpolate=(totalTime-Points[index-1].t)/(Points[index].t-Points[index-1].t);
        if(interpolate>1) interpolate=1.0;

        float posX=interpolate*Points[index].posx+(1.0f-interpolate)*Points[index-1].posx;
        float posY=interpolate*Points[index].posy+(1.0f-interpolate)*Points[index-1].posy;

		Quads[Quads.size()-1].top_rightX = posX;
		Quads[Quads.size()-1].top_rightY = posY+radius;
		Quads[Quads.size()-1].bottom_rightX = posX;
		Quads[Quads.size()-1].bottom_rightY = posY-radius;

		if(index!=current_animation_key_index)
		{
			current_animation_key_index=index;

			Quads[Quads.size()-1].top_rightX = Points[index-1].posx;
			Quads[Quads.size()-1].top_rightY = Points[index-1].posy+radius;
			Quads[Quads.size()-1].bottom_rightX = Points[index-1].posx;
			Quads[Quads.size()-1].bottom_rightY = Points[index-1].posy-radius;

			Quad temp;
			temp.top_leftX=temp.top_rightX = Points[index-1].posx;
			temp.top_leftY=temp.top_rightY = Points[index-1].posy+radius;
			temp.bottom_leftX=temp.bottom_rightX = Points[index-1].posx;
			temp.bottom_leftY=temp.bottom_rightY = Points[index-1].posy-radius;
			Quads.push_back(temp);

		}



		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
		glTranslatef(-1.5,1.5,0.0);
		glPushMatrix();
        glTranslatef(posX,posY,0.0f);
        sfera();
		glPopMatrix();
		glPushMatrix();

		GLfloat mat_ambient_white[]={1.0f,1.0f,1.0f,0.5f}; //boja traga je bijela sa alpha 0.5
		setMaterialColor(mat_ambient_white); 
		for(int i=0; i < (int)Quads.size(); ++i)
		{
			glBegin(GL_QUADS); //izrada traga koristeci quads
			glVertex2f(Quads[i].top_leftX, Quads[i].top_leftY);
			glVertex2f(Quads[i].top_rightX, Quads[i].top_rightY);
			glVertex2f(Quads[i].bottom_rightX, Quads[i].bottom_rightY);
			glVertex2f(Quads[i].bottom_leftX, Quads[i].bottom_leftY);
			glEnd();
		}
		glPopMatrix();
        glFlush();
        glutSwapBuffers();
}


void reshape(int w,int h)
{
        glViewport(0,0,w,h);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        if(w<=h)
        glOrtho(-2.0,2.0,-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-10.0,10.0);
        else
        glOrtho(-2.0*(GLfloat)h/(GLfloat)w,2.0*(GLfloat)h/(GLfloat)w,-2.0,2.0,-10.0,10.0);
        glMatrixMode(GL_MODELVIEW);
        glClearAccum(0.0, 0.0, 0.0, 0.5);
        glClear(GL_ACCUM_BUFFER_BIT);
        glLoadIdentity();
}

//Inicijalizacija programa
int main(int argc,char **argv)
{
        float c=3.0f;

        //punjenje vektora
        Cell k;              //definiranje tocke
        k.t=0.0f;			 //pocetno vrijeme
        k.posx=c*0.0f;		 //pocetna pozicija X
        k.posy=c*0.0f;		 //pocetna pozicija Y
        Points.push_back(k); //dodavanje novog elementa na kraj vektora Points

        k.t=1.0f;			 //pomaknuto vrijeme
        k.posx=c*0.7f;		 //pomak pozicije X
        k.posy=c*-0.3f;		 //pomak pozicije Y
        Points.push_back(k);

        k.t=2.0f;
        k.posx=c*0.3f;
        k.posy=c*-0.7f;
        Points.push_back(k);

        k.t=3.0f;
        k.posx=c*1.0f;
        k.posy=c*-1.0f;
        Points.push_back(k);

		current_animation_key_index=1;
		Quad temp;
		temp.top_leftX=temp.top_rightX = Points[0].posx;
		temp.top_leftY=temp.top_rightY = Points[0].posy+radius;
		temp.bottom_leftX=temp.bottom_rightX = Points[0].posx;
		temp.bottom_leftY=temp.bottom_rightY = Points[0].posy-radius;
		Quads.push_back(temp);

        glutInit(&argc,argv);
        glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH|GLUT_ACCUM);
        glutInitWindowSize(600,600);
        glutInitWindowPosition(0,0);
        glutCreateWindow("Zadatak 1 - Renato Baraba");
        glutReshapeFunc(reshape);
        glutDisplayFunc(display);
        glutKeyboardFunc(keyboard);
        glutTimerFunc(0,timer,0);
        init();
        glutMainLoop();
        return 0;
}