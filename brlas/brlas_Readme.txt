Kolegij: Racunalna grafika
Napravio: Ivan Brlas, 0135220696


1. Opis programa

	Program translatira zelenu, osjenčanu sferu po X i Z osima, kako je zadano u zadatku.
	Sfera iza sebe ostavlja osjenčani trag crvene boje, prozirnosti 0.5.


2. Tipke za navigaciju

	Key S; Pokreće animaciju
	Key X; Pauzira animaciju
	ESC;   Izlazi iz prozora animacije

3. Program je potrebno samo pokrenuti, svi argumenti su već unešeni

4. Korišten je prozor veličine 800x400, jer smatram da najbolje odgovara prikazu

	NOTE! veličinu prozora moguće je mijenjati mišem, tako da kliknemo na rubove, držimo tipku miša i razvučemo u željenoj veličini.

   Veličina Sfere (R), postavljen je na 0.35, jer najbolje odgovara veličini prozora i brzini animacije.

   Brzina animacije odgovara u pomaku od 0.01 za X ili Y os, po svakih 30 milisekundi.

   Svijetlost je postavljena tako da najbolje odgovara prikazu sfere, što vjerniji 3D.

   Boja pozadine je postavljena na svijetlo zeleno. (jer je bijela preobična).

5. Reference

	http://ogldev.atspace.co.uk/

		- upoznavanje sa OpenGl-om, učenje o prozorima, osnovnim elementima, bojama i translacijama

	http://www.lighthouse3d.com/tutorials/glut-tutorial/?2

		- GLUT tutorial, ovdje sam našao funkcije za tipkovnicu, te inicijalizaciju, mjenjanje veličine prozora mišem,
			tj. da se ne gubi integritet kad se mjenja velicina,
			osnove animacije itd.

	https://www.opengl.org/resources/libraries/glut/spec3/node81.html

		- ovdje sam našao upute za crtanje sfere

	https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html

		- jako koristan tutorial (čini 80% programa)
		- puno o main funkciji, ostalima funkcijama, timerima, vertexima, primitivima, translacijama, rotacijama,
	          kamerama, itd.

	http://www.programming-techniques.com/2012/05/rendering-spheres-glutsolidsphere-and.html

		- ovdje sam nasao potrebno za postavke ambijenta i svijetla

	http://www.swiftless.com/tutorials/opengl/square.html

		- na ovom linku sam tražio pomoć za crtanje traga pomoću kvadrata (QUADS)
		
	https://www.opengl.org/sdk/docs/man2/xhtml/glMaterial.xml
	
		- tutoriali za glMaterial()

	


