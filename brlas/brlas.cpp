#include <GL/glut.h>
#include <stdio.h>


int windowWidth  = 800;     // Windowed mode's width
int windowHeight = 400;     // Windowed mode's height
int windowPosX   = 100;      // Windowed mode's top-left corner x
int windowPosY   = 50;      // Windowed mode's top-left corner y

int counter = 0;

GLfloat sphereRadius = 0.35;   // Radius
GLfloat trailHeight = sphereRadius;
GLfloat sphereX = 0;         // center (x, y) position
GLfloat sphereY = 0;
GLfloat sphereZ = 0;
GLfloat sphereXMax, sphereXMin, sphereYMax, sphereYMin; // center (x, y) bounds
GLfloat xCoordPrimitive[10];
GLfloat yCoordPrimitive[10];
GLfloat zCoordPrimitive[10];

const GLfloat mat_ambient[] = {0.0,0.1,0.0,1.0};
const GLfloat mat_diffuse[] = {0.0,1.0,0.0,1.0};
const GLfloat mat_specular[] = {0.0,1.0,0.0,1.0};
const GLfloat mat_shininess[] = {100.0f};

const GLfloat mat_ambientT[] = {1.0,0.0,0.0,0.5};
const GLfloat mat_diffuseT[] = {1.0,0.0,0.0,0.5};
const GLfloat mat_specularT[] = {1.0,0.0,0.0,0.5};
const GLfloat mat_shininessT[] = {50.0f};



struct primitive{

    GLfloat xPocetni;
    GLfloat xZavrsni;
    GLfloat yPocetni;
    GLfloat yZavrsni;
    GLfloat zPocetni;
    GLfloat zZavrsni;

    bool init;
    bool nacrtan;


};

primitive kvadrati[6];


int refreshMillis = 30;      // Refresh period in milliseconds

int animationRun = 0;
int mode = 0;

void timer(int){

    if(animationRun == 1){

        if(!kvadrati[0].init){

        kvadrati[0].xPocetni = sphereX-trailHeight;
        kvadrati[0].yPocetni = sphereY;
        kvadrati[0].zPocetni = sphereZ;
        kvadrati[0].init = true;


        }

        if(!kvadrati[0].nacrtan){

            kvadrati[0].xZavrsni=sphereX;
            kvadrati[0].yZavrsni=sphereY;
            kvadrati[0].zZavrsni=sphereZ;



        }



        if(sphereX <= 3.0){



            if(sphereX >= 1.5 && sphereZ <= 1.5){


                counter = 1;

                kvadrati[0].nacrtan = true;
                sphereZ = sphereZ + 0.01;

                if(!kvadrati[1].init){

                kvadrati[1].xPocetni=sphereX;
                kvadrati[1].yPocetni=sphereY;
                kvadrati[1].zPocetni=sphereZ;
                kvadrati[1].init = true;


                }

                if(!kvadrati[1].nacrtan){

                    kvadrati[1].xZavrsni=sphereX;
                    kvadrati[1].yZavrsni=sphereY;
                    kvadrati[1].zZavrsni=sphereZ;



                }


            }

            else{

                if(counter == 1 || counter == 2){
                    kvadrati[1].nacrtan = true;

                    counter = 2;

                    if(!kvadrati[2].init){

                    kvadrati[2].xPocetni = sphereX;
                    kvadrati[2].yPocetni = sphereY;
                    kvadrati[2].zPocetni = sphereZ;
                    kvadrati[2].init = true;


                    }

                    if(!kvadrati[2].nacrtan){

                        kvadrati[2].xZavrsni = sphereX;
                        kvadrati[2].yZavrsni = sphereY;
                        kvadrati[2].zZavrsni = sphereZ;



                    }

                }

                sphereX = sphereX + 0.01;




            }
        }



    }

    else if(animationRun == 0){

        wait();

    }

    glutTimerFunc(refreshMillis, timer, 0); // refmilsc, timer, 0
    glutPostRedisplay();

}

void initKvadrati(){

    for(int i=0; i<6; i++){

        kvadrati[i].nacrtan = false;
        kvadrati[i].init = false;

    }

}


void playgroundSetup(){


    const GLfloat light_ambient[] = {0.5,0.5,0.5,1.0};
    const GLfloat light_diffuse[] = {1.0,1.0,1.0,1.0};
    const GLfloat light_specular[] = {1.0,1.0,1.0,1.0};
    const GLfloat light_position[] = {0.0,5.0,8.0,0.0};




    glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
    glLightfv(GL_LIGHT0,GL_POSITION,light_position);
    glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE,light_ambient);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND); // opacity enable
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // transparency enable

    glDepthFunc(GL_LESS); // depth enable

    //glColorMaterial(GL_FRONT, GL_DIFFUSE);
    //glEnable(GL_COLOR_MATERIAL);

}

void draw(){


    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,mat_ambient);
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,mat_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininess);

    glPushMatrix();
    glColor3f(0.0,9.0,0.0);
    glTranslatef(sphereX-1.65,1.65,sphereZ);
    glutSolidSphere(sphereRadius,100,100);

    glPopMatrix();



}


void renderPrimitive () { // Drawing of Sphere Trail

    glPushMatrix();
    glTranslatef(-1.65,1.65,0.0);






    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,mat_ambientT);
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,mat_diffuseT);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,mat_specularT);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,mat_shininessT);


    for(int i=0;i<=counter;i++){

glBegin(GL_QUAD_STRIP); // Start drawing a quad primitive

glColor4f(1.0,0.0,0.0, 0.5);

glVertex3f(kvadrati[i].xPocetni, kvadrati[i].yPocetni + trailHeight, kvadrati[i].zPocetni); // The top left corner
glVertex3f(kvadrati[i].xPocetni, kvadrati[i].yPocetni - trailHeight, kvadrati[i].zPocetni); // The bottom left corner
glVertex3f(kvadrati[i].xZavrsni, kvadrati[i].yPocetni + trailHeight, kvadrati[i].zZavrsni); // The top right corner
glVertex3f(kvadrati[i].xZavrsni, kvadrati[i].yPocetni - trailHeight, kvadrati[i].zZavrsni); // The bottom right corner

glEnd();

    }

    glPopMatrix();
}




void initGL() {
   glClearColor(0.8f, 0.9f, 0.8f, 1.0f); // Set background color


}


void display(){     //window repaint event



    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


    glRotatef(-25, 0.0, 1.0, 1.0);
    glRotatef(45, 1.0, 0.0, 0.0);



    draw(); //crtanje sfere


    renderPrimitive(); //crtanje traila



    glutSwapBuffers();

    glFlush(); //render now



}


void reshape(GLsizei width, GLsizei height){ //prilagodavanje prozora resizeu

    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    glOrtho(-2.0*aspect, 2.0*aspect, -2.0, 2.0, -3.0, 3.0);

    glMatrixMode(GL_MODELVIEW);
    glClearAccum(0.0, 0.0, 0.0, 1.0);
    glClear(GL_ACCUM_BUFFER_BIT);
    glLoadIdentity();

}

void pressKey(unsigned char key, int, int){ //keyboard animation control

    switch(key)
        {
            case 27:
                exit(0);
                break;

            case 's':
                animationRun = 1;
                break;

            case 'x':
                animationRun = 0;
                break;
        }
}


int main(int argc,char **argv){

    initKvadrati();
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(windowWidth, windowHeight);
    glutInitWindowPosition(windowPosX,windowPosY);
    glutCreateWindow("Ivan Brlas, projekt, Sfera (s, start; x, stop)");
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(pressKey);
    glutTimerFunc(0,timer,0);
    playgroundSetup();
    initGL();
    glutMainLoop();


    return 1;


}
