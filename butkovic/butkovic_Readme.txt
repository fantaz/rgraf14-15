Opis


Napravio sam funkciju koja inicijalizira sferu. Inicijalizacija animacije pocinje pritiskom tipke s, 
to je napravljeno u posebnoj funkciji. Za trag sam koristio accumulation buffer koji mnozi trenutnu 
sliku i prilikom pomaka stvara efekt kao da ostavlja trag. U posebnoj funkciji se provjeravaju tipke 
za upravljanje programom.

Reference:
https://www.opengl.org/sdk/docs/man2/xhtml/glAccum.xml
http://www.opengl-tutorial.org/
http://www.paulsprojects.net/opengl/projects1.html
youtube.com