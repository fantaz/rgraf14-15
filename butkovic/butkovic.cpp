#include<GL/glut.h>
#include<stdio.h>
#include <vector>

char flag = 0;
float x=0.0f, y=0.0f;
struct Vertex{
    float trenutni, pozx, pozy;
};
std::vector<Vertex> trenutna_tocka;
float ukupno_vrijeme=0.0f;

void Keyboard( unsigned char tipka, int, int){
        switch(tipka){
        case 's': //pritiskom slova 's' starta se animacija
                flag = 1;
		break;
        case 'x': //pritiskom slova 'x' zaustavlja se animacija
                flag = 0;
		break;
        case 27: //ESCAPE tipka
		exit(0);
                break;
	}
}

float pomak_X = 0.0f;
float pomak_Y = 0.0f;
char kraj=0;
//refresh slike
void timer(int){
    if(flag && !kraj){
                pomak_X= pomak_X + 0.1f;
                pomak_Y= pomak_Y - 0.1f;
    }
    glutTimerFunc( 60, timer, 0);
    glutPostRedisplay();
    if(flag) ukupno_vrijeme+=0.06f;
}

void init(){
    GLfloat material_ambient[]={0.0f,0.1f,0.0f,1.0f}; //ambient boja materijala
    GLfloat material_diffuse[]={0.0f,1.0f,0.0f,1.0f}; //difusse boja materijala
    GLfloat material_specular[]={0.0f,1.0f,0.0f,1.0f}; //specualar boja materijala
    GLfloat material_shininess[]={50.0f};

    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,material_ambient);
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,material_diffuse);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,material_specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,material_shininess);
	
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
}

void sphere(){
	glPushMatrix();
        glColor3f(0.0f, 1.0f, 0.0f); //poztavljanje boje na zelenu
        glTranslatef(-1.5f,1.5f,0.0f);//pocetni pomak sfere u lijevi kut
        glutSolidSphere(0.5f,50,50);//sfera radijusa 0.5
	glPopMatrix();
}

void display(){
    int index=-1;
    for(int i=0; i<(int)trenutna_tocka.size();++i){
        index=i;
        if(trenutna_tocka[i].trenutni>ukupno_vrijeme){
            break;
        }
    }
    //pomak pomocu linearne interpolacije lijevo pa desno
    float interpolacijski_pomak=(ukupno_vrijeme-trenutna_tocka[index-1].trenutni)/(trenutna_tocka[index].trenutni-trenutna_tocka[index-1].trenutni);
    if(interpolacijski_pomak>1) interpolacijski_pomak=1.0f;
    float pozX=interpolacijski_pomak*trenutna_tocka[index].pozx+(1.0f-interpolacijski_pomak)*trenutna_tocka[index-1].pozx;
    float pozY=interpolacijski_pomak*trenutna_tocka[index].pozy+(1.0f-interpolacijski_pomak)*trenutna_tocka[index-1].pozy;

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);    //ocisti buffer za boje i dubinu
    glLoadIdentity();
    glTranslatef(pozX,pozY,0.0f); //pomici sferu svakih 60 ms zadanih u timeru
    sphere();
    //izrada traga pomocu accumulation buffera
    glAccum(GL_ACCUM,0.9f);
    glAccum(GL_RETURN,1.0f); //ostavljanje traga
    glFlush();
    glutSwapBuffers();
}	

void resize(int width,int height){
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(width<=height)
    glOrtho(-2.0f,2.0f,-2.0f*(GLfloat)height/(GLfloat)width,2.0f*(GLfloat)height/(GLfloat)width,-10.0f,10.0f);
    else
    glOrtho(-2.0f*(GLfloat)height/(GLfloat)width,2.0f*(GLfloat)height/(GLfloat)width,-2.0f,2.0f,-10.0f,10.0f);
    glMatrixMode(GL_MODELVIEW);
    glClearAccum(0.0f, 0.0f, 0.0f, 0.5f);
    glClear(GL_ACCUM_BUFFER_BIT);
    glLoadIdentity();
}

int main(int argc,char **argv){
    float a=3.0f;
    Vertex vertica;
    vertica.trenutni=0.0f;		//pocetno vrijeme tocke
    vertica.pozx=a*0.0f;		//pocetna pozicija X
    vertica.pozy=a*0.0f;		//pocetna pozicija Y
    trenutna_tocka.push_back(vertica);	//dodavanje novog elementa na kraj vektora trenutna_tocka

    vertica.trenutni=1.0f;		//pomaknuto vrijeme tocke
    vertica.pozx=a*0.7f;		//pomaknuta pozicija X
    vertica.pozy=a*-0.3f;		//pomaknuta pozicija Y
    trenutna_tocka.push_back(vertica);
    vertica.trenutni=2.0f;
    vertica.pozx=a*0.3f;
    vertica.pozy=a*-0.7f;
    trenutna_tocka.push_back(vertica);
    vertica.trenutni=3.0f;
    vertica.pozx=a*1.0f;
    vertica.pozy=a*-1.0f;
    trenutna_tocka.push_back(vertica);

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH|GLUT_ACCUM);
    glutInitWindowSize(800,800);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Zadatak1 - Matko Butkovic");
    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(Keyboard);
    glutTimerFunc(0,timer,0);
    init();
    glutMainLoop();
    return 0;
}
 