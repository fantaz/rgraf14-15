Opis:

Na početku se crtaju obje kocke, žuta se skalira s 0.99 po svim dimenzijama kako ne bi bila vidljiva.

Početna udaljenost portala, korak pomaka, položaji vertexa kocke i boje kocaka su preddefinirani.

Pritiskom na tipku 's' počinje animacija, tipkom 'x' se ona zaustavlja a sa Esc tipkom prekida izvođenje programa.

Prilikom prelaska portala preko sive kocke update-aju se njene x-koordinate na x-koordinatu portala tako da se postupno otkriva donja, žuta kocka.

Za izradu programa sam se pomogao svemogućim Google-om.

