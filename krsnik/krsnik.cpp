#include <iostream>
#include <GL/glut.h>
#include <math.h>
using namespace std;

void init();
void display();
void drawCube(float []);
void drawCircle(float, float);
void keysHandler(unsigned char, int, int);
void resizeWindow(int, int);
void animation(float);

// polozaj vertexa kocke
float x_left=-1.0f; 
float x_right=1.0f;
float y_up=1.0f;
float y_down=-1.0f;
float z_front=1.0f;
float z_back=-1.0f;

float colorCube1[]={0.2,0.2,0.2}; //siva boja za prvu kocku (vanjsku)
float colorCube2[]={1.0,0.8,0.0}; //zuta boja za drugu kocku (unutarnju)
float deltaAngle=0.01f; //promjena kuta za crtanje portala
float portalRadius=3.0f; // radius kruznice portala
float distanceFromCube=2.0f; // inicijalna udaljenost portala od kocke
bool animate=false; // kontrola animacije
float offset=x_right+distanceFromCube; // offset za kontroliranje mijenjanja boje tijekom animacije
float offsetStep=0.05f; // korak kretnje portala

GLfloat vertex[8][3]={
		{x_left,y_down,z_front},
		{x_right,y_down,z_front},
		{x_right,y_up,z_front},
		{x_left,y_up,z_front},
		{x_left,y_down,z_back},
		{x_right,y_down,z_back},
		{x_right,y_up,z_back},
		{x_left,y_up,z_back}
};


GLfloat lightColor[]={1.0f, 1.0f, 1.0f, 1.0f}; // boja osvjetljenja
GLfloat lightPosition[]={0.0f, 4.0f, -4.0f, 1.0f}; // polozaj osvjetljenja , u pozadini
GLfloat attenuationConst=0.01f; //konstanta opadanja intenziteta osvjetljenja

int main(int argc, char **argv){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0,0);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Zadatak 3 - Krsnik");
	
	init();
	
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutKeyboardFunc(keysHandler);
	glutReshapeFunc(resizeWindow);
	
	glutMainLoop();
	return 0;
}

void init(){
	
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, attenuationConst);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
}
void display(){
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glClearColor(0.4,0.4,0.4,1);
	
	if(animate){ // ako je animacija ukljucena (tipka s je stisnuta)
		if(offset>(-distanceFromCube+x_left))
		 /*
		  *  kada portal stigne na odredenu udaljenost nakon prolaska kroz kocku prestaje kretnja,
		  * ne svida mi se ako stane odmah na x_left (na rubu kocke)
		
		*/
		{
			 offset-=offsetStep; // promjena
			 
		 }
	}
	animation(offset);
	glutSwapBuffers();
}
void resizeWindow(int width, int height) // prilagodjavanje prikaza uslijed promjene velicine prozora
{
	
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.0, (GLfloat) width/height, 1.0, 70.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(2.0, 3.0, 10.0,
			0.0, 0.0, 0.0,
			0.0, 1.0, 0.0);
	
}
void animation(float offset){
	if(offset>=x_left) drawCube(colorCube1); // sivu kocku crtam samo dok je portal na njoj, nakon toga vise ne
	
	glPushMatrix();
	glScalef(0.99,0.99,0.99); // skaliranje zute kocke kako se ne bi vidjela na pocetku
	drawCube(colorCube2);
	glPopMatrix();
	
	drawCircle(portalRadius, offset);
	glutSwapBuffers();
}
void drawCube(float colorCube[]){
	if(colorCube==colorCube1 && (offset>x_left && offset<x_right)) // ako se crta siva kocka a portal je na njoj onda :
	{
		//kod svake plohe koja sadrzi tocku x_right se ona zamjenjuje sa vrijednoscu offseta
		glColor3f(colorCube[0],colorCube[1],colorCube[2]);
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]); 
			glVertex3f(offset,vertex[1][1],vertex[1][2]);
			glVertex3f(offset,vertex[2][1],vertex[2][2]);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
		glEnd();
		//back
		glBegin(GL_QUADS);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
			glVertex3f(offset,vertex[5][1],vertex[5][2]);
			glVertex3f(offset,vertex[6][1],vertex[6][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
		glEnd();
		//left
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
		glEnd();
		//right
		glBegin(GL_QUADS);
			glVertex3f(offset,vertex[1][1],vertex[1][2]);
			glVertex3f(offset,vertex[5][1],vertex[5][2]);
			glVertex3f(offset,vertex[6][1],vertex[6][2]);
			glVertex3f(offset,vertex[2][1],vertex[2][2]);
		//top
		glBegin(GL_QUADS);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
			glVertex3f(offset,vertex[2][1],vertex[2][2]);
			glVertex3f(offset,vertex[6][1],vertex[6][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
		glEnd();
		//bottom
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]);
			glVertex3f(offset,vertex[1][1],vertex[1][2]);
			glVertex3f(offset,vertex[5][1],vertex[5][2]);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
		glEnd();
	}else{
		//front
		glColor3f(colorCube[0],colorCube[1],colorCube[2]);
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]);
			glVertex3f(vertex[1][0],vertex[1][1],vertex[1][2]);
			glVertex3f(vertex[2][0],vertex[2][1],vertex[2][2]);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
		glEnd();
		//back
		glBegin(GL_QUADS);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
			glVertex3f(vertex[5][0],vertex[5][1],vertex[5][2]);
			glVertex3f(vertex[6][0],vertex[6][1],vertex[6][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
		glEnd();
		//left
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
		glEnd();
		//right
		glBegin(GL_QUADS);
			glVertex3f(vertex[1][0],vertex[1][1],vertex[1][2]);
			glVertex3f(vertex[5][0],vertex[5][1],vertex[5][2]);
			glVertex3f(vertex[2][0],vertex[6][1],vertex[6][2]);
			glVertex3f(vertex[6][0],vertex[2][1],vertex[2][2]);
		//top
		glBegin(GL_QUADS);
			glVertex3f(vertex[3][0],vertex[3][1],vertex[3][2]);
			glVertex3f(vertex[2][0],vertex[2][1],vertex[2][2]);
			glVertex3f(vertex[6][0],vertex[6][1],vertex[6][2]);
			glVertex3f(vertex[7][0],vertex[7][1],vertex[7][2]);
		glEnd();
		//bottom
		glBegin(GL_QUADS);
			glVertex3f(vertex[0][0],vertex[0][1],vertex[0][2]);
			glVertex3f(vertex[1][0],vertex[1][1],vertex[1][2]);
			glVertex3f(vertex[5][0],vertex[5][1],vertex[5][2]);
			glVertex3f(vertex[4][0],vertex[4][1],vertex[4][2]);
		glEnd();
		}
}

void drawCircle(float radius, float offset){
	
	glBegin(GL_LINE_LOOP); // crtanje kruznice
	glLineWidth(0.8f);
	glColor4f(0.0,0.0,0.0,1.0);
	for(float angle=0;angle<2*M_PI;angle+=deltaAngle){
		glVertex3f(offset,radius*cos(angle), radius*sin(angle)); // offset je x-koordinata portala
	}
	glEnd();
	
	glBegin(GL_POLYGON); // crtanje kruga unutar kruznice
	glColor4f(0.8,0.8,0.1,0.01);
	for(float angle=0;angle<2*M_PI;angle+=deltaAngle){
		glVertex3f(offset,radius*cos(angle), radius*sin(angle)); // offset je x-koordinata portala
	}
	glEnd();
}
void keysHandler(unsigned char key, int /*x*/, int /*y*/) // tipka S pokrece animaciju, tipka X ju zaustavlja a Esc prekida program
{
	switch(key){
		case 's':
				animate=true;
				break;
		case 'S':
				animate=true;
				break;
		case 'x':
				animate=false;
				break;
		case 'X':
				animate=false;
				break;
		case 27:
				exit(0);
	}
}
