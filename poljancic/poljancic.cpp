#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>
#include <math.h>

void display();

double rotate_y=+205;
double rotate_x=-25;

float prazni_prednja=0.5;
float prazni_gornja=0.5;
float x_os=0.5;

bool crtaj= true;
bool pokreni_animaciju = false;
bool rotiraj = false;

GLfloat ambientLight[] = {0.2, 0.2, 0.2};
GLfloat diffuseLight[] = {1.0, 1.0, 1.0};


void display(){

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    // Reset transformacija

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    gluLookAt(3, 8, 3, 0, 0, 0, 0, 1, 0);
    glScalef(-1., -1., -1.);
    glRotatef( rotate_x, 1.0, 0.0, 0.0 );
    glRotatef( rotate_y, 0.0, 0.0, 1.0 );
    glTranslatef(-.5,.5,.5);


    if(crtaj){
        //gornji kvadrat
        glBegin(GL_POLYGON);
        glColor3f( 0.0, 0.0, 0.8 );
        glNormal3f(0, 1, 0);
        glVertex3f(  0.5,  0.5,  0.5 );
        glVertex3f(  0.5,  0.5, -0.5 );
        glVertex3f( -x_os,  prazni_gornja, -0.5 );
        glVertex3f( -x_os,  prazni_gornja,  0.5 );
        glEnd();


        //prednji kvadrat
        glBegin(GL_POLYGON);
        glColor3f(   0.2 , 0.2, 1.0 );
        glNormal3f(0, 0, 1);
        glVertex3f(  0.5, -0.5, -0.5 );
        glVertex3f(  0.5,  0.5, -0.5 );
        glVertex3f( -x_os,  prazni_prednja, -0.5 );
        glVertex3f( -0.5, -0.5, -0.5 );
        glEnd();


        //desni kvadrat
        glBegin(GL_POLYGON);
        glNormal3f(1, 0, 0);
        glColor3f(  0.2 , 0.2, 1.0 );
        glVertex3f( 0.5, -0.5, -0.5 );
        glVertex3f( 0.5,  0.5, -0.5 );
        glVertex3f( 0.5,  0.5,  0.5 );
        glVertex3f( 0.5, -0.5,  0.5 );
        glEnd();

    }


    glEnable(GL_LINE_SMOOTH);
    glBegin(GL_LINES);
    glColor3f(  0.0,  0.0, 0.0 );
    glVertex3f( 0.5, -0.5, 1.0  );
    glVertex3f( 0.5, -0.5, -1.0);
    glEnd();
    glDisable(GL_LINE_SMOOTH);

    /*zbog toga sto je pokretano na virtualci,3d acceleration nije prikazivao širinu linije,a bez njega sve je radilo
    užasno sporo, ovdje crtam kocku na kocku, inače bi sa glLineWidth(4) postavio širinu na 4 piksela,što je također 
    slučaj i na odlomku iznad pri crtanju linije
    */
    glColor3f( 0.0, 1.0, 0.0 );
    glutWireCube(0.994);
    glutWireCube(1.0);
    glutWireCube(1.006);
    glutWireCube(1.012);
    glutWireCube(1.018);

    glutPostRedisplay();
    glutSwapBuffers();


    if(pokreni_animaciju) {

        if(prazni_prednja<=-0.5 && prazni_gornja<=-0.5){
            prazni_prednja=(-0.5);
            prazni_gornja=(-0.5);
            x_os-=0.00232;
            rotate_y -= 0.08;

        } else {


            prazni_gornja -= 0.0016;
            prazni_prednja -= 0.0016;
            rotate_y -= 0.08;

        }
    }
    if (rotate_y<120) {
        rotate_y=120;
        crtaj=false;
        rotiraj=true;
    }

    glutPostRedisplay();

}

void resize(int w, int h)
{
    if (h == 0) { h = 1; }
    float ratio = 1.0* w / h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(45, ratio, 1.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();



}
void init() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(0.5f, 0.5f, 0.5f, 0.5f);

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);

    float pos[] = {0, 5, 0, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);

    glEnable(GL_LIGHT0);



}

void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
    switch(key)
    {
    case 27: //Escape key
        glutDestroyWindow(1);
    case 's': pokreni_animaciju=true;
        break;
    case 'e': pokreni_animaciju=false;
        break;

    case 'x': if(rotiraj) {
        rotate_y+=10;
        pokreni_animaciju=false;
        }


    }
}

int main(int argc, char* argv[]){

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (900, 600);
    glutInitWindowPosition (150, 100);
    glutCreateWindow("Poljancic Kocka");
    init();
    glEnable(GL_DEPTH_TEST);
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyboard);
    glutMainLoop();


    return 0;

}
