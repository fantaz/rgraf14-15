Zadaća iz računalne grafike,zadatak broj 4.


1)

- Upute za pokretanje:	->Tipka 's' pokreće animaciju.
			->Tipka 'e' zaustavlja animaciju u bilo kojem trenutnu.
			->Tipka 'x' nakon što završi animacija,rotira kocku oko linije kojom je kocka pričvršćena.
			->Tipka 'Esc' zatvara prozor animacije.

- Prilikom pokretanja nije potrebno unositi nikakve argumente.

2) Kao reference koristio sam primjere sa vježbi i primjere sa OpenGL wiki te dokumentaciju sa www.opengl.org.

3)
-Ideja za realizirati ovaj zadatak bila je nacrtati zelenu žičanu kocku koja te vodu unutar nje realizirati preko 3 kvadrata.
Nakon toga kocku rotirati oko crne osi i tako smanjivati te kvadrate zavisno u kojem položaju se nalazi kocka sve dok kocka ne 
dođe u završni položaj,gdje voda, odnosno kvadrati, nestanu iz scene i na sceni ostane samo kocka, dodatno sam napravio da se nakon
završetka animacije kocka i dalje može tipkom rotirati oko osi.

///
- Zbog problema sa 3d acceleration u virtualci na Ubuntu 14.04, glLineWidth() nije radio,pa sam ručno nacrtao više kocki,
i sa GL_LINE_SMOOTH podebljao os.
///

-Za rotaciju kocke oko osi koristio sam varijable rotate_x i rotate_y koje sam incijalno postavio na -25 i 205 kako bi kocka
bila u položaju kako je zadano zadatkom. Ideja je bila rotacijom kocke te simulacijom izlijevanja vode desni kvadrat cijelo
vrijeme ostaje na stranici kocke(osim dok se voda ne izlije),dok se y koordinata prednjeg i zadnjeg kvadrata mijenja preko
varijabli prazni_prednja i prazni_gornja kojima je inicijalno zadana koordinata 0.5,odnosno isto kao i kocki.
Varijablom crtaj ograničavam da kada je kocka došla u završni položaj,voda se prividno izlila odnosno nije više potrebno 
iscrtavati kvadrate. Još sam koristio dvije bool varijable,pokreni_animaciju te varijabla rotiraj za prestanak rotiranja kocke
nakon što je došla u završni položaj. Incijalizirao sam varijable za sivo ambijentno te bijelo difuzno svijetlo postavljajući
im RGB vrijednosti na sivu i bijelu.

- Program se sastoji od 4 void funkcije: display(),init(),resize(),keyboard().

- U main-u se vrši inicijalizacija GLUT-a te se stvara prozor,zovu se funkcije init(),glutDisplayFunc(display),funkcija reshape,
funkcija glutKeyboardFunc() za korištenje user inputa te glutMainLoop() koja je sastavni dio svakog programa,ona poziva ostale
funkcije,vrši zadane evente itd.

- U display funkciji postavio sam kameru na scenu sa gluLookAt,skalirao scenu za -1,postavio početnu rotaciju na
zadane vrijednosti uz varijable za rotaciju x i y koje se mijenjaju zavisno u kojem je trenutku animacija te sve skupa translatirao.
3 kvadrata nacrtana su sa glBegin(GL_POLYGON) te su im zadana 4 verteksa i boja. Zavisno o tome koje se koordinate moraju pomicati 
tijekom animacije za određeni kvadrat, gornjem kvadratu pomiču se 2 vertexa za vrijednost varijable prazni_gornja za y,sve dok ne dođe
u položaj kad je kocka točno na polovici rotacije,prilikom čega se počinje mijenjati x vrijednost istih verteksa varijablom x_os.
Prednjem kvadratu pomiče se samo y vrijednost jednog verteksa,također do polovice rotacije kocke gdje se vrijednost x tog verteksa 
smanjuje kao i za gornji kvadrat(sa x_os). Linija (crna os) realizirana je sa glBegin(GL_LINES) te je uključen GL_LINE_SMOOTH da bi mogao
liniju postaviti na određenu širinu.

- Kocka je nacrtana sa glutWireCube() te iscrtana 4 puta,kako je zadano zadatkom te obojana u zeleno. Kada varijabla pokreni_animaciju
poprimi vrijednost true,odnosno kad je pritisnuta tipka varijable prazni_gornja i prazni_prednje se smanjuju za 0.0016 odnosno 50
puta manje kako se rotira kocka odnosno rotate_y-=0.08. U slučaju kada je kocka točno na polovici animacije,odnosno kad varijable 
prazni_prednja i prazni_gornja poprime vrijednost manju ili jednaku -0.5, što je ostvari vrijedost gornjeg dijela kocke, počinje se 
smanjivati varijabla x_os sve do kraja rotacije,odnosno do vrijednosti kada se voda prividno izlije iz kocke.

- Funkcija resize() koristi za postavljanje veličine kocke ovisno od veličine prozora. U funkciji init() postavljaju se boje materijala,
osvjetljenje (ambijentno i difuzno),background te pozicija svijetla pomaknuta za 5 u y smjeru,directional light u beskonačnosti.
Uključeno svijetlo0 i pozicionirano na vrijednosti x=y=z=0.5

- Funkcija keyboard služi samo za user input, sa mogućnosti od 4 tipke.











			
			
