#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

//Pocetni parametri
float pomak=0.0; //Pomak koji se kasnije mjenja pomocu pomakInc(inkrementa)
float pomakInc=0.01;//Inkrement cijim se povecanjem/smanjivanjem utjece na "brzinu", tj. na skok u pomacima
int delay=30;//timer delay u ms. utjece na ucestalost pokretanja redisplay-a tj na "brzinu"
bool animate=false; //flag koji pokazuje radi li trenutno animacija
bool DISSAPEARING=true;//flag koji se koristi u inicijalizaciji renderBox()
bool APPEARING=false;//flag koji se koristi u inicijalizaciji renderBox()
float boxSize=0.5f;//velicina kocki
float portalSize=1.0f;//radijus portala(torusa i kruznice)
GLfloat box1Color[] = {0.5f, 0.5f, 0.5f};//boja prve kocke(sive)
GLfloat box2Color[] = {238.0/255.0 , 192.0/255.0, 21.0/255.0};//boja druge kocke(zlatne)
GLfloat portalColor[] = {1.0f, 1.0f, 1.0f, 1.0f};//boja portala i transparentnog punjenja


void init(void)
{
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //Light model init
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    //Intenziteti
    GLfloat ambient[] = {0.2, 0.2, 0.2, 1.0};
    GLfloat diffuse[] = {0.8, 0.8, 0.8, 1.0};
    GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

    //LPozicija svijetla

    GLfloat lightPos[] = {0.5, 0.0, 2.0, 1.0};
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);


}

void timer(int /*time*/)
{
    glutPostRedisplay();
    glutTimerFunc(delay, timer, 0);
}

void renderBox(float size, bool dissapearing, GLfloat color[])
{
    float pomakX=0.0;
    float pomakXneg=0.0;

    glPushMatrix();
    //Iscrtaj kocku 2, zute boje
    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);//omogucava koristenje vertex colora umjesto materjala kad je lighting upaljen
    glColor3fv(color);

    //Odabir kocke
    if(dissapearing) //dissapearing je true -> siva kocka // false(tj. appearing) -> zlatna kocka
    {
        pomakX=boxSize-pomak;
        pomakXneg=-boxSize;

        if(pomakX<=-boxSize)//ako je pomak dosegnuo -boxSize vrijednost zaustavi se, okreni postupak na drugu stranu.
        {
            pomakX=-boxSize;
            pomakInc=-pomakInc;
            animate=false;

        }else if(pomakX>=boxSize)
        {
            pomakX=boxSize;
            pomakInc=-pomakInc;
            animate=false;
        }


    }//appearing kocka, povecava se pomak
    else
    {
        pomakX=boxSize;
        pomakXneg=boxSize-pomak;



        if(pomakXneg<=-boxSize)
        {
            pomakXneg=-boxSize;
            pomakInc=-pomakInc;
            animate=false;
        }
        else if(pomakXneg>=boxSize)
        {
            pomakXneg=boxSize;
            pomakInc=-pomakInc;
            animate=false;
        }


    }

    //Straznja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(pomakXneg, -size, -size);
    glVertex3f(pomakX, -size, -size);
    glVertex3f(pomakX, size, -size);
    glVertex3f(pomakXneg, size, -size);
    glEnd();

    //Desna ploha
    glBegin(GL_QUADS);
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f(pomakX, -size, size);
    glVertex3f(pomakX, -size, -size);
    glVertex3f(pomakX, size, -size);
    glVertex3f(pomakX, size, size);
    glEnd();

    //Donja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(pomakXneg, -size, -size);
    glVertex3f(pomakX, -size, -size);
    glVertex3f(pomakX, -size, size);
    glVertex3f(pomakXneg, -size, size);
    glEnd();

    //Gornja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(pomakXneg, size, -size);
    glVertex3f(pomakX, size, -size);
    glVertex3f(pomakX, size, size);
    glVertex3f(pomakXneg, size, size);
    glEnd();

    //Lijeva ploha
    glBegin(GL_QUADS);
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(pomakXneg, -size, size);
    glVertex3f(pomakXneg, -size, -size);
    glVertex3f(pomakXneg, size, -size);
    glVertex3f(pomakXneg, size, size);
    glEnd();

    //Prednja ploha
    glBegin(GL_QUADS);
    glNormal3f(0.0, 0.0, 1.0);
    glVertex3f(pomakXneg, -size, size);
    glVertex3f(pomakX, -size, size);
    glVertex3f(pomakX, size, size);
    glVertex3f(pomakXneg, size, size);
    glEnd();


    glPopMatrix();

}

void renderPortal(GLfloat radius,GLfloat color[])
{
    glPushMatrix();
    glTranslatef(-pomak+boxSize, 0.0, 0.0);
    glRotatef(90.0, 0.0, 1.0, 0.0);
    glColor4fv(color);

    glutSolidTorus(0.015, radius, 4, 100);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(color[0], color[1], color[2], 0.5f);
    int i;
    int triangleAmount = 30;
    GLfloat twicePi = 2.0f * 3.14;
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0.0 , 0.0); // sredisnji vertex
    for(i = 0; i <= triangleAmount;i++)
    {
        glVertex2f( 0.0 + (radius * cos(i * twicePi / triangleAmount)), 0.0 + (radius * sin(i * twicePi / triangleAmount)));
    }
    glEnd();

    glPopMatrix();

}


// ////////////////Display funkcija/////////////////////////

void display(void)
{

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glTranslatef(0.0f, 0.0f, -0.5f);
    glRotatef(-25, -1.0, 1.0, 0.0);

    //pokretanje i zaustavljanje animacije (korak pomaka)
    if(animate)
    {
        pomak+=pomakInc;
    }

    renderBox(boxSize,DISSAPEARING,box1Color);
    renderBox(boxSize,APPEARING,box2Color);
    renderPortal(portalSize, portalColor);
    glutSwapBuffers();
}

void reshape (int w, int h)
{
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho(-2.0*w/h, 2.0*w/h, -2.0, 2.0, -3.0, 3.0);

    //glFrustum (-2.0, 2.0, -2.0, 2.0, 1.0, 20.0);

    glMatrixMode (GL_MODELVIEW);
}

void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key) {
    case 27:
        exit(0);
        break;
    case 's':case 'S':
        animate=true;
        break;
    case 'x':case 'X':
        animate=false;
        break;
    case 'r':case 'R':
        pomakInc-=2*pomakInc;
        break;
    case 'q':case 'Q':
        pomakInc*=1.5;
        break;
    case 'a':case 'A':
        pomakInc/=1.5;
        break;
    case 'p':case 'P':
        pomak=0.0;
        break;

    }
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); //Double buffering, rgb boja, z-buffer test
    glutInitWindowSize (800, 600);
    glutInitWindowPosition (200, 150);
    glutCreateWindow (argv[0]);
    init ();
    glutDisplayFunc(display);
    glutTimerFunc(0,timer,0);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}
