OPIS:
Program sam realizirao koristeći openGL metode(QUAD) za iscrtavanje kocki.
Program iscrtava 2 kocke istovremeno, te mjenja koordinate prilikom svakog redisplay poziva.
Animacija je realizirana sa pomakom koji globalno utječe na sve ostale pomake, te globalnim inkrementom pomaka(generalizirano).
Timer je vrlo jednostavan, namješten na 30 milisekundi.
Kod je iskomentiran, što uključuje i varijable s kojima se može podešavati program.
Uz navedene implementirao sam i neke dodatne funckionalnosti navedene u donjoj sekciji.


KORIŠTENJE PROGRAMA:
S - Pokreće animaciju
X - Pauzira animaciju
P - Resetira animaciju
R - (Reverse) mjenja smjer kretanja portala
Q - Ubrzava kretanje portala
A - Usporava kretanje portala
Esc - Izlazi iz programa


REFERENCE:
 - primjer koda za popunjeni krug koji sam iskoristio u renderPortal() funkciji - https://gist.github.com/strife25/803118
 - Poslužio sam se sljedećim tutorialima za većinu pitanja oko crtanja kocki, boja, osvjetljenja i animacije tj. timera - http://xoax.net/cpp/crs/opengl/index.php
 - Također koristio sam se i pokaznim templateom za OpenGL koji je stavljen na Mudri - https://mudri.uniri.hr/mod/resource/view.php?id=65584

