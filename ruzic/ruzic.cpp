#include <math.h>
#include <GL/glut.h>

// sets up the lighting
void setLighting()
{
	// enable lighting with a single light source
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// set the light colors and position
	float lightAmbient[4] = {0.3f, 0.3f, 0.3f, 1.0f}; // gray ambient color
	float lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f}; // white diffuse color
	float lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f}; // white specular color
	float lightPosition[4] = {-1.0f, 2.0f, 1.0f, 1.0f}; // light source at (-1, 2, 1)
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient); // set the ambient color
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse); // set the diffuse color
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular); // set the specular color
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition); // set the light source position

	// set the material properties
	float materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f}; // white specular material
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular); // set the specular material
	glMaterialf(GL_FRONT, GL_SHININESS, 128); // set the shininess power

	//use current color for ambient and diffuse materials
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// enable automatic normalization of normals for correct lighting
	glEnable(GL_NORMALIZE);
}

// draws the cuboid with specified sizes
void drawCuboid(double width, double height, double depth)
{
	glPushMatrix(); 
	glScaled(width, height, depth); // scale the current matrix to stretch the unit cube in each dimension
	glutSolidCube(1.0); 
	glPopMatrix(); 
}

// draws the cylinder with specified sizes
void drawCylinder(double radius, double height)
{
	GLUquadricObj* quadric = gluNewQuadric(); // create the quadric object
	gluCylinder(quadric, radius, radius, height, 20, 20); // draw the cylinder using 20x20 subdivision
	gluDeleteQuadric(quadric); 
}

// draws the sphere with specified size
void drawSphere(double radius)
{
	glutSolidSphere(radius, 50, 50); // draw the sphere using 50x50 subdivision
}

// draws the robot
void drawRobot()
{
	int i;
	int time = glutGet(GLUT_ELAPSED_TIME); // current time in milliseconds

	// draw the torso (cuboid)
	double torsoAngle = 10 * sin(time * 0.007); // wavy motion using sine function
	glRotated(torsoAngle, 0, 0, 1); // rotate the torso for dancing
	glColor3d(0.6, 0.5, 1); // purple color
	drawCuboid(0.5, 1, 0.3); 

	// draw the neck (cylinder)
	glTranslated(0, 0.5, 0); // move up from the torso
	glRotated(-90, 1, 0, 0); // set vertical cylinder direction
	glColor3d(0.5, 0.6, 0.3); // light green color
	drawCylinder(0.1, 0.2); 

	// draw the head (sphere)
	glPushMatrix(); // save the current transformation
	double headAngle = 10 + 20 * sin(time * 0.01); // wavy motion using sine function
	glRotated(headAngle, 1, 0, 0); // rotate the head for dancing
	glTranslated(0, 0, 0.4); // move up from the neck
	glColor3d(0.6, 0.6, 0.5); // yellow color
	drawSphere(0.3); 
	glPopMatrix(); 

	// draw the 2 arms
	for(i = 0; i < 2; i++)
	{
		glPushMatrix(); // save the current transformation
		if(i == 1) glRotated(180, 0, 0, 1); // rotate 180 degrees to opposite side

		// draw the shoulder joint (sphere)
		glTranslated(0.3, 0, -0.2); // shoulder position
		glColor3d(0.4, 0.6, 0.7); // light blue color
		drawSphere(0.12); 

		// draw the upper arm (cylinder)
		double armAngle = 120 + 20 * sin(time * 0.005); // wavy motion using sine function
		glRotated(armAngle, 0, 1, 0); // rotate the upper arm for dancing
		glColor3d(0.4, 0.5 + i * 0.4, 0.9); // light/dark blue color
		drawCylinder(0.1, 0.4); 

		// draw the elbow joint (sphere)
		glTranslated(0, 0, 0.4); // elbow position
		glColor3d(0.4, 0.6, 0.7); // light blue color
		drawSphere(0.12); 

		// draw the forearm (cylinder)
		double foreArmAngle = 30 + 30 * sin(time * 0.004); // wavy motion using sine function
		glColor3d(0.3, 0.3 + i * 0.3, 0.4); // gray/green color
		glRotated(foreArmAngle, 0, 1, 0); // rotate the forearm for dancing
		drawCylinder(0.08, 0.3); 

		// draw the hand (cuboid)
		glTranslated(0, 0, 0.3); // hand position
		glColor3d(0.2 + i * 0.5, 0.6, 0.2); // green/yellow color
		drawCuboid(0.2, 0.2, 0.08); 

		glPopMatrix(); 
	}

	// draw the 2 legs
	for(i = 0; i < 2; i++)
	{
		glPushMatrix(); // save the current transformation
		if(i == 1) glRotated(180, 0, 0, 1); // rotate 180 degrees to opposite side

		// draw the thigh joint (sphere)
		glTranslated(0.2, 0, -1); // thigh position
		glColor3d(0.4, 0.6, 0.7); // light blue color
		drawSphere(0.12); 

		// draw the thigh (cylinder)
		double thighAngle = 180 + 20 * sin(time * 0.005); // wavy motion using sine function
		glRotated(thighAngle, 1, 0, 0); // rotate the thigh for dancing
		glColor3d(0.4 + i * 0.3, 0.4, 0.3); // brown/orange color
		drawCylinder(0.1, 0.5); 

		// draw the knee joint (sphere)
		glTranslated(0, 0, 0.5); // knee position
		glColor3d(0.4, 0.6, 0.7); // light blue color
		drawSphere(0.12); 

		// draw the shin (cylinder)
		double shinAngle  = 10 - 10 * sin(time * 0.005); // wavy motion using sine function
		if(i == 1) shinAngle *= -1; // negate the shin angle for the opposite side
		glRotated(shinAngle, 1, 0, 0); // rotate the shin for dancing
		glColor3d(0.3, 0.2 + i * 0.3, 0.5); // purple/cyan color
		drawCylinder(0.08, 0.5); 

		// draw the foot (cuboid)
		glTranslated(0, 0, 0.5); // foot position
		glColor3d(0.2 + i * 0.3, 0.3, 0.4); // blue/red color
		drawCuboid(0.2, 0.4, 0.08); 

		glPopMatrix(); 
	}
}

// display function
void display()
{
	// clear the window
	glClearColor(0.5f, 0.6f, 0.7f, 1.0f); // light blue background color
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// set the perspective view
	double fieldOfView = 60; // camera field of view 60 degrees
	double aspectRatio = 1; // windows aspect ratio
	double zNear = 0.1; // near clipping plane distance
	double zFar = 1000; // far clipping plane distance
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fieldOfView, aspectRatio, zNear, zFar);

	// set the camera position and direction
	double camX = 1, camY = 0, camZ = 3; // camera position at (1, 0, 3)
	double targetX = 0, targetY = -0.2, targetZ = 0; // camera target position at (0, -0.2, 0)
	double upX = 0, upY = 1, upZ = 0; // camera up direction along (0, 1, 0)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(camX, camY, camZ, targetX, targetY, targetZ, upX, upY, upZ);

	// set the lighting and draw the scene
	setLighting();
	drawRobot();
	glutSwapBuffers(); // update the window
	glutPostRedisplay(); // request displaying again to enable animation
}

int main(int argc, char **argv)
{
	// initialize GLUT with double buffering 
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);

	// create the window
	glutInitWindowSize(600, 600);
	glutCreateWindow("Dancing robot");

	// register the display function
	glutDisplayFunc(display);

	// start the drawing loop
	glutMainLoop();
	return 0;
}
