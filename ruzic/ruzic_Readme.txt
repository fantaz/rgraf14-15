Kratak opis programa koji izvodi jedan plesni pokret.

glColor3ub(255, 0, 0);
Funkcije koja mijenja boje objekta. Prima tri vrijednosti kao unos. Vrijednosti su RGB(Red Green Blue) u rasponu od 0 do 255.

glTranslated(0.3, 0, -0.2);
Funkcija prima input koordinate matricnog sustava: x,y,z. Mjenjanjem vrijednosti tih varijabli pomicnemo mjesto,poziciju gdje cemo vrsiti neke radnje(npr. crtanje razih geometrisjkih likova). 

drawSphere(0.12);
drawCylinder(0.1, 0.4);
drawCuboid(0.2, 0.2, 0.08);
Funkcije za crtanje razlicitih objekata i primaju razlicite argumente. DrawSphere prima radijus,drawCylinder prima radijus i visinu,drawCuboid prima širinu,visinu i dubinu.

glScaled(width, height, depth);
Funkcija sluzi za skaliranje odredenih dijelova na kojima vrsimo radnju.Vrijednosti su koordinate "x,y,z" te se prema unesenim vrijednostima objekt skalira.

Korišten je dupli buffer za finiju animaciju.
Veličina prozora OpenGL-a je određena na 600x600.

Razvojna okolina:
	koristen je QtCreator 5.2.1 na Kubuntu 14.10
	koristeni su GL/glut i math paketi

Source:
https://www.opengl.org/resources/libraries/glut/spec3/spec3.html
https://www.khronos.org/opengles/sdk/1.1/docs/man/glEnable.xml
https://www.opengl.org/archives/resources/faq/technical/viewing.htm
http://www.cs.arizona.edu/classes/cs433/spring02/opengl/dblbuffer.html